How to prepare environment for TFM Selenium tests
=================================================

1.  Install IDE - IntelliJ IDEA from
    https://www.jetbrains.com/idea/download/download-thanks.html?platform=linux&code=IIC
    . I use it because it will import all the libraries that you need
    only by opening the project.
1.1 Download and install Java SDK from
    http://www.oracle.com/technetwork/java/javase/downloads/
    and add it to Intellij IDEA by going to: File -\> Project structure
    -\> Project SDK

2.  Import SeleniumTest folder

3.  In a different folder, download the following webdrivers:
    https://sites.google.com/a/chromium.org/chromedriver/downloads
    https://github.com/mozilla/geckodriver/releases

4.  In the same folder where you downloaded the webdrivers, download
    Selenium Standalone Server from https://goo.gl/s4o9Vx

5.  In terminal, go to the folder where you downloaded the webdrivers
    and Selenium Standalone Server and run the following command

\<\< java -Dwebdriver.chrome.driver=./chromedriver
-Dwebdriver.gecko.driver=./geckodriver -jar
./selenium-server-standalone-3.4.0.jar \>\>

5.  Run tests by going to: src -\> tests -\> java -\> right click on it
    -\> Run "All tests" -\> right click on specific test -\>eg. Run
    "LoginTest"

6.  Download https://github.com/rest-assured/rest-assured/wiki/Downloads
    -\> rest-assured 3.0.3 dist.zip and unzip it
7.  Add jar file to Intellij by doing : file-\> project structure -\>
    modules -\> dependencies -\> add Jar or directories.


