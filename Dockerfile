FROM xpub/xpub:latest

WORKDIR ${HOME}

COPY package.json yarn.lock ./
COPY lerna.json .babelrc .eslintignore .eslintrc .prettierrc .stylelintignore .stylelintrc ./
COPY packages packages

ENV NODE_ENV "development"

RUN [ "yarn", "config", "set", "workspaces-experimental", "true" ]

RUN [ "yarn", "install", "--frozen-lockfile" ]
RUN [ "yarn", "cache", "clean"]
RUN [ "rm", "-rf", "/npm-packages-offline-cache"]

WORKDIR ${HOME}/packages/xpub-faraday
ENV NODE_ENV "production"

RUN [ "npx", "pubsweet", "build"]

EXPOSE 3000

CMD []