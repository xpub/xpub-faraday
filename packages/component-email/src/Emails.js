const bodyParser = require('body-parser')

const CollectionsInvitations = app => {
  app.use(bodyParser.json())
  const basePath = '/api/emails'
  const routePath = './routes/emails'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {post} /api/emails Send a new email
   * @apiGroup Emails
   * @apiParamExample {json} Body
   *    {
   *      "email": "email@example.com",
   *      "type": "invite", [acceptedValues: invite, assign]
   *      "role": "editorInChief" [acceptedValues: editorInChief, admin, author, handlingEditor]
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK {}
   * @apiErrorExample {json} Send email errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 500 Internal Server Error
   */
  app.post(
    basePath,
    authBearer,
    require(`${routePath}/post`)(app.locals.models),
  )
}

module.exports = CollectionsInvitations
