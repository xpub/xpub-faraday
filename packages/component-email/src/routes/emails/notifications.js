const config = require('config')

const unsubscribeSlug = config.get('unsubscribe.url')
const { staffEmail, name: journalName } = config.get('journal')
const Email = require('@pubsweet/component-email-templating')
const { services } = require('pubsweet-component-helper-service')
const { getEmailCopy } = require('./emailCopy')

module.exports = {
  sendNewUserEmail: ({ user, baseUrl, role }) => {
    const resetPath = config.get('invite-reset-password.url')

    const emailType =
      role === 'Handling Editor' ? 'he-added-by-admin' : 'user-added-by-admin'

    const { paragraph, ...bodyProps } = getEmailCopy({
      role,
      emailType,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: user.email,
        name: `${user.lastName}`,
      },
      content: {
        subject: 'Confirm your account',
        ctaLink: services.createUrl(baseUrl, resetPath, {
          email: user.email,
          token: user.accessTokens.passwordReset,
          firstName: user.firstName,
          lastName: user.lastName,
          affiliation: user.affiliation,
          title: user.title,
          country: user.country,
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: 'Hindawi',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  sendSignupEmail: ({ user, baseUrl }) => {
    const confirmSignUp = config.get('confirm-signup.url')

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'user-signup',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: user.email,
        name: `${user.lastName}`,
      },
      content: {
        subject: 'Confirm your email address',
        ctaLink: services.createUrl(baseUrl, confirmSignUp, {
          userId: user.id,
          confirmationToken: user.accessTokens.confirmation,
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        signatureName: 'Hindawi',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
