A clickable text button.

```js
<ActionLink onClick={() => console.log('I am clicked.')}>
  Default action
</ActionLink>
```

A disabled text buton.

```js
<ActionLink disabled>Disabled</ActionLink>
```

A text button with an icon.

```js
<ActionLink icon="plus">Icon on the left</ActionLink>
```

A text button with an icon on the right.

```js
<ActionLink icon="eye" iconPosition="right">
  Icon on the right
</ActionLink>
```

A navigation link.

```js
<ActionLink icon="eye" iconPosition="right" to="https://www.google.com">
  Icon on the right
</ActionLink>
```
