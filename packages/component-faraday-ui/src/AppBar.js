import React, { Fragment } from 'react'
import { get, once } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { H2, Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { compose, setDisplayName, withProps } from 'recompose'
import { Item, Row, Text } from 'pubsweet-component-faraday-ui'

const AppBar = ({
  fixed,
  logo: Logo,
  menu: Menu,
  createDraft,
  canCreateDraft,
  currentUser,
  autosave: Autosave,
  journal: { metadata: { backgroundImage } },
}) => (
  <Fragment>
    <Root fixed={fixed}>
      {backgroundImage && <JournalBackground img={backgroundImage} />}
      <LogoContainer>
        <Logo />
      </LogoContainer>
      <RightContainer>
        <Item mr={2}>
          <Autosave />
        </Item>
        {createDraft &&
          currentUser.user && (
            <Button
              data-test-id="new-manuscript"
              disabled={!canCreateDraft}
              ml={2}
              mr={5}
              onClick={once(createDraft)}
              primary
              size="small"
            >
              SUBMIT
            </Button>
          )}
        <Menu />
      </RightContainer>
    </Root>
    {!canCreateDraft && (
      <RibbonRow fixed={fixed}>
        <Text pb={1 / 2} pt={1}>
          Your account is not confirmed. Please check your email.
        </Text>
      </RibbonRow>
    )}
  </Fragment>
)

AppBar.propTypes = {
  /** If true, it will be fixed at the top of the screen. */
  fixed: PropTypes.bool,
  /** Logo that will be added to the fragment. */
  logo: PropTypes.func,
  currentUser: PropTypes.shape({
    user: PropTypes.object,
    isAuthenticated: PropTypes.bool,
  }),
  /** If false an error message will appear. */
  canCreateDraft: PropTypes.bool,
  /** Pass the menu component. */
  menu: PropTypes.func,
  /** Custom component that will be used as an autosave indicator. */
  autosave: PropTypes.func,
}

AppBar.defaultProps = {
  fixed: true,
  currentUser: {},
  canCreateDraft: true,
  logo: () => <div />,
  menu: () => <div />,
  autosave: () => <div />,
}

export default compose(
  withProps(({ location = {}, submitPath = '/submit' }) => ({
    isSubmit: get(location, 'pathname', '').includes(submitPath),
  })),
  setDisplayName('AppBar'),
)(AppBar)

// #region styles
const RightContainer = styled.div`
  align-items: center;
  display: flex;
  height: ${th('appBar.height')};
  margin-right: ${th('gridUnit')};
  height: ${th('appBar.height')};

  position: absolute;
  right: 0;
  top: 0;
`

const LogoContainer = styled.div`
  align-items: center;
  display: flex;
  height: ${th('appBar.height')};
  margin-left: ${th('gridUnit')};

  position: absolute;
  top: 0;
  left: 0;
  z-index: 1;

  cursor: pointer;
`

const JournalBackground = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: ${th('appBar.height')};

  background: ${props =>
    props.img
      ? `url(${props.img})
    center / 100% 60px no-repeat;`
      : th('appBar.colorBackground')};

  -webkit-mask-image: linear-gradient(
    to right,
    rgba(0, 0, 0, 0.05),
    rgba(0, 0, 0, 0.2) 20%,
    rgba(0, 0, 0, 0.4) 50%,
    rgba(0, 0, 0, 0.2) 80%,
    rgba(0, 0, 0, 0.05)
  );
`
const Root = styled.div`
  align-items: center;
  background-color: ${th('appBar.colorBackground')};
  box-shadow: ${th('appBar.boxShadow')};
  height: ${th('appBar.height')};
  display: flex;
  justify-content: center;
  padding: 0 calc(${th('gridUnit')} * 2);
  position: ${props => (props.fixed ? 'fixed' : 'relative')};

  top: 0;
  left: 0;
  right: 0;

  z-index: ${th('appBar.zIndex')};

  ${H2} {
    z-index: 1;
  }
`

const RibbonRow = styled(Row)`
  background-color: ${th('colorInfo')};
  position: ${props => (props.fixed ? 'fixed' : 'relative')};
  top: ${props => (props.fixed ? th('appBar.height') : '0')};
`
// #endregion
