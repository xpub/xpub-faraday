App bar user menu.

```js
const currentUser = {
  admin: true,
  user: {
    username: 'cocojambo',
    firstName: 'Alex',
    lastName: 'Munteanu'
  }
};

<AppBarMenu
  currentUser={currentUser}
  goTo={path => console.log(`navigating to ${path}`)}
  logout={() => console.log('logging out')}
  />
```
