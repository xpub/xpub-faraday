A component that shows details about an author. It has two modes: a presentation mode and an edit mode. This component can be a part of a submission wizard as well as in a sortable list.

An author card.

```js
const author = {
  email: 'author.here@gmail.com',
  firstName: 'Sebastian',
  lastName: 'Teodorescu',
  affiliation: 'PSD',
  isSubmitting: true,
  isCorresponding: true,
}
;<div>
  <AuthorCard
    onEdit={e => console.log('s-a dat click pe edit', e)}
    index={0}
    item={author}
    deleteAuthor={item => () => {
      console.log('delete author', item)
    }}
    saveNewAuthor={(...args) => console.log('save new authot', args)}
    authorEditorSubmit={(...args) => console.log('edit the author', args)}
  />
  <AuthorCard
    onEdit={() => console.log('s-a dat click pe edit')}
    index={1}
    item={author}
    deleteAuthor={item => () => {
      console.log('delete author', item)
    }}
  />
  <AuthorCard
    onEdit={() => console.log('s-a dat click pe edit')}
    index={2}
    item={author}
    deleteAuthor={item => () => {
      console.log('delete author', item)
    }}
  />
</div>
```

Author card with drag handle (used for sortable lists).

```js
const author = {
  email: 'author.here@gmail.com',
  firstName: 'Sebastian',
  lastName: 'Teodorescu',
  affiliation: 'PSD',
  isSubmitting: true,
  isCorresponding: true,
}
;<AuthorCard
  item={author}
  dragHandle={DragHandle}
  deleteAuthor={item => () => {
    console.log('delete author', item)
  }}
/>
```
