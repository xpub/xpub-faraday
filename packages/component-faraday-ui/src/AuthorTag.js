import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PropTypes from 'prop-types'
import Tag from './Tag'
import Text from './Text'

const AuthorTag = ({
  author: {
    id,
    firstName,
    lastName,
    isCorresponding,
    isSubmitting,
    affiliationNumber,
  },
}) => (
  <Root data-test-id={`author-tag-${id}`}>
    <Text>{`${firstName} ${lastName}`}</Text>
    {isSubmitting && <Tag>SA</Tag>}
    {isCorresponding && <Tag>CA</Tag>}
    {affiliationNumber && <Superscript>{affiliationNumber}</Superscript>}
  </Root>
)

AuthorTag.propTypes = {
  /** The author you want to be on the card. */
  author: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    isCorresponding: PropTypes.bool,
    isSubmitting: PropTypes.bool,
    affiliationNumber: PropTypes.number,
  }),
}

AuthorTag.defaultProps = {
  author: {
    id: undefined,
    firstName: undefined,
    lastName: undefined,
    isCorresponding: undefined,
    isSubmitting: undefined,
    affiliationNumber: undefined,
  },
}

export default AuthorTag

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;
  height: calc(${th('gridUnit')} * 3);

  margin-left: calc(${th('gridUnit')} / 2);
  margin-right: calc(${th('gridUnit')} / 4);

  ${Tag} {
    margin-left: calc(${th('gridUnit')} / 2);
  }
`
const Superscript = styled.span`
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('fontInterface')};
`
// #endregion
