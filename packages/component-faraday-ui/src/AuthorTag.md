An author tag.

```js
const authors = [
  {
    id: 1,
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
  },
  {
    id: 2,
    email: 'michael.felps@gmail.com',
    firstName: 'Michael',
    lastName: 'Felps',
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id: 3,
    email: 'barrack.obama@gmail.com',
    firstName: 'Barrack',
    lastName: 'Obama',
    affiliation: 'US Presidency'
  },
];

<div style={{display: 'flex'}}>{authors.map(a => <AuthorTag key={a.email} author={a} />)}</div>
```
