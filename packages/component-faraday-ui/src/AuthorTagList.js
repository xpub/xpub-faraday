import React from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps, withStateHandlers } from 'recompose'
import PropTypes from 'prop-types'

import {
  Row,
  Item,
  Text,
  AuthorTag,
  ActionLink,
  AuthorWithTooltip,
} from 'pubsweet-component-faraday-ui'

const parseAffiliations = (authors = []) =>
  authors.reduce(
    (acc, curr) => {
      if (acc.affiliations.includes(curr.affiliation)) {
        acc.authors = [
          ...acc.authors,
          {
            ...curr,
            affiliationNumber:
              acc.affiliations.findIndex(e => e === curr.affiliation) + 1,
          },
        ]
      } else {
        acc.affiliations = [...acc.affiliations, curr.affiliation]
        acc.authors = [
          ...acc.authors,
          {
            ...curr,
            affiliationNumber:
              acc.affiliations.findIndex(e => e === curr.affiliation) + 1,
          },
        ]
      }
      return acc
    },
    { affiliations: [], authors: [] },
  )

const AuthorTagList = ({
  authors,
  affiliationList,
  separator,
  authorKey,
  withTooltip,
  withAffiliations,
  showAffiliation,
  toggleAffiliation,
}) => (
  <Root>
    {authors
      .map(
        a =>
          withTooltip ? (
            <AuthorWithTooltip author={a} key={a[authorKey]} />
          ) : (
            <AuthorTag author={a} key={a[authorKey]} />
          ),
      )
      .reduce(
        (prev, curr, index) =>
          index === 0 ? [prev, curr] : [prev, separator, curr],
        [],
      )}
    {withAffiliations && (
      <Item flex={0} ml={1}>
        <ActionLink
          data-test-id="author-affiliations"
          icon={showAffiliation ? 'minus' : 'plus'}
          onClick={toggleAffiliation}
        >
          {showAffiliation ? 'Hide Affiliations' : 'Show Affiliations'}
        </ActionLink>
      </Item>
    )}

    {withAffiliations &&
      showAffiliation && (
        <AffiliationRow>
          {affiliationList.map((a, i) => (
            <Item flex={1} key={a}>
              <Text>{a}</Text>
              <Superscript>{i + 1}</Superscript>
            </Item>
          ))}
        </AffiliationRow>
      )}
  </Root>
)
export default compose(
  withStateHandlers(
    { showAffiliation: false },
    {
      toggleAffiliation: ({ showAffiliation }) => () => ({
        showAffiliation: !showAffiliation,
      }),
    },
  ),
  withProps(({ authors = [] }) => ({
    parsedAffiliations: parseAffiliations(authors),
  })),
  withProps(({ authors = [], withAffiliations, parsedAffiliations }) => ({
    authors: withAffiliations
      ? get(parsedAffiliations, 'authors', [])
      : authors,
    affiliationList:
      withAffiliations && get(parsedAffiliations, 'affiliations', []),
  })),
)(AuthorTagList)

AuthorTagList.propTypes = {
  /** The identificator label that will be seen on the card. */
  authorKey: PropTypes.string,
  /** All authors we want to be seen on the card. */
  authors: PropTypes.arrayOf(PropTypes.object),
  /** Separator between authors. */
  separator: PropTypes.string,
  /** Tooltip about author details. */
  withTooltip: PropTypes.bool,
  /**  Show authors affifiations. */
  withAffiliations: PropTypes.bool,
}

AuthorTagList.defaultProps = {
  authorKey: 'id',
  authors: [],
  separator: `, `,
  withTooltip: false,
  withAffiliations: false,
}

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;
  flex-flow: row wrap;

  font-family: ${th('fontReading')};
`
const AffiliationRow = styled(Row)`
  padding-left: ${th('gridUnit')};
  flex-direction: column;
`
const Superscript = styled.span`
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('fontInterface')};
`
// #endregion
