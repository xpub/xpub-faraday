import React from 'react'
import PropTypes from 'prop-types'
import { Spinner } from '@pubsweet/ui'
import { compose, withState } from 'recompose'
import { Item } from 'pubsweet-component-faraday-ui'

import { withZipDownload } from './helpers'

const DownloadZipFiles = ({ disabled, fetching, children, downloadFiles }) => (
  <Item
    flex={0}
    justify="flex-end"
    ml={1}
    mr={1}
    onClick={!disabled ? downloadFiles : null}
  >
    {fetching ? <Spinner /> : children}
  </Item>
)

DownloadZipFiles.propTypes = {
  /** Name for the downloaded archive file. */
  archiveName: PropTypes.string.isRequired, // eslint-disable-line
  /** If the user is a reviewer. */
  isReviewer: PropTypes.bool, // eslint-disable-line
}

DownloadZipFiles.defaultProps = {
  isReviewer: false,
}

export default compose(
  withState('fetching', 'setFetching', false),
  withZipDownload,
)(DownloadZipFiles)
