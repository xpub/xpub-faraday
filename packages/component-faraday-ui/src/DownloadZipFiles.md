Download manuscript package as zip

```js
const collection = {id: '', customId: ''};
const fragment = {id: ''};


<DownloadZipFiles
  archiveName={`ID-${collection.customId}`}
  collectionId={collection.id}
  fragmentId={fragment.id}
  isReviewer
  token="abc-123"
>
  <div>Download</div>
</DownloadZipFiles>
```
