import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { ActionLink } from 'pubsweet-component-faraday-ui/src'
import { marginHelper } from './styledHelpers'

// icon not like in the design, but untill we + Pubsweet decide on an Icon
// implementation that supports custom icons this will do. Jen is working on it
const DragHandle = props => (
  <Handle {...props}>
    <DragIcon fontIcon="moveIcon" size={2.5} />
  </Handle>
)

DragHandle.displayName = 'DragHandle'

DragHandle.protoTypes = {
  /** Designed size for icon */
  size: PropTypes.number,
}
DragHandle.defaultProps = {
  size: 2,
}

export default DragHandle

// #region styles
const DragIcon = styled(ActionLink)`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${th('colorSecondary')};
  font-size: calc(${th('gridUnit')} * 2.5);
  cursor: move;
`
const Handle = styled.div`
  align-self: stretch;
  align-items: center;
  background-color: transparent;
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  cursor: move;
  display: flex;
  flex-direction: column;
  justify-content: center;

  width: calc(${th('gridUnit')} * 4);

  span {
    padding: 0;
  }

  ${marginHelper};
`
// #endregion
