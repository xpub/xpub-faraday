import React from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'

import { ActionLink, Row, Text } from './'

const Footer = ({ journal }) => (
  <Root alignItems="center" data-test-id="footer-row-id" justify="center">
    <ActionLink data-test-id="hindawi-redirect" to="https://hindawi.com">
      Hindawi
    </ActionLink>
    <ActionLink
      data-test-id="privacy-policy"
      ml={3}
      to="https://www.hindawi.com/privacy/"
    >
      Privacy Policy
    </ActionLink>
    <ActionLink
      data-test-id="terms-of-service"
      ml={3}
      to="https://www.hindawi.com/terms/"
    >
      Terms of Service
    </ActionLink>
    <Text ml={4} secondary>
      {`Support: `}
      <ActionLink
        data-test-id="email-support"
        to={`mailto:${get(journal, 'metadata.email')}`}
      >
        {get(journal, 'metadata.email')}
      </ActionLink>
    </Text>
  </Root>
)

export default withJournal(Footer)

const Root = styled(Row)`
  background-color: ${th('colorBackground')};
  border-top: 1px solid ${th('colorFurniture')};
  height: calc(${th('gridUnit')} * 5);

  bottom: 0;
  left: 0;
  right: 0;
  position: fixed;
  z-index: ${th('appBar.zIndex')};
`
