import React from 'react'
import { Icon } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { positionHelper, marginHelper, paddingHelper } from './styledHelpers'

const IconButton = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  justify-content: center;
  opacity: ${props => (props.disabled ? 0.7 : 1)};
  pointer-events: ${props => (props.unclickable ? 'none' : 'auto')};
  &:hover {
    opacity: ${props => (props.noHover ? 1 : 0.7)};
  }

  &[disabled] {
    cursor: not-allowed;
  }
  ${marginHelper};
  ${paddingHelper};
  ${positionHelper};
`

export default ({
  icon,
  size,
  onClick,
  primary,
  fontIcon,
  disabled,
  unclickable,
  iconSize = 1.8,
  className,
  ...props
}) => (
  <IconButton
    className={className}
    disabled={disabled}
    display="inline"
    onClick={!disabled ? onClick : null}
    primary={primary}
    unclickable={unclickable}
    {...props}
  >
    {icon && (
      <Icon size={iconSize} {...props}>
        {icon}
      </Icon>
    )}
    {fontIcon && (
      <FontIconButton
        className={`${fontIcon} fontIconStyle`}
        size={iconSize}
        {...props}
      />
    )}
  </IconButton>
)

const FontIconButton = styled.span`
  font-size: calc(${props => props.size} * ${th('gridUnit')});
  ${paddingHelper};
`
