import React, { Fragment } from 'react'
import 'react-tippy/dist/tippy.css'
import { Tooltip } from 'react-tippy'
import { ThemeProvider, withTheme } from 'styled-components'
import PropTypes from 'prop-types'

import { IconButton } from './'

const IconTooltip = ({
  theme,
  primary,
  interactive,
  fontIcon = 'tooltipIcon',
  iconSize = 2,
  ...rest
}) => (
  <Tooltip
    arrow
    data-test-id="icon-tooltip"
    html={<InfoTooltip theme={theme} {...rest} />}
    interactive={interactive}
    position="bottom"
    theme="light"
    trigger="click"
  >
    <IconButton
      fontIcon={fontIcon}
      iconSize={iconSize}
      mb={1 / 2}
      ml={1}
      primary={primary}
    />
  </Tooltip>
)

const InfoTooltip = ({ theme, content }) => (
  <ThemeProvider theme={theme}>
    <Fragment>{typeof content === 'function' ? content() : content}</Fragment>
  </ThemeProvider>
)
IconTooltip.propTypes = {
  /** What icon to be used. */
  icon: PropTypes.string,
  /** Size of the icon. */
  iconSize: PropTypes.number,
  /** What content to be used in tooltip. */
  content: PropTypes.func,
  /** If true the content can be clicked (can be interacted with). */
  interactive: PropTypes.bool,
}

IconTooltip.defaultProps = {
  icon: 'help-circle',
  iconSize: 2,
  content: () => {},
  interactive: false,
}

export default withTheme(IconTooltip)
