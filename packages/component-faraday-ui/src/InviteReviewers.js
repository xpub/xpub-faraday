import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import styled from 'styled-components'
import { reduxForm } from 'redux-form'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'
import { withModal } from 'pubsweet-component-modal/src/components'
import { Button, H4, TextField, ValidatedField } from '@pubsweet/ui'

import { MenuCountry } from 'pubsweet-component-faraday-ui'

import {
  Row,
  Item,
  Label,
  MultiAction,
  ItemOverrideAlert,
  withFetching,
  validators,
} from '../'

const InviteReviewers = ({ handleSubmit, reset }) => (
  <Root>
    <Row justify="space-between" mb={2}>
      <H4>Invite reviewer</H4>
      <Item justify="flex-end">
        <Button
          data-type-id="button-invite-reviewer-clear-fields"
          onClick={reset}
          size="small"
        >
          Clear Fields
        </Button>
        <Button
          data-type-id="button-invite-reviewer-invite"
          ml={2}
          onClick={handleSubmit}
          primary
          size="small"
        >
          Invite
        </Button>
      </Item>
    </Row>
    <Row>
      <Item mr={2} vertical>
        <Label required>Email</Label>
        <ValidatedField
          component={TextField}
          data-test-id="invite-reviewer-email"
          name="email"
          validate={[required, validators.emailValidator]}
        />
      </Item>
      <Item mr={2} vertical>
        <Label required>First Name</Label>
        <ValidatedField
          component={TextField}
          data-test-id="invite-reviewer-first-name"
          name="firstName"
          validate={[required]}
        />
      </Item>
      <Item mr={2} vertical>
        <Label required>Last Name</Label>
        <ValidatedField
          component={TextField}
          data-test-id="invite-reviewer-last-name"
          name="lastName"
          validate={[required]}
        />
      </Item>
      <Item mr={2} vertical>
        <Label required>Affiliation</Label>
        <ValidatedField
          component={TextField}
          data-test-id="invite-reviewer-affiliation"
          name="affiliation"
          validate={[required]}
        />
      </Item>

      <ItemOverrideAlert vertical>
        <Label required>Country</Label>
        <ValidatedField
          component={MenuCountry}
          name="country"
          placeholder="Please select"
          validate={[required]}
        />
      </ItemOverrideAlert>
    </Row>
  </Root>
)

InviteReviewers.propTypes = {
  /** Callback function fired after confirming a reviewer invitation.
   * @param {Reviewer} reviewer
   * @param {object} props
   */
  onInvite: PropTypes.func, // eslint-disable-line
}

InviteReviewers.defaultProps = {
  onInvite: () => {},
}

export default compose(
  withFetching,
  withModal(({ isFetching, modalKey }) => ({
    modalKey,
    isFetching,
    modalComponent: MultiAction,
  })),
  reduxForm({
    form: 'invite-reviewers-form',
    onSubmit: (values, dispatch, { showModal, onInvite, setFetching }) => {
      showModal({
        title: 'Send invitation to Review?',
        subtitle: values.email,
        onConfirm: modalProps => {
          onInvite(values, { ...modalProps, setFetching })
        },
      })
    },
  }),
)(InviteReviewers)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue3')};
  padding: calc(${th('gridUnit')} * 2);
`
// #endregion
