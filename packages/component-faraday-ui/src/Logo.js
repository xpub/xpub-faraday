/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'

const Logo = ({ src, onClick, title, height, ...rest }) => (
  <img
    alt={title}
    data-test-id={get(rest, 'data-test-id', 'journal-logo')}
    height={height}
    onClick={onClick}
    src={src}
    title={title}
  />
)

Logo.propTypes = {
  /** Height of the logo. */
  height: PropTypes.number,
}

Logo.defaultProps = {
  height: 36,
}
export default Logo
