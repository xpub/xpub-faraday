A manuscript card.

```js
const authors = [
  {
    id:1,
    email: 'john.doe@gmail.com',
    firstName: 'John',
    lastName: 'Doe',
    isSubmitting: true,
  },
  {
    id:2,
    email: 'michael.felps@gmail.com',
    firstName: 'Michael',
    lastName: 'Felps',
    isSubmitting: true,
    isCorresponding: true,
  },
  {
    id:3,
    email: 'barrack.obama@gmail.com',
    firstName: 'Barrack',
    lastName: 'Obama',
  },
  {
    id:5,
    email: 'barrack.obama@gmail1.com',
    firstName: 'Barrack 1',
    lastName: 'Obama',
  },
  {
    id:6,
    email: 'barrack.obama@gmail2.com',
    firstName: 'Barrack 2',
    lastName: 'Obama',
  },
  {
    id:7,
    email: 'barrack.obama@gmail3.com',
    firstName: 'Barrack 3',
    lastName: 'Obama',
  },
  {
    id:8,
    email: 'barrack.obama@gmail4.com',
    firstName: 'Barrack 4',
    lastName: 'Obama',
  },
]

const collection = {
  customId: '55113358',
  visibleStatus: 'Pending Approval',
  handlingEditor: {
    id: 'he-1',
    name: 'Handlington Ignashevici',
  },
}

const fragment = {
  authors,
  created: Date.now(),
  submitted: Date.now(),
  metadata: {
    journal: 'Awesomeness',
    title: 'A very ok title with many authors',
    type: 'Research article',
  },
};

<ManuscriptCard
  collection={collection}
  fragment={fragment}
  onDelete={() => alert('Deleted')}
  onClick={() => alert('click on card')}
/>
```
