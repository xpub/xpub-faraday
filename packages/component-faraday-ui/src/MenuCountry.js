import React, { Fragment } from 'react'
import styled from 'styled-components'
import { th, override, validationColor } from '@pubsweet/ui-toolkit'
import { startsWith, toLower, get } from 'lodash'
import { withCountries } from 'pubsweet-component-faraday-ui'
import { compose, withState, withHandlers, withProps } from 'recompose'

const filteredCountries = ({ countries, userInput }) =>
  countries.filter(o => startsWith(toLower(o.label), toLower(userInput)))

const Menu = ({
  open,
  options,
  onEnter,
  userInput,
  toggleMenu,
  placeholder,
  handleSelect,
  onTextChange,
  validationStatus,
  handleKeyDown,
  cursor,
}) => (
  <Fragment>
    {open && <CloseOverlay onClick={toggleMenu} />}
    <Main>
      <Input
        onChange={onTextChange}
        onClick={toggleMenu}
        onKeyDown={handleKeyDown}
        onKeyUp={onEnter}
        placeholder={placeholder}
        validationStatus={validationStatus}
        value={userInput}
      />
      {open && (
        <Options>
          {options.map((option, index) => (
            <Option
              active={cursor === index}
              key={option.value}
              onClick={handleSelect(option.value)}
            >
              {option.label}
            </Option>
          ))}
        </Options>
      )}
    </Main>
  </Fragment>
)

export default compose(
  withCountries,
  withState(
    'userInput',
    'updateUserInput',
    ({ value, countryLabel }) => (value ? countryLabel(value) : ''),
  ),
  withState('cursor', 'setCursor', 0),
  withState('open', 'updateOptionsVisibility', false),
  withProps(({ countries, userInput }) => ({
    options: filteredCountries({ countries, userInput }),
  })),
  withHandlers({
    handleSelect: ({
      onChange,
      countryLabel,
      updateUserInput,
      updateOptionsVisibility,
    }) => value => () => {
      const country = countryLabel(value)

      if (country) {
        onChange(value)
        updateUserInput(country)
        updateOptionsVisibility(false)
      }
    },
  }),
  withHandlers({
    toggleMenu: ({ updateOptionsVisibility, open }) => () => {
      updateOptionsVisibility(!open)
    },
    onTextChange: ({
      updateUserInput,
      countryLabel,
      onChange,
      setCursor,
    }) => event => {
      const inputValue = get(event, 'target.value', '')
      const country = countryLabel(inputValue)

      setCursor(0)
      updateUserInput(inputValue)
      if (!country) {
        onChange('')
      }
    },
    onEnter: ({ handleSelect, options, cursor }) => event => {
      if (event.which === 13) {
        handleSelect(get(options[cursor], 'value'))()
      }
    },
    handleKeyDown: ({ setCursor, options }) => event => {
      // arrow up
      if (event.which === 38) {
        setCursor(c => Math.max(0, c - 1))
      }
      // arrow down
      if (event.which === 40) {
        setCursor(c => Math.min(c + 1, options.length - 1))
      }
    },
  }),
)(Menu)

// #region styles
const Input = styled.input`
  width: 100%;
  height: calc(${th('gridUnit')} * 4);
  border: ${th('borderWidth')} ${th('borderStyle')} ${validationColor};
  border-radius: ${th('borderRadius')};
  padding: 0 ${th('gridUnit')};
  font-family: ${th('fontHeading')};
  ::placeholder {
    color: ${th('colorText')};
    opacity: 1;
    font-family: ${th('fontWriting')};
    font-style: italic;
  }
  :focus {
    border-color: ${th('action.colorActive')}
    outline: none;
  }
`

const CloseOverlay = styled.div`
  background-color: transparent;
  position: fixed;
  bottom: 0;
  left: 0;
  top: 0;
  right: 0;
  z-index: 10;

  ${override('ui.MenuCountry.CloseOverlay')};
`

const Main = styled.div.attrs(props => ({
  role: 'listbox',
}))`
  position: relative;

  ${override('ui.MenuCountry.Main')};
`

const Options = styled.div`
  position: absolute;
  top: 35px;
  left: 0;
  right: 0;

  background-color: ${th('colorBackground')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  border-radius: ${th('borderRadius')};
  overflow-y: auto;
  max-height: ${({ maxHeight }) => `${maxHeight || 250}px`};
  max-width: ${({ maxWidth }) => `${maxWidth || 200}px`};
  z-index: 100;

  ${override('ui.MenuCountry.Options')};
`

const Option = styled.div.attrs(props => ({
  role: 'option',
  tabIndex: '0',
  'aria-selected': props.active,
}))`
  color: ${props => (props.active ? props.theme.textColor : '#444')};
  font-weight: ${props => (props.active ? '600' : 'inherit')};
  cursor: pointer;
  font-family: ${th('fontAuthor')};
  padding: calc(${th('gridUnit')} - ${th('borderWidth')} * 2)
    calc(${th('gridUnit')} * 2);
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-width: ${th('borderWidth')} 0 ${th('borderWidth')} 0;
  white-space: nowrap;

  &:hover {
    background: ${th('colorBackgroundHue')};
    border-color: ${th('colorBorder')};
  }

  &:first-child:hover {
    border-top-color: ${th('colorBackgroundHue')};
  }

  &:last-child:hover {
    border-bottom-color: ${th('colorBackgroundHue')};
  }

  ${override('ui.MenuCountry.Option')};
`
// #endregion
