import styled from 'styled-components'
import { Row, Item } from './'

const OverrideAlert = styled.div`
  div[role='alert'] {
    margin-top: 0;
  }
`
export const RowOverrideAlert = OverrideAlert.withComponent(Row)
export const ItemOverrideAlert = OverrideAlert.withComponent(Item)

export const ModalMenuItem = styled(Item)`
  div[role='alert'] {
    margin-top: 0;
  }
  div[role='listbox'] {
    & button {
      & span:first-child {
        margin-bottom: 12px;
      }
    }
  }
`
