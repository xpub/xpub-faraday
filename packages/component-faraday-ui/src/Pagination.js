import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { IconButton, Label, Text } from './'

const PaginationComponent = ({
  page = 1,
  toLast,
  toFirst,
  hasMore = true,
  nextPage,
  prevPage,
  maxItems = 23,
  itemsPerPage = 10,
  changeItemsPerPage = () => {},
}) => (
  <Root>
    <Text mr={1} secondary>
      Showing
    </Text>
    <TextInput onChange={changeItemsPerPage} value={itemsPerPage} />
    <Chevrons
      className="arrowEndLeft"
      hide={page === 0}
      iconSize={2}
      onClick={toFirst}
      pb={0.5}
      pl={1}
      pr={2}
    />
    <Chevrons
      className="caratRight"
      hide={page === 0}
      iconSize={2}
      onClick={prevPage}
      pb={0.5}
      pr={2}
    />
    <Label>{`${page * itemsPerPage + 1} to ${
      hasMore ? itemsPerPage * (page + 1) : maxItems
    }`}</Label>
    <Chevrons
      className="caratLeft"
      hide={!hasMore}
      iconSize={2}
      onClick={nextPage}
      pb={0.5}
      pl={2}
    />
    <Chevrons
      className="arrowEndRight"
      hide={!hasMore}
      iconSize={2}
      onClick={toLast}
      pb={0.5}
      pl={2}
    />
  </Root>
)

export const Pagination = ({ Items, children, ...props }) => (
  <Fragment>
    <PaginationComponent {...props} />
    {typeof children === 'function' && children(Items, props)}
  </Fragment>
)

Pagination.propTypes = {
  /** Page current number. */
  page: PropTypes.number,
  /** Indicates if there are more pages to be displayed. */
  hasMore: PropTypes.bool,
  /** Maximum items displayed. */
  maxItems: PropTypes.number,
  /** Items displayed per page. */
  itemsPerPage: PropTypes.number,
  /** Change how many items should be displayed per page. */
  changeItemsPerPage: PropTypes.func,
}
Pagination.defaultProps = {
  page: 1,
  hasMore: false,
  maxItems: 23,
  itemsPerPage: 10,
  changeItemsPerPage: () => {},
}

export default PaginationComponent

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
`

const TextInput = styled(TextField)`
  margin: 0;
  width: calc(${th('gridUnit')} * 5);
  height: calc(${th('gridUnit')} * 4);

  & input {
    text-align: center;
    vertical-align: middle;
  }
`

const Chevrons = styled(IconButton)`
  cursor: ${({ hide }) => (hide ? 'auto' : 'pointer')};
  opacity: ${({ hide }) => (hide ? 0 : 1)};
`
// #endregion
