import React from 'react'
import { withCountries } from 'pubsweet-component-faraday-ui'
import PropTypes from 'prop-types'
import { Text, Row, Label, Item } from './'

const PersonInfo = ({
  person: { email, firstName, lastName, affiliation, country },
  countryLabel,
}) => (
  <Row>
    <Item mr={1} vertical>
      <Label>Email</Label>
      <Text>{email}</Text>
    </Item>
    <Item mr={1} vertical>
      <Label>First name</Label>
      <Text>{firstName}</Text>
    </Item>
    <Item mr={1} vertical>
      <Label>Last name</Label>
      <Text>{lastName}</Text>
    </Item>
    <Item mr={1} vertical>
      <Label>Affiliation</Label>
      <Text>{affiliation}</Text>
    </Item>
    <Item vertical>
      <Label>Country</Label>
      <Text>{countryLabel(country)}</Text>
    </Item>
  </Row>
)
PersonInfo.propTypes = {
  /** Person with information */
  person: PropTypes.shape({
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    affiliation: PropTypes.string,
    country: PropTypes.string,
  }),
}

PersonInfo.defaultProps = {
  person: {
    email: '',
    firstName: '',
    lastName: '',
    affiliation: '',
    country: '',
  },
}

export default withCountries(PersonInfo)
