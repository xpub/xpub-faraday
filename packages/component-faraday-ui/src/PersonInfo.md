Details of a person.

```js
const anAuthor = {
  email: 'author.manuscriptovici@gmail.com',
  firstName: 'Author',
  lastName: 'Manuscriptovici',
  affiliation: 'PSD',
};
<PersonInfo person={anAuthor} />
```
