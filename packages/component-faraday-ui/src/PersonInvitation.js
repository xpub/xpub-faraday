import PropTypes from 'prop-types'
import styled from 'styled-components'
import React, { Fragment } from 'react'
import { compose, withHandlers, defaultProps, setDisplayName } from 'recompose'

import { Text, OpenModal, IconButton, marginHelper, withFetching } from './'

const PersonInvitation = ({
  id,
  withName,
  hasAnswer,
  isFetching,
  isLatestVersion,
  revokeInvitation,
  resendInvitation,
  person: { name, email },
  role,
  ...rest
}) => (
  <Root {...rest}>
    {withName && <Text>{name}</Text>}
    {!hasAnswer &&
      name !== 'Unassigned' && (
        <Fragment>
          {role !== 'reviewer' && (
            <OpenModal
              confirmText="Resend"
              isFetching={isFetching}
              modalKey={`resend-${id}`}
              onConfirm={resendInvitation}
              subtitle={email}
              title="Resend the invitation?"
            >
              {showModal => (
                <IconButton
                  data-test-id="resend-invitation-to-HE"
                  fontIcon="resendIcon"
                  mb={1}
                  ml={2}
                  onClick={showModal}
                  secondary
                />
              )}
            </OpenModal>
          )}
          <OpenModal
            confirmText="Revoke"
            isFetching={isFetching}
            modalKey={`revoke-${id}`}
            onConfirm={revokeInvitation}
            subtitle={email}
            title="Revoke invitation?"
          >
            {showModal => (
              <IconButton
                data-test-id="revoke-button"
                fontIcon="removeIcon"
                mb={1}
                ml={2}
                onClick={showModal}
                secondary
              />
            )}
          </OpenModal>
        </Fragment>
      )}
    {hasAnswer &&
      isLatestVersion && (
        <Fragment>
          <OpenModal
            confirmText="Revoke"
            isFetching={isFetching}
            modalKey={`remove-${id}`}
            onConfirm={revokeInvitation}
            subtitle="Deleting the handling editor at this moment will also remove all his work."
            title="Revoke invitation?"
          >
            {showModal => (
              <IconButton
                data-test-id="revoke-button"
                fontIcon="removeIcon"
                mb={1}
                ml={2}
                onClick={showModal}
                secondary
              />
            )}
          </OpenModal>
        </Fragment>
      )}
  </Root>
)

PersonInvitation.propTypes = {
  /** Id of invitation. */
  id: PropTypes.string,
  /** Type of invitation. */
  type: PropTypes.string,
  /** Role of user. */
  role: PropTypes.string,
  /** Reason written for review. */
  reason: PropTypes.string,
  /** Id of user. */
  userId: PropTypes.string,
  /** If user has left an answer or not. */
  hasAnswer: PropTypes.bool,
  /** Date of invite. */
  invitedOn: PropTypes.number,
  /** If user was accepted or not. */
  isAccepted: PropTypes.bool,
  /** Date of user response. */
  respondedOn: PropTypes.number,
  /** Details of person. */
  person: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
  }),
}
PersonInvitation.defaultProps = {
  id: '',
  role: '',
  type: '',
  reason: '',
  userId: '',
  hasAnswer: false,
  invitedOn: Date.now(),
  isAccepted: false,
  respondedOn: Date.now(),
  person: {},
}

export default compose(
  defaultProps({
    person: {
      name: 'Unassigned',
    },
    onRevoke: id => {},
    onResend: id => {},
  }),
  withFetching,
  withHandlers({
    revokeInvitation: ({ id, onRevoke, setFetching }) => props => {
      typeof onRevoke === 'function' && onRevoke(id, { ...props, setFetching })
    },
    resendInvitation: ({
      id,
      onResend,
      setFetching,
      person: { email },
    }) => props => {
      typeof onResend === 'function' &&
        onResend(email, { ...props, setFetching })
    },
  }),
  setDisplayName('PersonInvitation'),
)(PersonInvitation)

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;

  ${marginHelper};
`
// #endregion
