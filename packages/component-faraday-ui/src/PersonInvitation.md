Shows the invited person and the possibility to cancel or resend the invite.

```js
const invitation = {
  id: 'b4305ab6-84e6-48a3-9eb9-fbe0ec80c694',
  role: 'handlingEditor',
  type: 'invitation',
  reason: 'because',
  userId: 'cb7e3e26-6a09-4b79-a6ff-4d1235ee2381',
  hasAnswer: false,
  invitedOn: 1533713919119,
  isAccepted: false,
  respondedOn: 1533714034932,
  person: {
    id: 'cb7e3e26-6a09-4b79-a6ff-4d1235ee2381',
    name: 'Toto Schilacci',
  },
};

<PersonInvitation
  {...invitation}
  onResend={id => console.log('resend invitation with id', id)}
  onRevoke={id => console.log('revoke invitation with id', id)}
  />
```
