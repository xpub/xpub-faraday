import { last } from 'lodash'
import React, { Fragment } from 'react'
import { withProps, withHandlers, compose } from 'recompose'
import IconButton from './IconButton'

import { withFilePreview } from './helpers'

const hasPreview = (name = '') => {
  const extension = last(name.split('.')).toLocaleLowerCase()
  return ['pdf', 'png', 'jpg'].includes(extension)
}

const PreviewFile = ({ onPreview, hasPreview }) => (
  <Fragment>
    {hasPreview && (
      <IconButton
        fontIcon="previewIcon"
        iconSize={2}
        ml={1}
        mr={2}
        onClick={onPreview}
        secondary
      />
    )}
  </Fragment>
)

export default compose(
  withFilePreview,
  withProps(({ file: { name } }) => ({
    hasPreview: hasPreview(name),
  })),
  withHandlers({
    onPreview: ({ previewFile, file }) => () => {
      typeof previewFile === 'function' && previewFile(file)
    },
  }),
)(PreviewFile)
