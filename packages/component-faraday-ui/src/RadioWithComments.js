import React from 'react'
import PropTypes from 'prop-types'
import { get } from 'lodash'
import { Field } from 'redux-form'
import styled from 'styled-components'
import { ValidatedField, YesOrNo } from '@pubsweet/ui'
import { required as requiredValidator } from 'xpub-validators'

import {
  Row,
  Item,
  Text,
  Label,
  Textarea,
  ActionLink,
  IconTooltip,
  marginHelper,
  RowOverrideAlert,
} from './'

const RadioWithComments = ({
  required,
  subtitle,
  radioLabel,
  formValues,
  commentsOn,
  radioFieldName,
  tooltipContent,
  commentsSubtitle,
  commentsFieldName,
  commentsPlaceholder,
  ...rest
}) => (
  <Root {...rest}>
    <Row alignItems="center" justify="flex-start">
      <Item>
        <Label required={required}>{radioLabel}</Label>
        {tooltipContent && (
          <IconTooltip content={tooltipContent} interactive primary />
        )}
      </Item>
    </Row>

    <Row
      alignItems="center"
      data-test-id={`submission-yes-or-no-${radioFieldName}`}
      justify="flex-start"
      mb={1}
    >
      <Field
        component={({ input }) => <YesOrNo {...input} />}
        name={radioFieldName}
      />
    </Row>

    {get(formValues, radioFieldName, '') === commentsOn && (
      <RowOverrideAlert alignItems="center" justify="flex-start">
        <Item data-test-id="submission-conflicts-text" vertical>
          {commentsSubtitle && (
            <Text secondary>
              {commentsSubtitle}
              {subtitle && (
                <ActionLink to={subtitle.link}>{subtitle.label}</ActionLink>
              )}
            </Text>
          )}
          <ValidatedField
            component={Textarea}
            name={commentsFieldName}
            placeholder={commentsPlaceholder}
            validate={required ? [requiredValidator] : []}
          />
        </Item>
      </RowOverrideAlert>
    )}
  </Root>
)

RadioWithComments.propTypes = {
  /** Defines if a fragment is required or not. */
  required: PropTypes.bool,
  /** Values taken by form. */
  formValues: PropTypes.object, //eslint-disable-line
  /** Name of field selected after using radio buttons. */
  radioFieldName: PropTypes.string,
  /** Name of comments field after radio buttons choice. */
  commentsFieldName: PropTypes.string,
  /** Name of field that was commented on. */
  commentsOn: PropTypes.string,
  /** Label name of the field that was commented on. */
  commentsLabel: PropTypes.string,
  /** Name of radio label field on witch it was commented. */
  radioLabel: PropTypes.string,
}
RadioWithComments.defaultProps = {
  required: false,
  formValues: {},
  radioFieldName: '',
  commentsFieldName: '',
  commentsOn: '',
  commentsLabel: '',
  radioLabel: '',
}

export default RadioWithComments

// #region styles
const Root = styled.div`
  width: 100%;

  ${marginHelper};
`
// #endregion
