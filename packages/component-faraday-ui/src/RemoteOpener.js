import PropTypes from 'prop-types'
import { withStateHandlers } from 'recompose'

const RemoteOpener = ({ expanded, toggle, children }) =>
  children({ expanded, toggle })

export default withStateHandlers(
  ({ startExpanded }) => ({ expanded: startExpanded }),
  {
    toggle: ({ expanded }) => () => ({
      expanded: !expanded,
    }),
  },
)(RemoteOpener)

RemoteOpener.propTypes = {
  /** Prop used together with toggle. */
  expanded: PropTypes.bool,
  /** Callback function used to control the state of the component.
   * To be used together with the `expanded` prop.
   */ toggle: PropTypes.func,
}
RemoteOpener.defaultProps = {
  expanded: false,
  toggle: () => {},
}
