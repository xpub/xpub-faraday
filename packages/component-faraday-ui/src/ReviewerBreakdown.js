import React from 'react'
import PropTypes from 'prop-types'

import { Text, Row } from './'

const roleFilter = role => i => i.role === role
const submittedFilter = r => r.review && r.review.submittedOn
const acceptedFilter = i => i.hasAnswer && i.isAccepted
const declinedFilter = i => i.hasAnswer && !i.isAccepted
const reviewerReduce = (acc, r) => ({
  ...acc,
  accepted: acceptedFilter(r) ? acc.accepted + 1 : acc.accepted,
  declined: declinedFilter(r) ? acc.declined + 1 : acc.declined,
  submitted: submittedFilter(r) ? acc.submitted + 1 : acc.submitted,
})

const ReviewerBreakdown = ({
  fragment: { invitations = [], recommendations = [] },
  ...rest
}) => {
  const reviewerInvitations = invitations.filter(roleFilter('reviewer'))
  const invitationsWithRecommendations = reviewerInvitations.map(r => ({
    ...r,
    review: recommendations.find(rec => rec.userId === r.userId),
  }))
  const report = invitationsWithRecommendations.reduce(reviewerReduce, {
    accepted: 0,
    declined: 0,
    submitted: 0,
  })
  return reviewerInvitations.length ? (
    <Row fitContent justify="flex-end" {...rest}>
      <Text customId mr={1 / 2}>
        {reviewerInvitations.length}
      </Text>
      <Text mr={1 / 2}> invited,</Text>

      <Text customId mr={1 / 2}>
        {report.accepted}
      </Text>
      <Text mr={1 / 2}> agreed,</Text>

      <Text customId mr={1 / 2}>
        {report.declined}
      </Text>
      <Text mr={1 / 2}> declined,</Text>

      <Text customId mr={1 / 2}>
        {report.submitted}
      </Text>
      <Text mr={1 / 2}> submitted</Text>
    </Row>
  ) : (
    <Text mr={1}>{`${reviewerInvitations.length} invited`}</Text>
  )
}

ReviewerBreakdown.propTypes = {
  fragment: PropTypes.shape({
    invitations: PropTypes.arrayOf(PropTypes.object),
    recommendations: PropTypes.arrayOf(PropTypes.object),
  }),
}
ReviewerBreakdown.defaultProps = {
  fragment: {
    invitations: [],
    recommendations: [],
  },
}

export default ReviewerBreakdown
