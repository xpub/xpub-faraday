Reviewer report.

```js
const report = {
  id: '71effdc0-ccb1-4ea9-9422-dcc9f8713347',
  userId: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
  comments: [
    {
      files: [
        {
          id:
            '5c0a233b-2569-443b-8110-ef98a18a60a4/2cac524e-0259-45fb-ad3c-9ebc94af8acc',
          name: '1508309142.png',
          size: 35249,
          originalName: '1508309142.png',
        },
      ],
      public: true,
      content: 'Public report here.',
    },
    {
      files: [],
      public: false,
      content: 'Confidential note is here.',
    },
  ],
  createdOn: 1538053564396,
  updatedOn: 1538053600643,
  submittedOn: 1538053600624,
  recommendation: 'publish',
  recommendationType: 'review',
  reviewerNumber: 1
}

const journal = {
  recommendations: [
    {
      value: 'publish',
      label: 'Publish',
    },
    {
      value: 'major',
      label: 'Major revision',
    },
    {
      value: 'minor',
      label: 'Minor revision',
    },
    {
      value: 'reject',
      label: 'Reject',
    },
  ],
}
;<ReviewerReportAuthor
  journal={journal}
  report={report}
  showOwner
  onPreview={file => console.log('preview file', file)}
/>
```
