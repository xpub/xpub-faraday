A list of reviewers.

```js
const invitations = [
  {
    "id": "0078cc96-6daf-4171-8e57-dc84d85600ee",
    "role": "reviewer",
    "type": "invitation",
    "userId": "9ac5b5b5-252c-4933-9e66-72ec7c644a5c",
    "hasAnswer": true,
    "invitedOn": 1538461178587,
    "isAccepted": true,
    "respondedOn": 1538461646415
  }
];

<ReviewersTable invitations={invitations} />
```
