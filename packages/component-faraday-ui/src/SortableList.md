A list with drag and drop support.

```js
const items = [
  { firstName: 'John', lastName: 'Doe' },
  { firstName: 'Michael', lastName: 'Jackson' },
  { firstName: 'David', lastName: 'Blaine' },
]

const Item = ({ dragHandle, isOver, isDragging, ...rest }) => (
  <div style={{ display: 'flex' }}>
    {dragHandle}
    <span>
      {rest.firstName} {rest.lastName}
    </span>
  </div>
)

class Example extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      items: [
        { firstName: 'John', lastName: 'Doe' },
        { firstName: 'Michael', lastName: 'Jackson' },
        { firstName: 'David', lastName: 'Blaine' },
      ],
    }
  }

  render() {
    return (
      <SortableList
        items={this.state.items}
        listItem={Item}
        itemKey="firstName"
        dragHandle={DragHandle}
        moveItem={(dragIndex, hoverIndex) => {
          this.setState({
            items: SortableList.moveItem(
              this.state.items,
              dragIndex,
              hoverIndex,
            ),
          })
        }}
        dropItem={(item, props) => {}}
      />
    )
  }
}

;<Example />
```
