import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { paddingHelper, marginHelper } from './styledHelpers'

const textHelper = props => {
  if (get(props, 'secondary')) {
    return css`
      color: ${th('colorSecondary')};
      font-family: ${th('fontReading')};
    `
  }
  if (get(props, 'error')) {
    return css`
      color: ${th('colorError')};
      font-family: ${th('fontReading')};
    `
  }
  if (get(props, 'customId')) {
    return css`
      color: ${th('colorPrimary')};
      font-family: ${th('fontInterface')};
    `
  }
  if (get(props, 'labelLine')) {
    return css`
      color: ${th('colorFurnitureHue')};
      font-family: ${th('fontInterface')};
      display: flex;
      &:after {
        display: block;
        content: ' ';
        border-bottom: 1px solid ${th('colorBorder')};
        flex: 1 1 auto;
        margin-left: calc(${th('gridUnit')} * 2);
      }
    `
  }
  if (get(props, 'journal')) {
    return css`
      color: ${th('colorSecondary')};
      font-family: ${th('fontReading')};
      &:before {
        content: '•';
        padding-right: ${th('gridUnit')};
      }
    `
  }
  return css`
    color: ${th('colorText')};
    font-family: ${th('fontReading')};
  `
}

const fontSize = css`
  font-size: ${props =>
    props.small ? th('fontSizeBaseSmall') : th('fontSizeBase')};
  line-height: ${props =>
    props.small ? th('lineHeightBaseSmall') : th('lineHeightBase')};
`

const StyledText = styled.span`
  align-items: center;
  display: ${props => get(props, 'display', 'inline-block')};
  font-style: ${props => get(props, 'fontStyle', 'normal')};
  text-align: ${props => get(props, 'align', 'start')};
  white-space: ${props => get(props, 'whiteSpace', 'initial')};

  ${fontSize};
  ${textHelper};
  ${marginHelper};
  ${paddingHelper};
`

const Bullet = styled.span`
  color: ${th('colorWarning')};
  font-size: ${th('fontSizeHeading2')};
  line-height: 0;
  margin-right: ${th('gridUnit')};
  padding-top: ${th('gridUnit')};
  vertical-align: middle;
`

const Root = styled.div`
  align-items: flex-start;
  display: flex;
`

const Text = ({ bullet, children, ...rest }) =>
  bullet ? (
    <Root>
      <Bullet>{'\u2022'}</Bullet>
      <StyledText {...rest}>{children}</StyledText>
    </Root>
  ) : (
    <StyledText {...rest}>{children}</StyledText>
  )

Text.propTypes = {
  /** Default color for non-primary actions. */
  secondary: PropTypes.bool,
  /** Default color for error actions. */
  error: PropTypes.bool,
  /** Default style for the customId text. */
  customId: PropTypes.bool,
  /** Default style for text used as a label Line. */
  labelLine: PropTypes.bool,
  /** Default style used for journal text. */
  journal: PropTypes.bool,
  /** Default style used for small text. */
  small: PropTypes.bool,
}

Text.defaultProps = {
  secondary: false,
  error: false,
  customId: false,
  labelLine: false,
  journal: false,
  small: false,
}

export default Text
