/* eslint-disable handle-callback-err  */
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { reduxForm } from 'redux-form'
import React, { Fragment } from 'react'
import { th } from '@pubsweet/ui-toolkit'
import { withCountries, MenuCountry } from 'pubsweet-component-faraday-ui'
import { required as requiredValidator } from 'xpub-validators'
import { compose, withStateHandlers, withProps } from 'recompose'
import { H3, Spinner, ValidatedField, TextField, Menu } from '@pubsweet/ui'

import {
  Row,
  Item,
  Text,
  Label,
  ActionLink,
  IconButton,
  ShadowedBox,
  withFetching,
  RowOverrideAlert,
} from './'

const Profile = ({
  title,
  toggleEdit,
  user: { affiliation, firstName, lastName, title: userTitle, country = '' },
  countryLabel,
  ...rest
}) => (
  <ShadowedBox position="relative" {...rest}>
    <IconButton
      fontIcon="editIcon"
      iconSize={2}
      onClick={toggleEdit}
      paddingRight={2}
      paddingTop={1}
      right={8}
      top={12}
    />

    <Fragment>
      <Row alignItems="center">
        <Item>
          <H3>Account Details</H3>
        </Item>
        <Item>
          <ActionLink internal to="/profile/change-password">
            Change password
          </ActionLink>
        </Item>
      </Row>

      <Row alignItems="baseline" mt={2}>
        <Item vertical>
          <Label>First Name</Label>
          <Text secondary>{firstName}</Text>
        </Item>
        <Item vertical>
          <Label>Last Name</Label>
          <Text secondary>{lastName}</Text>
        </Item>
      </Row>

      <Row alignItems="baseline" mt={2}>
        <Item vertical>
          <Label>Title</Label>
          <Text secondary>
            {get(title.find(t => t.value === userTitle), 'label', '')}
          </Text>
        </Item>
        <Item vertical>
          <Label>Country</Label>
          <Text secondary>{countryLabel(country)}</Text>
        </Item>
      </Row>

      <Row alignItems="baseline" mt={2}>
        <Item vertical>
          <Label>Affiliation</Label>
          <Text secondary>{affiliation}</Text>
        </Item>
      </Row>
    </Fragment>
  </ShadowedBox>
)

const EditModeIcons = ({ toggleEdit, onSaveChanges }) => (
  <Fragment>
    <IconButton
      fontIcon="removeIcon"
      iconSize={2}
      onClick={toggleEdit}
      right={36}
      top={12}
    />
    <IconButton
      fontIcon="saveIcon"
      iconSize={2}
      onClick={onSaveChanges}
      right={8}
      top={12}
    />
  </Fragment>
)

const EditUserProfile = compose(
  withFetching,
  withProps(
    ({
      user: { affiliation, firstName, lastName, title: userTitle, country },
    }) => ({
      initialValues: {
        country,
        lastName,
        firstName,
        affiliation,
        title: userTitle,
      },
    }),
  ),
  reduxForm({
    form: 'profile',
    onSubmit: (values, dispatch, { onSave, ...props }) => {
      typeof onSave === 'function' && onSave(values, props)
    },
  }),
)(
  ({
    toggleEdit,
    title,
    handleSubmit,
    isFetching,
    countries,
    fetchingError,
    ...rest
  }) => (
    <ShadowedBox position="relative" {...rest}>
      {isFetching ? (
        <StyledSpinner>
          <Spinner />
        </StyledSpinner>
      ) : (
        <EditModeIcons onSaveChanges={handleSubmit} toggleEdit={toggleEdit} />
      )}
      <Fragment>
        <Row>
          <Item>
            <H3>Edit Account Details</H3>
          </Item>
        </Row>

        <Row alignItems="baseline" mt={2}>
          <Item mr={1} vertical>
            <Label required>First Name</Label>
            <ValidatedField
              component={TextField}
              name="firstName"
              validate={[requiredValidator]}
            />
          </Item>
          <Item ml={1} vertical>
            <Label required>Last Name</Label>
            <ValidatedField
              component={TextField}
              name="lastName"
              validate={[requiredValidator]}
            />
          </Item>
        </Row>

        <RowOverrideAlert alignItems="baseline" mt={2}>
          <Item data-test-id="title" mr={1} vertical>
            <Label required>Title</Label>
            <ValidatedField
              component={input => <Menu {...input} options={title} />}
              name="title"
              validate={[requiredValidator]}
            />
          </Item>
          <Item data-test-id="country" ml={1} vertical>
            <Label required>Country</Label>
            <ValidatedField
              component={MenuCountry}
              name="country"
              placeholder="Please select"
              validate={[requiredValidator]}
            />
          </Item>
        </RowOverrideAlert>

        <Row alignItems="baseline" mt={2}>
          <Item vertical>
            <Label required>Affiliation</Label>
            <ValidatedField
              component={TextField}
              name="affiliation"
              validate={[requiredValidator]}
            />
          </Item>
        </Row>

        {fetchingError && (
          <Row alignItems="center" mt={2}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}
      </Fragment>
    </ShadowedBox>
  ),
)

const UserProfile = ({
  user,
  onSave,
  editMode,
  toggleEdit,
  journal: { title = [] },
  countries,
  countryLabel,
  ...rest
}) =>
  !editMode ? (
    <Profile
      countryLabel={countryLabel}
      title={title}
      toggleEdit={toggleEdit}
      user={user}
      {...rest}
    />
  ) : (
    <EditUserProfile
      countries={countries}
      onSave={onSave}
      title={title}
      toggleEdit={toggleEdit}
      user={user}
      {...rest}
    />
  )

UserProfile.propTypes = {
  /** Passes journals label and value which will appear in the signature. */
  journal: PropTypes.shape({
    title: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.string,
      }),
    ),
  }),
  /** Passes properties for the users profile */
  user: PropTypes.shape({
    /** Users unique id. */
    id: PropTypes.string,
    /** Type of created account. */
    type: PropTypes.string,
    /** Determine if account is admin ot not. */
    admin: PropTypes.bool,
    /** Email used for user authentification. */
    email: PropTypes.string,
    /**  */
    teams: PropTypes.array,
    /** Title of account userd. */
    title: PropTypes.string,
    /**  */
    agreeTC: PropTypes.bool,
    /** Country of account user. */
    contry: PropTypes.string,
    /** Status of account. */
    isActive: PropTypes.bool,
    /** Last Name of accounts user. */
    lastName: PropTypes.string,
    /** First name of accounts user. */
    username: PropTypes.string,
    /** Account user first name. */
    firstName: PropTypes.string,
    /**  */
    fragments: PropTypes.array,
    /** Accounts user affiliation. */
    affiliation: PropTypes.string,
    /** */
    collection: PropTypes.array,
    /** Determine if account is confirmed or not. */
    isConfirmed: PropTypes.bool,
    /** Determine if account is editor in chief or not. */
    editorInChief: PropTypes.bool,
    /**  */
    notifications: PropTypes.shape({
      email: PropTypes.shape({
        user: PropTypes.bool,
        system: PropTypes.bool,
      }),
    }),

    /** Determine if account is hendling editor or not. */
    handlingEditor: PropTypes.bool,
    /** Users unique token */
    token: PropTypes.string,
  }),
}

UserProfile.defaultProps = {
  journal: {},
  user: {},
}

export default compose(
  withCountries,
  withStateHandlers(
    {
      editMode: false,
    },
    {
      toggleEdit: ({ editMode }) => () => ({
        editMode: !editMode,
      }),
    },
  ),
)(UserProfile)

// #region styles
const StyledSpinner = styled.div`
  position: absolute;
  top: ${th('gridUnit')};
  right: ${th('gridUnit')};
`
// #endregion
