export const minLength = (value, min) => !!(value && value.length >= min)

export const atLeastOneUppercase = value => {
  const uppercaseRegex = new RegExp(/([A-Z])+/)
  return uppercaseRegex.test(value)
}
export const atLeastOneLowerrcase = value => {
  const lowercaseRegex = new RegExp(/([a-z])+/)
  return lowercaseRegex.test(value)
}
export const atLeastOneDigit = value => {
  const digitRegex = new RegExp(/([0-9])+/)
  return digitRegex.test(value)
}
export const atLeastOnePunctuation = value => {
  const punctuationRegex = new RegExp(/([,'!@#$%^&*=(){}[\]<>?/\\|.:;_-])+/)
  return punctuationRegex.test(value)
}

export const passwordValidator = values => {
  const errors = {}
  const { password, confirmNewPassword } = values
  if (
    !(
      minLength(password, 6) &&
      atLeastOneUppercase(password) &&
      atLeastOneLowerrcase(password) &&
      atLeastOnePunctuation(password) &&
      atLeastOneDigit(password)
    )
  ) {
    errors.password = 'Password criteria not met'
  }
  if (!password) {
    errors.password = 'Required'
  }
  if (!confirmNewPassword) {
    errors.confirmNewPassword = 'Required'
  } else if (confirmNewPassword !== password) {
    errors.confirmNewPassword = "Passwords don't match."
  }

  return errors
}

export const changePasswordValidator = values => {
  const {
    currentPassword = '',
    confirmNewPassword = '',
    password = '',
  } = values
  const errors = {}
  if (
    !(
      minLength(password, 6) &&
      atLeastOneUppercase(password) &&
      atLeastOneLowerrcase(password) &&
      atLeastOnePunctuation(password) &&
      atLeastOneDigit(password)
    )
  ) {
    errors.password = 'Password criteria not met'
  }
  if (!currentPassword) {
    errors.currentPassword = 'Required'
  }

  if (!password) {
    errors.password = 'Required'
  } else if (password !== confirmNewPassword) {
    errors.confirmNewPassword = "Passwords don't match."
  }

  return errors
}
