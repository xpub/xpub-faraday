import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Field } from 'formik'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {
  compose,
  withProps,
  withHandlers,
  shouldUpdate,
  setDisplayName,
} from 'recompose'

const WrappedComponent = compose(
  withHandlers({
    onChange: ({ field: { onChange, name }, form: { setFieldValue } }) => v => {
      if (typeof v === 'object') {
        onChange(v)
      } else {
        setFieldValue(name, v)
      }
    },
  }),
  withProps(({ form: { errors, touched, submitCount }, name }) => ({
    hasError: (get(touched, name) || submitCount > 0) && get(errors, name),
  })),
  withProps(({ name, hasError, form: { errors } }) => ({
    error: hasError && get(errors, name),
    validationStatus: hasError ? 'error' : 'default',
  })),
  shouldUpdate(
    (prev, next) =>
      get(prev, 'field.value') !== get(next, 'field.value') ||
      get(prev, 'error') !== get(next, 'error'),
  ),
)(
  ({
    error,
    onChange,
    validationStatus,
    component: Component,
    field: { name, value, onBlur },
    form: { errors, setFieldValue, touched, values, submitCount },
    ...props
  }) => (
    <Fragment>
      <Component
        name={name}
        onBlur={onBlur}
        onChange={onChange}
        value={value}
        {...props}
        validationStatus={validationStatus}
      />
      <MessageWrapper role="alert">
        {error && <ErrorMessage>{error}</ErrorMessage>}
      </MessageWrapper>
    </Fragment>
  ),
)

const ValidatedFormField = ({ name, component, validateFn, ...props }) => (
  <Field name={name} validate={validateFn}>
    {fieldProps => (
      <WrappedComponent
        component={component}
        name={name}
        {...props}
        {...fieldProps}
      />
    )}
  </Field>
)

ValidatedFormField.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func,
    PropTypes.string,
  ]).isRequired,
}

export default compose(
  setDisplayName('ValidatedFormikField'),
  withProps(({ validate = [] }) => ({
    validateFn: (value = '') =>
      validate.reduce((acc, fn) => acc || fn(value), ''),
  })),
  shouldUpdate(() => false),
)(ValidatedFormField)

// #region styles
const MessageWrapper = styled.div`
  font-family: ${th('fontInterface')};
  display: flex;
`

const Message = styled.div`
  &:not(:last-child) {
    margin-bottom: ${th('gridUnit')};
  }
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
`

const ErrorMessage = styled(Message)`
  color: ${th('colorError')};
`
// #endregion
