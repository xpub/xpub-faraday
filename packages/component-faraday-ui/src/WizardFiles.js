import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { get } from 'lodash'
import { compose, withState, withHandlers } from 'recompose'

import {
  FileSection,
  SortableList,
  withFilePreview,
  withFileDownload,
} from './'

const WizardFiles = ({
  files,
  addFile,
  fetching,
  moveFile,
  deleteFile,
  changeList,
  previewFile,
  downloadFile,
  manuscripts,
  coverLetter,
  supplementary,
}) => (
  <Fragment>
    <FileSection
      allowedFileExtensions={['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']}
      changeList={changeList}
      files={get(files, 'manuscripts', [])}
      isFetching={get(fetching, 'manuscripts', false)}
      isFirst
      listId="manuscripts"
      maxFiles={1}
      moveItem={moveFile('manuscripts')}
      onDelete={deleteFile('manuscripts')}
      onDownload={downloadFile}
      onFileDrop={addFile('manuscripts')}
      onFilePick={addFile('manuscripts')}
      onPreview={previewFile}
      required
      testId="main-manuscript"
      title="Main Manuscript"
    />
    <FileSection
      allowedFileExtensions={['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']}
      changeList={changeList}
      files={get(files, 'coverLetter', [])}
      isFetching={get(fetching, 'coverLetter', false)}
      listId="coverLetter"
      maxFiles={1}
      moveItem={moveFile('coverLetter')}
      onDelete={deleteFile('coverLetter')}
      onDownload={downloadFile}
      onFileDrop={addFile('coverLetter')}
      onFilePick={addFile('coverLetter')}
      onPreview={previewFile}
      testId="cover-letter"
      title="Cover Letter"
    />
    <FileSection
      changeList={changeList}
      files={get(files, 'supplementary', [])}
      isFetching={get(fetching, 'supplementary', false)}
      isLast
      listId="supplementary"
      moveItem={moveFile('supplementary')}
      onDelete={deleteFile('supplementary')}
      onDownload={downloadFile}
      onFileDrop={addFile('supplementary')}
      onFilePick={addFile('supplementary')}
      onPreview={previewFile}
      testId="supplemental-files"
      title="Supplemental Files"
    />
  </Fragment>
)

const initialFiles = {
  manuscripts: [],
  coverLetter: [],
  supplementary: [],
}

WizardFiles.propTypes = {
  /** Fragments manuscript file. */
  manuscripts: PropTypes.arrayOf(
    PropTypes.shape({
      /** Id of added manuscript file. */
      id: PropTypes.string,
      /** Name of added manuscript file. */
      name: PropTypes.string,
      /** Size of added manusript file. */
      size: PropTypes.number,
    }),
  ),
  /** Fragments cover letter file. */
  coverLetter: PropTypes.arrayOf(
    PropTypes.shape({
      /** Id of added cover letter file. */
      id: PropTypes.string,
      /** Name of added cover letter file. */
      name: PropTypes.string,
      /** Size of added cover letter file. */
      size: PropTypes.number,
    }),
  ),
  /** Fragments supplementary file. */
  supplementary: PropTypes.arrayOf(
    PropTypes.shape({
      /** Id of added cover letter file. */
      id: PropTypes.string,
      /** Name of added cover letter file. */
      name: PropTypes.string,
      /** Size of added cover letter file. */
      size: PropTypes.number,
    }),
  ),
}
WizardFiles.defaultProps = {
  manuscripts: [],
  coverLetter: [],
  supplementary: [],
}

export default compose(
  withFilePreview,
  withFileDownload,
  withState('files', 'setFiles', ({ files = initialFiles }) => files),
  withState('fetching', 'setFilesFetching', {
    manuscripts: false,
    coverLetter: false,
    supplementary: false,
  }),
  withHandlers({
    setFormFiles: ({
      setFiles,
      changeForm,
      formName = 'submission',
    }) => files => {
      setFiles(files)
      changeForm(formName, 'files', files)
    },
    setFilesFetching: ({ setFilesFetching }) => (type, value) => {
      setFilesFetching(p => ({
        ...p,
        [type]: value,
      }))
    },
  }),
  withHandlers({
    addFile: ({
      files,
      fragment,
      uploadFile,
      setFormFiles,
      setFilesFetching,
    }) => type => file => {
      setFilesFetching(type, true)
      uploadFile({ file, type, fragment })
        .then(f => {
          const newFiles = {
            ...files,
            [type]: [...files[type], f],
          }
          setFormFiles(newFiles)
          setFilesFetching(type, false)
        })
        .catch(() => {
          setFilesFetching(type, false)
        })
    },
    previewFile: ({ previewFile }) => previewFile,
    downloadFile: ({ downloadFile, token }) => downloadFile,
    deleteFile: ({
      files,
      deleteFile,
      setFormFiles,
      setFilesFetching,
    }) => type => file => {
      setFilesFetching(type, true)
      deleteFile({ fileId: file.id, type })
        .then(() => {
          const newFiles = {
            ...files,
            [type]: files[type].filter(f => f.id !== file.id),
          }
          setFormFiles(newFiles)
          setFilesFetching(type, false)
        })
        .catch(() => {
          setFilesFetching(type, false)
        })
    },
    moveFile: ({ files, setFormFiles }) => type => (dragIndex, hoverIndex) => {
      const newFiles = {
        ...files,
        [type]: SortableList.moveItem(files[type], dragIndex, hoverIndex),
      }
      setFormFiles(newFiles)
    },
    changeList: ({ files, setFormFiles }) => (from, to, fileId) => {
      const swappedFile = files[from].find(f => f.id === fileId)

      const newFiles = {
        ...files,
        [to]: [...files[to], swappedFile],
        [from]: files[from].filter(f => f.id !== fileId),
      }
      setFormFiles(newFiles)
    },
  }),
)(WizardFiles)
