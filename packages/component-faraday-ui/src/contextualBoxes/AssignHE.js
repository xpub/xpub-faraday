import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, TextField } from '@pubsweet/ui'
import {
  compose,
  withProps,
  defaultProps,
  withHandlers,
  setDisplayName,
  withStateHandlers,
} from 'recompose'

import {
  Row,
  Item,
  Text,
  Label,
  OpenModal,
  IconButton,
  paddingHelper,
} from '../'

const AssignHE = ({
  isFetching,
  clearSearch,
  searchValue,
  changeSearch,
  handlingEditors,
  inviteHandlingEditor,
}) => (
  <Root data-test-id="assign-handling-editor" pb={2}>
    <TextContainer>
      <TextField
        data-test-id="manuscript-assign-he-filter"
        onChange={changeSearch}
        placeholder="Filter by name or email"
        value={searchValue}
      />
      {searchValue !== '' && (
        <IconButton
          fontIcon="removeIcon"
          onClick={clearSearch}
          right={16}
          top={16}
        />
      )}
    </TextContainer>
    {handlingEditors.length > 0 && (
      <Fragment>
        <Row alignItems="center" height="4" pl={1}>
          <Item flex={1}>
            <Label>Name</Label>
          </Item>
          <Item flex={2}>
            <Label>Email</Label>
          </Item>
          <Item flex={1} />
        </Row>
        {handlingEditors.map((he, index) => (
          <CustomRow
            alignItems="center"
            data-test-id={`manuscript-assign-he-invite-${he.id}`}
            height="4"
            isFirst={index === 0}
            key={he.id}
            pl={1}
          >
            <Item flex={1}>
              <Text secondary>{he.name}</Text>
            </Item>
            <Item flex={2}>
              <Text secondary>{he.email}</Text>
            </Item>
            <Item flex={1} justify="center">
              <OpenModal
                confirmText="Invite"
                isFetching={isFetching}
                modalKey={`${he.id}-invitehe`}
                onConfirm={inviteHandlingEditor(he)}
                subtitle={he.name}
                title="Confirm Invitation"
              >
                {showModal => (
                  <CustomButton onClick={showModal} size="small">
                    INVITE
                  </CustomButton>
                )}
              </OpenModal>
            </Item>
          </CustomRow>
        ))}
      </Fragment>
    )}
  </Root>
)

AssignHE.propTypes = {
  /** Changes the search value to lowercase letters. */
  changeSearch: PropTypes.func,
  /** The value of the search input box. */
  searchValue: PropTypes.string,
  /** Clears the value of the search input box. */
  clearSearch: PropTypes.func,
  /** The list of available handling editors. */
  handlingEditors: PropTypes.arrayOf(PropTypes.object),
  /** Invites the selected handling editor. */
  inviteHandlingEditor: PropTypes.func,
}

AssignHE.defaultProps = {
  changeSearch: () => {},
  searchValue: '',
  clearSearch: () => {},
  handlingEditors: [],
  inviteHandlingEditor: () => {},
}

export default compose(
  defaultProps({
    inviteHandlingEditor: he => {},
  }),
  withProps(({ handlingEditors = [] }) => ({
    handlingEditors: handlingEditors.map(he => ({
      ...he,
      name: `${he.firstName} ${he.lastName}`,
      searchIndex: `${he.firstName} ${he.lastName} ${he.email}`,
    })),
  })),
  withStateHandlers(
    { searchValue: '' },
    {
      changeSearch: ({ searchValue }) => e => ({
        searchValue: e.target.value.toLowerCase(),
      }),
      clearSearch: () => () => ({
        searchValue: '',
      }),
    },
  ),
  withProps(({ searchValue, handlingEditors = [] }) => ({
    handlingEditors: handlingEditors.filter(he =>
      he.searchIndex.toLowerCase().includes(searchValue),
    ),
  })),
  withHandlers({
    inviteHandlingEditor: ({ inviteHandlingEditor }) => ({
      email = '',
    }) => props => inviteHandlingEditor(email, props),
  }),
  setDisplayName('AssignHandlingEditor'),
)(AssignHE)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  ${paddingHelper};
`

const TextContainer = styled.div`
  padding: ${th('gridUnit')};
  position: relative;
  width: calc(${th('gridUnit')} * 40);
`

const CustomButton = styled(Button)`
  opacity: 0;
`

const CustomRow = styled(Row)`
  border-top: ${props => props.isFirst && '1px solid #e7e7e7'};
  border-bottom: 1px solid #e7e7e7;
  &:hover {
    background-color: #eee;

    ${CustomButton} {
      opacity: 1;
    }
  }
`
// #endregion
