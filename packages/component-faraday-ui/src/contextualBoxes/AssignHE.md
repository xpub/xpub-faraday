Assign Handling Editor contextual box.

```js
const handlingEditors = [
  {
    id: '1',
    firstName: 'Handling',
    lastName: 'Edi',
    email: 'handling@edi.com',
  },
  {
    id: '2',
    firstName: 'Aurel',
    lastName: 'Vlaicu',
    email: 'aurel@vlaicu.com',
  },
  { id: '3', firstName: 'Gheorghe', lastName: 'Hagi', email: 'gica@hagi.com' },
]

;<ContextualBox label="Assign Handling Editor">
  <AssignHE
    handlingEditors={handlingEditors}
    inviteHandlingEditor={he => console.log('inviting: ', he)}
  />
</ContextualBox>
```
