HE recommendation.

```js
const formValues = {
  recommendation: 'minor',
}
const options = [
  {
    value: 'publish',
    label: 'Publish',
    message: 'Recommend Manuscript for Publishing',
    button: 'Submit Recommendation',
  },
  {
    value: 'reject',
    label: 'Reject',
    message: 'Recommend Manuscript for Rejection',
    button: 'Submit Recommendation',
  },
  {
    value: 'minor',
    label: 'Request Minor Revision',
    message: 'Request Minor Revision',
    button: 'Request Revision',
  },
  {
    value: 'major',
    label: 'Request Major Revision',
    message: 'Request Major Revision',
    button: 'Request Revision',
  },
]
;<HERecommendation
  formValues={formValues}
  options={options}
  modalKey="heRecommendation"
  onRecommendationSubmit={(values, props) => {
    props.setFetching(true)
  }}
/>
```
