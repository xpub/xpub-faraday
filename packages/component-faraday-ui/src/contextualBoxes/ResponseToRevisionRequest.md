ResponseToRevisionRequest.

```js
const fragment = {
  "id": "1378b0e4-5c29-46a6-8afe-2f5ec1b13899",
  "type": "fragment",
  "files": {
    "manuscripts": [
      {
        "id": "e891d6dd-fe46-472d-87db-5c80fdd48ac2/9e103d6a-dd0e-4d1e-b6ff-9b8383579649",
        "name": "Evolutionary Programming.pdf",
        "size": 81618,
        "originalName": "Evolutionary Programming.pdf"
      }
    ],
    "supplementary": []
  },
  "owners": [
    {
      "id": "f3a16660-ae1e-4ab2-9ae4-220a0c7ac575",
      "username": "tania.fecheta+a@thinslices.com"
    }
  ],
  "authors": [
    {
      "id": "f3a16660-ae1e-4ab2-9ae4-220a0c7ac575",
      "title": "mrs",
      "country": "AL",
      "lastName": "Tania",
      "firstName": "Author",
      "affiliation": "Ts",
      "isSubmitting": true,
      "isCorresponding": true
    }
  ],
  "created": "2018-10-30T07:33:26.128Z",
  "version": 2,
  "metadata": {
    "type": "research",
    "title": "Major revision and new version sunmited",
    "journal": "Bioinorganic Chemistry and Applications",
    "abstract": "something something"
  },
  "conflicts": {
    "hasFunding": "",
    "hasConflicts": "no",
    "hasDataAvailability": ""
  },
  "submitted": 1540884806175,
  "invitations": [
    {
      "id": "7b50667d-883c-43d4-aea5-71455e251801",
      "role": "reviewer",
      "type": "invitation",
      "userId": "b7554926-89dc-4b8b-b4d7-cd1bcc51f2de",
      "hasAnswer": true,
      "invitedOn": 1540884401711,
      "isAccepted": true,
      "respondedOn": 1540884410543
    }
  ],
  "collectionId": "383a5314-788c-49e5-aa00-617426b5c7c7",
  "declarations": {
    "agree": true
  },
  "fragmentType": "version",
  "responseToReviewers": {
    "file": null,
    "content": "i changed that thing"
  },
  "recommendations": []
};

<ResponseToRevisionRequest fragment={fragment} />
```
