import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { H4 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withProps, compose } from 'recompose'

import {
  Tag,
  Text,
  Tabs,
  marginHelper,
  ContextualBox,
  ReviewersTable,
  PublonsTable,
  indexReviewers,
  ReviewerReport,
  InviteReviewers,
  ReviewerBreakdown,
  withFilePreview,
  withFileDownload,
} from '../'

const ReviewerDetails = ({
  journal,
  reports = [],
  fragment,
  invitations,
  currentUser,
  publonReviewers,
  isFetching,
  previewFile,
  downloadFile,
  fetchingError,
  canInviteReviewers,
  onInviteReviewer,
  onInvitePublonReviewer,
  onResendReviewerInvite,
  onRevokeReviewerInvite,
  toggle,
  expanded,
  highlight,
  canViewReviewersDetails,
  authorCanViewReportsDetails,
  canSeeReviewerSuggestionsTab,
  isLatestVersion,
  ...rest
}) => (
  <ContextualBox
    data-test-id="reviewer-details-and-reports-box"
    expanded={expanded}
    highlight={highlight}
    label="Reviewer Details & Reports"
    rightChildren={<ReviewerBreakdown fitContent fragment={fragment} mr={1} />}
    toggle={toggle}
    {...rest}
  >
    <Tabs selectedTab={reports.length ? 1 : 0}>
      {({ selectedTab, changeTab }) => (
        <Fragment>
          <TabsHeader data-test-id="reviewer-details-and-reports-tabs">
            <TabButton
              ml={1}
              mr={1}
              onClick={() => changeTab(0)}
              selected={selectedTab === 0}
            >
              <H4>Reviewer Details</H4>
            </TabButton>
            {canInviteReviewers &&
              isLatestVersion &&
              get(currentUser, 'permissions.canInviteReviewersAsEiC') && (
                <TabButton
                  data-test-id="reviewer-tab-suggestions"
                  ml={1}
                  mr={1}
                  onClick={() => changeTab(2)}
                  selected={selectedTab === 2}
                >
                  <H4>Reviewer Suggestions</H4>
                </TabButton>
              )}
            <TabButton
              data-test-id="reviewer-tab-reports"
              ml={1}
              mr={1}
              onClick={() => changeTab(1)}
              selected={selectedTab === 1}
            >
              <H4>Reviewer Reports</H4>
              <Tag mr={1}>{reports.length}</Tag>
            </TabButton>
          </TabsHeader>
          <TabContent>
            {selectedTab === 0 && (
              <Fragment>
                {canInviteReviewers &&
                  isLatestVersion &&
                  get(currentUser, 'permissions.canInviteReviewersAsEiC') && (
                    <InviteReviewers
                      modalKey="invite-reviewers"
                      onInvite={onInviteReviewer}
                    />
                  )}

                <ReviewersTable
                  currentUser={currentUser}
                  invitations={invitations}
                  onResendReviewerInvite={onResendReviewerInvite}
                  onRevokeReviewerInvite={onRevokeReviewerInvite}
                />
              </Fragment>
            )}
            {selectedTab === 2 &&
              isLatestVersion &&
              get(currentUser, 'permissions.canInviteReviewersAsEiC') && (
                <PublonsTable
                  onInvite={onInvitePublonReviewer}
                  publonsError={fetchingError}
                  publonsFetching={isFetching}
                  reviewers={publonReviewers}
                />
              )}
            {selectedTab === 1 && (
              <Fragment>
                {reports.length === 0 && (
                  <Text align="center">No reports submitted yet.</Text>
                )}
                {reports.map(report => (
                  <ReviewerReport
                    journal={journal}
                    key={report.id}
                    onDownload={downloadFile}
                    onPreview={previewFile}
                    report={report}
                    reviewerNumber={report.reviewerNumber}
                    showOwner
                  />
                ))}
              </Fragment>
            )}
          </TabContent>
        </Fragment>
      )}
    </Tabs>
  </ContextualBox>
)

ReviewerDetails.propTypes = {
  /** Object containing the list of recommendations. */
  journal: PropTypes.object, //eslint-disable-line
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Specifies how many reviewers have been invited. */
  invitations: PropTypes.array, //eslint-disable-line
  /** Array that contains publon reviewers. */
  publonReviewers: PropTypes.array, //eslint-disable-line
  /** View content of the uploaded file. */
  previewFile: PropTypes.func,
  /** Downloads the file from the server. */
  downloadFile: PropTypes.func,
  /** Sends an invitation to the reviewer. */
  onInviteReviewer: PropTypes.func,
  /** Reviewers reports. */
  reports: PropTypes.array, //eslint-disable-line
  /** Sends an invitation to a Publon reviewer. */
  onInvitePublonReviewer: PropTypes.func,
  /** Resends an invitation to an already invited. */
  onResendReviewerInvite: PropTypes.func,
  /** Cancels an invitation to an invited reviewer. */
  onRevokeReviewerInvite: PropTypes.func,
  /** Callback function used to control the state of the component.
   * To be used together with the `expanded` prop.
   */
  toggle: PropTypes.func,
  /** Prop used together with toggle. */
  expanded: PropTypes.bool,
  /* Specifies if the contextual box should be highlighted */
  highlight: PropTypes.bool,
  /** Specifies if manuscript is at the latest version. */
  isLatestVersion: PropTypes.bool,
  /** Specifies if we can invite reviewers on the current version. */
  canInviteReviewers: PropTypes.bool,
  /** Specifies if we can view reviewers details on the current version. */
  canViewReviewersDetails: PropTypes.bool,
  /** Specifies if the author can view reports details on the current version. */
  authorCanViewReportsDetails: PropTypes.func,
}
ReviewerDetails.defaultProps = {
  journal: {},
  reports: [],
  fragment: {},
  invitations: [],
  publonReviewers: [],
  previewFile: () => {},
  downloadFile: () => {},
  onInviteReviewer: () => {},
  onInvitePublonReviewer: () => {},
  onResendReviewerInvite: () => {},
  onRevokeReviewerInvite: () => {},
  toggle: () => {},
  expanded: false,
  highlight: false,
  canInviteReviewers: false,
  canViewReviewersDetails: false,
  authorCanViewReportsDetails: () => {},
  isLatestVersion: false,
}

export default compose(
  withFilePreview,
  withFileDownload,
  withProps(
    ({
      invitations = [],
      publonReviewers = [],
      reviewerReports = [],
      currentUser,
    }) => ({
      token: get(currentUser, 'token', ''),
      publonReviewers,
      invitations: invitations.map(i => ({
        ...i,
        review: reviewerReports.find(r => r.userId === i.userId),
      })),
      reports: indexReviewers(
        reviewerReports.filter(r => r.submittedOn),
        invitations,
      ),
    }),
  ),
  withProps(({ currentUser }) => ({
    canInviteReviewers: get(currentUser, 'permissions.canInviteReviewers'),
    canViewReviewersDetails: get(
      currentUser,
      'permissions.canViewReviewersDetails',
    ),
  })),
)(ReviewerDetails)

// #region styles
const TabButton = styled.div`
  align-items: center;
  border-bottom: ${props =>
    props.selected
      ? `4px solid ${props.theme.colorFurnitureHue}`
      : '4px solid transparent'};
  box-sizing: border-box;
  cursor: pointer;
  display: flex;
  justify-content: center;
  height: calc(${th('gridUnit')} * 5);

  ${marginHelper};

  ${H4} {
    padding: 0 ${th('gridUnit')};
  }
`

const TabsHeader = styled.div`
  align-items: center;
  border-bottom: 1px solid ${th('colorFurniture')};
  background-color: ${th('colorBackgroundHue2')};
  box-sizing: border-box;
  display: flex;
  justify-content: flex-start;

  padding: 0 calc(${th('gridUnit')} * 3);
`

const TabContent = styled.div`
  align-items: stretch;
  background-color: ${th('colorBackgroundHue2')};
  display: flex;
  justify-content: center;
  flex-direction: column;
  min-height: calc(${th('gridUnit')} * 10);
`
// #endregion
