The ReviewerDetails contextual box.

```js
const fragment = {
  invitations: [
    {
      id: 'a69c1273-4073-4529-9e65-5424ad8034ea',
      role: 'reviewer',
      type: 'invitation',
      userId: '9c79c3bf-ccba-4540-aad8-ce4609325826',
      hasAnswer: false,
      invitedOn: 1534506511008,
      isAccepted: false,
      respondedOn: null,
    },
    {
      id: 'ca1c08bb-4da6-4cb4-972d-d16e3b66b884',
      role: 'reviewer',
      type: 'invitation',
      userId: 'd3ab9a0b-d8e8-41e5-ab3b-72b13f84fba1',
      hasAnswer: true,
      invitedOn: 1534506542522,
      isAccepted: true,
      respondedOn: 1534506569565,
    },
  ],
  recommendations: [
    {
      id: '87fb2c45-2685-47cc-9952-d58435ff8f3a',
      userId: 'd3ab9a0b-d8e8-41e5-ab3b-72b13f84fba1',
      comments: [
        {
          files: [],
          public: true,
          content: 'This is a great manuscript',
        },
      ],
      createdOn: 1534506583815,
      updatedOn: 1534506592527,
      submittedOn: 1534506591684,
      recommendation: 'publish',
      recommendationType: 'review',
    },
    {
      id: '9853453f-1049-4369-8543-1b812930430d',
      userId: 'ede6770d-8dbf-4bf9-bbe2-98facfd0a114',
      comments: [
        {
          public: true,
          content: 'nice job',
        },
        {
          public: false,
          content: 'please publish this',
        },
      ],
      createdOn: 1534506624583,
      updatedOn: 1534506624583,
      recommendation: 'publish',
      recommendationType: 'editorRecommendation',
    },
    {
      id: '64c5b596-fbfc-485c-9068-f3a58306efd7',
      userId: '5b53da0d-3f88-4e94-b8f9-7eae6a754168',
      createdOn: 1534506644873,
      updatedOn: 1534506644873,
      recommendation: 'publish',
      recommendationType: 'editorRecommendation',
    },
    {
      id: '3d43ff74-9a20-479d-a218-23bf8eac0b6a',
      userId: '5b53da0d-3f88-4e94-b8f9-7eae6a754168',
      createdOn: 1534506813446,
      updatedOn: 1534506813446,
      recommendation: 'publish',
      recommendationType: 'editorRecommendation',
    },
  ],
}

const invitations = [
  {
    id: '590db43d-f270-4cde-9452-d3cd94a59001',
    role: 'reviewer',
    type: 'invitation',
    userId: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
    hasAnswer: true,
    invitedOn: 1538053490366,
    isAccepted: true,
    respondedOn: 1538053549407,
    person: {
      id: '9ac5b5b5-252c-4933-9e66-72ec7c644a5c',
      type: 'user',
      admin: false,
      email: 'alexandru.munteanu+rev1@thinslices.com',
      teams: [
        '54d5ec8b-4ff0-4483-b725-4e06546cd98b',
        '02ac276c-447d-46fc-949b-a6581856b7fd',
        '2d30f34f-bdb9-4f55-a21f-a1cbcda3453b',
      ],
      isActive: true,
      lastName: 'REV1',
      username: 'alexandru.munteanu+rev1@thinslices.com',
      firstName: 'AlexREV1',
      fragments: [],
      affiliation: 'TSD',
      collections: [],
      isConfirmed: true,
      editorInChief: false,
      notifications: {
        email: {
          user: true,
          system: true,
        },
      },
      handlingEditor: false,
    },
  },
  {
    id: 'fbff261b-6f6e-433f-aaa9-0fa724bc32f3',
    role: 'reviewer',
    type: 'invitation',
    userId: 'afb42359-3ece-42db-b55a-b4d638dd7031',
    hasAnswer: false,
    invitedOn: 1538122139200,
    isAccepted: false,
    respondedOn: null,
    person: {
      id: 'afb42359-3ece-42db-b55a-b4d638dd7031',
      type: 'user',
      admin: false,
      email: 'alexandru.munteanu+rev2@thinslices.com',
      teams: ['02ac276c-447d-46fc-949b-a6581856b7fd'],
      isActive: true,
      lastName: 'REV2',
      username: 'alexandru.munteanu+rev2@thinslices.com',
      firstName: 'alexREv2',
      fragments: [],
      affiliation: 'TSD',
      collections: [],
      isConfirmed: false,
      editorInChief: false,
      notifications: {
        email: {
          user: true,
          system: true,
        },
      },
      handlingEditor: false,
    },
  },
]
;<ReviewerDetails
  fragment={fragment}
  invitations={invitations}
  onInviteReviewer={(reviewer, modalProps) => {
    modalProps.setFetching(true)
  }}
/>
```
