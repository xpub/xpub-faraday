const emailRegex = new RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i, //eslint-disable-line
)

export const emailValidator = value =>
  emailRegex.test(value) ? undefined : 'Invalid email'

export const passwordValidator = values => {
  const errors = {}
  if (!values.password) {
    errors.password = 'Required'
  }
  if (!values.confirmNewPassword) {
    errors.confirmNewPassword = 'Required'
  } else if (values.confirmNewPassword !== values.password) {
    errors.confirmNewPassword = "Passwords don't match."
  }

  return errors
}
