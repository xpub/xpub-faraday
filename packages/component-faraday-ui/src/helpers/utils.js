import { get, chain, find } from 'lodash'

export const handleError = fn => e => {
  fn(get(JSON.parse(e.response), 'error', 'Oops! Something went wrong!'))
}

export const getReportComments = ({ report, isPublic = false }) =>
  chain(report)
    .get('comments', [])
    .find(c => c.public === isPublic)
    .get('content')
    .value()

export const indexReviewers = (reports = [], invitations = []) => {
  reports.forEach(report => {
    report.reviewerNumber = get(
      find(invitations, ['userId', report.userId]),
      'reviewerNumber',
      0,
    )
  })
  return reports
}
