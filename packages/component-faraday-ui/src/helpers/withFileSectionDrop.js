import { DropTarget } from 'react-dnd'

export default DropTarget(
  'item',
  {
    drop(
      {
        files,
        setError,
        changeList,
        listId: toListId,
        allowedFileExtensions,
        maxFiles = Number.MAX_SAFE_INTEGER,
      },
      monitor,
    ) {
      const { listId: fromListId, id, name } = monitor.getItem()
      const fileExtention = name.split('.')[1]

      if (
        allowedFileExtensions &&
        !allowedFileExtensions.includes(fileExtention)
      ) {
        setError('Invalid file type.')
        return
      }

      if (files.length >= maxFiles) {
        setError('No more files can be added to this section.')
        return
      }
      if (toListId === fromListId) return
      changeList(fromListId, toListId, id)
    },
    canDrop({ listId: toListId, setError }, monitor) {
      const { listId: fromListId } = monitor.getItem()
      return toListId !== fromListId
    },
  },
  (connect, monitor) => ({
    isFileItemOver: monitor.isOver(),
    canDropFileItem: monitor.canDrop(),
    connectDropTarget: connect.dropTarget(),
  }),
)
