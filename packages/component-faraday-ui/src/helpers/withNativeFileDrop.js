import { DropTarget } from 'react-dnd'
import { NativeTypes } from 'react-dnd-html5-backend'

export default DropTarget(
  NativeTypes.FILE,
  {
    drop(
      { onFileDrop, files, maxFiles, setError, allowedFileExtensions },
      monitor,
    ) {
      const [file] = monitor.getItem().files
      const fileExtention = file.name.split('.')[1]

      if (files.length >= maxFiles) {
        setError('No more files can be added to this section.')
        return
      }

      if (
        allowedFileExtensions &&
        !allowedFileExtensions.includes(fileExtention)
      ) {
        setError('Invalid file type.')
      }

      typeof onFileDrop === 'function' && onFileDrop(file)
    },
  },
  (connect, monitor) => ({
    isFileOver: monitor.isOver(),
    canDropFile: monitor.canDrop(),
    connectFileDrop: connect.dropTarget(),
  }),
)
