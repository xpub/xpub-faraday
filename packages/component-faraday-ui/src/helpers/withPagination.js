import { compose, withStateHandlers, withProps } from 'recompose'

export default compose(
  withStateHandlers(
    { page: 0, itemsPerPage: 20 },
    {
      toFirst: () => () => ({ page: 0 }),
      nextPage: ({ page, itemsPerPage }, { items }) => () => ({
        page:
          page * itemsPerPage + itemsPerPage < items.length ? page + 1 : page,
      }),
      toLast: ({ itemsPerPage }, { items }) => () => {
        const floor = Math.floor(items.length / itemsPerPage)
        return { page: items.length % itemsPerPage ? floor : floor - 1 }
      },
      prevPage: ({ page }) => () => ({
        page: Math.max(0, page - 1),
      }),
      changeItemsPerPage: ({ itemsPerPage }) => e => ({
        itemsPerPage: Number(e.target.value),
        page: 0,
      }),
    },
  ),
  withProps(({ items = [], itemsPerPage = 1, page = 0 }) => ({
    maxItems: items.length,
    hasMore: itemsPerPage * (page + 1) < items.length,
    paginatedItems: items.slice(page * itemsPerPage, itemsPerPage * (page + 1)),
  })),
)
