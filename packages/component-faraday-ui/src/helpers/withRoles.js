import { get } from 'lodash'
import { withJournal } from 'xpub-journal'
import { compose, withProps } from 'recompose'

export default compose(
  withJournal,
  withProps(({ journal }) => ({
    titles: get(journal, 'title', []),
    roles: Object.entries(get(journal, 'roles', {})).reduce(
      (acc, el) => [
        ...acc,
        {
          value: el[0],
          label: el[1],
        },
      ],
      [],
    ),
  })),
)
