import React from 'react'
import { get } from 'lodash'
import { DateParser } from '@pubsweet/ui'
import { compose, withHandlers } from 'recompose'
import PropTypes from 'prop-types'

import {
  Row,
  Item,
  Text,
  IconButton,
  ActionLink,
  PreviewFile,
  DownloadZipFiles,
  ManuscriptVersion,
} from 'pubsweet-component-faraday-ui'

const ManuscriptDetailsTop = ({
  history,
  versions,
  goToEdit,
  getSignedUrl,
  goToTechnicalCheck,
  fragment,
  collection,
  currentUser: {
    isReviewer,
    token,
    permissions: { canEditManuscript, canOverrideTechChecks },
  },
}) => (
  <Row alignItems="center" mb={1} mt={1}>
    <Item alignItems="center" justify="flex-start">
      <ActionLink
        fontIcon="arrowLeft"
        onClick={() => history.push('/dashboard')}
        paddingBottom={1.2}
      >
        Dashboard
      </ActionLink>
    </Item>
    <Item
      alignItems="baseline"
      data-test-id="item-details-top"
      justify="flex-end"
    >
      {canOverrideTechChecks && (
        <ActionLink
          data-test-id="button-qa-manuscript-technical-checks"
          fontIcon="technicalChecks"
          onClick={goToTechnicalCheck(collection)}
          paddingBottom={1.5}
          paddingRight={1.5}
          pr={2}
        >
          Technical Checks
        </ActionLink>
      )}
      {canEditManuscript && (
        <ActionLink
          data-test-id="button-qa-manuscript-edit"
          fontIcon="dashboardEdit"
          onClick={goToEdit(collection, fragment)}
          paddingBottom={1.5}
          paddingRight={1.5}
          pr={2}
        >
          Edit
        </ActionLink>
      )}
      <PreviewFile
        file={get(fragment, 'files.manuscripts[0]', {})}
        getSignedUrl={getSignedUrl}
      />
      <DownloadZipFiles
        archiveName={`ID-${collection.customId}`}
        collectionId={collection.id}
        fragmentId={fragment.id}
        isReviewer={isReviewer}
        token={token}
      >
        <IconButton fontIcon="downloadZip" mr={2} secondary />
      </DownloadZipFiles>
      <DateParser durationThreshold={0} timestamp={fragment.submitted || ''}>
        {timestamp => (
          <Text mr={1} whiteSpace="nowrap">
            Updated on {timestamp}
          </Text>
        )}
      </DateParser>
      <ManuscriptVersion
        collection={collection}
        data-test-id="versions-dropdown"
        fragment={fragment}
        history={history}
        versions={versions}
      />
    </Item>
  </Row>
)

export default compose(
  withHandlers({
    goToEdit: ({ history }) => (collection, fragment) => () => {
      history.push(
        `/projects/${collection.id}/versions/${fragment.id}/submit`,
        {
          editMode: true,
        },
      )
    },
    goToTechnicalCheck: ({ history, fragment }) => collection => () => {
      const {
        id,
        status,
        customId,
        technicalChecks: { token = '' },
      } = collection
      const stage = status === 'technicalChecks' ? 'eqs' : 'eqa'
      const title = get(fragment, 'metadata.title', '')
      history.push({
        pathname: `/${stage}-decision`,
        search: `?collectionId=${id}&customId=${customId}&token=${token}&title=${title}`,
      })
    },
  }),
)(ManuscriptDetailsTop)

ManuscriptDetailsTop.propTypes = {
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
  /** Object with versions of manuscript. */
  versions: PropTypes.array, //eslint-disable-line
  /** An async call that returns the securized S3 file url. */
  getSignedUrl: PropTypes.func,
  /** An async call that takes you to edit. */
  goToEdit: PropTypes.func,
  /** An async call that takes you to thchnical check. */
  goToTechnicalCheck: PropTypes.func,
  /** Object containing token for current user. */
  currentUser: PropTypes.object, //eslint-disable-line
}

ManuscriptDetailsTop.defaultProps = {
  fragment: {},
  collection: {},
  versions: [],
  getSignedUrl: () => {},
  goToEdit: () => {},
  goToTechnicalCheck: () => {},
  currentUser: {},
}
