import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import { compose } from 'recompose'
import { ManuscriptFileSection } from 'pubsweet-component-faraday-ui'
import { withFilePreview, withFileDownload } from '../'

const ManuscriptFileList = ({
  files: { manuscripts = [], coverLetter = [], supplementary = [] },
  previewFile,
  downloadFile,
  ...rest
}) => (
  <Fragment>
    <ManuscriptFileSection
      label="MAIN MANUSCRIPT"
      list={manuscripts}
      onDownload={downloadFile}
      onPreview={previewFile}
      {...rest}
    />

    <ManuscriptFileSection
      label="COVER LETTER"
      list={coverLetter}
      onDownload={downloadFile}
      onPreview={previewFile}
      {...rest}
    />

    <ManuscriptFileSection
      label="SUPPLEMENTARY FILES"
      list={supplementary}
      onDownload={downloadFile}
      onPreview={previewFile}
      {...rest}
    />
  </Fragment>
)
ManuscriptFileList.propTypes = {
  /** Files that are uploaded by author. */
  files: PropTypes.shape({
    coverLetter: PropTypes.arrayOf(PropTypes.object),
    manuscripts: PropTypes.arrayOf(PropTypes.object),
    supplementary: PropTypes.arrayOf(PropTypes.object),
  }),
  /** Callback function fired when delete icon is pressed. */
  onDelete: PropTypes.func, // eslint-disable-line
  /** Callback function fired when download icon is pressed. */
  onDownload: PropTypes.func, // eslint-disable-line
  /** Callback function fired when preview icon is pressed. */
  onPreview: PropTypes.func, // eslint-disable-line
}

ManuscriptFileList.defaultProps = {
  files: {},
}

export default compose(withFilePreview, withFileDownload)(ManuscriptFileList)
