import PropTypes from 'prop-types'
import { withProps } from 'recompose'
import { isEmpty, get } from 'lodash'
import React, { Fragment } from 'react'

import {
  Text,
  Item,
  Row,
  ContextualBox,
  ManuscriptFileList,
} from 'pubsweet-component-faraday-ui'

const ManuscriptMetadata = ({
  filesLabel,
  getSignedUrl,
  currentUser: { token },
  fragment: { files = {}, conflicts = {}, metadata: { abstract = '' } },
  abstractMetadataExpanded,
  toggleAbstractMetadata,
  toggleConflictsOfInterest,
  conflictsOfInterestExpanded,
  filesMetadataExpanded,
  toggleFilesMetadata,
}) => (
  <Fragment>
    {!!abstract && (
      <Item mb={1}>
        <ContextualBox
          data-test-id="abstract-tab"
          expanded={abstractMetadataExpanded}
          label="Abstract"
          startExpanded
          toggle={toggleAbstractMetadata}
          transparent
        >
          <Text mb={1} mt={1}>
            {abstract}
          </Text>
        </ContextualBox>
      </Item>
    )}
    {conflicts.hasConflicts === 'yes' && (
      <Item mb={1}>
        <ContextualBox
          data-test-id="conflict-of-interest-tab"
          expanded={conflictsOfInterestExpanded}
          label="Conflict of Interest"
          toggle={toggleConflictsOfInterest}
          transparent
        >
          <Row
            alignItems="center"
            data-test-id="conflict-of-interest-text"
            justify="flex-start"
          >
            <Text mb={1} mt={1}>
              Conflicts of interest:
            </Text>
            <Text ml={1 / 2}>{get(conflicts, 'message', '')}</Text>
          </Row>
          {get(conflicts, 'dataAvailabilityMessage', '') && (
            <Row
              alignItems="center"
              data-test-id="data-availability-message"
              justify="flex-start"
            >
              <Text mb={1} mt={1}>
                Data availability statment:
              </Text>
              <Text ml={1 / 2}>
                {get(conflicts, 'dataAvailabilityMessage', '')}
              </Text>
            </Row>
          )}
          {get(conflicts, 'fundingMessage', '') && (
            <Row
              alignItems="center"
              data-test-id="funding-message"
              justify="flex-start"
            >
              <Text mb={1} mt={1}>
                Funding statment:
              </Text>
              <Text ml={1 / 2}>{get(conflicts, 'fundingMessage', '')}</Text>
            </Row>
          )}
        </ContextualBox>
      </Item>
    )}
    {!isEmpty(files) && (
      <Item mb={1}>
        <ContextualBox
          data-test-id="files-tab"
          expanded={filesMetadataExpanded}
          label={filesLabel}
          toggle={toggleFilesMetadata}
          transparent
        >
          <ManuscriptFileList
            files={files}
            getSignedUrl={getSignedUrl}
            token={token}
          />
        </ContextualBox>
      </Item>
    )}
  </Fragment>
)

ManuscriptMetadata.propTypes = {
  /** Files Label of the contextual box. */
  filesLabel: PropTypes.string,
  /** An async call that returns the securized S3 file url. */
  getSignedUrl: PropTypes.func,
  /** Object containing token for current user. */
  currentUser: PropTypes.object, //eslint-disable-line
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
}
ManuscriptMetadata.defaultProps = {
  filesLabel: '',
  getSignedUrl: () => {},
  currentUser: {},
  fragment: {},
}

export default withProps(({ fragment: { files } }) => ({
  filesLabel: `Files (${get(files, 'manuscripts', []).length +
    get(files, 'coverLetter', []).length +
    get(files, 'supplementary', []).length})`,
}))(ManuscriptMetadata)
