import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { Menu } from '@pubsweet/ui'

const ManuscriptVersion = ({
  history,
  versions = [],
  fragment = {},
  collection = {},
}) =>
  versions.length > 0 && (
    <Menu
      inline
      onChange={v =>
        history.push(`/projects/${collection.id}/versions/${v}/details`)
      }
      options={versions}
      placeholder="Please select"
      value={get(fragment, 'id')}
    />
  )

ManuscriptVersion.propTypes = {
  /** Object with versions of manuscript. */
  versions: PropTypes.array, //eslint-disable-line
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
}

ManuscriptVersion.defaultProps = {
  versions: [],
  fragment: {},
  collection: {},
}
export default ManuscriptVersion
