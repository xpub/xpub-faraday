import React from 'react'
import PropTypes from 'prop-types'
import { reduxForm } from 'redux-form'
import { required } from 'xpub-validators'
import { get, has, capitalize } from 'lodash'
import { compose, withProps } from 'recompose'
import { Button, RadioGroup, ValidatedField } from '@pubsweet/ui'
import { withModal } from 'pubsweet-component-modal/src/components'

import {
  Row,
  Item,
  Text,
  Label,
  Textarea,
  MultiAction,
  ContextualBox,
  RowOverrideAlert,
  withFetching,
} from '../'

const options = [
  { label: 'Accept', value: 'accept' },
  { label: 'Decline', value: 'decline' },
]

const ResponseToInvitation = ({
  label,
  title,
  toggle,
  expanded,
  isFetching,
  handleSubmit,
  onSubmitForm,
  shouldShowComments,
  buttonLabel = 'RESPOND TO INVITATION',
}) => (
  <ContextualBox
    data-test-id="respond-to-invitation-he-box"
    expanded={expanded}
    highlight
    label={title}
    mb={2}
    scrollIntoView
    toggle={toggle}
  >
    <RowOverrideAlert justify="flex-start" ml={1} mt={1}>
      <Item data-test-id="respond-to-editorial-invitation" vertical>
        <Label required>{label}</Label>
        <ValidatedField
          component={input => (
            <Row
              alignItems="center"
              data-test-id="radio-respond-to-invitation"
              justify="space-between"
            >
              <RadioGroup inline name="decision" options={options} {...input} />
              <Button
                mb={1.7}
                mr={2.6}
                onClick={handleSubmit}
                primary
                size="medium"
              >
                {buttonLabel}
              </Button>
            </Row>
          )}
          name="decision"
          validate={[required]}
        />
      </Item>
    </RowOverrideAlert>

    {shouldShowComments && (
      <RowOverrideAlert ml={1} mt={2} pr={2}>
        <Item vertical>
          <Label>
            Decline Reason{' '}
            <Text secondary small>
              Optional
            </Text>
          </Label>
          <ValidatedField component={Textarea} name="reason" />
        </Item>
      </RowOverrideAlert>
    )}
  </ContextualBox>
)

ResponseToInvitation.propTypes = {
  /** Label of the contextual box. */
  label: PropTypes.string,
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Expands a message for collapse or expand. */
  expanded: PropTypes.bool,
  /** Method used to toggle a message on click. */
  toggle: PropTypes.func,
  /* Handles the submission of the recommendation. */
  handleSubmit: PropTypes.func,
  /** Specifies if it will show comments on the current version. */
  shouldShowComments: PropTypes.bool,
  /** Specifies the Label name for the respond to invitation button. */
  buttonLabel: PropTypes.string,
}
ResponseToInvitation.defaultProps = {
  label: '',
  title: '',
  toggle: () => {},
  expanded: false,
  handleSubmit: () => {},
  shouldShowComments: false,
  buttonLabel: 'RESPOND TO INVITATION',
}

export default compose(
  withFetching,
  withModal(({ isFetching, modalKey }) => ({
    modalKey,
    isFetching,
    modalComponent: MultiAction,
  })),
  withProps(({ formValues = {}, commentsOn }) => ({
    disabled: !has(formValues, 'decision'),
    shouldShowComments: get(formValues, 'decision', 'agree') === commentsOn,
  })),
  reduxForm({
    form: 'answer-invitation',
    onSubmit: (values, dispatch, { showModal, onResponse, setFetching }) => {
      showModal({
        title: `${capitalize(values.decision)} this invitation?`,
        onConfirm: modalProps => {
          onResponse(values, { ...modalProps, setFetching })
        },
      })
    },
  }),
)(ResponseToInvitation)
