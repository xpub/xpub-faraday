import React, { Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { H2, Button, Spinner } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers } from 'recompose'
import PropTypes from 'prop-types'

import { IconButton, Text, Row } from '../'

const MultiAction = ({
  title,
  content,
  onClose,
  subtitle,
  onConfirm,
  modalError,
  isFetching,
  renderContent,
  confirmText,
  cancelText,
}) => (
  <Root>
    <IconButton icon="x" onClick={onClose} right={5} secondary top={5} />
    <H2>{title}</H2>
    {subtitle && (
      <Text mb={1} secondary>
        {subtitle}
      </Text>
    )}
    {renderContent()}
    {modalError && (
      <Text align="center" error mt={1}>
        {modalError}
      </Text>
    )}
    <Row mt={1}>
      {isFetching ? (
        <Spinner size={3} />
      ) : (
        <Fragment>
          <Button data-test-id="modal-cancel" onClick={onClose}>
            {cancelText}
          </Button>
          <Button data-test-id="modal-confirm" onClick={onConfirm} primary>
            {confirmText}
          </Button>
        </Fragment>
      )}
    </Row>
  </Root>
)

MultiAction.propTypes = {
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Subtitle that will be showed on the card. */
  subtitle: PropTypes.string,
  /** The text you want to see on the button when someone submit the report. */
  confirmText: PropTypes.string,
  /** The text you want to see on the button when someone cancel the report. */
  cancelText: PropTypes.string,
  /** Callback function fired when cancel confirmation card. */
  onCancel: PropTypes.func, // eslint-disable-line
  /** Callback function fired when confirm confirmation card. */
  onConfirm: PropTypes.func,
  /** When is true will show a spinner.  */
  onClose: PropTypes.func,
  /** Callback function fired when you want to close the card. */
  isFetching: PropTypes.bool,
  /** The component you want to show on the card. */
  content: PropTypes.func,
}
MultiAction.defaultProps = {
  title: undefined,
  subtitle: undefined,
  confirmText: 'OK',
  cancelText: 'Cancel',
  onCancel: undefined,
  onConfirm: undefined,
  onClose: undefined,
  isFetching: undefined,
  content: undefined,
}

export default compose(
  withHandlers({
    onConfirm: ({ onConfirm, ...props }) => () => {
      if (typeof onConfirm === 'function') {
        onConfirm(props)
      }
    },
    onClose: ({ onCancel, ...props }) => () => {
      if (typeof onCancel === 'function') {
        onCancel(props)
      }
      props.hideModal()
    },
    renderContent: ({ content, ...props }) => () => {
      if (!content) return null
      if (typeof content === 'object') {
        return content
      } else if (typeof content === 'function') {
        return content(props)
      }
      return <Text dangerouslySetInnerHTML={{ __html: content }} mb={1} />
    },
  }),
  setDisplayName('MultiActionModal'),
)(MultiAction)

// #region styles
const Root = styled.div`
  align-items: center;
  background: ${th('colorBackground')};
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 5);
  width: calc(${th('gridUnit')} * 70);

  ${H2} {
    margin: 0 0 ${th('gridUnit')} 0;
    text-align: center;
  }

  ${Text} {
    margin-bottom: ${th('gridUnit')};
  }
`
// #endregion
