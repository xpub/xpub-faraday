import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H2 } from '@pubsweet/ui'
import { compose, setDisplayName, withHandlers } from 'recompose'
import PropTypes from 'prop-types'

import { IconButton, Text } from '../'

const SingleActionModal = ({
  error,
  title,
  content,
  onClick,
  subtitle,
  confirmText,
  onConfirm,
}) => (
  <Root>
    <IconButton icon="x" onClick={onClick} right={5} secondary top={5} />
    <IconButton
      error={error}
      fontIcon={error ? 'removeIcon' : 'saveIcon'}
      primary={!error}
      size={2}
    />
    {title && <H2>{title}</H2>}
    {subtitle && <Text secondary>{subtitle}</Text>}
    <Button data-test-id="modal-confirm" onClick={onClick} primary>
      {confirmText}
    </Button>
  </Root>
)

export default compose(
  withHandlers({
    onClick: ({ onCancel, onConfirm, hideModal }) => {
      typeof onCancel === 'function' && onCancel()
      typeof onConfirm === 'function' && onConfirm()
      hideModal()
    },
  }),
  setDisplayName('SingleActionModal'),
)(SingleActionModal)

// #region styles
const Root = styled.div`
  align-items: center;
  border: ${th('borderWidth')} ${th('borderStyle')} transparent;
  border-radius: ${th('borderRadius')};
  background: ${th('colorBackground')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  position: relative;
  padding: calc(${th('gridUnit')} * 5);
  width: calc(${th('gridUnit')} * 40);

  ${H2} {
    margin: calc(${th('gridUnit')} * 2) 0;
  }

  ${Text} {
    margin-bottom: ${th('gridUnit')};
  }

  ${Button} {
    margin-top: ${th('gridUnit')};
  }
`

SingleActionModal.propTypes = {
  /** Title that will be showed on the card. */
  title: PropTypes.string,
  /** Subtitle that will be showed on the card. */
  subtitle: PropTypes.string,
  /** Callback function fired when confirm confirmation card. */
  onConfirm: PropTypes.func,
  /** The text you want to see on the button when someone submit the report. */
  confirmText: PropTypes.string,
  /** If true success icon is replaced with error icon.  */
  error: PropTypes.bool,
}

SingleActionModal.defaultProps = {
  title: undefined,
  subtitle: undefined,
  onConfirm: undefined,
  confirmText: 'OK',
  error: undefined,
}
// #endregion
