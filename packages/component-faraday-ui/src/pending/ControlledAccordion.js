import React from 'react'
import { Icon } from '@pubsweet/ui'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'

import { marginHelper } from '../'

const HeaderComponent = ({ icon, label, toggle, expanded, ...props }) => (
  <Header expanded={expanded} onClick={toggle} {...props}>
    <HeaderIcon expanded={expanded}>
      <Icon primary size={3}>
        {icon}
      </Icon>
    </HeaderIcon>
    <HeaderLabel>{label}</HeaderLabel>
  </Header>
)

class ControlledAccordion extends React.Component {
  componentDidMount() {
    this.scroller = document.querySelector('.faraday-root')
  }
  componentDidUpdate(prevProps) {
    const shouldScroll = !prevProps.expanded && this.props.expanded

    if (this.props.scrollIntoView && shouldScroll) {
      const appBarHeight = 70
      const appBarMargin = 16
      this.scroller.scrollTop =
        this._accordion.offsetTop - appBarHeight - appBarMargin
    }
  }

  _accordion = null

  render() {
    const {
      expanded,
      children,
      icon = 'chevron_up',
      header: Header = HeaderComponent,
      ...rest
    } = this.props
    return (
      <Root expanded={expanded} ref={r => (this._accordion = r)} {...rest}>
        <Header expanded={expanded} icon={icon} {...rest} />
        {expanded && children}
      </Root>
    )
  }
}

export default ControlledAccordion

// #region styles
const Root = styled.div`
  display: flex;
  flex-direction: column;
  transition: all ${th('transitionDuration')};

  ${marginHelper};
  ${override('ui.Accordion')};
`

const Header = styled.div.attrs(props => ({
  'data-test-id': props['data-test-id'] || 'accordion-header',
}))`
  align-items: center;
  cursor: pointer;
  display: flex;
  justify-content: flex-start;

  ${override('ui.Accordion.Header')};
`

const HeaderLabel = styled.span`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeBase')};

  ${override('ui.Accordion.Header.Label')};
`

const HeaderIcon = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;

  transform: ${({ expanded }) => `rotateZ(${expanded ? 0 : 180}deg)`};
  transition: transform ${th('transitionDuration')};

  ${override('ui.Accordion.Header.Icon')};
`
// #endregion
