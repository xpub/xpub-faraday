import { get, has } from 'lodash'
import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

export const marginHelper = props => {
  const marginTop = css`
    margin-top: calc(${th('gridUnit')} * ${get(props, 'mt', 0)});
  `

  const marginRight = props.center
    ? css`
        margin-right: auto;
      `
    : css`
        margin-right: calc(${th('gridUnit')} * ${get(props, 'mr', 0)});
      `

  const marginBottom = css`
    margin-bottom: calc(${th('gridUnit')} * ${get(props, 'mb', 0)});
  `
  const marginLeft = props.center
    ? css`
        margin-left: auto;
      `
    : css`
        margin-left: calc(${th('gridUnit')} * ${get(props, 'ml', 0)});
      `

  return css`
    ${marginTop};
    ${marginRight};
    ${marginBottom};
    ${marginLeft};
  `
}

export const paddingHelper = props => {
  const paddingTop = css`
    padding-top: calc(${th('gridUnit')} * ${get(props, 'pt', 0)});
  `

  const paddingRight = css`
    padding-right: calc(${th('gridUnit')} * ${get(props, 'pr', 0)});
  `

  const paddingBottom = css`
    padding-bottom: calc(${th('gridUnit')} * ${get(props, 'pb', 0)});
  `
  const paddingLeft = css`
    padding-left: calc(${th('gridUnit')} * ${get(props, 'pl', 0)});
  `

  return css`
    ${paddingTop};
    ${paddingRight};
    ${paddingBottom};
    ${paddingLeft};
  `
}

export const positionHelper = css`
  position: ${props =>
    has(props, 'top', 'bottom', 'left', 'right') ? 'absolute' : 'initial'};
  top: ${props => (has(props, 'top') ? `${get(props, 'top')}px` : 'unset')};
  bottom: ${props =>
    has(props, 'bottom') ? `${get(props, 'bottom')}px` : 'unset'};
  left: ${props => (has(props, 'left') ? `${get(props, 'left')}px` : 'unset')};
  right: ${props =>
    has(props, 'right') ? `${get(props, 'right')}px` : 'unset'};
`

export const displayHelper = css`
  display: ${({ display }) => display || 'initial'};
`

export const radiusHelpers = props => {
  const borderTop = props.isFirst
    ? css`
        border-top-left-radius: ${th('borderRadius')};
        border-top-right-radius: ${th('borderRadius')};
      `
    : css``

  const borderBottom = props.isLast
    ? css`
        border-bottom-left-radius: ${th('borderRadius')};
        border-bottom-right-radius: ${th('borderRadius')};
      `
    : css``

  return css`
    border-radius: none;
    ${borderTop};
    ${borderBottom};
  `
}

export const heightHelper = props =>
  has(props, 'height')
    ? css`
        height: calc(${th('gridUnit')} * ${get(props, 'height', 2)});
      `
    : css`
        height: auto;
      `
