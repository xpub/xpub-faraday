import React from 'react'
import { get, has } from 'lodash'
import { Field } from 'redux-form'
import PropTypes from 'prop-types'
import { Icon } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { ContextualBox, Row, Text, WizardFiles, IconButton } from '../'

const Empty = () => <div />

const ManuscriptFiles = ({
  token,
  fragment,
  formName,
  collection,
  changeForm,
  deleteFile,
  uploadFile,
  filesError,
  previewFile,
  getSignedUrl,
  formErrors,
}) => (
  <ContextualBox
    data-test-id="manuscript-files"
    label="Manuscript Files"
    startExpanded
    transparent
  >
    <Root>
      <Row justify="flex-start" mb={2}>
        <Text secondary>
          Drag & drop files in the specific section or click{' '}
          <CustomIcon secondary size={2}>
            plus
          </CustomIcon>{' '}
          to upload. Use the{' '}
          <CustomIconButton fontIcon="moveIcon" noHover secondary size={2} />{' '}
          icon to reorder or move files to a different type.
        </Text>
      </Row>
      <Field component={Empty} name="files" />
      <WizardFiles
        changeForm={changeForm}
        collection={collection}
        deleteFile={deleteFile}
        files={get(fragment, 'revision.files', {})}
        formName={formName}
        fragment={fragment}
        getSignedUrl={getSignedUrl}
        previewFile={previewFile}
        token={token}
        uploadFile={uploadFile}
      />
      {has(formErrors, 'files') && (
        <Row justify="flex-start" mt={1}>
          <Text error>{get(formErrors, 'files', '')}</Text>
        </Row>
      )}
    </Root>
  </ContextualBox>
)

ManuscriptFiles.propTypes = {
  /** Contains the token of the currently logged user. */
  token: PropTypes.string,
  /** Object containing the selected fragment. */
  fragment: PropTypes.object, //eslint-disable-line
  /** Object containing the selected collection. */
  collection: PropTypes.object, //eslint-disable-line
  /** Name of added form. */
  formName: PropTypes.func,
  /** Change added form. */
  changeForm: PropTypes.func,
  /** Removes the file from the server. */
  deleteFile: PropTypes.func,
  /** Uploads the file to the server. */
  uploadFile: PropTypes.func,
  /** View content of the uploaded file. */
  previewFile: PropTypes.func,
  /** An async call that returns the securized S3 file url. */
  getSignedUrl: PropTypes.func,
  /** Value representing if the form has any errors. */
  formErrors: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
}

ManuscriptFiles.defaultProps = {
  token: '',
  fragment: {},
  collection: {},
  formName: () => {},
  changeForm: () => {},
  deleteFile: () => {},
  uploadFile: () => {},
  previewFile: () => {},
  getSignedUrl: () => {},
  formErrors: {},
}

export default ManuscriptFiles

// #region styled-components
const Root = styled.div`
  border-left: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding-left: calc(${th('gridUnit')} * 1);
`

const CustomIcon = styled(Icon)`
  vertical-align: sub;
`
const CustomIconButton = styled(IconButton)`
  vertical-align: sub;
  cursor: default;
  display: inline-flex;
`
// #endregion
