import React from 'react'
import { isEmpty } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { FilePicker, Spinner, ValidatedField } from '@pubsweet/ui'

import {
  Row,
  Item,
  Label,
  FileItem,
  Textarea,
  ActionLink,
  ContextualBox,
  ItemOverrideAlert,
  withFilePreview,
  withFileDownload,
} from '../'

const allowedFileExtensions = ['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']
const ResponseToReviewer = ({
  file,
  token,
  onDelete,
  onUpload,
  isFetching,
  previewFile,
  downloadFile,
}) => (
  <ContextualBox
    data-test-id="response-to-reviewer-comments"
    label="Response to Reviewer Comments"
    startExpanded
    transparent
  >
    <Root>
      <Row alignItems="center" justify="space-between">
        <Item>
          <Label required>Your Reply</Label>
          {isFetching ? (
            <Spinner />
          ) : (
            <FilePicker
              allowedFileExtensions={allowedFileExtensions}
              data-test-id="submit-revision-reply-file"
              disabled={!isEmpty(file)}
              onUpload={onUpload}
            >
              <ActionLink disabled={!isEmpty(file)} icon="plus">
                UPLOAD FILE
              </ActionLink>
            </FilePicker>
          )}
        </Item>
      </Row>

      <Row>
        <ItemOverrideAlert vertical>
          <ValidatedField
            component={Textarea}
            name="responseToReviewers.content"
          />
        </ItemOverrideAlert>
      </Row>

      {!isEmpty(file) && (
        <Row justify="flex-start" mt={1}>
          <FileItem
            item={file}
            onDelete={onDelete}
            onDownload={downloadFile}
            onPreview={previewFile}
          />
        </Row>
      )}
    </Root>
  </ContextualBox>
)

ResponseToReviewer.propTypes = {
  /** Deletes the file from the server then updates the form. */
  onDelete: PropTypes.func,
  /** Uploads the file then updates the form. */
  onUpload: PropTypes.func,
  /** View content of the uploaded file. */
  previewFile: PropTypes.func,
  /** Downloads the file from the server. */
  downloadFile: PropTypes.func,
}
ResponseToReviewer.defaultProps = {
  onDelete: () => {},
  onUpload: () => {},
  previewFile: () => {},
  downloadFile: () => {},
}

const Root = styled.div`
  border-left: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  padding-left: calc(${th('gridUnit')} * 1);
`

export default withFileDownload(withFilePreview(ResponseToReviewer))
