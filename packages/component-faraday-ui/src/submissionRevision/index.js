export { default as ResponseToReviewer } from './ResponseToReviewer'
export { default as ManuscriptFiles } from './ManuscriptFiles'
export { default as DetailsAndAuthors } from './DetailsAndAuthors'
export { default as SubmitRevision } from './SubmitRevision'
