const fixtures = require('./fixtures/fixtures')
const Model = require('./helpers/Model')

module.exports = {
  fixtures,
  Model,
}
