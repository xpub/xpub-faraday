const Chance = require('chance')

const chance = new Chance()

module.exports = {
  standardCollID: chance.guid(),
  collectionNoInvitesID: chance.guid(),
  twoVersionsCollectionId: chance.guid(),
  minorRevisionCollectionID: chance.guid(),
  majorRevisionCollectionID: chance.guid(),
  collectionReviewCompletedID: chance.guid(),
  oneReviewedFragmentCollectionID: chance.guid(),
  noEditorRecomedationCollectionID: chance.guid(),
  minorRevisionWithoutReviewCollectionID: chance.guid(),
  majorRevisionWithoutReviewCollectionID: chance.guid(),
}
