const Chance = require('chance')

const chance = new Chance()

module.exports = {
  heTeamID: chance.guid(),
  revTeamID: chance.guid(),
  rev1TeamID: chance.guid(),
  authorTeamID: chance.guid(),
  majorRevisionHeTeamID: chance.guid(),
  revRecommendationTeamID: chance.guid(),
  heNoRecommendationTeamID: chance.guid(),
  revNoEditorRecommendationTeamID: chance.guid(),
}
