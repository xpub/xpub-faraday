const Chance = require('chance')

const chance = new Chance()
const generateUserData = () => ({
  id: chance.guid(),
  email: chance.email(),
  firstName: chance.first(),
  lastName: chance.last(),
})

module.exports = {
  handlingEditor: generateUserData(),
  noRecommendationHE: generateUserData(),
  user: generateUserData(),
  admin: generateUserData(),
  author: generateUserData(),
  reviewer: generateUserData(),
  reviewer1: generateUserData(),
  newReviewer: generateUserData(),
  answerReviewer: generateUserData(),
  submittingAuthor: generateUserData(),
  recReviewer: generateUserData(),
  answerHE: generateUserData(),
  inactiveReviewer: generateUserData(),
  inactiveUser: generateUserData(),
  editorInChief: generateUserData(),
}
