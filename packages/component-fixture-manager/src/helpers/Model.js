const UserMock = require('../mocks/User')
const TeamMock = require('../mocks/Team')
const FragmentMock = require('../mocks/Fragment')

const notFoundError = new Error()
notFoundError.name = 'NotFoundError'
notFoundError.status = 404

const build = fixtures => {
  const models = {
    User: {},
    Collection: {
      find: jest.fn(id => findMock(id, 'collections', fixtures)),
      all: jest.fn(() => Object.values(fixtures.collections)),
    },
    Team: {},
    Fragment: {},
  }

  UserMock.find = jest.fn(id => findMock(id, 'users', fixtures))
  UserMock.findByEmail = jest.fn(email => findByEmailMock(email, fixtures))
  UserMock.all = jest.fn(() => Object.values(fixtures.users))
  UserMock.findOneByField = jest.fn((field, value) =>
    findOneByFieldMock(field, value, 'users', fixtures),
  )
  UserMock.updateProperties = jest.fn(user =>
    updatePropertiesMock(user, 'users'),
  )
  UserMock.update = jest.fn((id, input) => input)
  UserMock.fetchOne = jest.fn(user => user)
  UserMock.create = jest.fn(input => input)

  TeamMock.find = jest.fn(id => findMock(id, 'teams', fixtures))
  TeamMock.updateProperties = jest.fn(team =>
    updatePropertiesMock(team, 'teams', fixtures),
  )
  TeamMock.all = jest.fn(() => Object.values(fixtures.teams))

  FragmentMock.find = jest.fn(id => findMock(id, 'fragments', fixtures))

  models.User = UserMock
  models.Team = TeamMock
  models.Fragment = FragmentMock

  return models
}

const findMock = (id, type, fixtures) => {
  const foundObj = Object.values(fixtures[type]).find(
    fixtureObj => fixtureObj.id === id,
  )

  if (!foundObj) return Promise.reject(notFoundError)
  return Promise.resolve(foundObj)
}

const findByEmailMock = (email, fixtures) => {
  const foundUser = Object.values(fixtures.users).find(
    fixtureUser => fixtureUser.email === email,
  )

  if (foundUser === undefined) return Promise.reject(notFoundError)
  return Promise.resolve(foundUser)
}

const updatePropertiesMock = (obj, type, fixtures) => {
  const foundObj = Object.values(fixtures[type]).find(
    fixtureObj => fixtureObj === obj,
  )

  if (foundObj === undefined) return Promise.reject(notFoundError)
  return Promise.resolve(foundObj)
}

const findOneByFieldMock = (field, value, type, fixtures) => {
  const foundObj = Object.values(fixtures[type]).find(
    fixtureObj => fixtureObj[field] === value,
  )
  if (foundObj === undefined) return Promise.reject(notFoundError)
  return Promise.resolve(foundObj)
}
module.exports = { build }
