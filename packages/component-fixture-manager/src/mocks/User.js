/* eslint-disable func-names-any */
const { reviewer } = require('../fixtures/users')

function User(properties) {
  this.type = 'user'
  this.email = properties.email
  this.username = properties.username
  this.password = properties.password
  this.roles = properties.roles
  this.title = properties.title
  this.affiliation = properties.affiliation
  this.firstName = properties.firstName
  this.lastName = properties.lastName
  this.admin = properties.admin
  this.isActive = true
  this.notifications = {
    email: {
      user: true,
      system: true,
    },
  }
}

User.prototype.save = jest.fn(function saveUser() {
  this.id = reviewer.id
  return Promise.resolve(this)
})

module.exports = User
