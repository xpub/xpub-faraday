const uuid = require('uuid')
const logger = require('@pubsweet/logger')

class Invitation {
  constructor({ userId, role, invitation }) {
    this.userId = userId
    this.role = role
    this.invitation = invitation
  }

  set _userId(newUserId) {
    this.userId = newUserId
  }

  getInvitationsData({ invitations = [] }) {
    const { userId, role } = this
    const matchingInvitation = invitations.find(
      inv => inv.role === role && inv.userId === userId,
    )
    if (!matchingInvitation) {
      logger.error(
        `There should be at least one matching invitation between User ${userId} and Role ${role}`,
      )
      throw Error('no matching invitation')
    }
    let status = 'pending'
    if (matchingInvitation.isAccepted) {
      status = 'accepted'
    } else if (matchingInvitation.hasAnswer) {
      status = 'declined'
    }

    const { invitedOn, respondedOn, id } = matchingInvitation
    return { invitedOn, respondedOn, status, id }
  }

  async createInvitation({ parentObject }) {
    const { userId, role } = this
    const invitation = {
      role,
      hasAnswer: false,
      isAccepted: false,
      invitedOn: Date.now(),
      id: uuid.v4(),
      userId,
      respondedOn: null,
      type: 'invitation',
    }
    parentObject.invitations = parentObject.invitations || []
    parentObject.invitations.push(invitation)
    await parentObject.save()

    return invitation
  }

  getInvitation({ invitations = [] }) {
    return invitations.find(
      invitation =>
        invitation.userId === this.userId && invitation.role === this.role,
    )
  }

  validateInvitation() {
    const { invitation } = this

    if (invitation === undefined)
      return { status: 404, error: 'Invitation not found.' }

    if (invitation.hasAnswer)
      return { status: 400, error: 'Invitation has already been answered.' }

    return { error: null }
  }
}

module.exports = Invitation
