const logger = require('@pubsweet/logger')
const get = require('lodash/get')

class Team {
  constructor({ TeamModel = {}, fragmentId = '', collectionId = '' }) {
    this.TeamModel = TeamModel
    this.fragmentId = fragmentId
    this.collectionId = collectionId
  }

  async createTeam({ role = '', members = [], objectType = '' }) {
    const { fragmentId, TeamModel, collectionId } = this
    const objectId = objectType === 'collection' ? collectionId : fragmentId

    let permissions, group, name
    switch (role) {
      case 'handlingEditor':
        permissions = 'handlingEditor'
        group = 'handlingEditor'
        name = 'Handling Editor'
        break
      case 'reviewer':
        permissions = 'reviewer'
        group = 'reviewer'
        name = 'Reviewer'
        break
      case 'author':
        permissions = 'author'
        group = 'author'
        name = 'author'
        break
      default:
        break
    }

    const teamBody = {
      teamType: {
        name: role,
        permissions,
      },
      group,
      name,
      object: {
        type: objectType,
        id: objectId,
      },
      members,
    }
    let team = new TeamModel(teamBody)
    team = await team.save()
    return team
  }

  async setupTeam({ user, role, objectType }) {
    const { TeamModel, fragmentId, collectionId } = this
    const objectId = objectType === 'collection' ? collectionId : fragmentId
    const teams = await TeamModel.all()
    user.teams = user.teams || []
    let foundTeam = teams.find(
      team =>
        team.group === role &&
        team.object.type === objectType &&
        team.object.id === objectId,
    )

    if (foundTeam !== undefined) {
      if (!foundTeam.members.includes(user.id)) {
        foundTeam.members.push(user.id)
      }

      try {
        foundTeam = await foundTeam.save()
        if (!user.teams.includes(foundTeam.id)) {
          user.teams.push(foundTeam.id)
          await user.save()
        }
        return foundTeam
      } catch (e) {
        logger.error(e)
      }
    } else {
      const team = await this.createTeam({
        role,
        members: [user.id],
        objectType,
      })
      user.teams.push(team.id)
      await user.save()
      return team
    }
  }

  async removeTeamMember({ teamId, userId }) {
    const { TeamModel } = this
    const team = await TeamModel.find(teamId)
    const members = team.members.filter(member => member !== userId)
    team.members = members
    await team.save()
  }

  async getTeamMembers({ role, objectType }) {
    const { TeamModel, collectionId, fragmentId } = this
    const objectId = objectType === 'collection' ? collectionId : fragmentId

    const teams = await TeamModel.all()

    const members = get(
      teams.find(
        team =>
          team.group === role &&
          team.object.type === objectType &&
          team.object.id === objectId,
      ),
      'members',
      [],
    )

    return members
  }

  async getTeam({ role, objectType }) {
    const { TeamModel, fragmentId, collectionId } = this
    const objectId = objectType === 'collection' ? collectionId : fragmentId
    const teams = await TeamModel.all()
    return teams.find(
      team =>
        team.group === role &&
        team.object.type === objectType &&
        team.object.id === objectId,
    )
  }

  async getTeams(objectType) {
    const { TeamModel, fragmentId, collectionId } = this
    const objectId = objectType === 'collection' ? collectionId : fragmentId
    const teams = await TeamModel.all()
    return teams.filter(
      team => team.object.type === objectType && team.object.id === objectId,
    )
  }

  async deleteHandlingEditor({ collection, role, user }) {
    const team = await this.getTeam({
      role,
      objectType: 'collection',
    })
    delete collection.handlingEditor
    await this.removeTeamMember({ teamId: team.id, userId: user.id })
    user.teams = user.teams.filter(userTeamId => team.id !== userTeamId)
    await user.save()
    await collection.save()
  }
}

module.exports = Team
