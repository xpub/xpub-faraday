const uuid = require('uuid')
const { get } = require('lodash')
const services = require('./services')

class User {
  constructor({ UserModel = {} }) {
    this.UserModel = UserModel
  }

  async createUser({ role, body }) {
    const { UserModel } = this
    const {
      email,
      firstName,
      lastName,
      affiliation,
      title = 'Dr',
      country,
    } = body
    const username = email
    const password = uuid.v4()
    const userBody = {
      email,
      title,
      country,
      username,
      lastName,
      password,
      firstName,
      affiliation,
      isActive: true,
      isConfirmed: false,
      admin: role === 'admin',
      editorInChief: role === 'editorInChief',
      handlingEditor: role === 'handlingEditor',
      notifications: {
        email: {
          system: true,
          user: true,
        },
      },
      accessTokens: {
        invitation: role === 'reviewer' ? services.generateHash() : null,
        unsubscribe: services.generateHash(),
        passwordReset: services.generateHash(),
      },
    }

    let newUser = new UserModel(userBody)

    newUser = await newUser.save()
    return newUser
  }

  async getEditorsInChief() {
    const { UserModel } = this
    const users = await UserModel.all()

    const eics = users.filter(user => user.editorInChief)
    if (eics.length === 0) {
      throw new Error('No Editor in Chief has been found')
    }

    return eics
  }

  async isAdmin(user) {
    const { UserModel } = this
    const users = await UserModel.all()
    const admin = users.filter(user => user.admin)
    return user === admin[0].id
  }

  async updateUserTeams({ userId, teamId }) {
    const user = await this.UserModel.find(userId)
    user.teams.push(teamId)
    await user.save()
  }

  async getActiveAuthors({ fragmentAuthors }) {
    const userData = (await Promise.all(
      fragmentAuthors.map(author => this.UserModel.find(author.id)),
    ))
      .filter(user => user.isActive && get(user, 'notifications.email.user'))
      .map(user => ({ id: user.id, accessTokens: user.accessTokens }))

    return fragmentAuthors
      .map(fAuthor => {
        const matchingAuthor = userData.find(user => user.id === fAuthor.id)
        if (matchingAuthor) {
          return {
            ...fAuthor,
            accessTokens: matchingAuthor.accessTokens,
          }
        }
        return false
      })
      .filter(Boolean)
  }

  async getEiCName() {
    const editorsInChief = await this.getEditorsInChief()
    const firstName = get(editorsInChief, '0.firstName', 'Editor')
    const lastName = get(editorsInChief, '0.lastName', 'in Chief')
    return `${firstName} ${lastName}`
  }
}

module.exports = User
