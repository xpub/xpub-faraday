process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { cloneDeep } = require('lodash')
const fixturesService = require('pubsweet-component-fixture-service')

const { fixtures, Model } = fixturesService
const { Collection, Fragment } = require('../Helper')

describe('Collection helper', () => {
  let testFixtures = {}
  let models

  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  describe('getReviewerNumber', () => {
    it('should assign reviewer number 1 on invitation if no other reviewer numbers exist', async () => {
      const { collection } = testFixtures.collections
      const { reviewer } = testFixtures.users
      const collectionHelper = new Collection({ collection })

      const reviewerNumber = await collectionHelper.getReviewerNumber({
        userId: reviewer.id,
      })

      expect(reviewerNumber).toBe(1)
    })
    it('should assign next reviewer number on invitation if another reviewer numbers exist', async () => {
      const { collectionReviewCompleted } = testFixtures.collections
      const { reviewer } = testFixtures.users
      const collectionHelper = new Collection({
        collection: collectionReviewCompleted,
      })

      const reviewerNumber = await collectionHelper.getReviewerNumber({
        userId: reviewer.id,
      })

      expect(reviewerNumber).toBe(3)
    })
    it('should keep reviewer number across fragment versions', async () => {
      const { oneReviewedFragmentCollection } = testFixtures.collections
      const { answerReviewer } = testFixtures.users
      const collectionHelper = new Collection({
        collection: oneReviewedFragmentCollection,
      })

      const reviewerNumber = await collectionHelper.getReviewerNumber({
        userId: answerReviewer.id,
      })

      expect(reviewerNumber).toBe(2)
    })
    it('should assign next reviewer number across fragment versions', async () => {
      const { oneReviewedFragmentCollection } = testFixtures.collections
      const { reviewer } = testFixtures.users
      const collectionHelper = new Collection({
        collection: oneReviewedFragmentCollection,
      })

      const reviewerNumber = await collectionHelper.getReviewerNumber({
        userId: reviewer.id,
      })

      expect(reviewerNumber).toBe(3)
    })
  })

  describe('hasAtLeastOneReviewReport', () => {
    it('should return true if collection has at least one report from reviewers.', async () => {
      const { collection } = testFixtures.collections
      const collectionHelper = new Collection({ collection })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const hasReviewReport = await collectionHelper.hasAtLeastOneReviewReport(
        fragments,
      )
      expect(hasReviewReport).toBe(true)
    })
    it('should return false if collection has at least one report from reviewers.', async () => {
      const { noInvitesFragment } = testFixtures.fragments
      const { collection } = testFixtures.collections
      collection.fragments = [noInvitesFragment.id]
      const collectionHelper = new Collection({ collection })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const hasReviewReport = await collectionHelper.hasAtLeastOneReviewReport(
        fragments,
      )
      expect(hasReviewReport).toBe(false)
    })
  })

  describe('canHEMakeRecommendation', () => {
    it('should return true when creating a recommendation as a HE when there is a single version with at least one review.', async () => {
      const { collection } = testFixtures.collections
      const { fragment } = testFixtures.fragments
      const collectionHelper = new Collection({
        collection,
      })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const fragmentHelper = new Fragment({ fragment })
      const canHEMakeRecommendation = await collectionHelper.canHEMakeRecommendation(
        fragments,
        fragmentHelper,
      )
      expect(canHEMakeRecommendation).toBe(true)
    })
    it('should return false when creating a recommendation with publish as a HE when there is a single version and there are no reviews.', async () => {
      const { collection } = testFixtures.collections
      const { fragment } = testFixtures.fragments
      fragment.recommendations = undefined

      const collectionHelper = new Collection({
        collection,
      })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const fragmentHelper = new Fragment({ fragment })
      const canHEMakeRecommendation = await collectionHelper.canHEMakeRecommendation(
        fragments,
        fragmentHelper,
      )
      expect(canHEMakeRecommendation).toBe(false)
    })
    it('should return true when creating a recommendation as a HE after minor revision and we have at least one review on collection.', async () => {
      const { collection } = testFixtures.collections
      const {
        minorRevisionWithReview,
        noInvitesFragment1,
      } = testFixtures.fragments
      collection.fragments = [minorRevisionWithReview.id, noInvitesFragment1.id]
      const collectionHelper = new Collection({
        collection,
      })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const fragmentHelper = new Fragment({ fragment: noInvitesFragment1 })

      const canHEMakeRecommendation = await collectionHelper.canHEMakeRecommendation(
        fragments,
        fragmentHelper,
      )
      expect(canHEMakeRecommendation).toBe(true)
    })
    it('should return false when creating a recommendation as a HE after minor revision and there are no reviews.', async () => {
      const { collection } = testFixtures.collections
      const {
        minorRevisionWithoutReview,
        noInvitesFragment1,
      } = testFixtures.fragments
      collection.fragments = [
        minorRevisionWithoutReview.id,
        noInvitesFragment1.id,
      ]

      const collectionHelper = new Collection({
        collection,
      })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const fragmentHelper = new Fragment({ noInvitesFragment1 })
      const canHEMakeRecommendation = await collectionHelper.canHEMakeRecommendation(
        fragments,
        fragmentHelper,
      )
      expect(canHEMakeRecommendation).toBe(false)
    })
    it('should return true when creating a recommendation as a HE after major revision and there are least one review on fragment.', async () => {
      const { collection } = testFixtures.collections
      const {
        majorRevisionWithReview,
        reviewCompletedFragment,
      } = testFixtures.fragments

      reviewCompletedFragment.collectionId = collection.id
      collection.fragments = [
        majorRevisionWithReview.id,
        reviewCompletedFragment.id,
      ]

      const collectionHelper = new Collection({
        collection,
      })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const fragmentHelper = new Fragment({ fragment: reviewCompletedFragment })
      const canHEMakeRecommendation = await collectionHelper.canHEMakeRecommendation(
        fragments,
        fragmentHelper,
      )
      expect(canHEMakeRecommendation).toBe(true)
    })
    it('should return false when creating a recommendation as a HE after major revision there are no reviews on fragment.', async () => {
      const { collection } = testFixtures.collections
      const {
        majorRevisionWithReview,
        noInvitesFragment1,
      } = testFixtures.fragments
      collection.fragments = [majorRevisionWithReview.id, noInvitesFragment1.id]
      const collectionHelper = new Collection({
        collection,
      })
      const FragmentModel = models.Fragment
      const fragments = await collectionHelper.getAllFragments({
        FragmentModel,
      })
      const fragmentHelper = new Fragment({ fragment: noInvitesFragment1 })
      const canHEMakeRecommendation = await collectionHelper.canHEMakeRecommendation(
        fragments,
        fragmentHelper,
      )
      expect(canHEMakeRecommendation).toBe(false)
    })
  })
})
