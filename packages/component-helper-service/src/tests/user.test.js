process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { cloneDeep } = require('lodash')
const fixturesService = require('pubsweet-component-fixture-service')

const { fixtures, Model } = fixturesService
const { User } = require('../Helper')

describe('User helper', () => {
  let testFixtures = {}
  let models

  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  describe('isAdmin', () => {
    it('should return true if user is admin', async () => {
      const { admin } = testFixtures.users
      const adminId = admin.id
      const UserModel = models.User
      const userHelper = new User({ UserModel })
      const isAdmin = await userHelper.isAdmin(adminId)

      expect(isAdmin).toBe(true)
    })

    it('should return false if user is not admin', async () => {
      const { reviewer } = testFixtures.users
      const reviewerId = reviewer.id
      const UserModel = models.User
      const userHelper = new User({ UserModel })
      const isAdmin = await userHelper.isAdmin(reviewerId)

      expect(isAdmin).toBe(false)
    })
  })
})
