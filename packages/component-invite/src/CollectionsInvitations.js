const bodyParser = require('body-parser')

const CollectionsInvitations = app => {
  app.use(bodyParser.json())
  const basePath = '/api/collections/:collectionId/invitations'
  const routePath = './routes/collectionsInvitations'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {post} /api/collections/:collectionId/invitations Invite a user to a collection
   * @apiGroup CollectionsInvitations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParamExample {json} Body
   *    {
   *      "email": "email@example.com",
   *      "role": "handlingEditor", [acceptedValues: handlingEditor]
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *   {
   *     "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
   *     "role": "handlingEditor",
   *     "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
   *     "hasAnswer": false,
   *     "invitedOn": 1525428890167,
   *     "isAccepted": false,
   *     "respondedOn": null
   *    }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.post(
    basePath,
    authBearer,
    require(`${routePath}/post`)(app.locals.models),
  )
  /**
   * @api {delete} /api/collections/:collectionId/invitations/:invitationId Delete invitation (or revoke HE if invitation is accepted)
   * @apiGroup CollectionsInvitations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {invitationId} invitationId Invitation id
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 204 No Content
   * @apiErrorExample {json} Delete errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.delete(
    `${basePath}/:invitationId`,
    authBearer,
    require(`${routePath}/delete`)(app.locals.models),
  )
  /**
   * @api {patch} /api/collections/:collectionId/invitations/:invitationId Update an invitation
   * @apiGroup CollectionsInvitations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {invitationId} invitationId Invitation id
   * @apiParamExample {json} Body
   *    {
   *      "isAccepted": false,
   *      "reason": "I am not ready" [optional]
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *   {
   *     "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
   *     "role": "handlingEditor",
   *     "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
   *     "hasAnswer": true,
   *     "invitedOn": 1525428890167,
   *     "isAccepted": false,
   *     "respondedOn": 1525428890299
   *    }
   * @apiErrorExample {json} Update invitations errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.patch(
    `${basePath}/:invitationId`,
    authBearer,
    require(`${routePath}/patch`)(app.locals.models),
  )
}

module.exports = CollectionsInvitations
