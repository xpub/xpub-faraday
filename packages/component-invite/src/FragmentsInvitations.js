const bodyParser = require('body-parser')

const FragmentsInvitations = app => {
  app.use(bodyParser.json())
  const basePath =
    '/api/collections/:collectionId/fragments/:fragmentId/invitations'
  const routePath = './routes/fragmentsInvitations'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {post} /api/collections/:collectionId/fragments/:fragmentId/invitations Invite a user to a fragment
   * @apiGroup FragmentsInvitations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiParamExample {json} Body
   *    {
   *      "email": "email@example.com",
   *      "role": "reviewer", [acceptedValues: reviewer],
   *      "firstName": "Julien",
   *      "lastName": "Hopfenkonig",
   *      "affiliation": "UCLA",
   *      "country": "RO"
   *      "isPublons": false [Boolean]
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *   {
   *     "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
   *     "role": "reviewer",
   *     "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
   *     "hasAnswer": false,
   *     "invitedOn": 1525428890167,
   *     "isAccepted": false,
   *     "respondedOn": null
   *    }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.post(
    basePath,
    authBearer,
    require(`${routePath}/post`)(app.locals.models),
  )
  /**
   * @api {get} /api/collections/:collectionId/fragments/:fragmentId/invitations/[:invitationId]?role=:role List fragment invitations
   * @apiGroup FragmentsInvitations
   * @apiParam {id} collectionId Collection id
   * @apiParam {id} fragmentId Fragment id
   * @apiParam {id} [invitationId] Invitation id
   * @apiParam {String} role The role to search for: reviewer
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    [{
   *      "name": "John Smith",
   *      "invitedOn": 1525428890167,
   *      "respondedOn": 1525428890299,
   *      "email": "email@example.com",
   *      "status": "pending",
   *      "invitationId": "1990881"
   *    }]
   * @apiErrorExample {json} List errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.get(
    `${basePath}/:invitationId?`,
    authBearer,
    require(`${routePath}/get`)(app.locals.models),
  )
  /**
   * @api {delete} /api/collections/:collectionId/fragments/:fragmentId/invitations/:invitationId Delete invitation
   * @apiGroup FragmentsInvitations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiParam {invitationId} invitationId Invitation id
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 204 No Content
   * @apiErrorExample {json} Delete errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.delete(
    `${basePath}/:invitationId`,
    authBearer,
    require(`${routePath}/delete`)(app.locals.models),
  )
  /**
   * @api {patch} /api/collections/:collectionId/fragments/:fragmentId/invitations/:invitationId Update an invitation
   * @apiGroup FragmentsInvitations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {invitationId} invitationId Invitation id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiParamExample {json} Body
   *    {
   *      "isAccepted": false,
   *      "reason": "I am not ready" [optional]
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *   {
   *     "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
   *     "role": "reviewer",
   *     "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
   *     "hasAnswer": true,
   *     "invitedOn": 1525428890167,
   *     "isAccepted": false,
   *     "respondedOn": 1525428890299
   *    }
   * @apiErrorExample {json} Update invitations errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.patch(
    `${basePath}/:invitationId`,
    authBearer,
    require(`${routePath}/patch`)(app.locals.models),
  )
  /**
   * @api {patch} /api/collections/:collectionId/fragments/:fragmentId/invitations/:invitationId/decline Decline an invitation as a reviewer
   * @apiGroup FragmentsInvitations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {invitationId} invitationId Invitation id
   * @apiParamExample {json} Body
   *    {
   *      "invitationToken": "f2d814f0-67a5-4590-ba4f-6a83565feb4f",
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {}
   * @apiErrorExample {json} Update invitations errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.patch(
    `${basePath}/:invitationId/decline`,
    require(`${routePath}/decline`)(app.locals.models),
  )
}

module.exports = FragmentsInvitations
