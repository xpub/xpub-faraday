const config = require('config')

const staffEmail = config.get('journal.staffEmail')
const journalName = config.get('journal.name')

const getEmailCopy = ({ emailType, titleText, targetUserName, comments }) => {
  let paragraph
  let hasLink = true
  let hasIntro = true
  let hasSignature = true
  switch (emailType) {
    case 'he-assigned':
      hasIntro = false
      hasSignature = false
      paragraph = `${targetUserName} has invited you to serve as the Handling Editor for ${titleText}.<br/><br/>
        To review this manuscript and respond to the invitation, please visit the manuscript details page.`
      break
    case 'he-accepted':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} agreed to serve as the Handling Editor on ${titleText}.
        Please click on the link below to access the manuscript.`
      break
    case 'he-declined':
      paragraph = `Dr. ${targetUserName} has declined to serve as the Handling Editor on ${titleText}.<br/><br/>
        ${comments}<br/><br/>
        To invite another Handling Editor, please click the link below.`
      hasIntro = false
      hasSignature = false
      break
    case 'he-revoked':
      hasIntro = false
      hasLink = false
      hasSignature = false
      paragraph = `${targetUserName} has removed you from the role of Handling Editor for ${titleText}.<br/><br/>
        The manuscript will no longer appear in your dashboard. Please contact ${staffEmail} if you have any questions about this change.`
      break
    case 'author-he-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `We had to replace the handling editor of your manuscript ${titleText}. We apologise for any inconvenience, but it was necessary in order to move your manuscript forward.<br/><br/>
        If you have questions please email them to ${staffEmail}.<br/><br/>
        Thank you for your submission to ${journalName}.`
      break
    case 'he-he-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `The editor in chief removed you from the manuscript "${titleText}".<br/><br/>
        If you have any questions regarding this action, please let us know at ${staffEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
      break
    case 'reviewer-he-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `We had to replace the handling editor of the manuscript "${titleText}". We apologise for any inconvenience this may cause.<br/><br/>
        If you have started the review process please email the content to ${staffEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return { paragraph, hasLink, hasIntro, hasSignature }
}

module.exports = {
  getEmailCopy,
}
