const config = require('config')
const { last } = require('lodash')
const Email = require('@pubsweet/component-email-templating')

const { name: journalName, staffEmail } = config.get('journal')

const {
  User,
  services,
  Fragment,
} = require('pubsweet-component-helper-service')

const { getEmailCopy } = require('./emailCopy')

const unsubscribeSlug = config.get('unsubscribe.url')

module.exports = {
  sendInvitedHEEmail: async ({
    baseUrl,
    invitedHE,
    collection,
    isCanceled = false,
    models: { User: UserModel, Fragment: FragmentModel },
  }) => {
    const fragmentId = last(collection.fragments)
    const fragment = await FragmentModel.find(fragmentId)
    const fragmentHelper = new Fragment({ fragment })
    const { title } = await fragmentHelper.getFragmentData()
    const { submittingAuthor } = await fragmentHelper.getAuthorData({
      UserModel,
    })

    const titleText = `the manuscript titled "${title}" by ${
      submittingAuthor.firstName
    } ${submittingAuthor.lastName}`

    const userHelper = new User({ UserModel })
    const eicName = await userHelper.getEiCName()
    const { customId } = collection

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      targetUserName: eicName,
      emailType: isCanceled ? 'he-revoked' : 'he-assigned',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: invitedHE.email,
      },
      content: {
        subject: isCanceled
          ? `${customId}: Editor invitation cancelled`
          : `${customId}: Invitation to edit a manuscript`,
        paragraph,
        signatureName: eicName,
        ctaLink: services.createUrl(
          baseUrl,
          `/projects/${collection.id}/versions/${fragment.id}/details`,
        ),
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: invitedHE.id,
          token: invitedHE.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  notifyAuthorWhenHERemoved: async ({
    baseUrl,
    collection,
    models: { User: UserModel, Fragment: FragmentModel },
  }) => {
    const fragmentId = last(collection.fragments)
    const fragment = await FragmentModel.find(fragmentId)
    const fragmentHelper = new Fragment({ fragment })
    const { title: titleText } = await fragmentHelper.getFragmentData()
    const { submittingAuthor } = await fragmentHelper.getAuthorData({
      UserModel,
    })

    const userHelper = new User({ UserModel })
    const eicName = await userHelper.getEiCName()
    const { customId } = collection

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      emailType: 'author-he-removed',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${eicName} <${staffEmail}>`,
      toUser: {
        email: submittingAuthor.email,
        name: `${submittingAuthor.lastName}`,
      },
      content: {
        subject: `${customId}:  Your manuscript's editor was changed`,
        paragraph,
        signatureName: eicName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.id,
          token: submittingAuthor.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  notifyInvitedHEWhenRemoved: async ({
    baseUrl,
    invitedHE,
    collection,
    models: { User: UserModel, Fragment: FragmentModel },
  }) => {
    const fragmentId = last(collection.fragments)
    const fragment = await FragmentModel.find(fragmentId)
    const fragmentHelper = new Fragment({ fragment })
    const { title: titleText } = await fragmentHelper.getFragmentData()

    const userHelper = new User({ UserModel })
    const eicName = await userHelper.getEiCName()
    const { customId } = collection

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      emailType: 'he-he-removed',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${eicName} <${staffEmail}>`,
      toUser: {
        email: invitedHE.email,
        name: `${invitedHE.lastName}`,
      },
      content: {
        subject: `${customId}: The editor in chief removed you from ${titleText}`,
        paragraph,
        signatureName: eicName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: invitedHE.id,
          token: invitedHE.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  notifyReviewersWhenHERemoved: async ({
    baseUrl,
    collection,
    reviewers,
    models: { User: UserModel, Fragment: FragmentModel },
  }) => {
    const fragmentId = last(collection.fragments)
    const fragment = await FragmentModel.find(fragmentId)
    const fragmentHelper = new Fragment({ fragment })
    const { title: titleText } = await fragmentHelper.getFragmentData()

    const userHelper = new User({ UserModel })
    const eicName = await userHelper.getEiCName()
    const { customId } = collection

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText,
      emailType: 'reviewer-he-removed',
    })

    reviewers.forEach(reviewer => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: reviewer.email,
          name: reviewer.lastName,
        },
        fromEmail: `${eicName} <${staffEmail}>`,
        content: {
          subject: `${customId}: The handling editor of a manuscript that you were reviewing was changed`,
          paragraph,
          signatureName: eicName,
          signatureJournal: journalName,
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: reviewer.id,
            token: reviewer.accessTokens.unsubscribe,
          }),
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },
  sendEiCEmail: async ({
    reason,
    baseUrl,
    invitedHE,
    collection,
    isAccepted = false,
    models: { User: UserModel, Fragment: FragmentModel },
  }) => {
    const fragmentId = last(collection.fragments)
    const fragment = await FragmentModel.find(fragmentId)
    const fragmentHelper = new Fragment({ fragment })
    const { title } = await fragmentHelper.getFragmentData()
    const { submittingAuthor } = await fragmentHelper.getAuthorData({
      UserModel,
    })

    const titleText = `the manuscript titled "${title}" by ${
      submittingAuthor.firstName
    } ${submittingAuthor.lastName}`

    const userHelper = new User({ UserModel })
    const eics = await userHelper.getEditorsInChief()
    const eic = eics[0]
    const eicName = `${eic.firstName} ${eic.lastName}`
    const { customId } = collection

    const emailType = isAccepted ? 'he-accepted' : 'he-declined'

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      comments: reason ? `Reason: "${reason}"` : '',
      targetUserName: `${invitedHE.lastName}`,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: eic.email,
      },
      content: {
        subject: isAccepted
          ? `${customId}: Editor invitation accepted`
          : `${customId}: Editor invitation declined`,
        paragraph,
        signatureName: eicName,
        ctaLink: services.createUrl(
          baseUrl,
          `/projects/${collection.id}/versions/${fragment.id}/details`,
        ),
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: services.createUrl(baseUrl),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
