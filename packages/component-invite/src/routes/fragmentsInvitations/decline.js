const {
  Team,
  services,
  Invitation,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const notifications = require('./emails/notifications')
const Job = require('pubsweet-component-jobs')

module.exports = models => async (req, res) => {
  const { collectionId, invitationId, fragmentId } = req.params
  const { invitationToken } = req.body

  if (!services.checkForUndefinedParams(invitationToken))
    return res.status(400).json({ error: 'Token is required' })

  const UserModel = models.User

  const users = await UserModel.all()

  const user = users.find(
    user =>
      user.accessTokens && user.accessTokens.invitation === invitationToken,
  )
  if (!user) {
    return res.status(404).json({
      error: `User not found.`,
    })
  }

  let collection = {}
  let fragment = {}
  try {
    collection = await models.Collection.find(collectionId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Fragment ${fragmentId} does not match collection ${collectionId}`,
      })
    fragment = await models.Fragment.find(fragmentId)
  } catch (err) {
    const notFoundError = await services.handleNotFoundError(err, 'Item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }

  fragment.invitations = fragment.invitations || []
  const invitation = await fragment.invitations.find(
    invitation => invitation.id === invitationId,
  )

  const invitationHelper = new Invitation({
    userId: user.id,
    role: 'reviewer',
    invitation,
  })

  const invitationValidation = invitationHelper.validateInvitation()
  if (invitationValidation.error)
    return res.status(invitationValidation.status).json({
      error: invitationValidation.error,
    })

  const authsome = authsomeHelper.getAuthsome(models)
  const canPatch = await authsome.can(req.user, 'PATCH', invitation)
  if (!canPatch)
    return res.status(403).json({
      error: 'Unauthorized.',
    })

  await Job.cancelQueue(`reminders-${user.id}-${invitation.id}`)
  await Job.cancelQueue(`removal-${user.id}-${invitation.id}`)

  invitation.respondedOn = Date.now()
  invitation.hasAnswer = true
  invitation.isAccepted = false
  await fragment.save()

  const baseUrl = services.getBaseUrl(req)

  const teamHelper = new Team({
    fragmentId,
    collectionId,
    TeamModel: models.Team,
  })

  const reviewerTeam = await teamHelper.getTeam({
    role: 'reviewer',
    objectType: 'fragment',
  })

  await teamHelper.removeTeamMember({
    teamId: reviewerTeam.id,
    userId: user.id,
  })

  user.teams = user.teams.filter(userTeamId => reviewerTeam.id !== userTeamId)
  await user.save()

  notifications.sendHandlingEditorEmail({
    baseUrl,
    fragment,
    collection,
    reviewer: user,
    UserModel: models.User,
    emailType: 'reviewer-declined',
  })

  return res.status(200).json({})
}
