const config = require('config')

const { get, forOwn } = require('lodash')
const Email = require('@pubsweet/component-email-templating')

const unsubscribeSlug = config.get('unsubscribe.url')
const inviteReviewerPath = config.get('invite-reviewer.url')
const { staffEmail, name: journalName } = config.get('journal')

const { services, Fragment } = require('pubsweet-component-helper-service')

const { getEmailCopy } = require('./emailCopy')
const { scheduleReminderJob } = require('../jobs/reminders')
const { scheduleRemovalJob } = require('../jobs/removal')

const daysList = config.get('reminders.reviewer.days')
const timeUnit = config.get('reminders.reviewer.timeUnit')
const removalDays = config.get('reminders.reviewer.remove')
const daysExpected = config.get('daysExpectedForReview')

module.exports = {
  async sendReviewInvitations({
    baseUrl,
    fragment,
    UserModel,
    collection,
    invitation,
    invitedUser,
  }) {
    const fragmentHelper = new Fragment({ fragment })
    const { title, abstract } = await fragmentHelper.getFragmentData({
      handlingEditor: collection.handlingEditor,
    })
    const {
      activeAuthors: authors,
      submittingAuthor,
    } = await fragmentHelper.getAuthorData({ UserModel })

    const subjectBaseText = `${collection.customId}: Review invitation`

    const detailsPath = `/projects/${collection.id}/versions/${
      fragment.id
    }/details`

    const declineLink = services.createUrl(baseUrl, inviteReviewerPath, {
      invitationId: invitation.id,
      agree: false,
      fragmentId: fragment.id,
      collectionId: collection.id,
      invitationToken: invitedUser.accessTokens.invitation,
    })

    let agreeLink = services.createUrl(baseUrl, detailsPath, {
      invitationId: invitation.id,
      agree: true,
    })

    if (!invitedUser.isConfirmed) {
      agreeLink = services.createUrl(baseUrl, inviteReviewerPath, {
        invitationId: invitation.id,
        agree: true,
        fragmentId: fragment.id,
        email: invitedUser.email,
        collectionId: collection.id,
        token: invitedUser.accessTokens.passwordReset,
      })
    }

    const authorsList = authors.map(
      author => `${author.firstName} ${author.lastName}`,
    )

    const handlingEditor = get(collection, 'handlingEditor', {})

    const authorName = `${submittingAuthor.firstName} ${
      submittingAuthor.lastName
    }`

    const emailType = 'reviewer-invitation'
    const titleText = `A manuscript titled <strong>"${title}"</strong> by <strong>${authorName}</strong> et al.`

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      expectedDate: services.getExpectedDate({
        timestamp: invitation.invitedOn,
        daysExpected,
      }),
    })

    const email = new Email({
      type: 'user',
      templateType: 'invitation',
      fromEmail: `${handlingEditor.name} <${staffEmail}>`,
      toUser: {
        email: invitedUser.email,
        name: invitedUser.lastName,
      },
      content: {
        title,
        abstract,
        agreeLink,
        declineLink,
        signatureJournal: journalName,
        signatureName: handlingEditor.name,
        authorsList: authorsList.join(', '),
        subject: subjectBaseText,
        paragraph,
        detailsLink: services.createUrl(baseUrl, detailsPath),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: invitedUser.id,
          token: invitedUser.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    await email.sendEmail()

    forOwn(daysList, (days, order) =>
      scheduleReminderJob({
        days,
        order,
        email,
        timeUnit,
        userId: invitedUser.id,
        invitationId: invitation.id,
        subject: `${subjectBaseText} reminder`,
        titleText: `the manuscript titled "${title}" by ${authorName}`,
        expectedDate: services.getExpectedDate({
          timestamp: invitation.invitedOn,
          daysExpected: 0,
        }),
      }),
    )

    scheduleRemovalJob({
      timeUnit,
      invitation,
      days: removalDays,
      userId: invitedUser.id,
      fragmentId: fragment.id,
      collectionId: collection.id,
    })
  },
}
