const config = require('config')
const {
  services,
  Team,
  Invitation,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const configRoles = config.get('roles')

module.exports = models => async (req, res) => {
  const { role } = req.query
  if (!services.checkForUndefinedParams(role)) {
    res.status(400).json({ error: 'Role is required' })
    return
  }

  if (!configRoles.collection.includes(role)) {
    res.status(400).json({ error: `Role ${role} is invalid` })
    return
  }
  const { collectionId, fragmentId } = req.params
  const teamHelper = new Team({
    TeamModel: models.Team,
    collectionId,
    fragmentId,
  })

  try {
    const collection = await models.Collection.find(collectionId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Fragment ${fragmentId} does not match collection ${collectionId}.`,
      })
    const fragment = await models.Fragment.find(fragmentId)

    const authsome = authsomeHelper.getAuthsome(models)
    const target = {
      fragment,
      path: req.route.path,
    }

    const canGet = await authsome.can(req.user, 'GET', target)

    if (!canGet)
      return res.status(403).json({
        error: 'Unauthorized.',
      })

    const members = await teamHelper.getTeamMembers({
      role,
      objectType: 'fragment',
    })

    if (!members) return res.status(200).json([])

    const invitationHelper = new Invitation({ role })

    const membersData = members.map(async member => {
      const user = await models.User.find(member)

      invitationHelper.userId = user.id
      const {
        invitedOn,
        respondedOn,
        status,
        id,
      } = invitationHelper.getInvitationsData({
        invitations: fragment.invitations,
      })

      return {
        name: `${user.firstName} ${user.lastName}`,
        invitedOn,
        respondedOn,
        email: user.email,
        status,
        userId: user.id,
        invitationId: id,
      }
    })

    const resBody = await Promise.all(membersData)

    res.status(200).json(resBody.filter(Boolean))
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
