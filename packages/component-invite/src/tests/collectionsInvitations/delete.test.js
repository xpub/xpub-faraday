process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))
jest.mock('pubsweet-component-mts-package/src/PackageManager', () => ({
  deleteFilesS3: jest.fn(),
}))

const path = '../routes/collectionsInvitations/delete'
const route = {
  path: '/api/collections/:collectionId/invitations/:invitationId',
}

describe('Delete Collections Invitations route handler', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })
  it('should return an error when the collection does not exist', async () => {
    const { editorInChief } = testFixtures.users
    const res = await requests.sendRequest({
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: 'invalid-id',
      },
    })
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Collection not found')
  })
  it('should return an error when the invitation does not exist', async () => {
    const { editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    const res = await requests.sendRequest({
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        invitationId: 'invalid-id',
      },
    })
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Invitation invalid-id not found')
  })
  it('should return success when the collection and invitation exist', async () => {
    const { editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    const res = await requests.sendRequest({
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        invitationId: collection.invitations[0].id,
      },
    })
    expect(res.statusCode).toBe(200)
  })
  it('should return an error when the user does not have invitation rights', async () => {
    const { author } = testFixtures.users
    const { collection } = testFixtures.collections
    const res = await requests.sendRequest({
      userId: author.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        invitationId: collection.invitations[0].id,
      },
    })
    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
  it('should return success when the EiC revokes a HE', async () => {
    const { editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    const res = await requests.sendRequest({
      userId: editorInChief.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
        invitationId: collection.invitations[1].id,
      },
    })
    expect(res.statusCode).toBe(200)
  })
})
