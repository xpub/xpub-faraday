process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const httpMocks = require('node-mocks-http')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const reqBody = {
  isAccepted: true,
}
const patchPath = '../../routes/collectionsInvitations/patch'
describe('Patch collections invitations route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return success when the handling editor accepts work on a collection', async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = collection.id
    const heInv = collection.invitations.find(
      inv => inv.role === 'handlingEditor' && inv.hasAnswer === false,
    )
    req.params.invitationId = heInv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)
    expect(res.statusCode).toBe(200)
  })
  it('should return success when the handling editor declines work on a collection', async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    body.isAccepted = false
    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = collection.id
    const heInv = collection.invitations.find(
      inv => inv.role === 'handlingEditor' && inv.hasAnswer === false,
    )
    req.params.invitationId = heInv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
  })
  it('should return an error if the collection does not exists', async () => {
    const { handlingEditor } = testFixtures.users
    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = 'invalid-id'
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('collection not found')
  })
  it('should return an error when the invitation does not exist', async () => {
    const { user } = testFixtures.users
    const { collection } = testFixtures.collections
    const req = httpMocks.createRequest({
      body,
    })
    req.user = user.id
    req.params.collectionId = collection.id
    req.params.invitationId = 'invalid-id'
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Invitation not found.')
  })
  it('should return an error when the invitation is already answered', async () => {
    const { handlingEditor } = testFixtures.users
    const { collection } = testFixtures.collections
    const req = httpMocks.createRequest({
      body,
    })
    req.user = handlingEditor.id
    req.params.collectionId = collection.id
    const inv = collection.invitations.find(inv => inv.hasAnswer)
    req.params.invitationId = inv.id
    const res = httpMocks.createResponse()
    await require(patchPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Invitation has already been answered.`)
  })
})
