module.exports = {
  backend: () => app => {
    require('./src/FragmentsRecommendations')(app)
    require('./src/Fragments')(app)
    require('./src/TechnicalChecks')(app)
    require('./src/Collections')(app)
  },
}
