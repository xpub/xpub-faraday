const bodyParser = require('body-parser')

const FragmentsRecommendations = app => {
  app.use(bodyParser.json())
  const basePath =
    '/api/collections/:collectionId/fragments/:fragmentId/recommendations'
  const routePath = './routes/fragmentsRecommendations'
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {post} /api/collections/:collectionId/fragments/:fragmentId/recommendations Create a recommendation on a fragment
   * @apiGroup FragmentsRecommendations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiParamExample {json} Body
   *    {
   *      "recommendation": "publish", [acceptedValues: publish, reject, minor, major],
   *      "comments":
   *      [
   *        {
   *          "content": "A very nice manuscript",
   *          "public": true
   *          "files":
   *          [
   *            {
   *              "id": "111-22-333",
   *              "name": "file.pdf",
   *              "size": 104232
   *            }
   *          ]
   *        }
   *      ],
   *      "recommendationType": "review" [acceptedValues: review, editorRecommendation]
   *    }
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
   *     "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
   *     "createdOn": 1525428890167,
   *     "updatedOn": 1525428890167,
   *     "recommendation": "publish",
   *     "comments":
   *     [
   *       {
   *         "content": "A very nice manuscript",
   *         "public": true
   *         "files":
   *         [
   *           {
   *           "id": "111-22-333",
   *           "name": "file.pdf",
   *           "size": 104232
   *           }
   *        ]
   *      }
   *     ],
   *     "recommendationType": "review"
   *   }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.post(
    basePath,
    authBearer,
    require(`${routePath}/post`)(app.locals.models),
  )
  /**
   * @api {patch} /api/collections/:collectionId/fragments/:fragmentId/recommendations/:recommendationId Modify a recommendation on a fragment
   * @apiGroup FragmentsRecommendations
   * @apiParam {collectionId} collectionId Collection id
   * @apiParam {fragmentId} fragmentId Fragment id
   * @apiParam {recommendationId} recommendationId Recommendation id
   * @apiParamExample {json} Body
   *   {
   *     "recommendation": "publish", [acceptedValues: publish, reject, minor, major],
   *     "comments":
   *     [
   *       {
   *         "content": "A very nice manuscript",
   *         "public": true
   *         "files":
   *         [
   *           {
   *             "id": "111-22-333",
   *             "name": "file.pdf",
   *             "size": 104232
   *           }
   *        ]
   *      }
   *      ],
   *     "recommendationType": "review" [acceptedValues: review, editorRecommendation]
   *   }
   * @apiSuccessExample {json} Success
   *   HTTP/1.1 200 OK
   *   {
   *     "id": "7b2431af-210c-49f9-a69a-e19271066ebd",
   *     "userId": "4c3f8ee1-785b-4adb-87b4-407a27f652c6",
   *     "createdOn": 1525428890167,
   *     "updatedOn": 1525428890167,
   *     "recommendation": "publish",
   *     "comments":
   *     [
   *       {
   *         "content": "A very nice manuscript",
   *         "public": true
   *         "files":
   *         [
   *           {
   *           "id": "111-22-333",
   *           "name": "file.pdf",
   *           "size": 104232
   *           }
   *        ]
   *      }
   *     ],
   *     "recommendationType": "review" [acceptedValues: review, editorRecommendation]
   *   }
   * @apiErrorExample {json} Invite user errors
   *    HTTP/1.1 403 Forbidden
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   *    HTTP/1.1 500 Internal Server Error
   */
  app.patch(
    `${basePath}/:recommendationId`,
    authBearer,
    require(`${routePath}/patch`)(app.locals.models),
  )
}

module.exports = FragmentsRecommendations
