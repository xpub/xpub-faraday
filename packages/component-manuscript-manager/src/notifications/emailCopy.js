const config = require('config')
const { upperFirst } = require('lodash')

const staffEmail = config.get('journal.staffEmail')
const journalName = config.get('journal.name')

const getEmailCopy = ({
  customId,
  emailType,
  titleText,
  comments = '',
  targetUserName = '',
  eicName = 'Editor in Chief',
  expectedDate = new Date(),
}) => {
  let upperContent, manuscriptText, subText, lowerContent, paragraph
  let hasLink = true
  let hasIntro = true
  let hasSignature = true
  switch (emailType) {
    case 'author-request-to-revision':
      paragraph = `After reviewing ${titleText}, I have requested further revisions to the manuscript. <br/><br/>
        ${comments}<br/><br/>
        For more information about the requested changes and to submit your revised manuscript, please visit the manuscript details page.`
      break
    case 'author-manuscript-rejected':
      paragraph = `The peer review of ${titleText} has now been completed. I regret to inform you that your manuscript has been rejected for publication.<br/><br/>
        ${comments}<br/><br/>
        Thank you for your submission to ${journalName}.`
      hasLink = false
      break
    case 'author-manuscript-published':
      paragraph = `I am delighted to inform you that the review of ${titleText} has been completed and your article will be published in ${journalName}.<br/><br/>
        Please visit the manuscript details page to review the editorial notes and any comments from external reviewers.<br/><br/>
        Your article will now be passed to our production team for processing, and you will hear directly from them should we require any further information.<br/><br/>
        Thank you for choosing to publish with ${journalName}.`
      break
    case 'he-manuscript-rejected':
      hasLink = false
      hasIntro = false
      hasSignature = false
      paragraph = `${targetUserName} has confirmed your decision to reject the ${titleText}.<br/><br/>
        No further action is required at this time. To review this decision, please visit the manuscript details page.<br/><br/>
        Thank you for handling this manuscript on behalf of ${journalName}.`
      break
    case 'he-manuscript-published':
      hasIntro = false
      hasSignature = false
      paragraph = `${targetUserName} has confirmed your decision to accept ${titleText}.<br/><br/>
        No further action is required at this time. To review this decision, please visit the manuscript details.<br/><br/>
        Thank you for handling this manuscript on behalf of ${journalName}.`
      break
    case 'he-manuscript-return-with-comments':
      paragraph = `${targetUserName} has responded with comments regarding your editorial recommendation on ${titleText}.<br/><br/>
        ${comments}<br/><br/>
        Please review these comments and take action on the manuscript details page.`
      break
    case 'accepted-reviewers-after-recommendation':
      hasLink = false
      paragraph = `I appreciate any time you may have spent reviewing ${titleText}. However, I am prepared to make an editorial decision and your review is no longer required at this time. I apologize for any inconvenience. <br/><br/>
        If you have comments on this manuscript you believe I should see, please email them to ${staffEmail} as soon as possible. <br/><br/>
        Thank you for your interest and I hope you will consider reviewing for ${journalName} again.`
      break
    case 'pending-reviewers-after-recommendation':
      hasLink = false
      paragraph = `I appreciate any time you may have spent reviewing ${titleText} for ${journalName}. However, I am prepared to make an editorial decision and your review is no longer required at this time. I apologize for any inconvenience. <br/><br/>
        If you have comments on this manuscript you believe I should see, please email them to ${staffEmail} as soon as possible. <br/><br/>
        Thank you for your interest and I hope you will consider reviewing for ${journalName} again.`
      break
    case 'submitted-reviewers-after-publish':
      paragraph = `Thank you for your review of ${titleText} for ${journalName}. After taking into account the reviews and the recommendation of the Handling Editor, I can confirm this article will now be published.<br/><br/>
        No further action is required at this time. To see more details about this decision please view the manuscript details page.<br/><br/>
        If you have any questions about this decision, then please email them to ${staffEmail} as soon as possible. Thank you reviewing for ${journalName}.`
      break
    case 'submitted-reviewers-after-reject':
      paragraph = `Thank you for your review of ${titleText} for ${journalName}. After taking into account the reviews and the recommendation of the Handling Editor, I can confirm this article has now been rejected.<br/><br/>
        No further action is required at this time. To see more details about this decision please view the manuscript details page.<br/><br/>
        If you have any questions about this decision, please email them to ${staffEmail} as soon as possible. Thank you for reviewing for ${journalName}.`
      break
    case 'he-review-submitted':
      paragraph = `We are pleased to inform you that Dr. ${targetUserName} has submitted a review for ${titleText}.<br/><br/>
        To see the full report, please visit the manuscript details page.`
      break
    case 'eic-recommend-to-publish-from-he':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} has recommended accepting ${titleText} for publication.<br/>
        ${comments}<br/>
        To review this decision, please visit the manuscript details page.`
      break
    case 'eic-recommend-to-reject-from-he':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} has recommended rejecting ${titleText}.<br/><br/>
        ${comments}<br/><br/>
        To review this decision, please visit the manuscript details page.`
      break
    case 'eic-request-revision-from-he':
      hasIntro = false
      hasSignature = false
      paragraph = `Dr. ${targetUserName} has asked the authors to submit a revised version of ${titleText}.<br/><br/>
        No action is required at this time. To see the requested changes, please visit the manuscript details page.`
      break
    case 'eqa-manuscript-request-for-approval':
      hasIntro = false
      hasSignature = false
      paragraph = `Manuscript ID ${customId} has passed peer-review and is now ready for EQA. Please click on the link below to either approve or return the manuscript to the Editor in Chief:`
      break
    case 'eqa-manuscript-published':
      hasLink = false
      hasIntro = false
      hasSignature = false
      paragraph = `${upperFirst(titleText)} has been accepted
        for publication by ${eicName}.<br/><br/>
        Please complete QA screening so that manuscript can be sent to production.`
      break
    case 'authors-manuscript-rejected-before-review':
      paragraph = `I regret to inform you that your manuscript has been rejected for publication in ${journalName} for the following reason:<br/><br/>
        ${comments}<br/><br/>
        Thank you for your submission, and please do consider submitting again in the future.`
      hasLink = false
      break
    case 'eic-manuscript-accepted-by-eqs':
      hasIntro = false
      hasSignature = false
      paragraph = `A new ${titleText} has been submitted to ${journalName}.<br/><br/>
        To begin the review process, please visit the manuscript details page.`
      break
    case 'eic-manuscript-returned-by-eqa':
      hasIntro = false
      hasSignature = false
      paragraph = `We regret to inform you that ${titleText} has been returned with comments. Please click the link below to access the manuscript.<br/><br/>
        Comments: ${comments}<br/><br/>`
      break
    case 'submitted-reviewers-after-revision':
      paragraph = `The authors have submitted a new version of ${titleText}, which you reviewed for ${journalName}.<br/><br/>
        As you reviewed the previous version of this manuscript, I would be grateful if you could review this revision and submit a new report by ${expectedDate}.
        To download the updated PDF and proceed with the review process, please visit the manuscript details page.<br/><br/>
        Thank you again for reviewing for ${journalName}.`
      break
    case 'he-new-version-submitted':
      hasIntro = false
      hasSignature = false
      paragraph = `The authors of ${titleText} have submitted a revised version. <br/><br/>
        To review this new submission and proceed with the review process, please visit the manuscript details page.`
      break
    case 'eic-revision-published':
      hasIntro = false
      hasSignature = false
      paragraph = `The authors of ${titleText} have submitted a revised version. <br/><br/>
        To review this new submission and proceed with the review process, please visit the manuscript details page.`
      break
    case 'author-request-to-revision-from-eic':
      paragraph = `In order for ${titleText} to proceed to the review process, there needs to be a revision. <br/><br/>
        ${comments}<br/><br/>
        For more information about what is required, please click the link below.<br/><br/>`
      break
    case 'reviewer-invitation':
      upperContent = `A new version of ${titleText}, has been submitted to ${journalName} for consideration.<div>&nbsp;</div>
      As you reviewed the previous version of this manuscript, I would be delighted if you would agree to review the new version and let me know whether you feel it is suitable for publication.`
      subText = `The manuscript's abstract and author information is below to help you decide. Once you have agreed to review, you will be able to download the full article PDF.`
      lowerContent = `If a potential conflict of interest exists between yourself and either the authors or
        the subject of the manuscript, please decline to handle the manuscript. If a conflict
        becomes apparent during the review process, please let me know at the earliest possible
        opportunity. For more information about our conflicts of interest policies, please
        see:
        <a style="color:#007e92; text-decoration: none;" href="https://www.hindawi.com/ethics/#coi">https://www.hindawi.com/ethics/#coi</a>.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    paragraph,
    hasLink,
    hasIntro,
    hasSignature,
    upperContent,
    subText,
    lowerContent,
    manuscriptText,
  }
}

module.exports = {
  getEmailCopy,
}
