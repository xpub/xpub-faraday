const { get, remove, concat, has } = require('lodash')
const config = require('config')

const {
  Team,
  services,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const {
  deleteFilesS3,
} = require('pubsweet-component-mts-package/src/PackageManager')

const s3Config = get(config, 'pubsweet-component-aws-s3', {})

module.exports = models => async (req, res) => {
  const { collectionId } = req.params
  let collection, fragment
  try {
    collection = await models.Collection.find(collectionId)

    if (has(collection, 'status')) {
      return res.status(400).json({
        error: 'You can only delete manuscripts while in draft.',
      })
    }

    const fragmentId = collection.fragments[0]
    fragment = await models.Fragment.find(fragmentId)

    const authsome = authsomeHelper.getAuthsome(models)

    const canDelete = await authsome.can(req.user, 'DELETE', collection)
    if (!canDelete)
      return res.status(403).json({
        error: 'Unauthorized.',
      })

    const teamHelper = new Team({
      TeamModel: models.Team,
      fragmentId,
      collectionId,
    })
    const teams = await teamHelper.getTeams('fragment')

    await Promise.all(
      teams.map(async team => {
        await Promise.all(
          team.members.map(async member => {
            const user = await models.User.find(member)

            remove(user.teams, teamId => teamId === team.id)

            return user.save()
          }),
        )
        return team.delete()
      }),
    )

    let fileKeys = concat(
      fragment.files.manuscripts,
      fragment.files.coverLetter,
      fragment.files.supplementary,
      { id: fragmentId },
    )

    fileKeys = fileKeys.map(file => file.id)

    if (fileKeys.length > 1) {
      await deleteFilesS3({ fileKeys, s3Config })
    }

    await fragment.delete()

    await collection.delete()

    return res.status(204).send()
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
