const {
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

module.exports = models => async (req, res) => {
  const authsome = authsomeHelper.getAuthsome(models)
  const target = {
    path: req.route.path,
  }

  const collections = await authsome.can(req.user, 'GET', target)

  if (!collections) {
    return res.status(403).json({
      error: 'Unauthorized.',
    })
  }

  res.status(200).json(collections)
}
