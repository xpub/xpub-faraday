const { v4 } = require('uuid')
const config = require('config')
const { get, set } = require('lodash')
const {
  services,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const notifications = require('./notifications/notifications')

const { features = {} } = config

const sendMTSPackage = async (collection, fragment) => {
  const s3Config = get(config, 'pubsweet-component-aws-s3', {})
  const mtsConfig = get(config, 'mts-service', {})
  const { sendPackage } = require('pubsweet-component-mts-package')

  const { journal, xmlParser, ftp } = mtsConfig

  const packageFragment = {
    ...fragment,
    metadata: {
      ...fragment.metadata,
      customId: collection.customId,
    },
  }

  await sendPackage({
    s3Config,
    ftpConfig: ftp,
    config: journal,
    options: xmlParser,
    fragment: packageFragment,
  })
}

module.exports = models => async (req, res) => {
  const { collectionId, fragmentId } = req.params
  let collection, fragment

  try {
    collection = await models.Collection.find(collectionId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Collection and fragment do not match.`,
      })
    fragment = await models.Fragment.find(fragmentId)

    const authsome = authsomeHelper.getAuthsome(models)
    const target = {
      fragment,
      path: req.route.path,
    }
    const canPost = await authsome.can(req.user, 'POST', target)
    if (!canPost)
      return res.status(403).json({
        error: 'Unauthorized.',
      })

    fragment.submitted = Date.now()
    fragment = await fragment.save()

    collection.status = 'technicalChecks'
    set(collection, 'technicalChecks.token', v4())
    await collection.save()

    if (features.mts) {
      await sendMTSPackage(collection, fragment)
    }

    notifications.sendEQSEmail({
      fragment,
      collection,
      UserModel: models.User,
      baseUrl: services.getBaseUrl(req),
    })

    notifications.sendAuthorsEmail({
      fragment,
      collection,
      UserModel: models.User,
      baseUrl: services.getBaseUrl(req),
    })

    return res.status(200).json(fragment)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
