const config = require('config')

const { features = {} } = config

module.exports = {
  execute: async ({
    models,
    notification,
    fragmentHelper,
    collectionHelper,
    newRecommendation,
  }) => {
    const latestRecommendation = fragmentHelper.getLatestRecommendation()
    if (latestRecommendation.recommendation === 'return-to-handling-editor') {
      throw new Error(
        'Cannot make decision to publish after the manuscript has been returned to Handling Editor.',
      )
    }

    await fragmentHelper.addRecommendation(newRecommendation)

    let newStatus = ''
    if (collectionHelper.hasEQA()) {
      newStatus = 'accepted'
      notification.notifyEAWhenEiCMakesFinalDecision()
      notification.notifyAuthorsWhenEiCMakesDecision()
      notification.notifyHEWhenEiCMakesDecision()
      notification.notifyReviewersWhenEiCMakesDecision()
    } else {
      if (features.mts) {
        await collectionHelper.sendToMTS({
          fragmentHelper,
          UserModel: models.User,
          FragmentModel: models.Fragment,
        })
      }

      newStatus = 'inQA'
      await collectionHelper.setTechnicalChecks()
      notification.notifyEAWhenEiCRequestsEQAApproval()
    }

    await collectionHelper.updateStatus({ newStatus })
  },
}
