module.exports = {
  execute: async ({
    notification,
    fragmentHelper,
    collectionHelper,
    newRecommendation,
  }) => {
    await fragmentHelper.addRecommendation(newRecommendation)
    await collectionHelper.updateStatus({ newStatus: 'rejected' })

    notification.notifyAuthorsWhenEiCMakesDecision()
    if (collectionHelper.hasHandlingEditor()) {
      notification.notifyHEWhenEiCMakesDecision()
    }
    if (fragmentHelper.hasReviewers()) {
      notification.notifyReviewersWhenEiCMakesDecision()
    }
  },
}
