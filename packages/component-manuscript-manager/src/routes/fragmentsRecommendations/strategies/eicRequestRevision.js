module.exports = {
  execute: async ({
    fragmentHelper,
    collectionHelper,
    newRecommendation,
    notification,
  }) => {
    if (collectionHelper.hasHandlingEditor()) {
      throw new Error(
        'Cannot make request a revision after a Handling Editor has been assigned.',
      )
    }

    await fragmentHelper.addRevision()
    await collectionHelper.updateStatus({ newStatus: 'revisionRequested' })
    await fragmentHelper.addRecommendation(newRecommendation)
    await notification.notifySAWhenEiCRequestsRevision()
  },
}
