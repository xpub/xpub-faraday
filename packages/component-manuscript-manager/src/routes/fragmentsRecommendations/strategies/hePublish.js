module.exports = {
  execute: async ({
    userId,
    notification,
    fragmentHelper,
    collectionHelper,
    newRecommendation,
  }) => {
    const fragments = await collectionHelper.collection.getFragments()

    if (!collectionHelper.canHEMakeRecommendation(fragments, fragmentHelper)) {
      throw new Error('Cannot publish without at least one reviewer report.')
    }

    const latestUserRecommendation = fragmentHelper.getLatestUserRecommendation(
      userId,
    )
    if (
      latestUserRecommendation &&
      !fragmentHelper.canHEMakeAnotherRecommendation(latestUserRecommendation)
    ) {
      throw new Error('Cannot make another recommendation on this version.')
    }

    await fragmentHelper.addRecommendation(newRecommendation)
    await collectionHelper.updateStatus({ newStatus: 'pendingApproval' })

    notification.notifyReviewersWhenHEMakesRecommendation()
    notification.notifyEiCWhenHEMakesRecommendation()
  },
}
