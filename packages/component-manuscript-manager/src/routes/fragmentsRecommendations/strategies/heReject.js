module.exports = {
  execute: async ({
    userId,
    notification,
    fragmentHelper,
    collectionHelper,
    newRecommendation,
  }) => {
    const latestUserRecommendation = fragmentHelper.getLatestUserRecommendation(
      userId,
    )
    if (
      latestUserRecommendation &&
      !fragmentHelper.canHEMakeAnotherRecommendation(latestUserRecommendation)
    ) {
      throw new Error('Cannot make another recommendation on this version.')
    }

    await fragmentHelper.addRecommendation(newRecommendation)
    await collectionHelper.updateStatus({ newStatus: 'pendingApproval' })

    if (fragmentHelper.hasReviewers()) {
      notification.notifyReviewersWhenHEMakesRecommendation()
    }
    notification.notifyEiCWhenHEMakesRecommendation()
  },
}
