const { get, find, isEmpty, last } = require('lodash')
const { services } = require('pubsweet-component-helper-service')

const Notification = require('../../notifications/notification')

const TECHNICAL_STEPS = {
  EQS: 'eqs',
  EQA: 'eqa',
}

const setNewStatus = (step, agree) => {
  if (step === TECHNICAL_STEPS.EQS) {
    return agree ? 'submitted' : 'rejected'
  } else if (step === TECHNICAL_STEPS.EQA) {
    return agree ? 'accepted' : 'pendingApproval'
  }
}

module.exports = ({ Collection, Fragment, User }) => async (req, res) => {
  const { collectionId } = req.params
  const { token, agree, step, comments, customId } = req.body

  try {
    const collections = await Collection.all()
    const collection = await Collection.find(collectionId)
    const technicalCheckToken = get(collection, `technicalChecks.token`)

    if (step === TECHNICAL_STEPS.EQS) {
      if (agree && !customId) {
        return res.status(400).json({
          error: 'A manuscript ID is required.',
        })
      }

      if (agree && find(collections, c => c.customId === customId)) {
        return res.status(400).json({
          error: `CustomID already assigned to a manuscript.`,
        })
      }
    }

    if (isEmpty(technicalCheckToken)) {
      return res.status(400).json({
        error: `Manuscript already handled.`,
      })
    }

    if (technicalCheckToken !== token) {
      return res.status(400).json({
        error: `Invalid token.`,
      })
    }

    if (agree) {
      if (step === TECHNICAL_STEPS.EQA) {
        collection.technicalChecks.eqa = true
      }
      if (step === TECHNICAL_STEPS.EQS) {
        collection.customId = customId
      }
    }

    delete collection.technicalChecks.token
    collection.status = setNewStatus(step, agree)
    await collection.save()

    const fragment = await Fragment.find(last(collection.fragments))
    const notification = new Notification({
      fragment,
      collection,
      UserModel: User,
      baseUrl: services.getBaseUrl(req),
      newRecommendation: {
        recommendation: 'publish',
        recommendationType: 'editorRecommendation',
      },
    })

    if (step === TECHNICAL_STEPS.EQA) {
      const hasPassedEQA = get(collection, 'technicalChecks.eqa')
      if (hasPassedEQA) {
        // the EA accepted the manuscript so we need to notify users of the final publication decision
        notification.notifyAuthorsWhenEiCMakesDecision()
        const hasPeerReview = !isEmpty(collection.handlingEditor)
        if (hasPeerReview) {
          notification.notifyHEWhenEiCMakesDecision()
          notification.notifyReviewersWhenEiCMakesDecision()
        }
      } else {
        notification.notifyEiCWhenEQARejectsManuscript(comments)
      }
    } else if (agree) {
      notification.notifyEiCWhenEQSAcceptsManuscript()
    }

    return res.status(200).json(collection)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Collection')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
