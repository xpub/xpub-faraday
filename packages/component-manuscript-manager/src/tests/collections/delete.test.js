process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { cloneDeep } = require('lodash')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService

jest.mock('pubsweet-component-mts-package/src/PackageManager', () => ({
  deleteFilesS3: jest.fn(),
}))
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const path = '../routes/collections/delete'
const route = {
  path: '/api/collections/:collectionId/',
}
describe('Delete collections route handler', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  it('should return an error when deleting a collection which is not in draft', async () => {
    const { admin } = testFixtures.users
    const { collection } = testFixtures.collections

    const res = await requests.sendRequest({
      userId: admin.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toBe('You can only delete manuscripts while in draft.')
  })

  it('should return success when deleting a draft collection', async () => {
    const { admin } = testFixtures.users
    const { collection } = testFixtures.collections
    delete collection.status

    const res = await requests.sendRequest({
      userId: admin.id,
      route,
      models,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(204)
  })
})
