process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { cloneDeep, size } = require('lodash')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const path = '../routes/collections/get'
const route = {
  path: '/api/collections',
}
describe('Get collections route handler', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  it('should return collections with the latest fragments if the request user is HE', async () => {
    const { handlingEditor } = testFixtures.users
    const { recommendations } = testFixtures.fragments.fragment

    const res = await requests.sendRequest({
      userId: handlingEditor.id,
      route,
      models,
      path,
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())

    expect(data).toHaveLength(handlingEditor.teams.length)
    expect(data[0].type).toEqual('collection')
    expect(data[0]).toHaveProperty('currentVersion')
    expect(data[0]).toHaveProperty('visibleStatus')
    expect(data[0].currentVersion.type).toEqual('fragment')
    expect(data[0].currentVersion.recommendations).toHaveLength(
      recommendations.length,
    )
  })

  it('should return collections with the latest fragments if the request user is reviewer', async () => {
    const { answerReviewer } = testFixtures.users

    const res = await requests.sendRequest({
      userId: answerReviewer.id,
      route,
      models,
      path,
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data).toHaveLength(2)
    expect(data[0].type).toEqual('collection')
    expect(data[0].currentVersion.authors[0]).not.toHaveProperty('email')
  })

  it('should return the latest fragment with invitation if the request user is reviewer', async () => {
    const { reviewer1 } = testFixtures.users

    const res = await requests.sendRequest({
      userId: reviewer1.id,
      route,
      models,
      path,
    })
    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data).toHaveLength(1)

    expect(data[0].type).toEqual('collection')
    expect(data[0].currentVersion.recommendations).toHaveLength(2)
    expect(data[0].currentVersion.authors[0]).not.toHaveProperty('email')
    expect(data[0].fragments).toHaveLength(2)
    expect(data[0].currentVersion.id).toEqual(data[0].fragments[0])
  })

  it('should return an error if the request user is unauthenticated', async () => {
    const res = await requests.sendRequest({
      route,
      models,
      path,
    })

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())

    expect(data.error).toEqual('Unauthorized.')
  })

  it('should return all collections with the latest fragments if the request user is admin/EiC', async () => {
    const { editorInChief } = testFixtures.users
    const { recommendations } = testFixtures.fragments.fragment

    const res = await requests.sendRequest({
      userId: editorInChief.id,
      route,
      models,
      path,
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())

    delete testFixtures.collections.noFragmentsCollection

    expect(data).toHaveLength(size(testFixtures.collections))
    expect(data[0].type).toEqual('collection')
    expect(data[0]).toHaveProperty('visibleStatus')
    expect(data[0].currentVersion.recommendations).toHaveLength(
      recommendations.length,
    )
  })
})
