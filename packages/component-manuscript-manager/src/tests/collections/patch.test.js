process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { cloneDeep } = require('lodash')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService

const reqBody = {
  comments: {},
}

const path = '../routes/collections/patch'
const route = {
  path: '/api/collections/:collectionId/archive',
}

describe('Patch colection route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })

  it('should return an error if the manuscript does not exist', async () => {
    const { admin } = testFixtures.users
    const res = await requests.sendRequest({
      userId: admin.id,
      body,
      models,
      route,
      path,
      params: {
        collectionId: '123',
      },
    })

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toBe('Collection not found')
  })

  it('should return success when deleting a manuscript as admin', async () => {
    const { admin } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'underReview'
    const res = await requests.sendRequest({
      userId: admin.id,
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(200)
    expect(JSON.parse(res._getData()).status).toEqual('deleted')
  })

  it('should return an error when deleting a manuscript as EiC', async () => {
    const { editorInChief } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'underReview'
    const res = await requests.sendRequest({
      userId: editorInChief.id,
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toBe('Unauthorized')
  })

  it('should return an error when deleting a manuscript as author', async () => {
    const { author } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'underReview'
    const res = await requests.sendRequest({
      userId: author.id,
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toBe('Unauthorized')
  })

  it('should return an error when deleting a manuscript while in draft', async () => {
    const { admin } = testFixtures.users
    const { collection } = testFixtures.collections
    delete collection.status
    const res = await requests.sendRequest({
      userId: admin.id,
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toBe('You cannot delete manuscripts while in draft.')
  })

  it('should return an error when deleting a manuscript already deleted', async () => {
    const { admin } = testFixtures.users
    const { collection } = testFixtures.collections
    collection.status = 'deleted'
    const res = await requests.sendRequest({
      userId: admin.id,
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toBe('Manuscript already deleted')
  })
})
