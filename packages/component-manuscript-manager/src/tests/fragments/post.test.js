process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))
jest.mock('pubsweet-component-mts-package')
const reqBody = {}

const path = '../routes/fragments/post'
const route = {
  path: '/api/collections/:collectionId/fragments/:fragmentId/submit',
}
describe('Post fragments route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return success when the parameters are correct', async () => {
    const { user } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const res = await requests.sendRequest({
      body,
      userId: user.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(200)
  })
  it('should return an error when the fragmentId does not match the collectionId', async () => {
    const { user } = testFixtures.users
    const { collection } = testFixtures.collections
    const { noParentFragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      body,
      userId: user.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: noParentFragment.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Collection and fragment do not match.')
  })
  it('should return an error when the collection does not exist', async () => {
    const { user } = testFixtures.users
    const { fragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      body,
      userId: user.id,
      models,
      route,
      path,
      params: {
        collectionId: 'invalid-id',
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Item not found')
  })
  it('should return an error when the user is inactive', async () => {
    const { inactiveUser } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const res = await requests.sendRequest({
      body,
      userId: inactiveUser.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
      },
    })

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
})
