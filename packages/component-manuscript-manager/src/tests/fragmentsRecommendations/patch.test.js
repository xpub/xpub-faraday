process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const chance = new Chance()
const reqBody = {
  recommendation: 'publish',
  comments: [
    {
      content: chance.paragraph(),
      public: chance.bool(),
      files: [
        {
          id: chance.guid(),
          name: 'file.pdf',
          size: chance.natural(),
        },
      ],
    },
  ],
  recommendationType: 'review',
}

const path = '../routes/fragmentsRecommendations/patch'
const route = {
  path:
    '/api/collections/:collectionId/fragments/:fragmentId/recommendations/:recommendationId',
}
describe('Patch fragments recommendations route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return success when the parameters are correct', async () => {
    const { answerReviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const recommendation = fragment.recommendations[0]
    const res = await requests.sendRequest({
      body,
      userId: answerReviewer.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
        recommendationId: recommendation.id,
      },
    })

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.userId).toEqual(answerReviewer.id)
  })
  it('should return an error when the fragmentId does not match the collectionId', async () => {
    const { reviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const recommendation = fragment.recommendations[0]

    collection.fragments.length = 0
    const res = await requests.sendRequest({
      body,
      userId: reviewer.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
        recommendationId: recommendation.id,
      },
    })

    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Collection and fragment do not match.')
  })
  it('should return an error when the collection does not exist', async () => {
    const { reviewer } = testFixtures.users
    const { fragment } = testFixtures.fragments
    const recommendation = fragment.recommendations[0]
    const res = await requests.sendRequest({
      body,
      userId: reviewer.id,
      models,
      route,
      path,
      params: {
        collectionId: 'invalid-id',
        fragmentId: fragment.id,
        recommendationId: recommendation.id,
      },
    })

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Item not found')
  })
  it('should return an error when the recommendation does not exist', async () => {
    const { reviewer } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const res = await requests.sendRequest({
      body,
      userId: reviewer.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
        recommendationId: 'invalid-id',
      },
    })

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Recommendation not found.')
  })
  it('should return an error when the request user is not a reviewer', async () => {
    const { user } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      body,
      userId: user.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
        recommendationId: fragment.recommendations[0].id,
      },
    })

    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
  it('should return an error when the user is inactive', async () => {
    const { inactiveUser } = testFixtures.users
    const { collection } = testFixtures.collections
    const { fragment } = testFixtures.fragments
    const res = await requests.sendRequest({
      body,
      userId: inactiveUser.id,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
        fragmentId: fragment.id,
        recommendationId: fragment.recommendations[0].id,
      },
    })
    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
})
