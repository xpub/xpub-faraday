const httpMocks = require('node-mocks-http')

const sendRequest = async ({
  body = {},
  userId,
  route,
  models,
  path,
  params = {},
  query = {},
}) => {
  const req = httpMocks.createRequest({
    body,
  })
  req.user = userId
  req.route = route
  req.params = params
  req.query = query
  const res = httpMocks.createResponse()
  await require(path)(models)(req, res)
  return res
}

module.exports = { sendRequest }
