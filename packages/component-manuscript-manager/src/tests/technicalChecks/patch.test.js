process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const reqBody = {
  step: 'eqs',
  agree: true,
  customId: '1234567',
}

const path = '../routes/technicalChecks/patch'
const route = {
  path: '/api/collections/:collectionId/status',
}
describe('Patch technical checks route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })

  it('should return error when no customId is provided', async () => {
    const { collection } = testFixtures.collections
    body.token = collection.technicalChecks.token
    delete body.customId
    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(400)
    expect(res._getData()).toBe(
      JSON.stringify({ error: 'A manuscript ID is required.' }),
    )
  })

  it('should return error when a customId already exists', async () => {
    const { collection } = testFixtures.collections
    body.token = collection.technicalChecks.token
    body.customId = '1234568'
    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(400)
    expect(res._getData()).toBe(
      JSON.stringify({ error: 'CustomID already assigned to a manuscript.' }),
    )
  })

  it('should return success when EQS is accepted', async () => {
    const { collection } = testFixtures.collections
    body.token = collection.technicalChecks.token
    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(200)
    expect(JSON.parse(res._getData()).customId).toBe('1234567')
  })

  it('should return success when the EQS is rejected', async () => {
    const { collection } = testFixtures.collections
    body.token = collection.technicalChecks.token
    body.agree = false

    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })
    expect(res.statusCode).toBe(200)
    expect(JSON.parse(res._getData()).status).toBe('rejected')
  })

  it('should return success when the EQA is accepted', async () => {
    const { collection } = testFixtures.collections
    body.token = collection.technicalChecks.token
    body.step = 'eqa'

    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(200)
  })

  it('should return success when the EQA is returned with comments', async () => {
    const { collection } = testFixtures.collections
    body.token = collection.technicalChecks.token
    body.agree = false
    body.step = 'eqa'
    body.comments = 'suspicion of plagiarism'

    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })

    expect(res.statusCode).toBe(200)
  })

  it('should return an error when the collection does not exist', async () => {
    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: 'invalid-id',
      },
    })

    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Collection not found')
  })

  it('should return an error when the collection is already checked', async () => {
    const { collection } = testFixtures.collections
    delete collection.technicalChecks

    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Manuscript already handled.')
  })

  it('should return an error when the collection token is invalid', async () => {
    const { collection } = testFixtures.collections
    body.token = 'invalid-token'

    const res = await requests.sendRequest({
      body,
      models,
      route,
      path,
      params: {
        collectionId: collection.id,
      },
    })
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Invalid token.')
  })
})
