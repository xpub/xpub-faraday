import React from 'react'
import {
  EditorialReportCard,
  ContextualBox,
  withFilePreview,
  withFileDownload,
} from 'pubsweet-component-faraday-ui'

const EditorialCommentCard = ({
  journal,
  reports = [],
  toggle,
  expanded,
  collection,
}) => (
  <ContextualBox
    data-test-id="contextual-box-editorial-comments"
    expanded={expanded}
    label="Editorial Comments"
    mb={2}
    scrollIntoView
    toggle={toggle}
  >
    {reports.map(report => (
      <EditorialReportCard
        collection={collection}
        journal={journal}
        key={report.id}
        privateLabel="Message For Editorial Team"
        publicLabel="Message For Author"
        report={report}
      />
    ))}
  </ContextualBox>
)

export default withFileDownload(withFilePreview(EditorialCommentCard))
