import React, { Fragment } from 'react'
import { get, isEmpty } from 'lodash'
import { withProps } from 'recompose'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { Expandable } from '../molecules'

const ResponseToReviewers = ({
  responseToReviewers: { comments = '', files },
}) => (
  <Root>
    <Expandable label="Response to reviewers" startExpanded>
      {comments && (
        <Fragment>
          <Label>Comments to reviewers</Label>
          <Text>{comments}</Text>
        </Fragment>
      )}
      {!isEmpty(files) && (
        <Fragment>
          <Label>Files</Label>
        </Fragment>
      )}
    </Expandable>
  </Root>
)

export default withProps(({ version }) => ({
  responseToReviewers: {
    comments: get(version, 'commentsToReviewers'),
    files: get(version, 'files.responseToReviewers'),
  },
}))(ResponseToReviewers)

// #region styled-components
const defaultText = css`
  color: ${th('colorPrimary')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`
const Root = styled.div`
  background-color: ${th('colorBackground')};
  margin-top: calc(${th('subGridUnit')} * 2);
  transition: height 0.3s;
`

const Label = styled.div`
  ${defaultText};
  text-transform: uppercase;
  i {
    text-transform: none;
    margin-left: ${th('gridUnit')};
  }
`

const Text = styled.div`
  ${defaultText};
  span {
    margin-left: calc(${th('subGridUnit')}*3);
  }
`
// #endregion
