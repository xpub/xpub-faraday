import React, { Fragment } from 'react'
import { compose } from 'recompose'
import { get, isEmpty } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import styled, { css } from 'styled-components'
import { DateParser } from 'pubsweet-components-faraday/src/components'

import { ShowMore } from '.'

const ReviewReportCard = ({
  i = 0,
  report = {},
  showBorder = false,
  journal: { recommendations },
}) => {
  const hasReviewer = !isEmpty(get(report, 'user'))
  const { submittedOn, comments = [], user } = report
  const publicComment = comments.find(c => c.public)
  const privateComment = comments.find(c => !c.public)
  const recommendationLabel = get(
    recommendations.find(r => report.recommendation === r.value),
    'label',
  )

  return (
    <Root showBorder={showBorder}>
      {hasReviewer && (
        <Row>
          <Text>
            <b>Reviewer {i}</b>
            <Underline>{user.name}</Underline>
            <span>{user.email}</span>
          </Text>
          <DateParser timestamp={submittedOn}>
            {timestamp => <Text>{timestamp}</Text>}
          </DateParser>
        </Row>
      )}
      <Row>
        <Label>Recommendation</Label>
        {!hasReviewer && (
          <DateParser timestamp={submittedOn}>
            {timestamp => <Text>{timestamp}</Text>}
          </DateParser>
        )}
      </Row>
      <Row>
        <Text>{recommendationLabel}</Text>
      </Row>
      {get(publicComment, 'content') && (
        <Fragment>
          <Spacing />
          <Row left>
            <Label>Report Text</Label>
          </Row>
          <Row>
            <ShowMore
              content={publicComment.content}
              id={`public-content-${i}`}
            />
          </Row>
        </Fragment>
      )}

      {get(publicComment, 'files') &&
        !!publicComment.files.length && (
          <Fragment>
            <Spacing />
            <Row left>
              <Label>Files</Label>
            </Row>
          </Fragment>
        )}

      {get(privateComment, 'content') && (
        <Fragment>
          <Spacing />
          <Row left>
            <Label>Confidential Note</Label>
          </Row>
          <Row>
            <ShowMore
              content={privateComment.content}
              id={`private-content-${i}`}
            />
          </Row>
        </Fragment>
      )}
    </Root>
  )
}

export default compose(withJournal)(ReviewReportCard)

// #region styled-components
const defaultText = css`
  color: ${th('colorPrimary')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const cardStyle = css`
  margin: 0 auto calc(${th('subGridUnit')}*3);
  border: ${th('borderDefault')};
  padding: calc(${th('subGridUnit')}*2);
`

const Root = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  border: none;
  padding: 0;
  ${({ showBorder }) => (showBorder ? cardStyle : null)};
`
const Text = styled.div`
  ${defaultText};
  span {
    margin-left: calc(${th('subGridUnit')}*3);
  }
`
const Underline = styled.span`
  text-decoration: underline;
`
const Label = styled.div`
  ${defaultText};
  text-transform: uppercase;
  i {
    text-transform: none;
    margin-left: ${th('gridUnit')};
  }
`

const Spacing = styled.div`
  margin-top: ${th('gridUnit')};
  flex: 1;
`

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex: 1;
  box-sizing: border-box;
  flex-wrap: wrap;
  justify-content: ${({ left }) => (left ? 'left' : 'space-between')};
  ${defaultText};
`

// #endregion
