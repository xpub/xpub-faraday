import { get } from 'lodash'
import { reduxForm } from 'redux-form'
import { withJournal } from 'xpub-journal'
import { withModal } from 'pubsweet-component-modal/src/components'
import { compose, withHandlers, withProps, withState } from 'recompose'

import {
  MultiAction,
  ReviewerReportForm,
  handleError,
  withFetching,
  withFilePreview,
  withFileDownload,
} from 'pubsweet-component-faraday-ui'

import {
  uploadFile,
  deleteFile,
  getSignedUrl,
} from 'pubsweet-components-faraday/src/redux/files'

import {
  onReviewSubmit,
  onReviewChange,
  reviewerReportValidate,
  parseReviewResponseToForm,
} from './utils'

// #region export
export default compose(
  withJournal,
  withFetching,
  withProps(({ review = {}, formValues = {} }) => ({
    getSignedUrl,
    initialValues: parseReviewResponseToForm(review),
  })),
  withFilePreview,
  withFileDownload,
  withModal(({ isFetching, modalKey }) => ({
    modalKey,
    isFetching,
    modalComponent: MultiAction,
  })),
  withState(
    'hasNote',
    'setNote',
    ({ review }) => get(review, 'comments', []).length === 2,
  ),
  withHandlers({
    addNote: ({ setNote }) => () => {
      setNote(true)
    },
    removeNote: ({ setNote, changeForm }) => () => {
      changeForm('reviewerReport', 'confidential', '')
      setNote(false)
    },
    addFile: ({ version, changeForm, setFetching, setError }) => file => {
      setFetching(true)
      setError('')
      uploadFile({ file, type: 'review', fragment: version })
        .then(file => {
          setFetching(false)
          changeForm('reviewerReport', 'file', file)
        })
        .catch(err => {
          setFetching(false)
          handleError(setError)(err)
        })
    },
    removeFile: ({ changeForm, setError, setFetching }) => file => {
      setFetching(true)
      setError('')
      deleteFile({ fileId: file.id })
        .then(r => {
          setFetching(false)
          changeForm('reviewerReport', 'file', null)
        })
        .catch(err => {
          setFetching(false)
          handleError(setError)(err)
        })
    },
  }),
  reduxForm({
    form: 'reviewerReport',
    onChange: onReviewChange,
    onSubmit: onReviewSubmit,
    enableReinitialize: false,
    keepDirtyOnReinitialize: true,
    validate: reviewerReportValidate,
  }),
)(ReviewerReportForm)
// #endregion
