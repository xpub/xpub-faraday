import React from 'react'
import { compose, withProps } from 'recompose'
import { get } from 'lodash'
import {
  ReviewerReport,
  ContextualBox,
  withFilePreview,
  withFileDownload,
  Text,
  Row,
  indexReviewers,
} from 'pubsweet-component-faraday-ui'

const SubmittedReports = ({ reports }) => (
  <Row fitContent justify="flex-end">
    <Text customId mr={1 / 2}>
      {reports}
    </Text>
    <Text mr={1 / 2} pr={1 / 2} secondary>
      submitted
    </Text>
  </Row>
)

const ReviewerReports = ({
  journal,
  reports,
  previewFile,
  downloadFile,
  isLatestVersion,
  currentUser,
  token,
  invitations,
  reviwerReports,
  ...rest
}) => (
  <ContextualBox
    label={isLatestVersion ? 'Your Report' : 'Reviewer Reports'}
    mb={2}
    rightChildren={<SubmittedReports reports={reports.length} />}
    startExpanded
    {...rest}
  >
    {reports.map(report => (
      <ReviewerReport
        currentUser={currentUser}
        journal={journal}
        key={report.id}
        onDownload={downloadFile}
        onPreview={previewFile}
        report={report}
        reviewerNumber={report.reviewerNumber}
        showOwner={report.userId === currentUser.id}
      />
    ))}
  </ContextualBox>
)

export default compose(
  withFileDownload,
  withFilePreview,
  withProps(
    ({
      invitations = [],
      publonReviewers = [],
      reviewerReports = [],
      currentUser,
      isLatestVersion,
    }) => ({
      token: get(currentUser, 'token', ''),
      publonReviewers,
      invitations: invitations.map(i => ({
        ...i,
        review: reviewerReports.find(r => r.userId === i.userId),
      })),
      reports: isLatestVersion
        ? indexReviewers(
            reviewerReports.filter(
              r => r.submittedOn && r.userId === currentUser.id,
            ),
            invitations,
          )
        : indexReviewers(
            reviewerReports.filter(r => r.submittedOn),
            invitations,
          ),
    }),
  ),
)(ReviewerReports)
