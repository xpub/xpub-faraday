import React, { Fragment } from 'react'
import { Icon } from '@pubsweet/ui'
import styled from 'styled-components'

const ShowMore = ({ content = '', words = 50, id = 'read-more-target' }) => {
  const contentSize = content.split(' ').length
  return (
    <Root>
      {contentSize > words && (
        <Fragment>
          <Input id={id} role="button" type="checkbox" />
          <Control htmlFor={id}>
            <div>
              <Icon primary>chevron-down</Icon> Show All
            </div>
            <div>
              <Icon primary>chevron-up</Icon> Show Less
            </div>
          </Control>
        </Fragment>
      )}
      <Container bigger={contentSize > words}>{content}</Container>
    </Root>
  )
}

export default ShowMore

// #region styled-components
const Root = styled.div`
  display: flex;
  flex-direction: column;
`
const Container = styled.section`
  height: ${({ bigger }) => (bigger ? '100px' : 'initial')};
  overflow: hidden;
  order: -1;
`

const Input = styled.input`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  width: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;

  &:checked {
    ~ section {
      height: initial;
      overflow: unset;
    }
    ~ label {
      div:first-of-type {
        display: none;
      }
      div:last-of-type {
        display: inline-block;
      }
    }
  }
  ~ label {
    div:first-of-type {
      display: inline-block;
    }
    div:last-of-type {
      display: none;
    }
  }
`

const Control = styled.label`
  cursor: pointer;
  width: fit-content;
  div {
    text-decoration: underline;
    text-transform: uppercase;
    > span {
      vertical-align: sub;
      padding-left: 0;
    }
  }
  &:hover {
    opacity: 0.7;
  }
`
// #endregion
