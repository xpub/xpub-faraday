export { default as Authors } from './Authors'
export { default as ShowMore } from './ShowMore'
export { default as SideBarRoles } from './SideBarRoles'
export { default as ManuscriptPage } from './ManuscriptPage'
export { default as EditorialComment } from './EditorialComment'
export { default as ReviewReportCard } from './ReviewReportCard'
export { default as ManuscriptLayout } from './ManuscriptLayout'
export {
  default as ManuscriptVersion,
} from '../../../component-faraday-ui/src/manuscriptDetails/ManuscriptVersion'
export { default as EditorialComments } from './EditorialComments'
export { default as ReviewReportsList } from './ReviewReportsList'
export { default as ReviewerReportForm } from './ReviewerReportForm'
export { default as ResponseToReviewers } from './ResponseToReviewers'
