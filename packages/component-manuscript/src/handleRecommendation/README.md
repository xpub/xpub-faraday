## Hindawi Handling Recommendation HOC.

Injects `createRecommendation` and `onEditorialRecommendation` handlers as props.

### withHandleRecommendation props

`recommendationHandler` namespace contains the following fields:

| Name                      | Type                                   | Description                                   |
| :------------------------ | :------------------------------------- | :-------------------------------------------- |
| createRecommendation      | `(reduxFormValues, modalProps) => any` | creates a recommendation for the manuscript   |
| onEditorialRecommendation | `(reduxFormValues, modalProps) => any` | handles the recommendation for the manuscript |

_Note: The functions must be used withing a modal._

```javascript
const HERecommendationPanel = ({ createRecommendation }) => (
  <Modal>
    <span>Recommend the manuscript for:</span>
    <select>
      <option>Approve</option>
      <option>Reject</option>
      <option>Minor revision</option>
      <option>Major revision</option>
    </select>
    <button
      onClick={() =>
        createRecommendation(reduxFormValues, { ...modalProps, setFetching })
      }
    >
      Submit
    </button>
  </Modal>
)

const EICDecisionPanel = ({ onEditorialRecommendation }) => (
  <Modal>
    <span>Take decision to:</span>
    <select>
      <option>Approve</option>
      <option>Reject</option>
      <option>Minor revision</option>
      <option>Major revision</option>
    </select>
    <button
      onClick={() =>
        onEditorialRecommendation(reduxFormValues, {
          ...modalProps,
          setFetching,
        })
      }
    >
      Submit
    </button>
  </Modal>
)
```
