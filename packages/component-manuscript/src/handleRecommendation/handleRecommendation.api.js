import { create } from 'pubsweet-client/src/helpers/api'

export const createRecommendation = ({
  fragmentId,
  collectionId,
  recommendation,
}) =>
  create(
    `/collections/${collectionId}/fragments/${fragmentId}/recommendations`,
    recommendation,
  )
