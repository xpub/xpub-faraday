export const parseEicDecision = ({ decision, message }) => ({
  recommendation: decision,
  recommendationType: 'editorRecommendation',
  comments: [
    {
      public: decision !== 'return-to-handling-editor',
      content: message,
    },
  ],
})
