## Hindawi Handling Editor Invite HOC.

Injects `assignHE`, `revokeHE` and `onHEResponse` handlers as props.

### withInviteHandlingEditor props

`inviteHandlingEditor` namespace contains the following fields:

| Name         | Type                                   | Description                                      |
| :----------- | :------------------------------------- | :----------------------------------------------- |
| assignHE     | `(email, modalProps) => any`           | sends an invitation to the handling editor       |
| revokeHE     | `(invitationId, modalProps) => any`    | revokes a sent invitation to the handling editor |
| onHEResponse | `(reduxFormValues, modalProps) => any` | handles the handling editor's response           |

_Note: The functions must be used withing a modal._

```javascript
const EditorInChiefPanel = ({ assignHE, revokeHE }) => (
  <Modal>
    <span>Handlin d'Editor</span>
    <button onClick={() => assignHE(email, { ...modalProps, setFetching })}>
      Resend Invitation
    </button>
    <button
      onClick={() => revokeHE(invitationId, { ...modalProps, setFetching })}
    >
      Cancel Invitation
    </button>
  </Modal>
)

const HandlingEditorPanel = ({ onHeResponse }) => (
  <Modal>
    <span>Accept invitation?</span>
    <button
      onClick={() =>
        onHeResponse(reduxFormValues, { ...modalProps, setFetching })
      }
    >
      Yes
    </button>
    <button
      onClick={() =>
        onHeResponse(reduxFormValues, { ...modalProps, setFetching })
      }
    >
      No
    </button>
  </Modal>
)
```
