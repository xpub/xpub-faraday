import { compose, withHandlers, withProps } from 'recompose'
import { get, pick } from 'lodash'
import { withRouter } from 'react-router-dom'
import { handleError, withFetching } from 'pubsweet-component-faraday-ui'
import {
  handlingEditorDecision,
  assignHandlingEditor,
  revokeHandlingEditor,
} from './inviteHE.api'

export default compose(
  withFetching,
  withRouter,
  withHandlers({
    assignHE: ({
      fetchUpdatedCollection,
      collection: { id: collectionId },
    }) => (email, modalProps) =>
      assignHandlingEditor({
        email,
        collectionId,
      })
        .then(() => {
          fetchUpdatedCollection()
          modalProps.hideModal()
        })
        .catch(handleError(modalProps.setModalError)),
    revokeHE: ({ getCollection, collection: { id: collectionId } }) => (
      invitationId,
      modalProps,
    ) =>
      revokeHandlingEditor({
        invitationId,
        collectionId,
      })
        .then(() => {
          getCollection({ id: collectionId })
          modalProps.hideModal()
        })
        .catch(handleError(modalProps.setModalError)),
    onHEResponse: ({
      history,
      collection,
      pendingHEInvitation,
      fetchUpdatedCollection,
    }) => (values, { hideModal, setModalError, setFetching }) => {
      const isAccepted = get(values, 'decision', 'decline') === 'accept'
      setFetching(true)
      return handlingEditorDecision({
        isAccepted,
        collectionId: collection.id,
        reason: get(values, 'reason', ''),
        invitationId: pendingHEInvitation.id,
      })
        .then(() => {
          setFetching(false)
          hideModal()
          if (isAccepted) {
            fetchUpdatedCollection()
          } else {
            history.replace('/')
          }
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
  }),
  withProps(props => ({
    inviteHandlingEditor: {
      ...pick(props, ['assignHE', 'revokeHE', 'onHEResponse']),
    },
  })),
)
