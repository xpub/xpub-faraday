import { get, pick } from 'lodash'
import { withRouter } from 'react-router-dom'
import { compose, withHandlers, withProps } from 'recompose'
import { handleError, withFetching } from 'pubsweet-component-faraday-ui'
import { inviteReviewer, revokeReviewer, reviewerDecision } from './invite.api'

export default compose(
  withFetching,
  withRouter,
  withHandlers({
    onReviewerResponse: ({
      history,
      fragment,
      collection,
      fetchUpdatedCollection,
      pendingReviewerInvitation,
    }) => (values, { hideModal, setModalError, setFetching }) => {
      const isAccepted = get(values, 'decision', 'decline') === 'accept'
      setFetching(true)
      reviewerDecision({
        agree: isAccepted,
        fragmentId: fragment.id,
        collectionId: collection.id,
        invitationId: pendingReviewerInvitation.id,
      })
        .then(() => {
          setFetching(false)
          hideModal()
          if (isAccepted) {
            fetchUpdatedCollection()
          } else {
            history.replace('/')
          }
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
    onInviteReviewer: ({ collection, fragment, fetchUpdatedCollection }) => (
      values,
      { hideModal, setModalError, setFetching },
    ) => {
      setFetching(true)
      inviteReviewer({
        reviewerData: values,
        fragmentId: fragment.id,
        collectionId: collection.id,
      })
        .then(() => {
          setFetching(false)
          hideModal()
          fetchUpdatedCollection()
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
    onInvitePublonReviewer: ({
      setError,
      fragment,
      collection,
      clearError,
      getPublonsReviewers,
      fetchUpdatedCollection,
      setFetching: setListFetching,
    }) => (reviewerData, { hideModal, setModalError, setFetching }) => {
      setFetching(true)
      inviteReviewer({
        reviewerData,
        isPublons: true,
        fragmentId: fragment.id,
        collectionId: collection.id,
      })
        .then(() => {
          setFetching(false)
          hideModal()
          fetchUpdatedCollection()
          getPublonsReviewers(fragment.id)
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
    onResendReviewerInvite: ({
      fragment,
      collection,
      fetchUpdatedCollection,
    }) => (email, { hideModal, setFetching, setModalError }) => {
      setFetching(true)
      inviteReviewer({
        reviewerData: {
          email,
          role: 'reviewer',
        },
        fragmentId: fragment.id,
        collectionId: collection.id,
      })
        .then(() => {
          setFetching(false)
          hideModal()
          fetchUpdatedCollection()
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
    onRevokeReviewerInvite: ({
      fragment,
      collection,
      fetchUpdatedCollection,
    }) => (invitationId, { hideModal, setFetching, setModalError }) => {
      setFetching(true)
      revokeReviewer({
        invitationId,
        fragmentId: fragment.id,
        collectionId: collection.id,
      })
        .then(() => {
          setFetching(false)
          hideModal()
          fetchUpdatedCollection()
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
  }),
  withProps(props => ({
    inviteReviewer: {
      ...pick(props, [
        'onInviteReviewer',
        'onReviewerResponse',
        'onInvitePublonReviewer',
        'onResendReviewerInvite',
        'onRevokeReviewerInvite',
      ]),
    },
  })),
)
