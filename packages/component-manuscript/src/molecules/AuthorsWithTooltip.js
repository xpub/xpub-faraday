import React from 'react'
import 'react-tippy/dist/tippy.css'
import { Tooltip } from 'react-tippy'
import { th } from '@pubsweet/ui-toolkit'
import styled, { ThemeProvider, withTheme, css } from 'styled-components'

const DefaultTooltip = ({
  theme,
  email,
  authorName,
  affiliation,
  isSubmitting,
  isCorresponding,
}) => (
  <ThemeProvider theme={theme}>
    <TooltipRoot>
      <TooltipRow>
        <Name>{authorName}</Name>
        {isSubmitting && <SpecialAuthor>Submitting Author</SpecialAuthor>}
        {isCorresponding &&
          !isSubmitting && <SpecialAuthor>Corresponding Author</SpecialAuthor>}
      </TooltipRow>
      <TooltipRow>
        <AuthorDetails>{email}</AuthorDetails>
      </TooltipRow>
      <TooltipRow>
        <AuthorDetails>{affiliation}</AuthorDetails>
      </TooltipRow>
    </TooltipRoot>
  </ThemeProvider>
)

const DefaultLabel = ({
  arr,
  index,
  lastName,
  firstName,
  isSubmitting,
  isCorresponding,
}) => (
  <Author>
    <AuthorName>{`${firstName} ${lastName}`}</AuthorName>
    {isSubmitting && <AuthorStatus>SA</AuthorStatus>}
    {isCorresponding && !isSubmitting && <AuthorStatus>CA</AuthorStatus>}
    {arr.length - 1 === index ? '' : ','}
  </Author>
)

const TooltipComponent = ({ children, component: Component, ...rest }) => (
  <Tooltip arrow html={<Component {...rest} />} position="bottom">
    {children}
  </Tooltip>
)

const AuthorTooltip = withTheme(TooltipComponent)

const AuthorsWithTooltip = ({
  authors = [],
  theme,
  tooltipComponent = DefaultTooltip,
  labelComponent: DefaultComponent = DefaultLabel,
}) => (
  <AuthorList>
    {authors.map(
      (
        {
          id,
          isSubmitting,
          isCorresponding,
          email = '',
          lastName = '',
          firstName = '',
          affiliation = '',
          ...rest
        },
        index,
        arr,
      ) => (
        <AuthorTooltip
          affiliation={affiliation}
          authorName={`${firstName} ${lastName}`}
          component={tooltipComponent}
          email={email}
          isCorresponding={isCorresponding}
          isSubmitting={isSubmitting}
          key={id}
        >
          <DefaultComponent
            arr={arr}
            firstName={firstName}
            index={index}
            isCorresponding={isCorresponding}
            isSubmitting={isSubmitting}
            lastName={lastName}
          />
        </AuthorTooltip>
      ),
    )}
  </AuthorList>
)

export default AuthorsWithTooltip

// #region styled-components
const defaultText = css`
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const AuthorList = styled.span`
  ${defaultText};
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  text-align: left;
`

const AuthorName = styled.span`
  cursor: default;
  padding-left: calc(${th('subGridUnit')} / 3);
  text-decoration: underline;
`
const Author = styled.div`
  padding-right: ${th('subGridUnit')};
`

const AuthorStatus = styled.span`
  border: ${th('borderDefault')};
  ${defaultText};
  margin-left: ${th('subGridUnit')};
  padding: 0 calc(${th('subGridUnit')} / 3);
  text-align: center;
  text-transform: uppercase;
`

const TooltipRoot = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: calc(${th('subGridUnit')} * 2);
`

const TooltipRow = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;
  margin: ${th('subGridUnit')} 0;
`

const Name = styled.span`
  color: ${th('colorSecondary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeBase')};
`

const AuthorDetails = styled.span`
  color: ${th('colorSecondary')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const SpecialAuthor = styled.span`
  background-color: ${th('colorBackground')};
  color: ${th('colorTextPlaceholder')};
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBaseSmall')};
  font-weight: bold;
  margin-left: ${th('subGridUnit')};
  padding: 0 ${th('subGridUnit')};
`
// #endregion
