import React from 'react'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { compose, withState, withHandlers } from 'recompose'

const Tabs = ({ activeKey, sections, title, children, setActiveKey }) => (
  <Root>
    <TabsContainer>
      {title && <Title>{title}</Title>}
      {sections.map(({ key, label }) => (
        <Tab active={activeKey === key} key={key} onClick={setActiveKey(key)}>
          {label || key}
        </Tab>
      ))}
    </TabsContainer>
    {activeKey && (
      <ContentContainer>
        {sections.find(section => section.key === activeKey).content}
      </ContentContainer>
    )}
  </Root>
)

export default compose(
  withState('activeKey', 'setActive', ({ activeKey }) => activeKey),
  withHandlers({
    setActiveKey: ({ setActive }) => key => () => {
      setActive(key)
    },
  }),
)(Tabs)

// #region styled-components
const defaultText = css`
  color: ${th('colorPrimary')};
  font-family: ${th('fontReading')};
  font-size: ${th('fontSizeBaseSmall')};
`

const Root = styled.div`
  cursor: pointer;
  display: flex;
  flex-direction: column;
  transition: all 0.3s;
`

const Title = styled.div`
  ${defaultText};
  padding: ${th('subGridUnit')};
  font-size: ${th('fontSizeBase')};
`

const TabsContainer = styled.div`
  display: flex;
  flex-direction: row;
  border-bottom: ${th('borderDefault')};
  margin-bottom: ${th('gridUnit')};
`

const Tab = styled.div`
  ${defaultText};
  margin-right: ${th('gridUnit')};
  border-width: 0 0 calc(${th('borderWidth')} * 3) 0;
  border-style: ${th('borderStyle')};
  border-color: ${({ active }) =>
    active ? `${th('colorBorder')}` : 'transparent'};
  ${({ active }) => (active ? 'font-weight: bold' : null)};
  transition: all 0.2s;
`

const ContentContainer = styled.div`
  cursor: default;
  padding-top: 0;
`

// #endregion
