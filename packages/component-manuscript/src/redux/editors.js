import { get, chain } from 'lodash'
import {
  create,
  remove,
  update,
  get as apiGet,
} from 'pubsweet-client/src/helpers/api'
import { currentUserIs } from 'pubsweet-component-faraday-selectors'

const EDITORS_DONE = 'EDITORS_DONE'
const EDITORS_REQUEST = 'EDITORS_REQUEST'
const SET_HANDLING_EDITORS = 'SET_HANDLING_EDITORS'

const setHandlingEditors = editors => ({
  type: SET_HANDLING_EDITORS,
  payload: { editors },
})

export const selectFetching = state => get(state, 'editors.isFetching', false)
export const selectHandlingEditors = state =>
  chain(state)
    .get('editors.editors', [])
    .filter(editor => editor.isActive && editor.isConfirmed)
    .value()

const canAssignHEStatuses = ['submitted']
export const canAssignHE = (state, collection = {}, isLatestVersion) => {
  const isEIC = currentUserIs(state, 'adminEiC')
  const hasHE = get(collection, 'handlingEditor', false)

  return (
    isEIC &&
    !hasHE &&
    isLatestVersion &&
    canAssignHEStatuses.includes(get(collection, 'status', 'draft'))
  )
}

const editorsRequest = () => ({ type: EDITORS_REQUEST })
const editorsDone = () => ({ type: EDITORS_DONE })

export const getHandlingEditors = () => dispatch =>
  apiGet(`/users?handlingEditor=true`).then(res =>
    dispatch(setHandlingEditors(res.users)),
  )

export const assignHandlingEditor = ({ email, collectionId }) =>
  create(`/collections/${collectionId}/invitations`, {
    email,
    role: 'handlingEditor',
  })

export const revokeHandlingEditor = ({
  invitationId,
  collectionId,
}) => dispatch => {
  dispatch(editorsRequest())
  return remove(
    `/collections/${collectionId}/invitations/${invitationId}`,
  ).then(
    res => {
      dispatch(editorsDone())
      return res
    },
    err => {
      dispatch(editorsDone())
      throw err
    },
  )
}

export const handlingEditorDecision = ({
  reason,
  isAccepted,
  collectionId,
  invitationId,
}) =>
  update(`/collections/${collectionId}/invitations/${invitationId}`, {
    isAccepted,
    reason,
  })

const initialState = {
  isFetching: false,
  editors: [],
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case EDITORS_REQUEST:
      return {
        ...state,
        isFetching: true,
      }
    case EDITORS_DONE:
      return {
        ...state,
        isFetching: false,
      }
    case SET_HANDLING_EDITORS:
      return {
        ...state,
        editors: action.payload.editors,
      }
    default:
      return state
  }
}
