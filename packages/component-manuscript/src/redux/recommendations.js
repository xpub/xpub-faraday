import { get } from 'lodash'
import { create, update } from 'pubsweet-client/src/helpers/api'

// #region Selectors
export const selectRecommendations = (state, fragmentId) =>
  get(state, `fragments.${fragmentId}.recommendations`, [])
export const selectEditorialRecommendations = (state, fragmentId) =>
  selectRecommendations(state, fragmentId).filter(
    r => r.recommendationType === 'editorRecommendation' && r.comments,
  )
export const selectReviewRecommendations = (state, fragmentId) =>
  selectRecommendations(state, fragmentId)
    .filter(r => r.recommendationType === 'review')
    .map(r => ({
      ...r,
      reviewer: get(state, 'users.users', []).find(
        user => user.id === r.userId,
      ),
    }))

// #endregion

// #region Actions
export const createRecommendation = ({
  fragmentId,
  collectionId,
  recommendation,
}) =>
  create(
    `/collections/${collectionId}/fragments/${fragmentId}/recommendations`,
    recommendation,
  )

export const updateRecommendation = ({
  fragmentId,
  collectionId,
  recommendation,
}) =>
  update(
    `/collections/${collectionId}/fragments/${fragmentId}/recommendations/${
      recommendation.id
    }`,
    recommendation,
  )
// #endregion
