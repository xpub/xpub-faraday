import React from 'react'
import ReactDOM from 'react-dom'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const createModalRootElement = () => {
  const modalDiv = document.createElement('div')
  modalDiv.setAttribute('id', 'ps-modal-root')
  document.body.insertAdjacentElement('beforeend', modalDiv)

  return modalDiv
}

const modalRoot = createModalRootElement()

class Modal extends React.Component {
  render() {
    const { component: Component, overlayColor, ...rest } = this.props
    return ReactDOM.createPortal(
      <ModalRoot
        data-test-id="modal-root"
        onClick={rest.dismissable ? rest.hideModal : null}
        overlayColor={overlayColor}
      >
        <Component {...rest} />
      </ModalRoot>,
      modalRoot,
    )
  }
}

export default Modal

const modalPosition = () => css`
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: fixed;
`

const ModalRoot = styled.div`
  align-items: center;
  display: flex;
  color: ${th('colorText')};
  background-color: ${({ overlayColor }) =>
    overlayColor || 'rgba(0, 0, 0, 0.8)'};
  justify-content: center;
  z-index: ${({ theme }) => theme.modalIndex};

  font-family: ${th('fontInterface')}, sans-serif;
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};

  ${modalPosition};

  * {
    box-sizing: border-box;
  }
`
