import React, { Fragment } from 'react'
import { omit, get } from 'lodash'
import { compose } from 'recompose'
import { connect } from 'react-redux'

import Modal from './Modal'
import { showModal, hideModal, setModalError } from '../redux/modal'

const mapState = state => ({
  modalVisibility: omit(state.modal, ['props', 'error']),
  modalProps: state.modal.props,
  modalError: state.modal.error,
})

const mapDispatch = (dispatch, props) => ({
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps = {}) =>
    dispatch(showModal(get(props, 'modalKey'), modalProps)),
  setModalError: errorMessage => dispatch(setModalError(errorMessage)),
})

const withModal = mapperFn => BaseComponent =>
  compose(connect(mapState, mapDispatch))(baseProps => {
    const { modalComponent: Component, overlayColor } = mapperFn(baseProps)
    const {
      modalKey,
      showModal,
      hideModal,
      modalProps,
      modalError,
      isFetching,
      setModalError,
      modalVisibility,
      ...rest
    } = baseProps
    return (
      <Fragment>
        {modalVisibility[modalKey] && (
          <Modal
            {...modalProps}
            component={Component}
            hideModal={hideModal}
            isFetching={isFetching}
            modalError={modalError}
            overlayColor={overlayColor}
            setModalError={setModalError}
            showModal={showModal}
          />
        )}
        <BaseComponent
          hideModal={hideModal}
          isFetching={isFetching}
          setModalError={setModalError}
          showModal={showModal}
          {...rest}
        />
      </Fragment>
    )
  })

export default withModal
