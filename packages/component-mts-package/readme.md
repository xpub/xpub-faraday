## MTS service integration

This component is running as a service to integrate current Hindawi Manuscript Tracking System for Editorial Quality Screening and for Editorial Quality Assurance.

### Use-case

As an Editor in Chief, I want the manuscript to go through Editorial Quality Screening before I assign a Handling Editor to it and start the review process.

### Workflow

Stage 1: When an article is submitted, Faraday needs to convert the submission to XML JATS format, and upload as a .zip file to a FTP server.

Stage 2: When new .zip file has been detected on FTP server, then third party system MTS (Manuscript Tracking System - Hindawi) needs to consume package and populate DB records.

Stage 3: When check is completed, then Faraday system needs to be updated to move status from Technical Checks to Submitted - which allows the Editor in Chief to assign a Handling Editor.



### Configuration
| Params        | Required           | Description  |
| ------------- |:-------------:| -----|
| journalConfig     | Yes | Journal configuration in .xml file as: doctype, dtdVersion, journalTitle, articleType, journal email, journalIdPublisher, issn |
| xmlParserOptions      | No      |   parsing config options used by xml-js library |
| s3Config | Yes      |  Access to AWS S3 Bucket where fragment files are saved  |
| FTPConfig | Yes | FTP server connection credentials |


### Usage

MTS service gathers all the fragment files, creates an .xml file in a specific JATS format out of fragment JSON, creates a .zip archive and sends it to a specific FTP server with a configurable package name (also a backup it's uploaded to AWS S3).

.xml structure of Hindawi use-case can be checked `/tests/sample.xml` from generated json  `/tests/sample.json`.

#### Example

```javascript
const fragment = {} //fragment json here
const { journalConfig, xmlParser, s3Config, ftpConfig } = config //import your config
const MTS = new MTSService(journalConfig, xmlParser, s3Config, ftpConfig)
MTS.sendPackage({ fragment })
```
 
[GIFs Demo](https://gitlab.coko.foundation/xpub/xpub-faraday/wikis/mts-integration)
 

