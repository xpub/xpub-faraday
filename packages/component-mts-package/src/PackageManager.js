const fs = require('fs')
const AWS = require('aws-sdk')
const { get } = require('lodash')
const { promisify } = require('util')
const FtpDeploy = require('ftp-deploy')
const nodeArchiver = require('archiver')
const logger = require('@pubsweet/logger')

const filterByType = (fileTypes = []) => ({ Metadata: { filetype } }) =>
  fileTypes.length > 0 ? fileTypes.includes(filetype) : true

const createFilesPackage = (s3Config, archiver = nodeArchiver) => {
  AWS.config.update({
    secretAccessKey: s3Config.secretAccessKey,
    accessKeyId: s3Config.accessKeyId,
    region: s3Config.region,
  })
  const s3 = new AWS.S3()
  const asyncGetObject = promisify(s3.getObject.bind(s3))

  return async ({ fragment, fileTypes, xmlFile, isEQA = false }) => {
    const { files = {} } = fragment
    let packageName = get(xmlFile, 'name', '').replace('.xml', '')
    if (isEQA) {
      packageName = `ACCEPTED_${packageName}.${fragment.version}`
    }
    try {
      const s3FileIDs = Object.values(files)
        .reduce((acc, f) => [...acc, ...f], [])
        .map(f => f.id)

      if (s3FileIDs) {
        const s3Files = await Promise.all(
          s3FileIDs.map(fileID =>
            asyncGetObject({
              Bucket: s3Config.bucket,
              Key: fileID,
            }),
          ),
        )
        if (s3Files) {
          const packageOutput = fs.createWriteStream(`${packageName}.zip`)
          const archive = archiver('zip')

          archive.pipe(packageOutput)
          archive.append(xmlFile.content, { name: xmlFile.name })

          s3Files.filter(filterByType(fileTypes)).forEach(f => {
            const name = get(f, 'Metadata.filename', f.ETag)
            archive.append(f.Body, {
              name,
            })
          })

          archive.on('error', err => {
            logger.error(err)
            throw err
          })
          archive.on('end', err => {
            if (err) throw err
          })
          archive.finalize()
        }
      } else {
        logger.error('Failed to create package')
        throw new Error('Failed to create a package')
      }
    } catch (err) {
      logger.error(err.message)
      return err.message
    }
  }
}

const readFile = filename =>
  new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) reject(err)
      resolve(data)
    })
  })

const fileError = filename => err => {
  logger.error(err)
  deleteFile(filename)
  throw err
}

const uploadFiles = async ({ filename, s3Config, config }) => {
  const data = await readFile(filename)
  AWS.config.update({
    secretAccessKey: s3Config.secretAccessKey,
    accessKeyId: s3Config.accessKeyId,
    region: s3Config.region,
  })
  const s3 = new AWS.S3()

  const params = {
    Bucket: s3Config.bucket,
    Body: data,
    Key: `mts/${filename}`,
  }

  const asyncUploadS3 = promisify(s3.upload.bind(s3))

  return asyncUploadS3(params)
    .then(() => {
      logger.info(`Successfully uploaded ${filename} to S3`)
      return uploadFTP({ filename, config })
    })
    .then(() => {
      logger.info(`Successfully uploaded ${filename} to FTP`)
      deleteFile(filename)
    })
    .catch(fileError(filename))
}

const deleteFilesS3 = async ({ fileKeys, s3Config }) => {
  AWS.config.update({
    secretAccessKey: s3Config.secretAccessKey,
    accessKeyId: s3Config.accessKeyId,
    region: s3Config.region,
  })
  const s3 = new AWS.S3()

  const params = {
    Bucket: s3Config.bucket,
    Delete: {
      Objects: fileKeys.map(file => ({ Key: file })),
    },
  }

  const deleteObjectsS3 = promisify(s3.deleteObjects.bind(s3))

  return deleteObjectsS3(params)
}

const deleteFile = filename => {
  fs.access(filename, fs.constants.F_OK, err => {
    if (!err) {
      fs.unlink(filename, err => {
        if (err) throw err
        logger.info(`Deleted ${filename}`)
      })
    }
  })
}

const uploadFTP = ({ filename, config }) => {
  const ftpDeploy = new FtpDeploy()
  const configs = {
    ...config,
    include: [filename],
  }
  return ftpDeploy.deploy(configs)
}

module.exports = {
  createFilesPackage,
  uploadFiles,
  deleteFilesS3,
}
