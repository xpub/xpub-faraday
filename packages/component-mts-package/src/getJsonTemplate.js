module.exports = {
  getJsonTemplate: (config = {}) => ({
    _declaration: {
      _attributes: {
        version: '1.0',
        encoding: 'utf-8',
      },
    },
    _doctype: config.doctype,
    article: {
      _attributes: {
        'dtd-version': config.dtdVersion,
        'article-type': config.articleType,
      },
      front: {
        'journal-meta': {
          'journal-id': [
            {
              _attributes: {
                'journal-id-type': 'publisher',
              },
              _text: config.journalIdPublisher,
            },
            {
              _attributes: {
                'journal-id-type': 'email',
              },
              _text: config.email,
            },
          ],
          'journal-title-group': {
            'journal-title': {
              _text: config.journalTitle,
            },
          },
          issn: [
            {
              _attributes: {
                'pub-type': 'ppub',
              },
              _text: config.issn,
            },
            {
              _attributes: {
                'pub-type': 'epub',
              },
            },
          ],
        },
        'article-meta': {
          'article-id': [
            {
              _attributes: {
                'pub-id-type': 'publisher-id',
              },
              _text: 'FARADAY-D-00-00000',
            },
            {
              _attributes: {
                'pub-id-type': 'manuscript',
              },
              _text: 'FARADAY-D-00-00000',
            },
          ],
          'article-categories': {
            'subj-group': [
              {
                _attributes: {
                  'subj-group-type': 'Article Type',
                },
                subject: {
                  _text: 'Research Article',
                },
              },
            ],
          },
          'title-group': {
            'article-title': {
              _text: 'Untitled Article Title ',
            },
          },
          'contrib-group': {
            contrib: {
              _attributes: {
                'contrib-type': 'author',
                corresp: 'yes',
              },
              role: {
                _attributes: {
                  'content-type': '1',
                },
              },
              name: {
                surname: {
                  _text: 'First Name',
                },
                'given-names': {
                  _text: 'Last Name',
                },
                prefix: {
                  _text: 'Dr.',
                },
              },
              email: {
                _text: 'faraday@hindawi.com',
              },
              xref: {
                _attributes: {
                  'ref-type': 'aff',
                  rid: 'aff1',
                },
              },
            },
            aff: {
              _attributes: {
                id: 'aff1',
              },
              country: {
                _text: 'UNITED STATES',
              },
            },
          },
          history: {
            date: {
              _attributes: {
                'date-type': 'received',
              },
              day: {
                _text: '01',
              },
              month: {
                _text: '01',
              },
              year: {
                _text: '1970',
              },
            },
          },
          abstract: {
            p: {
              _text: 'No abstract provided',
            },
          },
          'funding-group': {},
        },
        files: {
          file: [],
        },
        questions: {
          question: [],
        },
        'rev-group': {
          rev: [
            {
              _attributes: {
                'rev-type': 'reviewer',
              },
              name: {
                surname: {
                  _text: 'Last Name',
                },
                'given-names': {
                  _text: 'First Name',
                },
                prefix: {
                  _text: 'Dr',
                },
              },
              email: {
                _text: 'faraday@hindawi.com',
              },
              xref: {
                _attributes: {
                  'ref-type': 'aff',
                  rid: 'aff1',
                },
              },
              date: [
                {
                  _attributes: {
                    'date-type': 'assignment',
                  },
                  day: {
                    _text: '24',
                  },
                  month: {
                    _text: '10',
                  },
                  year: {
                    _text: '2018',
                  },
                },
                {
                  _attributes: {
                    'date-type': 'submission',
                  },
                  day: {
                    _text: '24',
                  },
                  month: {
                    _text: '10',
                  },
                  year: {
                    _text: '2018',
                  },
                },
              ],
              comment: {
                _attributes: {
                  'comment-type': 'comment',
                },
                _text:
                  'This article is really great! well written and does not need any update. Please publish it as such',
              },
            },
          ],
          aff: {
            _attributes: {
              id: 'aff1',
            },
            'addr-line': {
              _text: 'University of Fort Hare, Alice 5700, South Africa',
            },
          },
        },
      },
    },
  }),
}
