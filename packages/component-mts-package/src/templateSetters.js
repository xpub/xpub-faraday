const config = require('config')
const convert = require('xml-js')

const { set, get, reduce, isEmpty, capitalize } = require('lodash')

const manuscriptTypes = config.get('journalConfig.manuscriptTypes')

module.exports = {
  setMetadata: ({ metadata, jsonTemplate, options, fileName }) => {
    const titleGroup = {
      'article-title': parseHtml(metadata.title, options),
    }
    const articleId = [
      {
        _attributes: {
          'pub-id-type': 'publisher-id',
        },
        _text: fileName,
      },
      {
        _attributes: {
          'pub-id-type': 'manuscript',
        },
        _text: fileName,
      },
    ]

    const articleType = {
      'subj-group': [
        {
          _attributes: {
            'subj-group-type': 'Article Type',
          },
          subject: {
            _text: get(
              manuscriptTypes.find(v => v.value === metadata.type),
              'label',
              'Research Article',
            ),
          },
        },
      ],
    }
    set(jsonTemplate, 'article.front.article-meta.title-group', titleGroup)
    set(jsonTemplate, 'article.front.article-meta.article-id', articleId)
    set(
      jsonTemplate,
      'article.front.article-meta.article-categories',
      articleType,
    )
    set(jsonTemplate, 'article.front.article-meta.abstract', metadata.abstract)

    return jsonTemplate
  },
  setHistory: (submitted, jsonTemplate) => {
    const date = new Date(submitted)
    const parsedDate = {
      date: {
        _attributes: {
          'date-type': 'received',
        },
        day: {
          _text: date.getDate(),
        },
        month: {
          _text: date.getMonth() + 1,
        },
        year: {
          _text: date.getFullYear(),
        },
      },
    }
    set(jsonTemplate, 'article.front.article-meta.history', parsedDate)

    return jsonTemplate
  },
  setFiles: (files, jsonTemplate) => {
    const jsonFiles = reduce(
      files,
      (result, value, key) => {
        value.map(v =>
          result.push({
            item_type: {
              _text: key,
            },
            item_description: {
              _text: v.originalName,
            },
            item_name: {
              _text: v.name,
            },
          }),
        )

        return result
      },
      [],
    )
    set(jsonTemplate, 'article.front.files.file', jsonFiles)

    return jsonTemplate
  },
  setQuestions: (conflicts, jsonTemplate) => {
    const {
      hasConflicts = 'no',
      message = '',
      hasDataAvailability = 'no',
      dataAvailabilityMessage = '',
      hasFunding = 'no',
      fundingMessage = '',
    } = conflicts
    const questions = []
    const funding = isEmpty(hasFunding) ? 'no' : hasFunding
    const dataAvailability = isEmpty(hasDataAvailability)
      ? 'no'
      : hasDataAvailability

    const getQuestionMessage = (selection, message, defaultMessage) => {
      if (selection === 'yes') {
        return ''
      }
      return isEmpty(message) ? defaultMessage : message
    }
    if (!isEmpty(hasConflicts)) {
      questions.push({
        _attributes: {
          type: 'COI',
        },
        answer: {
          _text: capitalize(hasConflicts),
        },
        statement: {
          _text: message,
        },
      })
    }
    if (!isEmpty(dataAvailability)) {
      questions.push({
        _attributes: {
          type: 'DA',
        },
        answer: {
          _text: capitalize(dataAvailability),
        },
        statement: {
          _text: getQuestionMessage(
            dataAvailability,
            dataAvailabilityMessage,
            'The authors for this paper did not provide a data availability statement',
          ),
        },
      })
    }
    if (!isEmpty(funding)) {
      questions.push({
        _attributes: {
          type: 'Fund',
        },
        answer: {
          _text: capitalize(funding),
        },
        statement: {
          _text: getQuestionMessage(
            funding,
            fundingMessage,
            'The authors for this paper did not provide a funding statement',
          ),
        },
      })
    }

    set(jsonTemplate, 'article.front.questions.question', questions)
    return jsonTemplate
  },
  setContributors: (authors = [], jsonTemplate) => {
    const contrib = authors.map((a, i) => ({
      _attributes: {
        'contrib-type': 'author',
        corresp: a.isCorresponding ? 'Yes' : 'No',
      },
      role: {
        _attributes: {
          'content-type': '1',
        },
      },
      name: {
        surname: {
          _text: a.firstName,
        },
        'given-names': {
          _text: a.lastName,
        },
        prefix: {
          _text: a.title || 'Dr.',
        },
      },
      email: {
        _text: a.email,
      },
      xref: {
        _attributes: {
          'ref-type': 'aff',
          rid: `aff${i + 1}`,
        },
      },
    }))
    const aff = authors.map((a, i) => ({
      _attributes: {
        id: `aff${i + 1}`,
      },
      country: a.country || 'UK',
      'addr-line': {
        _text: a.affiliation || '',
      },
    }))

    set(
      jsonTemplate,
      'article.front.article-meta.contrib-group.contrib',
      contrib,
    )
    set(jsonTemplate, 'article.front.article-meta.contrib-group.aff', aff)

    return jsonTemplate
  },
  createFileName: ({ id = Date.now(), prefix }) => `${prefix}${id}`,
  setReviewers: (users = [], jsonTemplate) => {
    const xmlReviewers = users.map((user, i) => {
      const assigmentDate = new Date(user.assignmentDate)
      const revType = user.isReviewer ? 'reviewer' : 'editor'
      const revObj = {
        _attributes: {
          'rev-type': revType,
        },
        name: {
          surname: {
            _text: user.lastName,
          },
          'given-names': {
            _text: user.firstName,
          },
          prefix: {
            _text: user.title || 'Dr.',
          },
        },
        email: {
          _text: user.email,
        },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `aff${i + 1}`,
          },
        },
        date: [
          {
            _attributes: {
              'date-type': 'assignment',
            },
            day: {
              _text: assigmentDate.getDate(),
            },
            month: {
              _text: assigmentDate.getMonth() + 1,
            },
            year: {
              _text: assigmentDate.getFullYear(),
            },
          },
        ],
      }

      if (user.recommendation) {
        const submissionDate = new Date(user.submissionDate)
        revObj.date.push({
          _attributes: {
            'date-type': 'submission',
          },
          day: {
            _text: submissionDate.getDate(),
          },
          month: {
            _text: submissionDate.getMonth() + 1,
          },
          year: {
            _text: submissionDate.getFullYear(),
          },
        })
        revObj.comment = user.recommendation.comments.map(comm => ({
          _attributes: {
            'comment-type': comm.public ? 'comment' : 'Confidential',
          },
          _text: comm.content,
        }))
      }

      return revObj
    })
    const aff = users.map((user, i) => ({
      _attributes: {
        id: `aff${i + 1}`,
      },
      country: user.country || 'UK',
      'addr-line': {
        _text: user.affiliation || '',
      },
    }))

    set(jsonTemplate, 'article.front.rev-group.rev', xmlReviewers)
    set(jsonTemplate, 'article.front.rev-group.aff', aff)

    return jsonTemplate
  },
}

const parseHtml = (content = '', options) => {
  if (/<\/?[^>]*>/.test(content)) {
    return convert.xml2js(content, options)
  }
  return content
}
