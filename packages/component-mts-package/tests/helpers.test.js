process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { cloneDeep } = require('lodash')
const Chance = require('chance')
const fixturesService = require('pubsweet-component-fixture-service')

const { getJsonTemplate } = require('../src/getJsonTemplate')
const { defaultConfig, defaultParseXmlOptions } = require('../config/default')

const { convertToXML, composeJson } = require('../src/helpers')

const { fixtures } = fixturesService
const chance = new Chance()
describe('MTS helpers', () => {
  let testFixtures = {}
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
  })
  describe('composeJson', () => {
    it('should return a json with a rev-group when isEQA is true', async () => {
      const { fragment } = testFixtures.fragments
      const fragmentUsers = [
        {
          isReviewer: true,
          assignmentDate: chance.timestamp(),
          email: chance.email(),
          title: 'Dr',
          recommendation: {
            id: chance.guid(),
            userId: chance.guid(),
            comments: [
              {
                files: [],
                public: true,
                content: chance.sentence(),
              },
              {
                files: [],
                public: false,
                content: chance.sentence(),
              },
            ],
            createdOn: chance.timestamp(),
            updatedOn: chance.timestamp(),
            submittedOn: chance.timestamp(),
            recommendation: 'publish',
            recommendationType: 'review',
          },
          country: chance.country(),
          lastName: chance.first(),
          firstName: chance.last(),
          affiliation: chance.company(),
          submissionDate: chance.timestamp(),
        },
        {
          isReviewer: false,
          assignmentDate: chance.timestamp(),
          email: chance.email(),
          title: 'Dr',
          recommendation: {
            id: chance.guid(),
            userId: chance.guid(),
            comments: [
              {
                files: [],
                public: true,
                content: chance.sentence(),
              },
              {
                files: [],
                public: false,
                content: chance.sentence(),
              },
            ],
            createdOn: chance.timestamp(),
            updatedOn: chance.timestamp(),
            submittedOn: chance.timestamp(),
            recommendation: 'publish',
            recommendationType: 'editorRecommendation',
          },
          country: chance.country(),
          lastName: chance.first(),
          firstName: chance.last(),
          affiliation: chance.company(),
          submissionDate: chance.timestamp(),
        },
      ]
      const composedJson = composeJson({
        fragment,
        isEQA: true,
        fragmentUsers,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(composedJson.article.front['rev-group']).toHaveProperty('rev')
      expect(composedJson.article.front['rev-group'].rev).toHaveLength(
        fragmentUsers.length,
      )
    })
    it('should return a json with correct article-meta data', async () => {
      const { fragment } = testFixtures.fragments
      const composedJson = composeJson({
        fragment,
        isEQA: false,
        config: defaultConfig,
        options: defaultParseXmlOptions,
      })

      expect(
        composedJson.article.front['article-meta']['contrib-group'],
      ).toHaveProperty('contrib')
      expect(
        composedJson.article.front['article-meta']['contrib-group'].contrib,
      ).toHaveLength(fragment.authors.length)
      expect(
        composedJson.article.front['article-meta']['title-group'][
          'article-title'
        ],
      ).toEqual(fragment.metadata.title)
    })
  })
  describe('convertToXML', () => {
    it('should return a properly formatted object', async () => {
      const jsonTemplate = getJsonTemplate(defaultConfig)
      const xmlFile = convertToXML({
        prefix: defaultConfig.prefix,
        options: defaultParseXmlOptions,
        json: jsonTemplate,
      })

      expect(xmlFile).toHaveProperty('name')
      expect(xmlFile).toHaveProperty('content')
      expect(xmlFile.content).toContain(
        jsonTemplate.article.front['article-meta']['title-group'][
          'article-title'
        ]._text,
      )
    })
  })
})
