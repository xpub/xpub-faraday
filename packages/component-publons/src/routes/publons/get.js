const config = require('config')

const axios = require('axios')
const logger = require('@pubsweet/logger')

const {
  Team,
  services,
  Fragment,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')
const helpers = require('./helpers')

const publonsKey = config.get('publons.key')
const publonsUrl = config.get('publons.reviewersUrl')

module.exports = models => async (req, res) => {
  const { fragmentId } = req.params

  try {
    const fragment = await models.Fragment.find(fragmentId)

    const authsome = authsomeHelper.getAuthsome(models)
    const target = {
      fragment,
      path: req.route.path,
    }

    const canGet = await authsome.can(req.user, 'GET', target)

    if (!canGet) {
      return res.status(403).json({
        error: 'Unauthorized.',
      })
    }

    const fragmentHelper = new Fragment({ fragment })
    const teamHelper = new Team({
      TeamModel: models.Team,
      fragmentId: fragment.id,
    })
    const parsedFragment = await fragmentHelper.getFragmentData()
    let memberIds = []
    memberIds = await teamHelper.getTeamMembers({
      role: 'reviewer',
      objectType: 'fragment',
    })

    const existingReviewers = await Promise.all(
      memberIds.map(id => models.User.find(id)),
    )

    const authors = fragment.authors.map(fa => ({
      email: fa.email,
      firstName: fa.firstName,
      lastName: fa.lastName,
    }))

    const postBody = {
      searchArticle: {
        title: parsedFragment.title,
        abstract: parsedFragment.abstract,
        journal: {
          name: 'Research',
        },
        authors,
      },
    }

    try {
      const { data } = await axios({
        method: 'post',
        url: publonsUrl,
        data: postBody,
        headers: { 'x-apikey': publonsKey },
      })

      const reviewers = helpers.parseReviewers({
        publonsReviewers: data.recommendedReviewers,
        existingReviewers,
      })

      res.status(200).json(reviewers)
    } catch (e) {
      logger.error(e.message)
      res.status(e.response.status).json(e.response.statusText)
    }
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'Fragment')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
