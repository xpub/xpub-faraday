const { chain } = require('lodash')

module.exports = {
  parseReviewers: ({ publonsReviewers, existingReviewers }) =>
    chain(publonsReviewers)
      .filter(rev => rev.profileUrl && rev.contact.emails.length > 0)
      .filter(
        rev =>
          !existingReviewers.find(
            exRev => exRev.email === rev.contact.emails[0].email,
          ),
      )
      .map(reviewer => ({
        email: reviewer.contact.emails[0].email,
        name: reviewer.publishingName,
        profileUrl: reviewer.profileUrl,
        reviews: reviewer.numVerifiedReviews,
        affiliation: reviewer.recentOrganizations[0].name,
      }))
      .uniqBy('email')
      .value(),
}
