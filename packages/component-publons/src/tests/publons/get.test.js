process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')
const requests = require('../requests')

const { Model, fixtures } = fixturesService
// jest.mock('@pubsweet/component-send-email', () => ({
//   send: jest.fn(),
// }))

const path = '../routes/publons/get'
const route = {
  path: '/api/fragments/:fragmentId/publons',
}
describe('Publons route handler', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  it('should return an error when the fragment does not exist', async () => {
    const { handlingEditor } = testFixtures.users

    const res = await requests.sendRequest({
      userId: handlingEditor.id,
      route,
      models,
      path,
      params: {
        fragmentId: 'invalid-id',
      },
    })
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(`Fragment not found`)
  })

  it('should return an error when a user does not have publons rights', async () => {
    const { user } = testFixtures.users
    const { fragment } = testFixtures.fragments

    const res = await requests.sendRequest({
      userId: user.id,
      route,
      models,
      path,
      params: {
        fragmentId: fragment.id,
      },
    })
    expect(res.statusCode).toBe(403)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('Unauthorized.')
  })
})
