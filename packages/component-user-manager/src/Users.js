const config = require('config')
const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser')
const orcidRoutes = require('./routes/users/linkOrcid')

const Users = app => {
  app.use(bodyParser.json())
  const authBearer = app.locals.passport.authenticate('bearer', {
    session: false,
  })
  /**
   * @api {post} /api/users/reset-password Reset password
   * @apiGroup Users
   * @apiParamExample {json} Body
   *    {
   *      "email": "email@example.com",
   *      "token": "123123123",
   *      "password": "password",
   *      "firstName": "John",
   *      "lastName": "Smith",
   *      "affiliation": "UCLA",
   *      "title": "Mr"
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email@example.com",
   *      "teams": [],
   *      "username": "email@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": true,
   *      "editorInChief": false,
   *      "handlingEditor": false
   *    }
   * @apiErrorExample {json} Reset password errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.post(
    '/api/users/reset-password',
    require('./routes/users/resetPassword')(app.locals.models),
  )
  /**
   * @api {post} /api/users/confirm Confirm user
   * @apiGroup Users
   * @apiParamExample {json} Body
   *    {
   *      "userId": "valid-user-id",
   *      "confirmationToken": "12312321"
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email@example.com",
   *      "teams": [],
   *      "username": "email@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": true,
   *      "editorInChief": false,
   *      "handlingEditor": false
   *    }
   * @apiErrorExample {json} Forgot Password errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.post(
    '/api/users/confirm',
    require('./routes/users/confirm')(app.locals.models),
  )
  /**
   * @api {post} /api/users/forgot-password Forgot password
   * @apiGroup Users
   * @apiParamExample {json} Body
   *    {
   *      "email": "email@example.com",
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "message": "A password reset email has been sent to email@example.com"
   *    }
   * @apiErrorExample {json} Forgot Password errors
   *    HTTP/1.1 400 Bad Request
   */
  app.post(
    '/api/users/forgot-password',
    require('./routes/users/forgotPassword')(app.locals.models),
  )
  /**
   * @api {post} /api/users/change-password Change password
   * @apiGroup Users
   * @apiParamExample {json} Body
   *    {
   *      "password": "currentPassword",
   *      "newPassword": "newPassword",
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email@example.com",
   *      "teams": [],
   *      "username": "email@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": true,
   *      "editorInChief": false,
   *      "handlingEditor": false
   *    }
   * @apiErrorExample {json} Reset password errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.post(
    '/api/users/change-password',
    authBearer,
    require('./routes/users/changePassword')(app.locals.models),
  )

  /**
   * @api {patch} /api/users/subscriptions Change user's email subscription flag
   * @apiGroup Users
   * @apiParamExample {json} Body
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "token": "dssds-132131-42f8-b02b-ae1d755cdc6b"
   *      "subscribe": true,
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email@example.com",
   *      "teams": [],
   *      "username": "email@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": true,
   *      "editorInChief": false,
   *      "handlingEditor": false,
   *      "notifications": {
   *        "email": {
   *           "system": true,
   *           "user": true
   *         }
   *       }
   *    }
   * @apiErrorExample {json} Reset password errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.patch(
    '/api/users/subscribe',
    require('./routes/users/subscribe')(app.locals.models),
  )
  /**
   * @api {get} /api/users List users
   * @apiGroup Users
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    { users:
   *    [{
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email@example.com",
   *      "teams": [
   *        "c576695a-7cda-4e27-8e9c-31f3a0e9d592"
   *      ],
   *      "username": "email@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": false,
   *      "editorInChief": false,
   *      "handlingEditor": false,
   *      "isSubmitting": true,
   *      "isCorresponding": false,
   *    },
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email2@example.com",
   *      "teams": [
   *        "c576695a-7cda-4e27-8e9c-31f3a0e9d592"
   *      ],
   *      "username": "email2@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": true,
   *      "editorInChief": false,
   *      "handlingEditor": false",
   *      "isSubmitting": false,
   *      "isCorresponding": false,
   *    }]
   *    }
   * @apiErrorExample {json} List errors
   *    HTTP/1.1 403 Unauthorized
   */
  app.get(
    '/api/users',
    authBearer,
    require(`./routes/users/get`)(app.locals.models),
  )
  /**
   * @api {post} /api/users Create user
   * @apiGroup Users
   * @apiParamExample {json} Body
   *    {
   *      "password": "currentPassword",
   *      "email": "email@example.com",
   *      "firstName": "John",
   *      "lastName": "Smith",
   *      "affiliation": "UCLA",
   *      "title": "Mr"
   *    }
   * @apiSuccessExample {json} Success
   *    HTTP/1.1 200 OK
   *    {
   *      "id": "a6184463-b17a-42f8-b02b-ae1d755cdc6b",
   *      "type": "user",
   *      "admin": false,
   *      "email": "email@example.com",
   *      "teams": [],
   *      "username": "email@example.com",
   *      "fragments": [],
   *      "collections": [],
   *      "isConfirmed": true,
   *      "editorInChief": false,
   *      "handlingEditor": false,
   *      "notifications": {
   *        "email": {
   *           "system": true,
   *           "user": true
   *         }
   *       }
   *    }
   * @apiErrorExample {json} Reset password errors
   *    HTTP/1.1 400 Bad Request
   *    HTTP/1.1 404 Not Found
   */
  app.post(
    '/api/users',
    authorize,
    require('./routes/users/post')(app.locals.models),
  )

  // register ORCID authentication strategy
  orcidRoutes(app)
}

const authorize = async (req, res, next) => {
  if (req.headers.authorization) {
    const [, bToken] = req.headers.authorization.split(' ')
    try {
      const payload = jwt.verify(bToken, config.get('pubsweet-server.secret'))
      req.user = payload.id
      return next()
    } catch (e) {
      return res.status(403).json({ error: 'Unauthorized' })
    }
  }
  return next()
}

module.exports = Users
