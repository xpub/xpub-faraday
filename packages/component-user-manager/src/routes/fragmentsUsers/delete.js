const { Team, services } = require('pubsweet-component-helper-service')

module.exports = models => async (req, res) => {
  // TO DO: handle route  access with authsome
  const { collectionId, userId, fragmentId } = req.params
  try {
    const collection = await models.Collection.find(collectionId)
    const user = await models.User.find(userId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Fragment ${fragmentId} does not match collection ${collectionId}`,
      })
    const fragment = await models.Fragment.find(fragmentId)
    const teamHelper = new Team({ TeamModel: models.Team, fragmentId })

    const team = await teamHelper.getTeam({
      role: 'author',
      objectType: 'fragment',
    })

    await teamHelper.removeTeamMember({ teamId: team.id, userId })
    user.teams = user.teams.filter(userTeamId => team.id !== userTeamId)
    delete user.passwordResetToken
    user.save()

    fragment.authors = fragment.authors || []
    fragment.authors = fragment.authors.filter(author => author.id !== userId)
    fragment.save()

    return res.status(200).json({})
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
