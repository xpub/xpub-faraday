const config = require('config')

const resetPath = config.get('invite-reset-password.url')
const unsubscribeSlug = config.get('unsubscribe.url')
const { name: journalName, staffEmail } = config.get('journal')
const Email = require('@pubsweet/component-email-templating')

const { services, Fragment } = require('pubsweet-component-helper-service')

const { getEmailCopy } = require('./emailCopy')

module.exports = {
  async sendNotifications({
    baseUrl,
    fragment,
    UserModel,
    collection,
    submittingAuthor,
  }) {
    const fragmentHelper = new Fragment({ fragment })
    const { title } = await fragmentHelper.getFragmentData({
      handlingEditor: collection.handlingEditor,
    })

    const titleText = `The manuscript titled "${title}" has been submitted to ${journalName} by Editorial Assistant.`

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'submitting-author-added-by-admin',
      titleText,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: submittingAuthor.email,
        name: `${submittingAuthor.lastName}`,
      },
      content: {
        ctaText: 'LOG IN',
        signatureJournal: journalName,
        subject: `Manuscript submitted`,
        ctaLink: services.createUrl(baseUrl, ''),
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.id,
          token: submittingAuthor.accessTokens.unsubscribe,
        }),
      },
      bodyProps,
    })

    if (!submittingAuthor.isConfirmed) {
      email.content.ctaLink = services.createUrl(baseUrl, resetPath, {
        email: submittingAuthor.email,
        title: submittingAuthor.title.toLowerCase(),
        country: submittingAuthor.country,
        firstName: submittingAuthor.firstName,
        lastName: submittingAuthor.lastName,
        affiliation: submittingAuthor.affiliation,
        token: submittingAuthor.accessTokens.passwordReset,
      })
      email.content.ctaText = 'CONFIRM ACCOUNT'
    }

    const { html, text } = email._getNotificationBody({
      emailBodyProps: getEmailCopy({
        emailType: 'submitting-author-added-by-admin',
        titleText,
      }),
    })

    email.sendEmail({ html, text })
  },
}
