const { services, User } = require('pubsweet-component-helper-service')

module.exports = models => async (req, res) => {
  // TO DO: add authsome
  const { collectionId, fragmentId } = req.params
  try {
    const collection = await models.Collection.find(collectionId)
    if (!collection.fragments.includes(fragmentId))
      return res.status(400).json({
        error: `Fragment ${fragmentId} does not match collection ${collectionId}`,
      })

    const { authors = [] } = await models.Fragment.find(fragmentId)

    const userHelper = new User({ UserModel: models.User })
    const activeAuthors = await userHelper.getActiveAuthors({
      fragmentAuthors: authors,
    })

    return res.status(200).json(activeAuthors)
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'item')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
