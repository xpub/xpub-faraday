const { services } = require('pubsweet-component-helper-service')
const { token } = require('pubsweet-server/src/authentication')
const { passwordStrengthRegex } = require('config')

module.exports = models => async (req, res) => {
  const { currentPassword, password } = req.body
  if (!services.checkForUndefinedParams(currentPassword, password))
    return res.status(400).json({ error: 'Missing required params.' })
  if (!passwordStrengthRegex.test(password))
    return res.status(400).json({
      error: 'Password is too weak. Please check password requirements.',
    })

  let user
  try {
    user = await models.User.find(req.user)

    if (!await user.validPassword(currentPassword)) {
      return res.status(400).json({ error: 'Wrong username or password.' })
    }
    user.password = password
    user = await user.save()

    return res.status(200).json({
      ...user,
      token: token.create(user),
    })
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'User')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
