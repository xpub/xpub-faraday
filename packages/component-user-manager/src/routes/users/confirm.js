const { token } = require('pubsweet-server/src/authentication')
const {
  services,
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

module.exports = models => async (req, res) => {
  const { userId, confirmationToken } = req.body

  if (!services.checkForUndefinedParams(userId, confirmationToken))
    return res.status(400).json({ error: 'Missing required params' })

  let user
  try {
    user = await models.User.find(userId)
    const authsome = authsomeHelper.getAuthsome(models)
    const canConfirm = await authsome.can(req.user, '', {
      targetUser: user,
    })
    if (!canConfirm) {
      return res.status(403).json({ error: 'Unauthorized.' })
    }

    if (user.accessTokens.confirmation !== confirmationToken) {
      return res.status(400).json({ error: 'Wrong confirmation token.' })
    }

    if (user.isConfirmed)
      return res.status(400).json({ error: 'User is already confirmed.' })

    user.isConfirmed = true
    delete user.accessTokens.confirmation
    await user.save()

    return res.status(200).json({
      ...user,
      token: token.create(user),
    })
  } catch (e) {
    const notFoundError = await services.handleNotFoundError(e, 'User')
    return res.status(notFoundError.status).json({
      error: notFoundError.message,
    })
  }
}
