const {
  authsome: authsomeHelper,
} = require('pubsweet-component-helper-service')

const { createFilterFromQuery } = require('pubsweet-server/src/routes/util')

module.exports = models => async (req, res) => {
  const authsome = authsomeHelper.getAuthsome(models)
  const target = {
    path: req.route.path,
  }

  const parsedUsers = await authsome.can(req.user, 'GET', target)

  if (!parsedUsers) {
    return res.status(403).json({
      error: 'Unauthorized.',
    })
  }

  const filteredUsers = parsedUsers.filter(createFilterFromQuery(req.query))

  return res.status(200).json({ users: filteredUsers })
}
