const OrcidStrategy = require('passport-orcid')
const config = require('config')
const get = require('lodash/get')

const { clientID, clientSecret, callbackPath, successPath } = config.get(
  'orcid',
)

let userId
const LinkOrcid = app => {
  const { passport } = app.locals

  passport.serializeUser((user, done) => {
    done(null, user)
  })

  passport.deserializeUser((user, done) => {
    done(null, user)
  })

  passport.use(
    new OrcidStrategy(
      {
        sandbox: process.env.NODE_ENV !== 'production',
        callbackURL: `${config.get('pubsweet-client.baseUrl')}${callbackPath}`,
        clientID,
        clientSecret,
      },
      (accessToken, refreshToken, params, profile, done) => {
        profile = {
          orcid: params.orcid,
          name: params.name,
          accessToken,
          refreshToken,
          scope: params.scope,
          expiry: params.expires_in,
        }
        return done(null, profile)
      },
    ),
  )

  app.get(
    '/api/users/orcid',
    (req, res, next) => {
      userId = get(req, 'query.userId')
      next()
    },
    passport.authenticate('orcid'),
  )

  app.get(
    callbackPath,
    passport.authenticate('orcid', {
      failureRedirect: successPath,
    }),
    linkOrcidHandler(app.locals.models),
  )
}

const linkOrcidHandler = models => async (req, res) => {
  const user = await models.User.find(userId)
  user.orcid = req.user
  await user.save()
  res.redirect(successPath)
}

module.exports = LinkOrcid
