const { services } = require('pubsweet-component-helper-service')

const { passwordStrengthRegex } = require('config')

module.exports = models => async (req, res) => {
  const { email, password, token } = req.body
  if (!services.checkForUndefinedParams(email, password, token))
    return res.status(400).json({ error: 'missing required params' })

  if (!passwordStrengthRegex.test(req.body.password))
    return res.status(400).json({
      error: 'Password is too weak. Please check password requirements.',
    })

  const validateResponse = await services.validateEmailAndToken({
    email,
    token,
    userModel: models.User,
  })

  if (validateResponse.success === false)
    return res
      .status(validateResponse.status)
      .json({ error: validateResponse.message })

  let { user } = validateResponse

  req.body.isConfirmed = true
  req.body.isActive = true
  delete user.accessTokens.passwordReset
  delete user.passwordResetTimestamp
  delete req.body.token
  user = await user.updateProperties(req.body)

  user = await user.save()
  res.status(200).json(user)
}
