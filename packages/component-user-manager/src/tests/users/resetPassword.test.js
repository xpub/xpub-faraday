process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const httpMocks = require('node-mocks-http')
const cloneDeep = require('lodash/cloneDeep')
const fixturesService = require('pubsweet-component-fixture-service')

const { Model, fixtures } = fixturesService
const chance = new Chance()

const { user } = fixtures.users
jest.mock('@pubsweet/component-send-email', () => ({
  send: jest.fn(),
}))

const reqBody = {
  email: user.email,
  firstName: user.firstName,
  lastName: user.lastName,
  title: user.title,
  affiliation: user.affiliation,
  password: 'P4ssword!',
  token: user.accessTokens.passwordReset,
  isConfirmed: false,
}

const notFoundError = new Error()
notFoundError.name = 'NotFoundError'
notFoundError.status = 404
const resetPasswordPath = '../../routes/users/resetPassword'
describe('Users password reset route handler', () => {
  let testFixtures = {}
  let body = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    body = cloneDeep(reqBody)
    models = Model.build(testFixtures)
  })
  it('should return an error when some parameters are missing', async () => {
    delete body.email
    const req = httpMocks.createRequest({ body })

    const res = httpMocks.createResponse()
    await require(resetPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('missing required params')
  })
  it('should return an error when the password is too small', async () => {
    body.password = 'small'
    const req = httpMocks.createRequest({ body })

    const res = httpMocks.createResponse()
    await require(resetPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual(
      'Password is too weak. Please check password requirements.',
    )
  })
  it('should return an error when user is not found', async () => {
    body.email = chance.email()
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(resetPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(404)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('user not found')
  })
  it('should return an error when the tokens do not match', async () => {
    body.token = chance.hash()
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(resetPasswordPath)(models)(req, res)
    expect(res.statusCode).toBe(400)
    const data = JSON.parse(res._getData())
    expect(data.error).toEqual('invalid request')
  })
  it('should return success when the body is correct', async () => {
    const req = httpMocks.createRequest({ body })
    const res = httpMocks.createResponse()
    await require(resetPasswordPath)(models)(req, res)

    expect(res.statusCode).toBe(200)
    const data = JSON.parse(res._getData())
    expect(data.firstName).toEqual(body.firstName)
    expect(data.email).toEqual(body.email)
  })
})
