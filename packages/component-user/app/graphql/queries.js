import gql from 'graphql-tag'

import { userFragment } from './fragments'

export const getUsers = gql`
  {
    users {
      ...userDetails
    }
  }
  ${userFragment}
`
