import { graphql } from 'react-apollo'
import { compose, withProps } from 'recompose'

import * as queries from './queries'
import * as mutations from './mutations'

export default compose(
  graphql(queries.getUsers),
  graphql(mutations.addUserAsAdmin, {
    name: 'addUser',
    options: {
      refetchQueries: [{ query: queries.getUsers }],
    },
  }),
  graphql(mutations.editUserAsAdmin, {
    name: 'updateUser',
  }),
  graphql(mutations.activateUserAsAdmin, {
    name: 'activateUser',
  }),
  withProps(({ data }) => ({
    users: data.users,
  })),
)
