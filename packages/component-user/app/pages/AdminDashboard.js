import React, { Fragment } from 'react'
import { H1 } from '@pubsweet/ui'
import { Row, IconCard } from 'pubsweet-component-faraday-ui'

const AdminDashboard = ({ history }) => (
  <Fragment>
    <H1 mt={2}>Admin dashboard</H1>
    <Row justify="flex-start" mt={2}>
      <IconCard
        icon="users"
        iconSize={6}
        label="Users"
        onClick={() => history.push('/admin/users')}
      />
    </Row>
  </Fragment>
)

export default AdminDashboard
