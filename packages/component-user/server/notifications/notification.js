const config = require('config')
const Email = require('@pubsweet/component-email-templating')
const { services } = require('pubsweet-component-helper-service')

const { getEmailCopyForUsersAddedByAdmin } = require('./emailCopy')

const { name: journalName, staffEmail } = config.get('journal')
const unsubscribeSlug = config.get('unsubscribe.url')

class Notification {
  constructor(user) {
    this.user = user
  }

  async notifyUserAddedByAdmin(role) {
    const resetPath = config.get('invite-reset-password.url')
    const { user } = this
    const baseUrl = config.get('pubsweet-client.baseUrl')

    const { paragraph, ...bodyProps } = getEmailCopyForUsersAddedByAdmin({
      role,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: user.email,
        name: user.lastName,
      },
      content: {
        subject: 'Confirm your account',
        ctaLink: services.createUrl(baseUrl, resetPath, {
          email: user.email,
          token: user.accessTokens.passwordReset,
          firstName: user.firstName,
          lastName: user.lastName,
          affiliation: user.affiliation,
          title: user.title,
          country: user.country,
        }),
        ctaText: 'CONFIRM ACCOUNT',
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.accessTokens.unsubscribe,
        }),
        signatureJournal: journalName,
      },
      bodyProps,
    })

    return email.sendEmail()
  }
}

module.exports = Notification
