const Notification = require('./notifications/notification')

const { parseUserFromAdmin } = require('./user')

const {
  editUser,
  activateUser,
  deactivateUser,
  createUserAsAdmin,
} = require('./use-cases')

const resolvers = {
  Mutation: {
    async addUserAsAdmin(_, { input }, ctx) {
      ctx.parseUserFromAdmin = parseUserFromAdmin
      return createUserAsAdmin
        .initialize({
          ctx,
          Notification,
          User: ctx.connectors.User,
        })
        .execute({ input })
    },
    async editUserAsAdmin(_, { id, input }, ctx) {
      return editUser
        .initialize({ User: ctx.connectors.User, ctx })
        .execute({ id, input: parseUserFromAdmin(input) })
    },
    async toggleUserActiveStatusAsAdmin(_, { id, input }, ctx) {
      if (input.isActive) {
        return activateUser
          .initialize({ User: ctx.connectors.User, ctx })
          .execute({ id, input })
      }
      return deactivateUser
        .initialize({ User: ctx.connectors.User, ctx })
        .execute({ id, input })
    },
  },
}

module.exports = resolvers
