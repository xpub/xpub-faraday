const { cloneDeep } = require('lodash')
const { Model, fixtures } = require('pubsweet-component-fixture-service')

const { activateUser } = require('../use-cases')

describe('activate use case', () => {
  let testFixtures = {}
  let models
  const invalidPrefix = 'invalid***'

  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  it('activate an user', async () => {
    const { user } = testFixtures.users
    user.username = `${invalidPrefix}${user.username}`
    user.isActive = false

    const result = await activateUser
      .initialize({ User: models.User })
      .execute({ id: user.id, input: user })

    expect(result.isActive).toBeTruthy()
    expect(result.username.indexOf(invalidPrefix)).toBeLessThan(0)
  })
})
