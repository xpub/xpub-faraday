const { cloneDeep } = require('lodash')
const { Model, fixtures } = require('pubsweet-component-fixture-service')

const { editUser } = require('../use-cases')

describe('edit user as admin', () => {
  let testFixtures = {}
  let models

  beforeEach(() => {
    testFixtures = cloneDeep(fixtures)
    models = Model.build(testFixtures)
  })

  it('edit an user', async () => {
    const { user } = testFixtures.users
    const input = {
      ...user,
      affiliation: 'TSD',
      firstName: 'Sebibastian',
    }

    const result = await editUser.initialize({ User: models.User }).execute({
      id: input.id,
      input,
    })

    expect(result).toMatchObject(input)
  })
})
