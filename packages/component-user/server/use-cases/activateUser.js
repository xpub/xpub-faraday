module.exports.initialize = ({ User, ctx }) => ({
  execute: async ({ id, input }) => {
    const username = input.username.replace('invalid***', '')
    return User.update(id, { ...input, username, isActive: true }, ctx)
  },
})
