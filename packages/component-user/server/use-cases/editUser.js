module.exports.initialize = ({ User, ctx }) => ({
  execute: ({ id, input }) => User.update(id, input, ctx),
})
