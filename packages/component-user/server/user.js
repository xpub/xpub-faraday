const Chance = require('chance')
const { omit } = require('lodash')

const chance = new Chance()

module.exports = {
  parseUserFromAdmin: input => {
    const roles = {
      admin: false,
      editorInChief: false,
      handlingEditor: false,
    }
    if (input.role && input.role !== 'author') {
      roles[input.role] = true
    }

    return {
      ...omit(input, ['role']),
      ...roles,
      isActive: true,
      isConfirmed: false,
      notifications: {
        email: {
          system: true,
          user: true,
        },
      },
      accessTokens: {
        passwordReset: chance.hash(),
        unsubscribe: chance.hash(),
      },
    }
  },
}
