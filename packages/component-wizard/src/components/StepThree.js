import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Field } from 'redux-form'
import styled from 'styled-components'
import { H2, Icon } from '@pubsweet/ui'
import {
  Row,
  Text,
  WizardFiles,
  IconButton,
} from 'pubsweet-component-faraday-ui'

import { Empty } from './'

const StepThree = ({
  token,
  fragment,
  collection,
  changeForm,
  deleteFile,
  uploadFile,
  filesError,
  getSignedUrl,
}) => (
  <Fragment>
    <H2>3. Manuscript Files Upload</H2>
    <Row mb={2}>
      <Text align="center" display="inline-block" secondary>
        Drag & drop files in the specific section or click{' '}
        <CustomIcon secondary size={2}>
          plus
        </CustomIcon>{' '}
        to upload. Use the{' '}
        <CustomIconButton fontIcon="moveIcon" noHover secondary size={2} /> icon
        to reorder or move files to a different type.
      </Text>
    </Row>

    <Field component={Empty} name="files" />
    <WizardFiles
      changeForm={changeForm}
      collection={collection}
      deleteFile={deleteFile}
      files={get(fragment, 'files', {})}
      fragment={fragment}
      getSignedUrl={getSignedUrl}
      token={token}
      uploadFile={uploadFile}
    />
    {filesError && (
      <Row justify="flex-start" mt={1}>
        <Text error>{filesError}</Text>
      </Row>
    )}
  </Fragment>
)

export default StepThree

// #region styled-components
const CustomIcon = styled(Icon)`
  vertical-align: sub;
`

const CustomIconButton = styled(IconButton)`
  vertical-align: sub;
  cursor: default;
  display: inline-flex;
`
// #endregion
