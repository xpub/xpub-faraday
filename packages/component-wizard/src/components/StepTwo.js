import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Field } from 'redux-form'
import { required } from 'xpub-validators'
import { withHandlers } from 'recompose'
import {
  Row,
  Text,
  Item,
  Label,
  Textarea,
  ActionLink,
  WizardAuthors,
  RowOverrideAlert,
  ItemOverrideAlert,
  RadioWithComments,
} from 'pubsweet-component-faraday-ui'
import { H2, Menu, TextField, ValidatedField } from '@pubsweet/ui'

import { Empty } from './'

const StepTwo = ({
  fragment,
  journal,
  addAuthor,
  collection,
  changeForm,
  formValues,
  isAuthorEdit,
  authorsError,
  deleteAuthor,
  manuscriptTypes,
  getTooltipContent,
  isAuthorsFetching,
  setAuthorEditIndex,
  journal: { submission: { questions = [] } },
  ...rest
}) => (
  <Fragment>
    <Row alignItems="center" mb={1}>
      <H2>2. Manuscript & Author Details</H2>
    </Row>
    <Row mb={3}>
      <Text align="center" secondary>
        Please provide the details of all the authors of this manuscript, in the
        order that they appear on the manuscript. Your details have been
        prefilled as the submitting author.
      </Text>
    </Row>

    <Row mb={3}>
      <Item data-test-id="submission-title" flex={3} mr={1} vertical>
        <Label required>MANUSCRIPT TITLE</Label>
        <ValidatedField
          component={TextField}
          name="metadata.title"
          validate={[required]}
        />
      </Item>
      <ItemOverrideAlert data-test-id="submission-type" vertical>
        <Label required>MANUSCRIPT TYPE</Label>
        <ValidatedField
          component={input => (
            <Menu
              options={manuscriptTypes}
              {...input}
              placeholder="Please select"
            />
          )}
          name="metadata.type"
          validate={[required]}
        />
      </ItemOverrideAlert>
    </Row>

    <RowOverrideAlert mb={2}>
      <Item data-test-id="submission-abstract" vertical>
        <Label required>ABSTRACT</Label>
        <ValidatedField
          component={Textarea}
          minHeight={15}
          name="metadata.abstract"
          validate={[required]}
        />
      </Item>
    </RowOverrideAlert>

    <Field component={Empty} name="authors" />
    <WizardAuthors
      addAuthor={addAuthor}
      authors={get(fragment, 'authors', [])}
      changeForm={changeForm}
      collection={collection}
      deleteAuthor={deleteAuthor}
      error={authorsError}
      fragment={fragment}
      isAuthorEdit={isAuthorEdit}
      isAuthorsFetching={isAuthorsFetching}
      journal={journal}
      onAuthorEdit={setAuthorEditIndex}
    />

    {questions.map(q => (
      <RadioWithComments
        formValues={formValues}
        key={q.id}
        tooltipContent={getTooltipContent(q.id)}
        {...q}
      />
    ))}
  </Fragment>
)

export default withHandlers({
  getTooltipContent: () => questionId => () => {
    switch (questionId) {
      case 'conflictsOfInterest':
        return <ConflictsTooltip />
      case 'dataAvailability':
        return <DataAvailabilityTooltip />
      case 'funding':
        return <FundingTooltip />
      default:
        return null
    }
  },
})(StepTwo)

const ConflictsTooltip = () => (
  <Text secondary>
    When an author, editor, or reviewer has a financial/personal interest or
    belief that could affect his/her objectivity, or inappropriately influence
    his/her actions, a potential conflict of interest exists.{' '}
    <ActionLink to="https://www.hindawi.com/editors/coi/">More info</ActionLink>
  </Text>
)

const DataAvailabilityTooltip = () => (
  <Text>
    Statement about where data supporting the results reported in a published
    article can be found, including, where applicable, hyperlinks to publicly
    archived datasets analysed or generated during the study.
  </Text>
)

const FundingTooltip = () => (
  <Text>
    Statement about how the research and publication of an article is funded,
    naming each financially supporting body followed by any associated grant
    numbers in square brackets.
  </Text>
)
