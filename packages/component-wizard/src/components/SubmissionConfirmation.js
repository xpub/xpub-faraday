import React from 'react'
import { get } from 'lodash'
import { H2, Button } from '@pubsweet/ui'
import { withJournal } from 'xpub-journal'
import { Text, Row, ShadowedBox } from 'pubsweet-component-faraday-ui'

const SubmissionConfirmation = ({ history, journal }) => (
  <ShadowedBox center mt={5} width={75}>
    <H2>Thank You for Submitting Your Manuscript</H2>
    <Row justify="center">
      <Text secondary>
        Your manuscript has been successfully submitted to{' '}
        <b>{get(journal, 'metadata.nameText', '')}</b>.
      </Text>
    </Row>

    <Row mb={3} mt={2} pl={2} pr={2}>
      <Text align="justify">
        An acknowledgment email will be sent to all authors when our system has
        finished processing the submission - at which point a manuscript ID will
        be assigned, and you will be able to track the manuscript status on your
        dashboard.
      </Text>
    </Row>
    <Row justify="center">
      <Button
        data-test-id="go-to-dashboard"
        onClick={() => history.push('/')}
        primary
      >
        GO TO DASHBOARD
      </Button>
    </Row>
  </ShadowedBox>
)

export default withJournal(SubmissionConfirmation)

// #region styles
// #endregion
