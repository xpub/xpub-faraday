import StepOne from './StepOne'
import StepTwo from './StepTwo'
import StepThree from './StepThree'

export { default as Empty } from './Empty'
export { default as SubmissionWizard } from './SubmissionWizard'
export { default as SubmissionStatement } from './SubmissionStatement'
export { default as SubmissionConfirmation } from './SubmissionConfirmation'

export const wizardSteps = [
  {
    stepTitle: 'Pre-submission Checklist',
    component: StepOne,
  },
  {
    stepTitle: 'Manuscript & Author Details',
    component: StepTwo,
  },
  {
    stepTitle: 'Files Upload',
    component: StepThree,
  },
]
