import { get } from 'lodash'

export const AUTOSAVE_REQUEST = 'autosave/AUTOSAVE_REQUEST'
export const AUTOSAVE_FAILURE = 'autosave/AUTOSAVE_FAILURE'
export const AUTOSAVE_SUCCESS = 'autosave/AUTOSAVE_SUCCESS'

export const autosaveRequest = () => ({
  type: AUTOSAVE_REQUEST,
})

export const autosaveFailure = () => ({
  type: AUTOSAVE_FAILURE,
  error: 'Something went wrong. Please try again.',
})

export const autosaveSuccess = lastUpdate => ({
  type: AUTOSAVE_SUCCESS,
  lastUpdate,
})

const initialState = {
  isFetching: false,
  lastUpdate: null,
  error: null,
}

export const getAutosave = state => get(state, 'autosave', initialState)
export const getAutosaveFetching = state =>
  get(state, 'autosave.isFetching', false)

// due to faulty error handing inside the updateFragment action, we have to
// handle error and success here
export default (state = initialState, action) => {
  switch (action.type) {
    case AUTOSAVE_REQUEST:
      return {
        ...initialState,
        isFetching: true,
      }
    case 'UPDATE_FRAGMENT_FAILURE':
    case AUTOSAVE_FAILURE:
      return {
        ...initialState,
        error: action.error,
      }
    case 'UPDATE_FRAGMENT_SUCCESS':
    case AUTOSAVE_SUCCESS:
      return {
        ...initialState,
        lastUpdate: new Date(),
      }
    default:
      return state
  }
}
