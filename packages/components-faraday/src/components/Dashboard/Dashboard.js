import React, { Fragment } from 'react'
import { compose, withProps } from 'recompose'
import { DashboardItems, DashboardFilters } from './'

const Dashboard = ({
  journal,
  isAdmin,
  isFetching,
  dashboardItems,
  deleteCollection,
  deleteManuscript,
  getFilterOptions,
  changeFilterValue,
  getDefaultFilterValue,
}) => (
  <Fragment>
    <DashboardFilters
      changeFilterValue={changeFilterValue}
      getDefaultFilterValue={getDefaultFilterValue}
      getFilterOptions={getFilterOptions}
    />

    <DashboardItems
      deleteCollection={deleteCollection}
      deleteManuscript={deleteManuscript}
      isAdmin={isAdmin}
      isFetching={isFetching}
      list={dashboardItems}
    />
  </Fragment>
)

export default compose(
  withProps(({ dashboard, filterItems }) => ({
    dashboardItems: filterItems(dashboard),
  })),
)(Dashboard)
