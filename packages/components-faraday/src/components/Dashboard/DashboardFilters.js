import React from 'react'
import { Menu } from '@pubsweet/ui'
import { compose, withHandlers } from 'recompose'
import { Row, Item, Text, Label } from 'pubsweet-component-faraday-ui'

const DashboardFilters = ({
  getFilterOptions,
  changeFilterValue,
  getDefaultFilterValue,
}) => (
  <Row alignItems="flex-end" justify="flex-start" mb={1} mt={2}>
    <Text mr={1} pb={1} secondary>
      Filters
    </Text>
    <Item
      alignItems="flex-start"
      data-test-id="dashboard-filter-priority"
      flex={0}
      mr={1}
      vertical
    >
      <Label>Priority</Label>
      <Menu
        inline
        onChange={changeFilterValue('priority')}
        options={getFilterOptions('priority')}
        placeholder="Please select"
        value={getDefaultFilterValue('priority')}
      />
    </Item>
    <Item
      alignItems="flex-start"
      data-test-id="dashboard-filter-order"
      flex={0}
      vertical
    >
      <Label>Order</Label>
      <Menu
        inline
        onChange={changeFilterValue('order')}
        options={getFilterOptions('order')}
        placeholder="Please select"
        value={getDefaultFilterValue('order')}
      />
    </Item>
  </Row>
)

export default compose(
  withHandlers({
    changeFilter: ({ changeFilter }) => filterKey => value => {
      changeFilter(filterKey, value)
    },
  }),
)(DashboardFilters)
