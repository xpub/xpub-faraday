import React from 'react'
import { H3 } from '@pubsweet/ui'
import { get, has } from 'lodash'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withRouter } from 'react-router-dom'
import { ManuscriptCard, Row } from 'pubsweet-component-faraday-ui'
import { canViewReports } from 'pubsweet-component-faraday-selectors'
import { compose, setDisplayName, withHandlers, withProps } from 'recompose'

const DashboardItem = compose(
  connect((state, { collection }) => ({
    canViewReports: canViewReports(state, get(collection, 'id', '')),
  })),
  withProps(({ collection }) => ({
    fragment: get(collection, 'currentVersion', {}),
  })),
)(ManuscriptCard)

const DashboardItems = ({
  list,
  onClick,
  isAdmin,
  isFetching,
  canViewReports,
  deleteManuscript,
  deleteCollection,
}) => (
  <Root data-test-id="dashboard-list-items">
    {!list.length ? (
      <Row justify="center" mt={4}>
        <H3>Manuscripts will appear here!</H3>
      </Row>
    ) : (
      list.map(collection => (
        <HideLoading key={collection.id}>
          <DashboardItem
            collection={collection}
            deleteManuscript={deleteManuscript}
            isAdmin={isAdmin}
            isFetching={isFetching}
            key={collection.id}
            onClick={onClick}
            onDelete={deleteCollection(collection)}
          />
        </HideLoading>
      ))
    )}
  </Root>
)

export default compose(
  withRouter,
  withHandlers({
    onClick: ({ history }) => (collection, fragment) => {
      const hasDetails = get(collection, 'status', 'draft') !== 'draft'
      const isNotSubmitted =
        get(collection, 'status', 'draft') === 'draft' &&
        !has(fragment, 'submitted')
      if (isNotSubmitted) {
        history.push(
          `/projects/${collection.id}/versions/${fragment.id}/submit`,
        )
      } else if (hasDetails) {
        history.push(
          `/projects/${collection.id}/versions/${fragment.id}/details`,
        )
      }
    },
  }),
  setDisplayName('DashboardItems'),
)(DashboardItems)

// #region styles
const Root = styled.div`
  max-height: calc(100vh - ${th('gridUnit')} * 25);
  overflow-y: auto;
  padding-right: ${th('gridUnit')};
  overflow-x: hidden;
  div[open] {
    width: auto;
  }
`
const HideLoading = styled.div`
  > div {
    display: none;
  }

  &:first-of-type > div {
    display: flex;
  }

  > div[data-test-id|='fragment'] {
    display: flex;
  }
`
// #endregion
