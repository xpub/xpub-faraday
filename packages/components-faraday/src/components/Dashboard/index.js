import DashboardPage from './DashboardPage'

export { default as Dashboard } from './Dashboard'
export { default as DashboardItems } from './DashboardItems'
export { default as DashboardFilters } from './DashboardFilters'

export default DashboardPage
