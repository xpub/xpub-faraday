import { get } from 'lodash'

import { utils } from './'
import cfg from '../../../../xpub-faraday/config/default'

const statuses = get(cfg, 'statuses')
export const SORT_VALUES = {
  MORE_IMPORTANT: 'more_important',
  LESS_IMPORTANT: 'less_important',
}

const options = [
  { label: 'Important first', value: SORT_VALUES.MORE_IMPORTANT },
  { label: 'Less important first', value: SORT_VALUES.LESS_IMPORTANT },
]

const sortFn = sortValue => (item1, item2) => {
  const item1Importance = utils.getCollectionImportance(statuses, item1)
  const item2Importance = utils.getCollectionImportance(statuses, item2)

  if (item1Importance - item2Importance === 0) {
    return item1.created - item2.created
  }

  if (sortValue === SORT_VALUES.MORE_IMPORTANT) {
    return item2Importance - item1Importance
  }
  return item1Importance - item2Importance
}

export default {
  sortFn,
  options,
  type: 'sort',
}
