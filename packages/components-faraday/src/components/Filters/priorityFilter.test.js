import fixturesService from 'pubsweet-component-fixture-service'

import { FILTER_VALUES } from './priorityFilter'
import { priorityFilter, utils } from './'

const {
  fixtures: { collections: { collection }, users, teams },
} = fixturesService

const { filterFn } = priorityFilter

describe('Priority filter function for reviewersInvited status', () => {
  describe('ALL', () => {
    it('should return true if ALL is selected', () => {
      const filterResult = filterFn(FILTER_VALUES.ALL, { currentUser: {} })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeTruthy()
    })
  })

  describe('NEEDS ATTENTION', () => {
    it('should return falsy for AUTHOR', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.author,
        userPermissions: [utils.parsePermission(teams.authorTeam)],
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeFalsy()
    })

    it('should return truthy for REVIEWER', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.reviewer,
        userPermissions: [utils.parsePermission(teams.revTeam)],
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return truthy for HANDLING EDITOR', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.handlingEditor,
        userPermissions: [utils.parsePermission(teams.heTeam)],
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return falsy for EDITOR IN CHIEF', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.editorInChief,
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return truthy for ADMIN', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeTruthy()
    })
  })

  describe('IN PROGRESS', () => {
    it('should return truthy for AUTHOR', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.author,
        userPermissions: [utils.parsePermission(teams.authorTeam)],
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeTruthy()
    })

    it('should return falsy for REVIEWER', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.reviewer,
        userPermissions: [utils.parsePermission(teams.revTeam)],
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return falsy for HANDLING EDITOR', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.handlingEditor,
        userPermissions: [utils.parsePermission(teams.heTeam)],
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return truthy for EDITOR IN CHIEF', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.editorInChief,
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return falsy for ADMIN', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeFalsy()
    })
  })

  describe('ARCHIVED', () => {
    it('should return falsy', () => {
      const filterResult = filterFn(FILTER_VALUES.ARCHIVED, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'reviewersInvited',
      })
      expect(filterResult).toBeFalsy()
    })
  })
})

describe('Priority filter function for technicalChecks status', () => {
  describe('ALL', () => {
    it('should return true if ALL is selected', () => {
      const filterResult = filterFn(FILTER_VALUES.ALL, { currentUser: {} })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeTruthy()
    })
  })

  describe('NEEDS ATTENTION', () => {
    it('should return falsy for AUTHOR', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.author,
        userPermissions: [utils.parsePermission(teams.authorTeam)],
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeFalsy()
    })

    it('should return truthy for REVIEWER', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.reviewer,
        userPermissions: [utils.parsePermission(teams.revTeam)],
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return truthy for HANDLING EDITOR', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.handlingEditor,
        userPermissions: [utils.parsePermission(teams.heTeam)],
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return falsy for EDITOR IN CHIEF', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.editorInChief,
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return truthy for ADMIN', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeTruthy()
    })
  })

  describe('IN PROGRESS', () => {
    it('should return truthy for AUTHOR', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.author,
        userPermissions: [utils.parsePermission(teams.authorTeam)],
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeTruthy()
    })

    it('should return falsy for REVIEWER', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.reviewer,
        userPermissions: [utils.parsePermission(teams.revTeam)],
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return falsy for HANDLING EDITOR', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.handlingEditor,
        userPermissions: [utils.parsePermission(teams.heTeam)],
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return truthy for EDITOR IN CHIEF', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.editorInChief,
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return falsy for ADMIN', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeFalsy()
    })
  })

  describe('ARCHIVED', () => {
    it('should return falsy', () => {
      const filterResult = filterFn(FILTER_VALUES.ARCHIVED, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeFalsy()
    })
  })
})

describe('Priority filter function for pendingApproval status', () => {
  describe('ALL', () => {
    it('should return true if ALL is selected', () => {
      const filterResult = filterFn(FILTER_VALUES.ALL, { currentUser: {} })({
        ...collection,
        status: 'technicalChecks',
      })
      expect(filterResult).toBeTruthy()
    })
  })

  describe('NEEDS ATTENTION', () => {
    it('should return falsy for AUTHOR', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.author,
        userPermissions: [utils.parsePermission(teams.authorTeam)],
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeFalsy()
    })

    it('should return truthy for REVIEWER', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.reviewer,
        userPermissions: [utils.parsePermission(teams.revTeam)],
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return truthy for HANDLING EDITOR', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.handlingEditor,
        userPermissions: [utils.parsePermission(teams.heTeam)],
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return falsy for EDITOR IN CHIEF', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.editorInChief,
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return truthy for ADMIN', () => {
      const filterResult = filterFn(FILTER_VALUES.NEEDS_ATTENTION, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeTruthy()
    })
  })

  describe('IN PROGRESS', () => {
    it('should return truthy for AUTHOR', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.author,
        userPermissions: [utils.parsePermission(teams.authorTeam)],
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeTruthy()
    })

    it('should return falsy for REVIEWER', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.reviewer,
        userPermissions: [utils.parsePermission(teams.revTeam)],
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return falsy for HANDLING EDITOR', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.handlingEditor,
        userPermissions: [utils.parsePermission(teams.heTeam)],
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeTruthy()
    })
    it('should return truthy for EDITOR IN CHIEF', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.editorInChief,
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeFalsy()
    })
    it('should return falsy for ADMIN', () => {
      const filterResult = filterFn(FILTER_VALUES.IN_PROGRESS, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeFalsy()
    })
  })

  describe('ARCHIVED', () => {
    it('should return falsy', () => {
      const filterResult = filterFn(FILTER_VALUES.ARCHIVED, {
        currentUser: users.admin,
      })({
        ...collection,
        status: 'pendingApproval',
      })
      expect(filterResult).toBeFalsy()
    })
  })
})

describe('Priority filter function for archived statuses', () => {
  it('should show rejected manuscripts', () => {
    const filterResult = filterFn(FILTER_VALUES.ARCHIVED, {
      currentUser: users.admin,
    })({
      ...collection,
      status: 'rejected',
    })
    expect(filterResult).toBeTruthy()
  })

  it('should show withdrawn manuscripts', () => {
    const filterResult = filterFn(FILTER_VALUES.ARCHIVED, {
      currentUser: users.admin,
    })({
      ...collection,
      status: 'withdrawn',
    })
    expect(filterResult).toBeTruthy()
  })

  it('should show accepted manuscripts', () => {
    const filterResult = filterFn(FILTER_VALUES.ARCHIVED, {
      currentUser: users.admin,
    })({
      ...collection,
      status: 'accepted',
    })
    expect(filterResult).toBeTruthy()
  })

  it('should not show pendingApproval manuscripts', () => {
    const filterResult = filterFn(FILTER_VALUES.ARCHIVED, {
      currentUser: users.admin,
    })({
      ...collection,
      status: 'pendingApproval',
    })
    expect(filterResult).toBeFalsy()
  })
})
