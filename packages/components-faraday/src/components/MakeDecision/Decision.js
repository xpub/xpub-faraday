import React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { actions } from 'pubsweet-client'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers, setDisplayName, withProps } from 'recompose'

import {
  ConfirmationModal,
  withModal,
} from 'pubsweet-component-modal/src/components'
import { handleError } from '../utils'
import { createRecommendation } from '../../redux/recommendations'

import { DecisionForm } from './'

const Decision = ({ showDecisionModal, buttonText }) => (
  <Root onClick={showDecisionModal}>{buttonText}</Root>
)

const ModalComponent = ({ type, ...rest }) => {
  switch (type) {
    case 'decision':
      return <DecisionForm {...rest} />
    default:
      return <ConfirmationModal {...rest} />
  }
}

export default compose(
  setDisplayName('EICDecisionRoot'),
  withModal(() => ({
    modalComponent: ModalComponent,
  })),
  connect(null, {
    createRecommendation,
    getFragments: actions.getFragments,
    getCollections: actions.getCollections,
  }),
  withProps(({ status }) => ({
    buttonText: status === 'submitted' ? 'Reject' : 'Make Decision',
  })),
  withHandlers({
    showDecisionModal: ({
      status,
      showModal,
      hideModal,
      fragmentId,
      collectionId,
      getFragments,
      setModalError,
      getCollections,
      createRecommendation,
    }) => () => {
      status !== 'submitted'
        ? showModal({
            type: 'decision',
            hideModal,
            fragmentId,
            collectionId,
          })
        : showModal({
            hideModal,
            fragmentId,
            collectionId,
            title: 'Reject Manuscript?',
            confirmText: 'Reject',
            onConfirm: () => {
              const recommendation = {
                recommendation: 'reject',
                recommendationType: 'editorRecommendation',
              }
              createRecommendation(
                collectionId,
                fragmentId,
                recommendation,
              ).then(() => {
                getCollections()
                getFragments()
                hideModal()
              }, handleError(setModalError))
            },
          })
    },
  }),
)(Decision)

// #region styled components
const Root = styled.div`
  align-items: center;
  background-color: ${th('colorPrimary')};
  color: ${th('colorTextReverse')};
  cursor: pointer;
  display: flex;
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBaseSmall')};
  height: calc(${th('subGridUnit')} * 5);
  justify-content: center;
  padding: 0 calc(${th('subGridUnit')} * 2);
  text-transform: uppercase;
  white-space: nowrap;
`
// #endregion
