import React from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { actions } from 'pubsweet-client'
import { required } from 'xpub-validators'
import { reduxForm, formValueSelector } from 'redux-form'
import { compose, setDisplayName, withProps } from 'recompose'
import { Icon, Button, RadioGroup, ValidatedField } from '@pubsweet/ui'

import { FormItems } from '../UIComponents'
import { createRecommendation } from '../../redux/recommendations'
import { subtitleParser, decisions, parseFormValues } from './utils'
import { getHERecommendation } from '../../../../component-faraday-selectors'

const {
  Row,
  Title,
  Label,
  RowItem,
  Subtitle,
  RootContainer,
  FormContainer,
  TextAreaField,
  CustomRadioGroup,
} = FormItems
const Form = RootContainer.withComponent(FormContainer)

const DecisionForm = ({
  decision,
  hideModal,
  handleSubmit,
  heRecommendation: { reason, message = '' },
}) => (
  <Form onSubmit={handleSubmit}>
    <IconButton onClick={hideModal}>
      <Icon primary>x</Icon>
    </IconButton>
    <Title>Make decision</Title>
    {!!reason && (
      <CustomSubtitle>
        Recommended to<BoldSubtitle>{reason}</BoldSubtitle>
      </CustomSubtitle>
    )}
    {!!message && (
      <Row>
        <RowItem vertical>
          <Label>Message from Handling Editor</Label>
          <span>{message}</span>
        </RowItem>
      </Row>
    )}
    <Row>
      <RowItem vertical>
        <Label>Your Decision</Label>
        <ValidatedField
          component={input => (
            <CustomRadioGroup
              className="custom-radio-group"
              justify={reason ? 'space-between' : 'space-around'}
            >
              <RadioGroup
                name="decision"
                options={reason ? decisions : decisions.slice(0, 2)}
                {...input}
              />
            </CustomRadioGroup>
          )}
          name="decision"
        />
      </RowItem>
    </Row>
    {decision === 'return-to-handling-editor' && (
      <Row>
        <RowItem vertical>
          <Label>Comments for Author</Label>
          <ValidatedField
            component={TextAreaField}
            name="messageToHE"
            validate={[required]}
          />
        </RowItem>
      </Row>
    )}
    <Row>
      <RowItem centered>
        <Button onClick={hideModal}>Cancel</Button>
      </RowItem>
      <RowItem centered>
        <Button primary type="submit">
          Submit
        </Button>
      </RowItem>
    </Row>
  </Form>
)

const selector = formValueSelector('eicDecision')
export default compose(
  setDisplayName('DecisionForm'),
  connect(
    (state, { fragmentId, collectionId }) => ({
      decision: selector(state, 'decision'),
      heRecommendation: getHERecommendation(state, collectionId, fragmentId),
    }),
    {
      createRecommendation,
      getFragments: actions.getFragments,
      getCollections: actions.getCollections,
    },
  ),
  withProps(({ heRecommendation: { recommendation = '', comments = [] } }) => ({
    heRecommendation: {
      reason: subtitleParser(recommendation),
      message: get(comments.find(c => !c.public), 'content'),
    },
  })),
  reduxForm({
    form: 'eicDecision',
    onSubmit: (
      values,
      dispatch,
      {
        showModal,
        hideModal,
        fragmentId,
        collectionId,
        getFragments,
        getCollections,
        createRecommendation,
      },
    ) => {
      const recommendation = parseFormValues(values)
      createRecommendation(collectionId, fragmentId, recommendation).then(
        () => {
          showModal({
            onCancel: () => {
              getCollections()
              getFragments()
              hideModal()
            },
            title: 'Decision submitted',
            cancelText: 'OK',
          })
        },
      )
    },
  }),
)(DecisionForm)

// #region styled-components
const IconButton = styled.div`
  align-self: flex-end;
  cursor: pointer;
`

const CustomSubtitle = styled(Subtitle)`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: center;
`

const BoldSubtitle = styled(Subtitle)`
  font-weight: bold;
  margin-left: 5px;
`
// #endregion
