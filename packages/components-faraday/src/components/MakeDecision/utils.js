export const decisions = [
  { label: 'Publish', value: 'publish' },
  { label: 'Reject', value: 'reject' },
  { label: 'Return to Handling Editor', value: 'return-to-handling-editor' },
]

export const subtitleParser = t => {
  switch (t) {
    case 'major':
      return 'Revise(major)'
    case 'minor':
      return 'Revise(minor)'
    case 'reject':
      return 'Reject'
    case 'publish':
      return 'Publish'
    default:
      return ''
  }
}

export const parseFormValues = ({ decision, messageToHE }) => {
  const recommendation = {
    recommendation: decision,
    recommendationType: 'editorRecommendation',
  }
  return decision === 'return-to-handling-editor'
    ? {
        ...recommendation,
        comments: [
          {
            public: false,
            content: messageToHE,
          },
        ],
      }
    : recommendation
}
