import React from 'react'
import { pick } from 'lodash'
import { Icon } from '@pubsweet/ui'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers, withProps } from 'recompose'
import { DateParser } from 'pubsweet-components-faraday/src/components'

import { revokeReviewer, inviteReviewer } from '../../redux/reviewers'

const ResendRevoke = ({ showConfirmResend, showConfirmRevoke, status }) => (
  <ActionButtons>
    <div onClick={showConfirmResend}>
      <Icon primary>refresh-cw</Icon>
    </div>
    {status === 'pending' && (
      <div onClick={showConfirmRevoke}>
        <Icon primary>x-circle</Icon>
      </div>
    )}
  </ActionButtons>
)

const ReviewersList = ({
  reviewers,
  showConfirmResend,
  showConfirmRevoke,
  renderAcceptedLabel,
}) =>
  reviewers.length > 0 && (
    <Root>
      <ScrollContainer>
        {reviewers.map((r, index) => (
          <ReviewerItem key={r.invitationId}>
            <Column flex={3}>
              <div>
                <ReviewerName>{r.name}</ReviewerName>
                {r.status === 'accepted' && (
                  <AcceptedReviewer>
                    {renderAcceptedLabel(index)}
                  </AcceptedReviewer>
                )}
              </div>
              <ReviewerEmail>{r.email}</ReviewerEmail>
            </Column>
            <Column>
              <StatusText>
                {r.status === 'accepted' ? 'Agreed' : r.status}
              </StatusText>
              <DateParser timestamp={r.respondedOn}>
                {timestamp => <DateText>{timestamp}</DateText>}
              </DateParser>
            </Column>
            {r.status === 'pending' ? (
              <ResendRevoke
                showConfirmResend={showConfirmResend(r)}
                showConfirmRevoke={showConfirmRevoke(r.invitationId)}
                status={r.status}
              />
            ) : (
              <Column />
            )}
          </ReviewerItem>
        ))}
      </ScrollContainer>
    </Root>
  )

export default compose(
  connect(null, { inviteReviewer, revokeReviewer }),
  withProps(({ reviewers = [] }) => ({
    firstAccepted: reviewers.findIndex(r => r.status === 'accepted'),
  })),
  withHandlers({
    goBackToReviewers: ({
      showModal,
      hideModal,
      versionId,
      getReviewers,
      collectionId,
    }) => () => {
      getReviewers()
      showModal({
        collectionId,
        type: 'invite-reviewers',
        versionId,
        onConfirm: () => {
          hideModal()
        },
      })
    },
    renderAcceptedLabel: ({ firstAccepted }) => index =>
      `Reviewer ${index - firstAccepted + 1}`,
  }),
  withHandlers({
    showConfirmResend: ({
      showModal,
      versionId,
      collectionId,
      inviteReviewer,
      goBackToReviewers,
    }) => reviewer => () => {
      showModal({
        title: 'Resend reviewer invite',
        confirmText: 'Resend',
        onConfirm: () => {
          inviteReviewer(
            pick(reviewer, ['email', 'firstName', 'lastName', 'affiliation']),
            collectionId,
            versionId,
          ).then(goBackToReviewers, goBackToReviewers)
        },
        onCancel: goBackToReviewers,
      })
    },
    showConfirmRevoke: ({
      showModal,
      hideModal,
      versionId,
      collectionId,
      revokeReviewer,
      goBackToReviewers,
    }) => invitationId => () => {
      showModal({
        title: 'Unassign Reviewer',
        confirmText: 'Unassign',
        onConfirm: () => {
          revokeReviewer(invitationId, collectionId, versionId).then(
            goBackToReviewers,
            goBackToReviewers,
          )
        },
        onCancel: goBackToReviewers,
      })
    },
  }),
)(ReviewersList)

// #region styled-components
const ReviewerEmail = styled.span`
  font-size: ${th('fontSizeBaseSmall')};
  color: ${th('colorPrimary')};
  font-family: ${th('fontReading')};
`

const ReviewerName = ReviewerEmail.extend`
  font-size: ${th('fontSizeBase')};
  text-decoration: underline;
`

const AcceptedReviewer = ReviewerEmail.extend`
  font-weight: bold;
  margin-left: ${th('subGridUnit')};
`

const StatusText = ReviewerEmail.extend`
  text-transform: uppercase;
`

const DateText = ReviewerEmail.extend``

const Column = styled.div`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex: ${({ flex }) => flex || 1};
`

const ReviewerItem = styled.div`
  align-items: center;
  border-bottom: ${th('borderDefault')};
  display: flex;
  justify-content: space-between;
  padding: ${th('subGridUnit')} calc(${th('subGridUnit')} * 2);

  &:hover {
    background-color: ${th('colorSecondary')};
  }
`

const ActionButtons = styled.div`
  align-items: center;
  display: flex;
  flex: 1;
  justify-content: space-evenly;
  opacity: 0;

  div {
    cursor: pointer;
  }

  ${ReviewerItem}:hover & {
    opacity: 1;
  }
`

const ScrollContainer = styled.div`
  align-self: stretch;
  flex: 1;
  overflow: auto;
`
const Root = styled.div`
  align-items: stretch;
  align-self: stretch;
  background-color: ${th('backgroundColorReverse')};
  border: ${th('borderDefault')};
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 25vh;
`
// #endregion
