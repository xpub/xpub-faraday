import React from 'react'
import { pick, get } from 'lodash'
import { Icon } from '@pubsweet/ui'
import { connect } from 'react-redux'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { compose, withHandlers, withProps } from 'recompose'
import { DateParser } from 'pubsweet-components-faraday/src/components'
import {
  withModal,
  ConfirmationModal,
} from 'pubsweet-component-modal/src/components'

import {
  revokeReviewer,
  inviteReviewer,
  selectFetchingInvite,
  selectReviewersError,
  getCollectionReviewers,
} from '../../redux/reviewers'

const ResendRevoke = ({ showConfirmResend, showConfirmRevoke, status }) => (
  <ActionButtons>
    <div onClick={showConfirmResend}>
      <Icon primary>refresh-cw</Icon>
    </div>
    {status === 'pending' && (
      <div onClick={showConfirmRevoke}>
        <Icon primary>x-circle</Icon>
      </div>
    )}
  </ActionButtons>
)

const TR = ({
  index,
  reviewer: r,
  showConfirmResend,
  showConfirmRevoke,
  renderAcceptedLabel,
}) => {
  const submittedOn = get(r, 'review.submittedOn')
  return (
    <Row>
      <td>
        <ReviewerName>{r.name}</ReviewerName>{' '}
        {r.status === 'accepted' && (
          <AcceptedReviewer>{renderAcceptedLabel(index)}</AcceptedReviewer>
        )}
      </td>
      <DateParser timestamp={r.invitedOn}>
        {timestamp => <td width="150">{timestamp}</td>}
      </DateParser>
      <td width="200">
        <StatusText>
          {`${r.status === 'accepted' ? 'Agreed' : r.status}`}
        </StatusText>
        {r.respondedOn && (
          <DateParser timestamp={r.respondedOn}>
            {timestamp => <DateText>{`: ${timestamp}`}</DateText>}
          </DateParser>
        )}
      </td>
      <DateParser timestamp={submittedOn}>
        {timestamp => <td width="150">{timestamp}</td>}
      </DateParser>
      <td width={100}>
        {r.status === 'pending' && (
          <ResendRevoke
            showConfirmResend={showConfirmResend(r)}
            showConfirmRevoke={showConfirmRevoke(r.invitationId)}
            status={r.status}
          />
        )}
      </td>
    </Row>
  )
}
const ReviewersDetailsList = ({
  reviewers,
  showConfirmResend,
  showConfirmRevoke,
  renderAcceptedLabel,
}) =>
  reviewers.length > 0 ? (
    <Root>
      <Table>
        <thead>
          <tr>
            <td>Full Name</td>
            <td width="150">Invited On</td>
            <td width="200">Responded On</td>
            <td width="150">Submitted On</td>
            <td width="100" />
          </tr>
        </thead>
        <tbody>
          {reviewers.map((r, index) => (
            <TR
              index={index}
              key={`${`${r.email} ${index}`}`}
              renderAcceptedLabel={renderAcceptedLabel}
              reviewer={r}
              showConfirmResend={showConfirmResend}
              showConfirmRevoke={showConfirmRevoke}
            />
          ))}
        </tbody>
      </Table>
    </Root>
  ) : (
    <div> No reviewers details </div>
  )

const ModalWrapper = compose(
  connect(state => ({
    modalError: selectReviewersError(state),
    fetchingInvite: selectFetchingInvite(state),
  })),
)(({ fetchingInvite, ...rest }) => (
  <ConfirmationModal {...rest} isFetching={fetchingInvite} />
))

export default compose(
  connect(null, { inviteReviewer, revokeReviewer, getCollectionReviewers }),
  withModal(props => ({
    modalComponent: ModalWrapper,
  })),
  withProps(({ reviewers = [] }) => ({
    firstAccepted: reviewers.findIndex(r => r.status === 'accepted'),
  })),
  withHandlers({
    renderAcceptedLabel: ({ firstAccepted }) => index =>
      `Reviewer ${index - firstAccepted + 1}`,
    showConfirmResend: ({
      showModal,
      hideModal,
      collectionId,
      setModalError,
      inviteReviewer,
      goBackToReviewers,
      getCollectionReviewers,
    }) => reviewer => () => {
      showModal({
        title: 'Resend reviewer invite',
        confirmText: 'Resend',
        onConfirm: () => {
          inviteReviewer(
            pick(reviewer, ['email', 'firstName', 'lastName', 'affiliation']),
            collectionId,
          )
            .then(() => getCollectionReviewers(collectionId))
            .then(hideModal)
        },
        onCancel: hideModal,
      })
    },
    showConfirmRevoke: ({
      showModal,
      hideModal,
      collectionId,
      setModalError,
      revokeReviewer,
      goBackToReviewers,
      getCollectionReviewers,
    }) => invitationId => () => {
      showModal({
        title: 'Unassign Reviewer',
        confirmText: 'Unassign',
        onConfirm: () => {
          revokeReviewer(invitationId, collectionId)
            .then(() => getCollectionReviewers(collectionId))
            .then(hideModal)
        },
        onCancel: hideModal,
      })
    },
  }),
)(ReviewersDetailsList)

// #region styled-components
const defaultText = css`
  color: ${th('colorPrimary')};
  font-size: ${th('fontSizeBaseSmall')};
  font-family: ${th('fontReading')};
`

const ReviewerEmail = styled.span`
  ${defaultText};
`

const ReviewerName = ReviewerEmail.extend`
  text-decoration: underline;
`

const AcceptedReviewer = ReviewerEmail.extend`
  font-weight: bold;
  margin-left: ${th('subGridUnit')};
`

const StatusText = ReviewerEmail.extend`
  text-transform: uppercase;
`

const DateText = ReviewerEmail.extend``

const Root = styled.div`
  align-items: stretch;
  align-self: stretch;
  background-color: ${th('backgroundColorReverse')};
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 25vh;
  display: table;
`

const Table = styled.table`
  border-spacing: 0;
  border-collapse: collapse;
  width: 100%;
  thead {
    display: table;
    width: calc(100% - 1em);
  }
  tbody {
    overflow: auto;
    max-height: 180px;
    margin-bottom: ${th('gridUnit')};
    display: block;
    tr {
      display: table;
      width: 100%;
      table-layout: fixed;
    }
  }
  & thead tr {
    ${defaultText};
    border-bottom: ${th('borderDefault')};
    font-weight: bold;
    height: calc(${th('subGridUnit')} * 7);
    text-align: left;
    line-height: 1.5;
  }
`
const Row = styled.tr`
  ${defaultText};
  border-bottom: ${th('borderDefault')};
  height: calc(${th('subGridUnit')} * 7);
  text-align: left;
  line-height: 1.5;

  &:hover {
    background-color: ${th('backgroundColor')};
  }
`

const ActionButtons = styled.div`
  align-items: center;
  display: flex;
  flex: 1;
  justify-content: space-evenly;
  opacity: 0;

  div {
    cursor: pointer;
  }

  ${Row}:hover & {
    opacity: 1;
  }
`
// #endregion
