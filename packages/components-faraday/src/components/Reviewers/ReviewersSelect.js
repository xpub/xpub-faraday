import React from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { Field, formValueSelector, getFormMeta } from 'redux-form'

import { emailValidator } from '../utils'

const renderField = ({ input: { onBlur, ...rest }, meta }) => {
  const hasError = meta.touched && meta.error
  return (
    <RenderRoot>
      <Input
        hasError={hasError}
        {...rest}
        autoComplete="off"
        onBlur={() => {
          setTimeout(onBlur, 300)
        }}
        type="text"
      />
      {hasError && <ErrorMessage>Invalid email</ErrorMessage>}
    </RenderRoot>
  )
}

const ReviewersSelect = ({
  onSelect,
  formMeta,
  reviewerEmail,
  values = [],
  label = 'Email*',
}) => {
  const active = !!get(formMeta, 'email.active')
  const filteredValues = values
    .filter(v => v.isActive)
    .filter(v => v.email.includes(reviewerEmail))
  return (
    <Root>
      <FormLabel>{label}</FormLabel>
      <Field component={renderField} name="email" validate={emailValidator} />
      {active &&
        filteredValues.length > 0 && (
          <SuggestionsContainer>
            <ScrollContainer>
              {filteredValues.map(v => (
                <SuggestionItem key={v.email} onClick={onSelect(v)}>
                  {`${v.firstName} ${v.lastName}`} - {v.email}
                </SuggestionItem>
              ))}
            </ScrollContainer>
          </SuggestionsContainer>
        )}
    </Root>
  )
}

const reviewerSelector = formValueSelector('inviteReviewer')

export default connect(state => ({
  formMeta: getFormMeta('inviteReviewer')(state),
  reviewerEmail: reviewerSelector(state, 'email'),
}))(ReviewersSelect)

// #region styled-components
const defaultText = css`
  color: ${({ theme }) => theme.colorText};
  font-size: ${({ theme }) => theme.fontSizeBaseSmall};
  font-family: ${({ theme }) => theme.fontReading};
`

const ErrorMessage = styled.span`
  color: ${th('colorError')};
  font-family: ${th('fontInterface')};
  font-size: ${({ theme }) => theme.fontSizeBaseSmall};
`

const Input = styled.input`
  border: ${({ hasError, ...rest }) =>
    `1px solid ${
      hasError ? th('colorError')(rest) : th('colorPrimary')(rest)
    }`};
  border-radius: 2px;
  font-family: inherit;
  font-size: inherit;
  padding: 0 calc(${th('gridUnit')} / 2);
  height: calc(${th('gridUnit')} * 2);
`

const RenderRoot = styled.div`
  display: flex;
  flex-direction: column;
  max-width: calc(${th('gridUnit')} * 14);
`

const SuggestionItem = styled.span`
  align-items: center;
  align-self: stretch;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  height: calc(${th('subGridUnit')} * 6);
  padding-left: calc(${th('subGridUnit')} * 2);
  white-space: nowrap;

  &:hover {
    background-color: ${th('colorSecondary')};
  }
`

const ScrollContainer = styled.div`
  align-self: stretch;
  flex: 1;
  overflow: auto;
`

const SuggestionsContainer = styled.div`
  align-items: flex-start;
  background-color: ${th('backgroundColorReverse')};
  border: ${th('borderDefault')};
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  position: absolute;
  padding-top: calc(${th('subGridUnit')} * 2);
  top: 68px;
  max-height: calc(${th('subGridUnit')} * 6 * 4);
  width: calc(${th('gridUnit')} * 16);
`

const FormLabel = styled.span`
  ${defaultText};
  font-weight: 300;
  height: calc(${th('subGridUnit')} * 3);
  text-transform: uppercase;
`

const Root = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  height: 92px;
  margin-right: ${th('subGridUnit')};
  position: relative;
`
// #endregion
