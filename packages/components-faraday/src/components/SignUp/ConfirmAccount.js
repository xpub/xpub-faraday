import React from 'react'
import { get } from 'lodash'
import { connect } from 'react-redux'
import { Button, H2, Spinner } from '@pubsweet/ui'
import { compose, lifecycle, withState } from 'recompose'
import { Row, Text, ShadowedBox } from 'pubsweet-component-faraday-ui'

import { parseSearchParams } from '../utils'
import { confirmUser } from '../../redux/users'

const loading = `Loading...`
const confirmTitle = `Welcome to Hindawi!`
const confirmSubtitle = `Your account has been successfully confirmed.`
const errorTitle = `Something went wrong. Please try again.`

const ConfirmAccount = ({ message: { title, subtitle }, history }) => (
  <ShadowedBox center mt={5}>
    <H2>{title}</H2>
    <Row mb={title !== errorTitle ? 2 : 0} mt={2}>
      <Text>{subtitle}</Text>
    </Row>
    {title !== errorTitle && (
      <Row>
        {title === loading ? (
          <Spinner />
        ) : (
          <Button onClick={() => history.replace('/')} primary>
            Go to Dashboard
          </Button>
        )}
      </Row>
    )}
  </ShadowedBox>
)

export default compose(
  connect(null, { confirmUser }),
  withState('message', 'setConfirmMessage', {
    title: loading,
  }),
  lifecycle({
    componentDidMount() {
      const { location, confirmUser, setConfirmMessage } = this.props
      const { confirmationToken, userId } = parseSearchParams(location.search)

      if (userId) {
        confirmUser(userId, confirmationToken)
          .then(() => {
            setConfirmMessage({
              title: confirmTitle,
              subtitle: confirmSubtitle,
            })
          })
          .catch(err => {
            const subtitle = get(
              JSON.parse(err.response),
              'error',
              'Something went wrong. Please try again.',
            )
            setConfirmMessage({
              title: errorTitle,
              subtitle,
            })
          })
      }
    },
  }),
)(ConfirmAccount)
