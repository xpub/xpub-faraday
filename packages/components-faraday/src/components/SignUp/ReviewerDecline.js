import React from 'react'
import { get } from 'lodash'
import { withJournal } from 'xpub-journal'
import { compose, lifecycle } from 'recompose'
import { H2 } from '@pubsweet/ui'
import {
  Row,
  Text,
  ActionLink,
  ShadowedBox,
} from 'pubsweet-component-faraday-ui'

import { redirectToError } from '../utils'
import { reviewerDecline } from '../../redux/reviewers'

const ReviewerDecline = ({ journal }) => (
  <ShadowedBox center mt={2} width={60}>
    <H2>Thank you for letting us know</H2>

    <Row mt={2}>
      <Text align="center">
        We hope you will review for Hindawi in the future. If you want any more
        information, or would like to submit a review for this article, then
        please contact us at{' '}
        <ActionLink to={`mailto:${get(journal, 'metadata.email')}`}>
          {get(journal, 'metadata.email')}
        </ActionLink>.
      </Text>
    </Row>
  </ShadowedBox>
)

export default compose(
  withJournal,
  lifecycle({
    componentDidMount() {
      const {
        history,
        fragmentId,
        collectionId,
        invitationId,
        invitationToken,
      } = this.props
      reviewerDecline({
        fragmentId,
        invitationId,
        collectionId,
        invitationToken,
      }).catch(redirectToError(history.replace))
    },
  }),
)(ReviewerDecline)
