import React from 'react'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { Button, H2, Spinner } from '@pubsweet/ui'
import { compose, withState, lifecycle } from 'recompose'
import { loginUser } from 'pubsweet-component-login/actions'
import {
  Row,
  Text,
  ShadowedBox,
  PasswordValidation,
  handleError,
  withFetching,
  passwordValidator,
} from 'pubsweet-component-faraday-ui'

import { redirectToError } from '../utils'
import { reviewerDecision, setReviewerPassword } from '../../redux/reviewers'

const agreeText = `You have been invited to review a manuscript on the Hindawi platform. Please set a password and proceed to the manuscript.`
const declineText = `You have decline to work on a manuscript.`

const ReviewerInviteDecision = ({
  agree,
  error,
  isFetching,
  handleSubmit,
  errorMessage,
  reviewerEmail,
  fetchingError,
}) => (
  <ShadowedBox center mt={2} width={60}>
    <H2>Reviewer Invitation</H2>
    <Text align="center" secondary>
      {reviewerEmail}
    </Text>
    <Row mt={2}>
      <Text align="center">{agree === 'true' ? agreeText : declineText}</Text>
    </Row>
    <PasswordValidation formLabel="Password" formName="invite-reviewer" />
    {fetchingError && (
      <Row mt={2}>
        <Text align="center" error>
          {fetchingError}
        </Text>
      </Row>
    )}
    <Row mt={2}>
      {isFetching ? (
        <Spinner />
      ) : (
        <Button onClick={handleSubmit} primary size="medium">
          CONFIRM
        </Button>
      )}
    </Row>
  </ShadowedBox>
)

export default compose(
  withFetching,
  withState('reviewerEmail', 'setEmail', ''),
  connect(null, {
    loginUser,
  }),
  lifecycle({
    componentDidMount() {
      const {
        agree,
        email,
        history,
        setEmail,
        fragmentId,
        collectionId,
        invitationId,
      } = this.props
      setEmail(email)

      if (agree === 'false') {
        reviewerDecision({
          fragmentId,
          agree: false,
          collectionId,
          invitationId,
        }).catch(redirectToError(history.replace))
      }
    },
  }),
  reduxForm({
    form: 'invite-reviewer',
    validate: passwordValidator,
    onSubmit: (
      { password },
      dispatch,
      {
        email,
        token,
        setError,
        loginUser,
        fragmentId,
        setFetching,
        collectionId,
        invitationId,
      },
    ) => {
      setFetching(true)
      setError('')
      setReviewerPassword({
        email,
        token,
        password,
      })
        .then(() => {
          setError('')
          setFetching(false)
          loginUser(
            {
              username: email,
              password,
            },
            `/projects/${collectionId}/versions/${fragmentId}/details?agree=${true}&invitationId=${invitationId}`,
          )
        })
        .catch(err => {
          setFetching(false)
          handleError(setError)(err)
        })
    },
  }),
)(ReviewerInviteDecision)
