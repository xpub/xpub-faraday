import React from 'react'
import { H2 } from '@pubsweet/ui'
import { Text, ShadowedBox } from 'pubsweet-component-faraday-ui'

import Step0 from './SignUpStep0'
import Step1 from './SignUpStep1'

const containerPadding = {
  pt: 4,
  pb: 4,
  pl: 4,
  pr: 4,
}

const SignUpInvitation = ({
  type,
  step,
  error,
  onBack,
  journal,
  onSubmit,
  nextStep,
  prevStep,
  onSubmitText,
  initialValues,
  title = 'Add New Account Details',
}) => (
  <ShadowedBox center mb={3} mt={10} {...containerPadding}>
    <H2 mb={step === 0 ? 0 : 2}>{title}</H2>
    {error && <Text error>Token expired or Something went wrong.</Text>}
    {step === 0 && (
      <Step0
        error={error}
        initialValues={initialValues}
        journal={journal}
        onSubmit={nextStep}
        type={type}
      />
    )}
    {step === 1 && (
      <Step1
        error={error}
        initialValues={initialValues}
        journal={journal}
        onBack={onBack}
        onSubmit={onSubmit}
        onSubmitText={onSubmitText}
        type={type}
      />
    )}
  </ShadowedBox>
)

export default SignUpInvitation
