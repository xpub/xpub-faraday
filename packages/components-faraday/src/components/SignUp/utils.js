/* eslint-disable */
import { omit } from 'lodash'
import { create } from 'pubsweet-client/src/helpers/api'
import { loginUser } from 'pubsweet-component-login/actions'

import { handleFormError } from '../utils'

export const parseSignupAuthor = ({
  token,
  confirmNewPassword,
  ...values
}) => ({
  ...values,
})

export const parseSearchParams = url => {
  const params = new URLSearchParams(url)
  const parsedObject = {}
  for ([key, value] of params) {
    parsedObject[key] = value
  }
  return parsedObject
}

export const login = (dispatch, values, history) =>
  dispatch(loginUser(values))
    .then(() => {
      history.replace('/')
    })
    .catch(handleFormError)

export const confirmUser = (email, token, history) => (values, dispatch) => {
  const request = { ...values, email, token }
  if (values) {
    return create(
      '/users/reset-password',
      omit(request, ['confirmNewPassword']),
    )
      .then(r => {
        const { username } = r
        const { password } = values
        login(dispatch, { username, password }, history)
      })
      .catch(handleFormError)
  }
}

export const signUpUser = history => (values, dispatch) =>
  create('/users', parseSignupAuthor(values))
    .then(r => {
      const { username } = r
      const { password } = values
      login(dispatch, { username, password }, history).then(() => {
        create('/emails', {
          email: values.email,
          type: 'signup',
        })
      })
    })
    .catch(handleFormError)

export const resetUserPassword = history => ({ email }, dispatch) =>
  create(`/users/forgot-password`, { email })
    .then(() => {
      history.push('/info-page', {
        path: '/',
        buttonText: 'Go to login screen',
        title: 'Reset Password Email Sent',
      })
    })
    .catch(handleFormError)

export const setNewPassword = history => (
  { email, token, password },
  dispatch,
) =>
  create(`/users/reset-password`, { email, token, password })
    .then(() => {
      login(dispatch, { username: email, password }, history).then(() =>
        history.push('/'),
      )
    })
    .catch(handleFormError)
