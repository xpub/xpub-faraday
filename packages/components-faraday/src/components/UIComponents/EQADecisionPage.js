import React, { Fragment } from 'react'
import { isEmpty } from 'lodash'
import { reduxForm } from 'redux-form'
import { Button, H2, TextArea, ValidatedField } from '@pubsweet/ui'
import {
  compose,
  withState,
  lifecycle,
  withHandlers,
  setDisplayName,
} from 'recompose'
import {
  Row,
  Text,
  Label,
  OpenModal,
  ShadowedBox,
  ItemOverrideAlert,
} from 'pubsweet-component-faraday-ui'

import { parseSearchParams } from '../utils'
import withEditorialDecision from './withEditorialDecision'

const Enhanced = () => (
  <Row>
    <ItemOverrideAlert data-test-id="manuscript-return-reason" vertical>
      <Label required>Return reason</Label>
      <ValidatedField component={TextArea} name="comments" />
    </ItemOverrideAlert>
  </Row>
)

const FormModal = reduxForm({
  form: 'eqa',
  onSubmit: (
    { comments },
    dispatch,
    { technicalDecision, params: { collectionId, token } },
  ) => modalProps =>
    technicalDecision({
      token,
      comments,
      step: 'eqa',
      agree: false,
      collectionId,
      modalProps,
    }),
})(({ isFetching, handleSubmit }) => (
  <OpenModal
    content={Enhanced}
    isFetching={isFetching}
    modalKey="rejectManuscript"
    onConfirm={modalProps => handleSubmit()(modalProps)}
    title="Reject Manuscript"
  >
    {showModal => (
      <Button data-test-id="eqs-no-button" onClick={showModal}>
        RETURN TO EiC
      </Button>
    )}
  </OpenModal>
))

const EQADecisionPage = ({
  params,
  isFetching,
  errorMessage,
  successMessage,
  acceptManuscript,
  ...rest
}) => (
  <ShadowedBox center mt={5}>
    <H2>Editorial decision</H2>
    {errorMessage && (
      <Row mt={1}>
        <Text>{errorMessage}</Text>
      </Row>
    )}
    {successMessage && (
      <Row mt={1}>
        <Text>{successMessage}</Text>
      </Row>
    )}
    {isEmpty(errorMessage) &&
      isEmpty(successMessage) && (
        <Fragment>
          <Row>
            <Text mb={2} mt={2} secondary>
              Take a decision for manuscript <b>{params.customId}</b>.
            </Text>
          </Row>
          <Row>
            <FormModal isFetching={isFetching} params={params} {...rest} />
            <OpenModal
              isFetching={isFetching}
              modalKey="acceptManuscript"
              onConfirm={acceptManuscript}
              subtitle="Are you sure you want to accept this manuscript?"
              title="Accept Manuscript"
            >
              {showModal => (
                <Button
                  data-test-id="eqa-yes-button"
                  onClick={showModal}
                  primary
                >
                  ACCEPT
                </Button>
              )}
            </OpenModal>
          </Row>
        </Fragment>
      )}
  </ShadowedBox>
)

export default compose(
  withEditorialDecision,
  setDisplayName('EQA Decision page'),
  withState('params', 'setParams', {
    title: '',
    token: null,
    comments: null,
    collectionId: null,
  }),
  lifecycle({
    componentDidMount() {
      const { location, setParams } = this.props
      const { comments, collectionId, token, title } = parseSearchParams(
        location.search,
      )
      setParams({ comments, collectionId, token, title })
    },
  }),
  withHandlers({
    acceptManuscript: ({
      technicalDecision,
      params: { collectionId, token },
    }) => modalProps =>
      technicalDecision({
        token,
        modalProps,
        step: 'eqa',
        agree: true,
        collectionId,
      }),
  }),
)(EQADecisionPage)
