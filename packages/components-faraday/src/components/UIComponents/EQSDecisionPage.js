import React from 'react'
import { isEmpty } from 'lodash'
import { reduxForm } from 'redux-form'
import { required } from 'xpub-validators'
import { Button, H2, TextField, ValidatedField } from '@pubsweet/ui'
import {
  compose,
  withState,
  lifecycle,
  withHandlers,
  setDisplayName,
} from 'recompose'
import {
  Row,
  Text,
  Item,
  Label,
  OpenModal,
  ShadowedBox,
} from 'pubsweet-component-faraday-ui'

import withEditorialDecision from './withEditorialDecision'

import {
  digitValidator,
  parseSearchParams,
  manuscriptIdSizeValidator,
} from '../utils'

const sizeValidator = manuscriptIdSizeValidator(7)

const Enhanched = () => (
  <Row>
    <Item data-test-id="manuscript-id" vertical>
      <Label required>Manuscript ID</Label>
      <ValidatedField
        component={TextField}
        data-test-id="eqs-manuscript-id"
        name="customId"
        validate={[required, digitValidator, sizeValidator]}
      />
    </Item>
  </Row>
)

const FormModal = reduxForm({
  form: 'eqs',
  onSubmit: (
    { customId },
    dispatch,
    { params: { collectionId, token }, technicalDecision },
  ) => modalProps =>
    technicalDecision({
      token,
      customId,
      step: 'eqs',
      agree: true,
      collectionId,
      modalProps,
    }),
})(({ isFetching, handleSubmit }) => (
  <OpenModal
    content={Enhanched}
    isFetching={isFetching}
    modalKey="acceptManuscript"
    onConfirm={modalProps => handleSubmit()(modalProps)}
    title="Accept Manuscript"
  >
    {showModal => (
      <Button data-test-id="eqs-yes-button" onClick={showModal} primary>
        YES
      </Button>
    )}
  </OpenModal>
))

const EQSDecisionPage = ({
  params,
  isFetching,
  eqsDecision,
  successMessage,
  rejectManuscript,
  ...rest
}) => (
  <ShadowedBox center mt={5}>
    <H2>Editorial decision</H2>
    {!successMessage && (
      <Row mt={2}>
        <Text secondary>
          Did manuscript titled <b>{params.title}</b> pass EQS checks?
        </Text>
      </Row>
    )}
    {successMessage && (
      <Row mt={1}>
        <Text>{successMessage}</Text>
      </Row>
    )}
    {isEmpty(successMessage) && (
      <Row justify="space-around" mt={2}>
        <OpenModal
          isFetching={isFetching}
          modalKey="denyManuscript"
          onConfirm={rejectManuscript}
          subtitle="Are you sure you want to reject this manuscript?"
          title="Reject manuscript"
        >
          {showModal => (
            <Button data-test-id="eqs-no-button" onClick={showModal}>
              NO
            </Button>
          )}
        </OpenModal>
        <FormModal isFetching={isFetching} params={params} {...rest} />
      </Row>
    )}
  </ShadowedBox>
)

export default compose(
  withEditorialDecision,
  setDisplayName('EQS Decision page'),
  withState('params', 'setParams', {
    title: '',
    token: null,
    customId: null,
    collectionId: null,
  }),
  lifecycle({
    componentDidMount() {
      const { location, setParams } = this.props
      const { customId, collectionId, token, title } = parseSearchParams(
        location.search,
      )
      setParams({ customId, collectionId, token, title })
    },
  }),
  withHandlers({
    rejectManuscript: ({
      technicalDecision,
      params: { collectionId, token },
    }) => modalProps =>
      technicalDecision({
        token,
        modalProps,
        step: 'eqs',
        agree: false,
        collectionId,
      }),
  }),
)(EQSDecisionPage)

// #region styles
// #endregion
