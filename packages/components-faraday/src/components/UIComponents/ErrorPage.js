import React from 'react'
import { Button, H2 } from '@pubsweet/ui'
import { Row, ShadowedBox, Text } from 'pubsweet-component-faraday-ui'

const ErrorPage = ({ location: { state }, history }) => (
  <ShadowedBox center mt={2} width={60}>
    <H2>Error</H2>

    <Row mt={2}>
      <Text align="center">{state}</Text>
    </Row>

    <Row mt={2}>
      <Button onClick={() => history.push('/')} primary>
        Go to Dashboard
      </Button>
    </Row>
  </ShadowedBox>
)

export default ErrorPage
