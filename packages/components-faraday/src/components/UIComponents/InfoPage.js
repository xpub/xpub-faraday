import React from 'react'
import { Button, H2 } from '@pubsweet/ui'
import { Row, ShadowedBox, Text } from 'pubsweet-component-faraday-ui'

const InfoPage = ({
  location: {
    state: {
      content = '',
      path = '/dashboard',
      title = 'Successfully',
      buttonText = 'Go to Dashboard',
    },
  },
  history,
}) => (
  <ShadowedBox center mt={5}>
    <H2>{title}</H2>
    <Row mb={2} mt={2}>
      <Text>{content}</Text>
    </Row>

    <Row>
      <Button onClick={() => history.push(path)} primary>
        {buttonText}
      </Button>
    </Row>
  </ShadowedBox>
)

export default InfoPage
