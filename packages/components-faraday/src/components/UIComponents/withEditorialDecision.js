import { update } from 'pubsweet-client/src/helpers/api'
import { compose, withHandlers, withState } from 'recompose'
import { withFetching, handleError } from 'pubsweet-component-faraday-ui'

const technicalDecision = ({ collectionId, ...body }) =>
  update(`/collections/${collectionId}/status`, body)

export default compose(
  withFetching,
  withState('successMessage', 'setSuccess', ''),
  withHandlers({
    technicalDecision: ({ setSuccess, setFetching }) => ({
      modalProps: { setModalError, hideModal },
      ...decision
    }) => {
      setFetching(true)
      return technicalDecision(decision)
        .then(() => {
          setFetching(false)
          hideModal()
          setSuccess(
            decision.step === 'eqs'
              ? `Manuscript ${
                  decision.agree ? 'accepted' : 'declined'
                }. Thank you for your technical check!`
              : 'Manuscript decision submitted. Thank you!',
          )
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
  }),
)
