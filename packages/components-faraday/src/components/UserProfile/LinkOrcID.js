import React from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import {
  Row,
  Item,
  Text,
  Label,
  OpenModal,
  ActionLink,
  ShadowedBox,
  IconTooltip,
  withFetching,
} from 'pubsweet-component-faraday-ui'

const ORCIDLinkTooltip = () => (
  <Text secondary>
    Hindawi is collecting and displaying your ORCID iD so we can ensure your
    work is recognised. When you click the “Authorize” button, we will ask you
    to share your iD using an authenticated process: either by{' '}
    <ActionLink to="https://support.orcid.org/knowledgebase/topics/141838">
      {' '}
      registering for an ORCID iD{' '}
    </ActionLink>{' '}
    or, if you already have one, to{' '}
    <ActionLink to="https://support.orcid.org/knowledgebase/topics/141838">
      sign into your ORCID account{' '}
    </ActionLink>, then granting us permission to get your ORCID iD. We do this
    to ensure that you are correctly identified and securely connecting your
    ORCID iD.
  </Text>
)

const ORCIDDisplayTooltip = () => (
  <Text secondary>
    To acknowledge that you have used your iD and that it has been
    authenticated, we display the ORCID iD icon alongside your name on our
    website. Learn more in{' '}
    <ActionLink to="https://orcid.org/blog/2013/02/22/how-should-orcid-id-be-displayed">
      {' '}
      How should an ORCID iD be displayed
    </ActionLink>.
  </Text>
)

const LinkOrcID = ({ orcid = {}, unlinkOrcid, id, ...rest }) => (
  <ShadowedBox mt={2}>
    <Row alignItems="center">
      <Item alignItems="center">
        <Label>ORCID iD </Label>
        <IconTooltip
          content={get(orcid, 'orcid') ? ORCIDDisplayTooltip : ORCIDLinkTooltip}
          iconSize={2}
          interactive
          primary
        />
      </Item>
      <Item justify="flex-end">
        {orcid.orcid ? (
          <OpenModal
            confirmText="Unlink"
            onConfirm={unlinkOrcid}
            subtitle="Are you sure you want to unlink your ORCID account?"
            title="Unlink ORCID"
            {...rest}
          >
            {showModal => <ActionLink onClick={showModal}>Unlink</ActionLink>}
          </OpenModal>
        ) : (
          <Link href={`/api/users/orcid?userId=${id}`}>Link</Link>
        )}
      </Item>
    </Row>

    {orcid.orcid && (
      <Row>
        <Item>
          <Text>
            <a
              href="https://orcid.org"
              rel="noopener noreferrer"
              target="_blank"
            >
              <Img
                alt="ORCID Logo"
                src="https://orcid.org/sites/default/files/images/orcid_16x16.png"
              />
            </a>
            <ActionLink to={`https://orcid.org/${orcid.orcid}`}>
              {`https://orcid.org/${orcid.orcid}`}
            </ActionLink>
          </Text>
        </Item>
      </Row>
    )}
  </ShadowedBox>
)

export default withFetching(LinkOrcID)

// #region styles
const Link = styled.a`
  color: ${th('action.color')};
  cursor: default;
  text-decoration: none;

  &:hover {
    color: ${th('colorSecondary')};
    text-decoration: none;
  }

  &:active,
  &:focus {
    color: ${th('action.colorActive')};
    text-decoration: none;
  }
`

const Img = styled.img`
  vertical-align: text-bottom;
  padding-right: calc(${th('gridUnit')} / 2);
`
// #endregion
