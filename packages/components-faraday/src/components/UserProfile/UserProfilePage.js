import React from 'react'
import { get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { withJournal } from 'xpub-journal'
import { compose, withHandlers } from 'recompose'
import { selectCurrentUser } from 'xpub-selectors'
import {
  Row,
  Text,
  ActionLink,
  UserProfile,
  handleError,
} from 'pubsweet-component-faraday-ui'

import { saveUserDetails } from '../utils'
import { LinkOrcID, EmailNotifications } from './'
import { changeEmailSubscription } from '../../redux/users'

const UserProfilePage = ({
  user,
  journal,
  history,
  saveUser,
  unlinkOrcid,
  changeEmailSubscription,
}) => (
  <Root>
    <Row alignItems="center" justify="flex-start">
      <ActionLink
        fontIcon="arrowLeft"
        onClick={() => history.push('/dashboard')}
        paddingBottom={1.2}
      >
        Dashboard
      </ActionLink>
    </Row>
    <Row alignItems="center" justify="flex-start">
      <H2>Account Settings</H2>
      <Text ml={2} secondary>
        {user.email}
      </Text>
    </Row>

    <UserProfile journal={journal} mt={2} onSave={saveUser} user={user} />

    <EmailNotifications
      changeEmailSubscription={changeEmailSubscription}
      isSubscribed={get(user, 'notifications.email.user', true)}
      token={get(user, 'accessTokens.unsubscribe')}
      userId={get(user, 'id')}
    />
    <LinkOrcID
      id={get(user, 'id')}
      orcid={get(user, 'orcid')}
      unlinkOrcid={unlinkOrcid}
    />
  </Root>
)

export default compose(
  withJournal,
  connect(
    state => ({
      user: selectCurrentUser(state),
    }),
    { changeEmailSubscription, saveUserDetails },
  ),
  withHandlers({
    saveUser: ({ user, saveUserDetails }) => (
      values,
      { setFetching, toggleEdit, setError, clearError },
    ) => {
      setFetching(true)
      clearError()
      saveUserDetails(user.id, values)
        .then(() => {
          setFetching(false)
          toggleEdit()
        })
        .catch(handleError(setError))
    },
    unlinkOrcid: ({ user, saveUserDetails }) => ({
      hideModal,
      clearError,
      setFetching,
      setModalError,
    }) => {
      setFetching(true)
      setModalError('')
      saveUserDetails(user.id, { orcid: {} })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(err => {
          setFetching(false)
          handleError(setModalError)(err)
        })
    },
  }),
)(UserProfilePage)

// #region styles
const Root = styled.div`
  display: flex;
  flex-direction: column;
`
// #endregion
