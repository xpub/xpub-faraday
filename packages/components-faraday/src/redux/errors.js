export const hasManuscriptFailure = state =>
  state.customError === 'manuscriptFailure'

export const clearCustomError = () => ({
  type: 'CLEAR_CUSTOM_ERROR',
})

export default (state = '', action) => {
  switch (action.type) {
    case 'CLEAR_CUSTOM_ERROR':
      return ''
    case 'GET_FRAGMENT_FAILURE':
      return 'manuscriptFailure'
    default:
      return state
  }
}
