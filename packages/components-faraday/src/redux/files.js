import request, { remove, get as apiGet } from 'pubsweet-client/src/helpers/api'
import { get } from 'lodash'

const initialState = {
  isFetching: {
    manuscripts: false,
    supplementary: false,
    coverLetter: false,
  },
  error: null,
}
// constants
const UPLOAD_REQUEST = 'files/UPLOAD_REQUEST'
const UPLOAD_FAILURE = 'files/UPLOAD_FAILURE'
const UPLOAD_SUCCESS = 'files/UPLOAD_SUCCESS'

const REMOVE_REQUEST = 'files/REMOVE_REQUEST'
const REMOVE_FAILURE = 'files/REMOVE_FAILURE'
const REMOVE_SUCCESS = 'files/REMOVE_SUCCESS'

// action creators
const createFileData = ({ file, type, fragmentId, newName }) => {
  const data = new FormData()
  data.append('fileType', type)
  data.append('fragmentId', fragmentId)
  data.append('newName', newName)
  data.append('file', file)

  return {
    method: 'POST',
    headers: {
      Accept: 'text/plain',
    },
    body: data,
  }
}

const setFileName = (file, { files = [] }) => {
  let newFilename = file.name
  const fileCount = Object.values(files)
    .reduce((acc, f) => [...acc, ...f], [])
    .map(f => f.originalName)
    .reduce((count, name) => {
      if (file.name === name) {
        count += 1
      }
      return count
    }, 0)
  if (fileCount > 0) {
    const ext = file.name.split('.').reverse()[0]
    const filename = file.name.replace(`.${ext}`, '')
    newFilename = `${filename} (${fileCount}).${ext}`
  }

  return newFilename
}

// selectors
export const getRequestStatus = state => get(state, 'files.isFetching', false)
export const getFileFetching = state =>
  Object.values(get(state, 'files.isFetching', initialState.isFetching)).some(
    v => v,
  )
export const getFileError = state => get(state, 'files.error', null)

// thunked actions
export const uploadFile = ({ file, type, fragment }) => {
  const newName = setFileName(file, fragment)
  return request(
    '/files',
    createFileData({ file, type, fragmentId: fragment.id, newName }),
  )
}

export const deleteFile = ({ fileId, type = 'manuscripts' }) =>
  remove(`/files/${fileId}`)

export const getSignedUrl = fileId => apiGet(`/files/${fileId}`)

// reducer
export default (state = initialState, action) => {
  switch (action.type) {
    case REMOVE_REQUEST:
    case UPLOAD_REQUEST:
      return {
        ...state,
        error: null,
        isFetching: {
          ...state.isFetching,
          [action.fileType]: true,
        },
      }
    case UPLOAD_FAILURE:
    case REMOVE_FAILURE:
      return {
        ...state,
        isFetching: initialState.isFetching,
        error: action.error,
      }
    case UPLOAD_SUCCESS:
    case REMOVE_SUCCESS:
      return {
        ...state,
        isFetching: initialState.isFetching,
        error: null,
      }
    default:
      return state
  }
}
