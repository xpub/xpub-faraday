import { get } from 'lodash'
import { actions } from 'pubsweet-client'
import { create, update } from 'pubsweet-client/src/helpers/api'

const LOGIN_SUCCESS = 'LOGIN_SUCCESS'

const loginSuccess = user => ({
  type: LOGIN_SUCCESS,
  token: user.token,
  user,
})

export const currentUserIs = (state, role) =>
  get(state, `currentUser.user.${role}`)

export const confirmUser = (userId, confirmationToken) => dispatch =>
  create(`/users/confirm`, {
    userId,
    confirmationToken,
  }).then(user => {
    localStorage.setItem('token', user.token)
    return dispatch(loginSuccess(user))
  })

export const changeEmailSubscription = (
  id,
  subscribe = true,
  token,
) => dispatch =>
  update(`/users/subscribe`, {
    id,
    token,
    subscribe,
  }).then(() => dispatch(actions.getCurrentUser()))
