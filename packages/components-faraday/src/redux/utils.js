export const orderReviewers = r => {
  switch (r.status) {
    case 'pending':
      return -1
    case 'accepted':
      return 0
    case 'declined':
    default:
      return 1
  }
}

export const handleError = (fn, dispatch = null) => err => {
  if (typeof dispatch === 'function') {
    dispatch(fn(err))
  } else {
    fn(err)
  }
  throw err
}
