describe('Logged user should not reach /signup', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
  })

  it('Log in and should not reach /signup', function loggedUserNotReachSignUp() {
    const { admin, author } = this

    cy.loginApi(admin.username, admin.password)
    cy.visit('/')
    cy.url().should('contain', '/dashboard')
    cy.visit('/signup')
    cy.url().should('not.eq', '/signup')

    cy.loginApi(author.email, author.password)
    cy.visit('/')
    cy.url().should('contain', '/dashboard')
    cy.visit('/signup')
    cy.url().should('not.eq', '/signup')
  })
})
