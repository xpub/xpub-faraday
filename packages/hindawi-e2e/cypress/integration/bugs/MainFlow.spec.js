describe('Submit Manuscript', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Give a review for major revision as Reviewer', function majorRevisionReview() {
    const { reviewer, he, statuses } = this

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.wait(3000)

    cy.submitReview('Major Revision')
    cy.wait(3000)
    // cy.checkStatus(statuses.reviewCompleted.reviewer) // Bug

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE makes recommendation for major revision', function majorRevisionRecommendation() {
    const { he, eic, statuses } = this
    cy.loginApi(he.email, he.password)
    cy
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.heMakesRecommendation('Major Revision')

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.editorInChief)
  })

  it('Author submit major revision', function majorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.visit('/').wait(2000)
    cy
      .get(`[data-test-id="dashboard-list-items"]`)
      .find(`[data-test-id="row"]`)
      .first()
      .click()
    cy.location().then(loc => {
      Cypress.env(
        'fragmentIdV2',
        loc.pathname
          .replace('/details', '')
          .split('/')
          .pop(),
      )
    })
  })

  it('Reviewer invited in previous version but still pending should not see manuscript', function pendingReviewerPrevVersion() {
    const { reviewer } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit(`/`)
    cy.wait(3000)

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.be.visible')
      .click()

    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('not.be.visible')
  })

  // Minor Revision
  it('Give a review for minor revision as Reviewer', function minorRevisionReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)
    cy.submitReview('Minor Revision')

    cy.wait(3000)
    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE makes recommendation for minor revision', function minorRevisionRecommendation() {
    const { he, eic, statuses } = this
    cy.loginApi(he.email, he.password)
    cy
      .visit('/')
      .wait(2000)
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`)
      .should('be.visible')
      .click()
    cy.heMakesRecommendation('Minor Revision')

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.editorInChief)
  })

  it('Author submit minor revision', function minorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV2')}"]`).click()

    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.visit('/').wait(2000)
    cy
      .get(`[data-test-id="dashboard-list-items"]`)
      .find(`[data-test-id="row"]`)
      .first()
      .click()
    cy.location().then(loc => {
      Cypress.env(
        'fragmentIdV3',
        loc.pathname
          .replace('/details', '')
          .split('/')
          .pop(),
      )
    })
  })

  // Publish
  it('Invite reviewers as HE for another version', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Give a review for publish as Reviewer', function submitPublishReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')
    cy.wait(3000)
    // cy.checkStatus(statuses.reviewCompleted.reviewer) // Bug

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('HE makes recommendation to publish', function heMakesRecommendationToPublish() {
    const { he, eic, statuses } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`).click()
    cy.heMakesRecommendation('Publish')
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.pendingApproval.editorInChief)
  })

  it('EiC makes decision to publish', function eicMakesDecisioToPublish() {
    const { eic, statuses } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.editorInChief)

    cy
      .get('[data-test-id="contextual-box-editorial-comments"]')
      .should('be.visible')
      .contains('Publish')

    cy.eicMakesDecision('Publish')
    cy.checkStatus(statuses.inQA.editorInChief)
  })

  it('Manuscript approved by EQA', function approveManuscriptByEQA() {
    const { admin, author, statuses } = this
    cy.loginApi(admin.username, admin.password)
    cy.visit('/')
    cy.wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`).click()
    cy.approveManuscriptByEQA({ admin })
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV3')}"]`).click()
    cy.checkStatus(statuses.accepted.author)
  })
})
