describe('Submit manuscript with conflicts of interests', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
    cy.fixture('models/fragment').as('fragment')
  })

  it('Successfully submit a manuscript with conflicts of interests', function submitManuscriptWithConflicts() {
    const { author, fragment } = this

    cy.loginApi(author.email, author.password)
    cy.visit(`/`)

    cy
      .get('[data-test-id="new-manuscript"]')
      .should('be.visible')
      .click()

    cy
      .get('[data-test-id="agree-checkbox"] input')
      .should('be.visible')
      .check({ force: true })
    cy.get('[data-test-id="submission-next"]').click()

    cy.location().then(loc => {
      Cypress.env(
        'fragmentId',
        loc.pathname
          .replace('/submit', '')
          .split('/')
          .pop(),
      )
      Cypress.env(
        'collectionId',
        loc.pathname.replace('/submit', '').split('/')[2],
      )
    })

    cy
      .get('[data-test-id="submission-title"] input')
      .clear()
      .type(fragment.title)

    cy.get('[data-test-id="submission-type"] button').click()
    cy.contains(fragment.manuscriptType).click({ force: true })

    cy
      .get('[data-test-id="submission-abstract"] textarea')
      .clear()
      .type(fragment.abstract)

    fragment.authors.forEach(author => {
      cy.get('[data-test-id="submission-add-author"] button').click()
      cy.get('[data-test-id="author-card-email"]').type(
        `${Cypress.env('email') +
          Math.random()
            .toString(22)
            .substring(8)}@thinslices.com`,
      )
      cy.get('[data-test-id="author-card-firstname"]').type(author.firstName)
      cy.get('[data-test-id="author-card-lastname"]').type(author.lastName)
      cy
        .get('[data-test-id="author-card-affiliation"]')
        .type(author.affiliation)

      cy
        .get('[data-test-id="item"]')
        .find('[role="listbox"]')
        .click()
      cy
        .get('[role="option"]')
        .contains(author.country)
        .click()

      cy.get('[data-test-id="author-card-save"] span').click()

      cy.get('input[name="conflicts.hasConflicts"][value="yes"]').click()
      cy.get('textarea[name="conflicts.message"]').type('conflict')

      cy.get('input[name="conflicts.hasDataAvailability"][value="no"]').click()
      cy.get('input[name="conflicts.hasDataAvailability"][value="no"]').click()

      cy.get('textarea[name="conflicts.dataAvailabilityMessage"]').type('lorem')

      cy.get('input[name="conflicts.hasFunding"][value="no"]').click()
      cy.get('input[name="conflicts.hasFunding"][value="no"]').click()

      cy.get('textarea[name="conflicts.fundingMessage"]').type('lorererem')

      cy.get('[data-test-id="submission-next"]').click()
      cy.wait(5000)

      cy
        .get('[data-test-id="main-manuscript"]')
        .find('input[type="file"]')
        .first()
        .then(filePicker => {
          cy.uploadFile({ fileName: 'file1.pdf', filePicker })
        })
      cy.wait(4000)

      cy
        .get('[data-test-id="cover-letter"]')
        .find('input[type="file"]')
        .first()
        .then(filePicker => {
          cy.uploadFile({ fileName: 'file1.pdf', filePicker })
        })
      cy.wait(4000)

      cy
        .get('[data-test-id="supplemental-files"]')
        .find('input[type="file"]')
        .first()
        .then(filePicker => {
          cy.uploadFile({ fileName: 'file1.pdf', filePicker })
        })
      cy.wait(4000)
      cy
        .get('[data-test-id="submission-next"]')
        .scrollIntoView()
        .click({ force: true })
      cy.get(`[data-test-id="modal-root"]`).should('be.visible')
      cy.get('[data-test-id="modal-cancel"]').click()
      cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
      cy
        .get('[data-test-id="submission-next"]')
        .scrollIntoView()
        .click({ force: true })

      cy.wait(4000)
      cy
        .get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click({ force: true })
      cy.wait(2000)

      cy
        .get('[data-test-id="go-to-dashboard"]')
        .click()
        .wait(2000)
        .then(() => {
          cy
            .get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
            .should('be.visible')
            .click()
        })
      cy.get('[data-test-id="conflict-of-interest-tab"]')
    })
  })
  it('Submit a manuscript with message for data availability, funding and without conflict', function submitManuscriptWthoutConflict() {
    const { author, fragment } = this

    cy.loginApi(author.email, author.password)
    cy.visit(`/`)

    cy
      .get('[data-test-id="new-manuscript"]')
      .should('be.visible')
      .click()

    cy
      .get('[data-test-id="agree-checkbox"] input')
      .should('be.visible')
      .check({ force: true })
    cy.get('[data-test-id="submission-next"]').click()

    cy.location().then(loc => {
      Cypress.env(
        'fragmentId',
        loc.pathname
          .replace('/submit', '')
          .split('/')
          .pop(),
      )
      Cypress.env(
        'collectionId',
        loc.pathname.replace('/submit', '').split('/')[2],
      )
    })

    cy
      .get('[data-test-id="submission-title"] input')
      .clear()
      .type(fragment.title)

    cy.get('[data-test-id="submission-type"] button').click()
    cy.contains(fragment.manuscriptType).click({ force: true })

    cy
      .get('[data-test-id="submission-abstract"] textarea')
      .clear()
      .type(fragment.abstract)

    fragment.authors.forEach(author => {
      cy.get('[data-test-id="submission-add-author"] button').click()
      cy.get('[data-test-id="author-card-email"]').type(
        `${Cypress.env('email') +
          Math.random()
            .toString(22)
            .substring(8)}@thinslices.com`,
      )
      cy.get('[data-test-id="author-card-firstname"]').type(author.firstName)
      cy.get('[data-test-id="author-card-lastname"]').type(author.lastName)
      cy
        .get('[data-test-id="author-card-affiliation"]')
        .type(author.affiliation)

      cy
        .get('[data-test-id="item"]')
        .find('[role="listbox"]')
        .click()
      cy
        .get('[role="option"]')
        .contains(author.country)
        .click()

      cy.get('[data-test-id="author-card-save"] span').click()

      cy.get('input[name="conflicts.hasDataAvailability"][value="no"]').click()
      cy.get('input[name="conflicts.hasDataAvailability"][value="no"]').click()

      cy.get('textarea[name="conflicts.dataAvailabilityMessage"]').type('lorem')

      cy.get('input[name="conflicts.hasFunding"][value="no"]').click()
      cy.get('input[name="conflicts.hasFunding"][value="no"]').click()

      cy.get('textarea[name="conflicts.fundingMessage"]').type('lorererem')

      cy.get('[data-test-id="submission-next"]').click()
      cy.wait(5000)

      cy
        .get('[data-test-id="main-manuscript"]')
        .find('input[type="file"]')
        .first()
        .then(filePicker => {
          cy.uploadFile({ fileName: 'file1.pdf', filePicker })
        })
      cy.wait(4000)

      cy
        .get('[data-test-id="cover-letter"]')
        .find('input[type="file"]')
        .first()
        .then(filePicker => {
          cy.uploadFile({ fileName: 'file1.pdf', filePicker })
        })
      cy.wait(4000)

      cy
        .get('[data-test-id="supplemental-files"]')
        .find('input[type="file"]')
        .first()
        .then(filePicker => {
          cy.uploadFile({ fileName: 'file1.pdf', filePicker })
        })
      cy.wait(4000)
      cy
        .get('[data-test-id="submission-next"]')
        .scrollIntoView()
        .click({ force: true })
      cy.get(`[data-test-id="modal-root"]`).should('be.visible')
      cy.get('[data-test-id="modal-cancel"]').click()
      cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
      cy
        .get('[data-test-id="submission-next"]')
        .scrollIntoView()
        .click({ force: true })

      cy.wait(4000)
      cy
        .get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click({ force: true })
      cy.wait(2000)

      cy
        .get('[data-test-id="go-to-dashboard"]')
        .click()
        .wait(2000)
        .then(() => {
          cy
            .get(`[data-test-id="fragment-${Cypress.env('fragmentId')}"]`)
            .should('be.visible')
            .click()
        })
      cy.get('[data-test-id="conflict-of-interest-tab"]')
    })
  })
})
