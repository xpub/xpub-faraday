describe('Clear Manuscripts from DB', () => {
  it('Remove manuscripts from DB', () => {
    cy.task('deleteAllManuscripts').should('equal', true)
  })
})
