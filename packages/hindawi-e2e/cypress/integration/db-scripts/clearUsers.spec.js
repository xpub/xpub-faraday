describe('Clear users from DB', () => {
  it('Remove users except admin from DB', () => {
    cy.task('deleteAllUsers').should('equal', true)
  })
})
