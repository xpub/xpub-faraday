describe('Admin edits before EQS', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Author creates draft', function submitManuscript() {
    const { fragment, author } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy
      .get('[data-test-id="new-manuscript"]')
      .should('be.visible')
      .click()

    cy
      .get('[data-test-id="agree-checkbox"] input')
      .should('be.visible')
      .check({ force: true })
    cy.get('[data-test-id="submission-next"]').click()

    cy.location().then(loc => {
      Cypress.env(
        'fragmentIdV1',
        loc.pathname
          .replace('/submit', '')
          .split('/')
          .pop(),
      )
    })

    cy
      .get('[data-test-id="submission-title"] input')
      .clear()
      .type(fragment.title)

    cy.get('[data-test-id="submission-type"] button').click()
    cy.contains(fragment.manuscriptType).click({ force: true })

    cy
      .get('[data-test-id="submission-abstract"] textarea')
      .clear()
      .type(fragment.abstract)

    fragment.authors.forEach(author => {
      cy.get('[data-test-id="submission-add-author"] button').click()
      cy.get('[data-test-id="author-card-email"]').type(
        `${Cypress.env('email') +
          Math.random()
            .toString(22)
            .substring(8)}@thinslices.com`,
      )
      cy.get('[data-test-id="author-card-firstname"]').type(author.firstName)
      cy.get('[data-test-id="author-card-lastname"]').type(author.lastName)
      cy
        .get('[data-test-id="author-card-affiliation"]')
        .type(author.affiliation)

      cy
        .get('[data-test-id="item"]')
        .find('[role="listbox"]')
        .click()
      cy
        .get('[role="option"]')
        .contains(author.country)
        .click()

      cy.get('[data-test-id="author-card-save"] span').click()
    })
    cy.wait(5000)
    cy.get('[data-test-id="submission-next"]').click()
    cy.wait(5000)

    cy
      .get('[data-test-id="main-manuscript"]')
      .find('input[type="file"]')
      .first()
      .then(filePicker => {
        cy.uploadFile({ fileName: 'file1.pdf', filePicker })
      })
    cy.wait(4000)

    cy
      .get('[data-test-id="cover-letter"]')
      .find('input[type="file"]')
      .first()
      .then(filePicker => {
        cy.uploadFile({ fileName: 'file1.pdf', filePicker })
      })
    cy.wait(4000)

    cy
      .get('[data-test-id="supplemental-files"]')
      .find('input[type="file"]')
      .first()
      .then(filePicker => {
        cy.uploadFile({ fileName: 'file1.pdf', filePicker })
      })
    cy.wait(4000)
  })

  it('Admin edits manuscript before EQS', function editManuscript() {
    const { admin, fragment } = this
    const id = Cypress.env('fragmentIdV1')
    cy.loginApi(admin.username, admin.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${id}"]`).click()

    cy.editManuscript(fragment)
    cy.visit('/')
    cy.get(`[data-test-id="fragment-${id}"]`).contains('Fragment 2 - modified')
    cy.get(`[data-test-id="fragment-${id}"]`).contains('Complete Submission')
  })
})
