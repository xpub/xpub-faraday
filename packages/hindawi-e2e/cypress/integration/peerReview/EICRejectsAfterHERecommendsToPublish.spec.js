describe('EIC rejects after HE recommends to publish ', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/').wait(3000)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('accept')
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/').wait(3000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.inviteReviewer({ reviewer, he })
  })

  it('Give a review for publish as Reviewer', function minorRevisionReview() {
    const { reviewer } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/').wait(3000)

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.respondToInvitationAsReviewer('accept')
    cy.submitReview('Publish')
    cy.wait(3000)
  })

  it('HE makes recommendation to publish', function heMakesRecommendationToPublish() {
    const { he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/').wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.wait(2000)
    cy.heMakesRecommendation('Publish')
  })

  it('EiC makes decision to reject', function rejectManuscript() {
    const { eic, author, admin, statuses } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/').wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.eicMakesDecision('Reject')
    cy.checkStatus(statuses.rejected.editorInChief)

    cy.loginApi(author.email, author.password)
    cy.visit('/').wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy
      .checkStatus(statuses.rejected.author)
      .get(`[data-test-id="contextual-box-editorial-comments"]`)
      .should('be.visible')
      .click()
      .wait(2000)
      .contains(Cypress.env('eicDecisionText'))

    cy.loginApi(admin.username, admin.password)
    cy.visit('/').wait(2000)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy
      .checkStatus(statuses.rejected.admin)
      .get(`[data-test-id="contextual-box-editorial-comments"]`)
      .should('be.visible')
      .click()
      .wait(3000)
      .contains(Cypress.env('eicDecisionText'))
  })
})
