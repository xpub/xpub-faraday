describe('Submit Manuscript', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('be.visible')
      .should('contain', 'Accept')
      .should('contain', 'Decline')
      .should('contain', 'RESPOND TO INVITATION')
  })

  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('HE makes recommendation to reject', function majorRevisionRecommendation() {
    const { he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.wait(2000)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.heMakesRecommendation('Reject')
  })

  it('EiC return manuscript to HE', function returnManuscript() {
    const { eic, statuses } = this

    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.pendingApproval.editorInChief)
    cy.eicMakesDecision('Return to Handling Editor').then(() => {
      cy
        .get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .contains(Cypress.env('eicDecisionText'))
    })
  })

  it('HE sees correct information', function checkHE() {
    const { he, statuses } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
    cy
      .get(`[data-test-id="contextual-box-editorial-comments"]`)
      .should('be.visible')
      .contains(Cypress.env('eicDecisionText'))
  })

  it('Admin sees correct information', function checkAdmin() {
    const { admin, statuses } = this

    cy.loginApi(admin.email, admin.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.admin)
    cy
      .get(`[data-test-id="contextual-box-editorial-comments"]`)
      .should('be.visible')
      .contains(Cypress.env('eicDecisionText'))
  })
})
