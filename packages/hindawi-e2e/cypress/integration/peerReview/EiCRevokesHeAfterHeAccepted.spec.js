describe('EiC revokes HE after HE accepted invitation', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
    cy
      .get('[data-test-id="fragment-status"]')
      .should('be.visible')
      .should('contain', 'HE Invited')

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy
      .get('[data-test-id="fragment-status"]')
      .should('be.visible')
      .should('contain', 'Respond to Invite')
    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('be.visible')
      .should('contain', 'Accept')
      .should('contain', 'Decline')
      .should('contain', 'RESPOND TO INVITATION')
  })

  it('EiC revokes HE after He accepted invitation', function EiCRevokesHe() {
    const { eic, he, statuses } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
    cy.loginApi(eic.email, eic.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.heAssigned.editorInChief)
    cy.eicRevokesHE()
    cy.checkStatus(statuses.submitted.editorInChief)
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.be.visible')
  })
})
