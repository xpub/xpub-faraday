describe('EiC revokes HE after reviewers send reports', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite HE as EiC', function inviteHE() {
    const { eic, he, statuses } = this
    cy.inviteHE({ eic, he })
    cy.checkStatus(statuses.heInvited.editorInChief)

    cy.loginApi(he.username, he.password)
    cy.visit(`/`)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy
      .get('[data-test-id="respond-to-invitation-he-box"]')
      .should('be.visible')
      .should('contain', 'Accept')
      .should('contain', 'Decline')
      .should('contain', 'RESPOND TO INVITATION')
  })
  it('Should accept invitation as HE', function respondToInvitationAsHE() {
    const { statuses, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit(`/`)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heInvited.handlingEditor)
    cy.respondToInvitationAsHE('accept')
    cy.checkStatus(statuses.heAssigned.handlingEditor)
  })

  it('Invite reviewers as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.username, he.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Give a review for publish as Reviewer', function submitMajReview() {
    const { reviewer, he, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('accept')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.wait(3000)

    cy.submitReview('Publish')
    cy.wait(3000)
    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(he.email, he.password)
    cy.visit(`/`)
    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
    cy.checkStatus(statuses.reviewCompleted.handlingEditor)
  })

  it('EiC revokes He after the reviewers submitted reports', function EiCRevokesHeAfterReviewersSubmitedReports() {
    const { eic, he, reviewer, statuses } = this
    cy.loginApi(eic.email, eic.password)
    cy.visit(`/`)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.editorInChief)
    cy
      .get('[data-test-id="reviewer-details-and-reports-box"]')
      .should('be.visible')
      .click()
      .should('contain', 'Recommendation')
      .should('contain', 'Report')
    cy
      .get(`[data-test-id="contextual-box-manuscript-eic-decision"]`)
      .should('be.visible')
      .click()

    cy
      .get('[data-test-id="contextual-box-manuscript-eic-decision"]')
      .find('[role="listbox"]')
      .click()

    cy
      .get('[data-test-id="contextual-box-manuscript-eic-decision"]')
      .find('[role="option"]')
      .should('contain', 'Reject')
      .should('not.contain', 'Publish')
      .should('not.contain', 'Request Revison')

    cy.eicRevokesHE()
    cy.checkStatus(statuses.submitted.editorInChief)

    cy
      .get('[data-test-id="reviewer-details-and-reports-box"]')
      .should('not.be.visible')

    cy.get('[data-test-id="manuscript-invite-he-button"]').should('be.visible')
    cy.get('[data-test-id="assign-handling-editor"]').should('be.visible')
    cy
      .get(`[data-test-id="contextual-box-manuscript-eic-decision"]`)
      .should('be.visible')
      .click()

    cy
      .get('[data-test-id="contextual-box-manuscript-eic-decision"]')
      .find('[role="listbox"]')
      .click()
      .should('contain', 'Reject')
      .should('contain', 'Request Revision')
      .should('not.contain', 'Publish')

    cy.loginApi(he.email, he.password)
    cy.visit(`/`)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.be.visible')

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit(`/`)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.be.visible')

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('not.be.visible')
  })
})
