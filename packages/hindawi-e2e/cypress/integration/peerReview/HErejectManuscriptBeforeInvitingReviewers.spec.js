describe('Reject Manuscript as HE before inviting reviewers', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/he').as('he')
    cy.fixture('users/eic').as('eic')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Should invite HE to manuscript', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Successfully accept invitation for manuscript as HE', function respondToInvitationAsHE() {
    const { he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsHE('accept')
  })

  it('Successfully rejects a manuscript as HE', function heMakesRecommendation() {
    const { he, eic, statuses, admin } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.heAssigned.handlingEditor)
    cy.heMakesRecommendation('Reject').then(() => {
      cy.checkStatus(statuses.pendingApproval.handlingEditor)
      cy
        .get(`h3`)
        .contains('Your Editorial Recommendation')
        .should('not.be.visible')

      cy.loginApi(eic.email, eic.password)
      cy.visit('/')

      cy
        .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.editorInChief)
      cy
        .get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText'))
        .should('contain', Cypress.env('heOptionalText'))

      cy.loginApi(admin.username, admin.password)
      cy.visit('/')

      cy
        .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.admin)
      cy
        .get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText'))
        .should('contain', Cypress.env('heOptionalText'))
    })
  })
})
