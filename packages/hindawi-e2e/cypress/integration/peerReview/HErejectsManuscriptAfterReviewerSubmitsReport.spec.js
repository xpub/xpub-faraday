describe('HE rejects manuscript after reviewer submit report', () => {
  beforeEach(() => {
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('users/eic').as('eic')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Succesfully submit a manuscript', function submitManuscript() {
    const { author } = this
    cy.createManuscriptViaAPI(author)
  })

  it('Should Invite HE to manuscript', function inviteHE() {
    const { eic, he } = this
    cy.inviteHE({ eic, he })
  })

  it('Succesfully accept invitation for manuscript as HE', function respondToInvitationAsHE() {
    const { he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy
      .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
      .should('be.visible')
      .click()

    cy.respondToInvitationAsHE('accept')
  })

  it('Succesfully invite reviewer to manuscript as HE', function inviteReviewer() {
    const { statuses, reviewer, he } = this
    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    cy.inviteReviewer({ reviewer, he })
    cy.checkStatus(statuses.reviewersInvited.handlingEditor)
  })

  it('Accept invitation as reviewer and submit a report', function acceptInvitationAsreviewer() {
    const { reviewer, statuses } = this

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('accept')
    cy.checkStatus(statuses.underReview.reviewer)
    cy.submitReview('Minor Revision')
    cy.wait(1000)

    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('accept')
    cy.checkStatus(statuses.underReview.reviewer)
    cy.submitReview('Major Revision')
  })

  it('HE rejects manuscript after reviewers make an report', function heRejectsManuscript() {
    const { he, statuses, eic, admin } = this

    cy.loginApi(he.email, he.password)
    cy.visit('/')

    cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

    // cy.checkStatus(statuses.reviewersInvited.handlingEditor)
    cy.heMakesRecommendation('Reject').then(() => {
      cy.checkStatus(statuses.pendingApproval.handlingEditor)
      cy
        .get('h3')
        .contains('Your Editorial Recommendation')
        .should('not.be.visible')

      cy.loginApi(eic.email, eic.password)
      cy.visit('/')

      cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
      cy.checkStatus(statuses.pendingApproval.editorInChief)
      cy
        .get('[data-test-id="contextual-box-editorial-comments"]')
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText'))
        .should('contain', Cypress.env('heOptionalText'))

      cy.loginApi(admin.username, admin.password)
      cy.visit('/')

      cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()
      cy.checkStatus(statuses.pendingApproval.admin)
      cy
        .get('[data-test-id="contextual-box-editorial-comments"]')
        .should('be.visible')
        .should('contain', Cypress.env('heRecommendationText'))
        .should('contain', Cypress.env('heOptionalText'))
    })
  })
})
