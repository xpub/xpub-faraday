describe('Admin edits during EQS', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/eic').as('eic')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  const num = Math.floor(Math.random() * 9000000) + 1000000

  it('Successfully submits a manuscript', function submitManuscript() {
    const { admin, fragment, author } = this
    cy.submitManuscript({ admin, fragment, author })
  })

  it('Admin edits manuscript during EQS', function editManuscript() {
    const { admin, author, statuses, fragment } = this
    const id = Cypress.env('fragmentIdV1')
    cy.loginApi(admin.username, admin.password)
    cy.visit('/')
    cy.wait(2000)

    cy.get(`[data-test-id="fragment-${id}"]`).click()
    cy
      .get(`[data-test-id="button-qa-manuscript-edit"]`)
      .should('be.visible')
      .click()

    cy.wait(2000)
    cy.editManuscript(fragment)
    cy.get('[data-test-id="submission-next"]').click()
    cy.wait(2000)
    cy
      .get(`[data-test-id="button-qa-manuscript-technical-checks"]`)
      .should('be.visible')
      .click()
    cy.contains('Did manuscript titled Fragment 2 - modified pass EQS checks?')
    cy.get('[data-test-id="eqs-no-button"]').click()
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.contains('Did manuscript titled Fragment 2 - modified pass EQS checks?')
    cy.get('[data-test-id="eqs-yes-button"]').click()
    cy.get('[data-test-id="eqs-manuscript-id"]').should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.contains('Did manuscript titled Fragment 2 - modified pass EQS checks?')
    cy.get('[data-test-id="eqs-yes-button"]').click()
    cy
      .get('[data-test-id="eqs-manuscript-id"]')
      .should('be.visible')
      .type(num)
    cy.get('[data-test-id="modal-confirm"]').click()

    cy.wait(2000)

    cy.contains('Manuscript accepted. Thank you for your technical check!')

    cy.get(`[data-test-id="journal-logo"]`).click()
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.checkStatus(statuses.submitted.author)
    cy.get(`[data-test-id="fragment-${id}"]`).click()
    cy
      .get(`[data-test-id="abstract-tab"]`)
      .should('be.visible')
      .click()
    cy
      .get(`[data-test-id="files-tab"]`)
      .should('be.visible')
      .click()
  })
})
