describe('Change Password', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/he').as('he')
    cy.fixture('users/author').as('author')
    cy.fixture('users/eic').as('eic')
    cy.visit('/')
  })

  it('Change user password', function resetPassword() {
    const { he } = this

    cy.loginApi(he.username, he.password)
    cy.visit('/')

    cy.get('.arrowDown').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()
    cy
      .get('[data-test-id="item"]')
      .contains('Change password')
      .click()
    cy.get('input[name="currentPassword"]').type('password')
    cy.get('input[name="password"]').type('password')
    cy.get('input[name="confirmNewPassword"]').type('password')
    cy
      .get('button')
      .contains('Update password')
      .click()
  })
})
