describe('Edit user profile as admin', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
  })

  it('Edit user profile', function editUserProfileAsAdmin() {
    const { admin } = this

    cy.loginApi(admin.username, admin.password)
    cy.visit('/')

    cy.get('.arrowDown').click()
    cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
    cy.get('span[role="img"]').click()
    cy.get('table tbody tr:second .editIcon').click()
    cy.get('input[name="firstName"]').type('v1')
    cy.get('input[name="lastName"]').type('v1')
    cy.get('[data-test-id="title"] button').click()
    cy
      .get('div[role="option"][aria-selected="true"]')
      .next()
      .click()
    cy
      .get('[data-test-id="country"]:eq(1)')
      .get('div[role="listbox"] input')
      .clear()
      .type('al', { force: true })
      .type('{enter}', { force: true })

    cy.get('[data-test-id="role"] button').click()
    cy
      .get('div[role="option"][aria-selected="true"]')
      .next()
      .click()
    cy.get('input[name="affiliation"]').type('v1')
    cy
      .get('button')
      .contains('EDIT USER')
      .click()
  })
})
