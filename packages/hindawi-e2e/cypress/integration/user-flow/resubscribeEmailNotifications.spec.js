describe('Re-subscribe email notifications', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
  })

  it('Unsubscribe email notifications', function unsubscribe() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('.arrowDown').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()
    cy
      .get('button')
      .contains('Re-subscribe')
      .click()
    cy.get('[data-test-id="modal-root"]').should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get('[data-test-id="modal-root"]').should('not.be.visible')
    cy
      .get('button')
      .contains('Re-subscribe')
      .click()

    cy.get('[data-test-id="modal-confirm"]').click()
  })
})
