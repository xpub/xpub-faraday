// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const uuid = require('uuid')

const getDBConfig = config => ({
  user: config.env.dbConfig.user,
  host: config.env.dbConfig.host,
  database: config.env.dbConfig.database,
  password: config.env.dbConfig.password,
  port: 5432,
})

const createUserData = (userData, config) => ({
  firstName: userData.firstName,
  lastName: userData.lastName,
  email: config.env.email + userData.email,
  username: config.env.email + userData.email,
  editorInChief: userData.editorInChief,
  handlingEditor: userData.handlingEditor,
  admin: userData.admin,
  agreeTC: true,
  type: 'user',
  orcid: {},
  teams: [],
  title: 'mrs',
  country: 'RO',
  isActive: true,
  fragments: [],
  collections: [],
  isConfirmed: true,
  affiliation: 'Hindawi Research',
  accessTokens: {
    unsubscribe: '6c1e12999266f4c7f535878c961a19f0',
  },
  passwordHash: '$2a$12$KA.cxkhmlyL9DfmBYLZV3.lgD5aQ2/88zXiesdaTcqMLNmGlOhzn.',
  notifications: {
    email: {
      system: true,
      user: true,
    },
  },
})

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config

  on('task', {
    async deleteAllManuscripts() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()
      await qaClient.query(
        `DELETE  FROM entities WHERE data->>'type' != 'user'`,
      )
      await qaClient.query(
        `UPDATE entities SET data = jsonb_set(data, '{teams}', '[]')  WHERE data->>'type' = 'user'`,
      )

      qaClient.end()
      return true
    },
    async deleteAllUsers() {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()
      await qaClient.query(
        `DELETE FROM entities WHERE data->>'admin' <> 'true' OR data->>'admin' is null`,
      )

      qaClient.end()
      return true
    },
    async createUser(users) {
      const { Client } = require('pg')
      const qaClient = new Client(getDBConfig(config))
      await qaClient.connect()

      await Promise.all(
        users.map(async user => {
          const userId = uuid.v4()

          await qaClient.query(
            `INSERT INTO entities (id, data) VALUES ('${userId}', '${JSON.stringify(
              createUserData(user, config),
            )}')`,
          )
        }),
      )
      qaClient.end()
      return true
    },
  })
}
