const eicRevokesHE = () => {
  cy
    .get('[data-test-id="revoke-button"]')
    .find('span')
    .should('be.visible')
    .click({ force: true })
  cy.get('[data-test-id="modal-root"]').should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get('[data-test-id="modal-root"]').should('not.be.visible')
  cy
    .get('[data-test-id="revoke-button"]')
    .find('span')
    .should('be.visible')
    .click({ force: true })

  cy.get('[data-test-id="modal-confirm"]').click()
}

Cypress.Commands.add('eicRevokesHE', eicRevokesHE)
