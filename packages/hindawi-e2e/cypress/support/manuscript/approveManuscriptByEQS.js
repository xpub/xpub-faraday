const approveManuscriptByEQS = ({ admin, num }) => {
  cy.loginApi(admin.username, admin.password)
  cy.visit(`/`)

  cy.wait(5000)

  cy.get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`).click()

  cy
    .get('[data-test-id="button-qa-manuscript-technical-checks"]')
    .should('be.visible')
    .click()
  cy.contains('Did manuscript titled Fragment 2 - modified pass EQS checks?')
  cy.get('[data-test-id="eqs-no-button"]').click()
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.contains('Did manuscript titled Fragment 2 - modified pass EQS checks?')
  cy.get('[data-test-id="eqs-yes-button"]').click()
  cy.get('[data-test-id="eqs-manuscript-id"]').should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.contains('Did manuscript titled Fragment 2 - modified pass EQS checks?')
  cy.get('[data-test-id="eqs-yes-button"]').click()
  cy
    .get('[data-test-id="eqs-manuscript-id"]')
    .should('be.visible')
    .type(num)
  cy.get('[data-test-id="modal-confirm"]').click()

  cy.wait(2000)

  cy.contains('Manuscript accepted. Thank you for your technical check!')
}

Cypress.Commands.add('approveManuscriptByEQS', approveManuscriptByEQS)
