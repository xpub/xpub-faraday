const authorSubmitRevision = ({ updatedFragment }) => {
  cy
    .get('[data-test-id="submit-revision"]')
    .should('be.visible')
    .click()

  cy
    .get('[data-test-id="submission-title"] input')
    .clear()
    .type(updatedFragment.title)

  cy.get('[data-test-id="submission-type"] button').click()
  cy.contains(updatedFragment.manuscriptType).click({ force: true })

  cy
    .get('[data-test-id="submission-abstract"] textarea')
    .scrollIntoView()
    .clear()
    .type(updatedFragment.abstract)

  cy
    .get('[data-test-id="main-manuscript"]')
    .find('[data-test-id="delete-file"]')
    .first()
    .click()
  cy.wait(3000)

  cy
    .get('[data-test-id="main-manuscript"]')
    .find('input[type="file"]')
    .first()
    .then(filePicker => {
      cy.uploadFile({ fileName: 'file2.pdf', filePicker })
    })
  cy.wait(4000)

  cy
    .get('[data-test-id="cover-letter"]')
    .find('[data-test-id="delete-file"]')
    .first()
    .click()
  cy.wait(3000)

  cy
    .get('[data-test-id="cover-letter"]')
    .find('input[type="file"]')
    .first()
    .then(filePicker => {
      cy.uploadFile({ fileName: 'file2.pdf', filePicker })
    })
  cy.wait(4000)

  cy
    .get('[data-test-id="supplemental-files"]')
    .find('[data-test-id="delete-file"]')
    .first()
    .click()
  cy.wait(3000)

  cy
    .get('[data-test-id="supplemental-files"]')
    .find('input[type="file"]')
    .first()
    .then(filePicker => {
      cy.uploadFile({ fileName: 'file2.pdf', filePicker })
    })
  cy.wait(4000)

  cy
    .get('[data-test-id="response-to-reviewer-comments"]')
    .scrollIntoView()
    .find('input[type="file"]')
    .first()
    .then(filePicker => {
      cy.uploadFile({ fileName: 'file2.pdf', filePicker })
    })
  cy.wait(4000)

  cy
    .get('[data-test-id="response-to-reviewer-comments"] textarea')
    .clear()
    .type(Math.random().toString(22))

  cy
    .get('[data-test-id="submit-revision"] button')
    .contains('Submit revision')
    .click()
  cy.get('[data-test-id="modal-root"]').should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get('[data-test-id="modal-root"]').should('not.be.visible')
  cy
    .get('[data-test-id="submit-revision"] button')
    .contains('Submit revision')
    .click()

  cy.get('[data-test-id="modal-confirm"]').click()
}

Cypress.Commands.add('authorSubmitRevision', authorSubmitRevision)
