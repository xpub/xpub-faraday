const createDraft = ({ author }) => {
  cy.loginApi(author.email, author.password)
  cy.visit(`/`)

  cy
    .get('[data-test-id="new-manuscript"]')
    .should('be.visible')
    .click()

  cy.wait(3000)

  cy.location().then(loc => {
    Cypress.env(
      'fragmentIdV1',
      loc.pathname
        .replace('/submit', '')
        .split('/')
        .pop(),
    )
  })
}

Cypress.Commands.add('createDraft', createDraft)
