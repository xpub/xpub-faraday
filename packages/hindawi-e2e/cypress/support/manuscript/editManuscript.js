const editManuscript = fragment => {
  cy
    .get('[data-test-id="agree-checkbox"] input')
    .should('be.visible')
    .check({ force: true })
  cy.get('[data-test-id="submission-next"]').click()

  cy.wait(2000)

  cy
    .get('[data-test-id="submission-title"] input')
    .clear()
    .type('Fragment 2 - modified')

  cy.get('[data-test-id="submission-type"] button').click()
  cy.contains('Review Article').click({ force: true })

  cy
    .get('[data-test-id="submission-abstract"] textarea')
    .clear()
    .type('802.11B-modified')

  fragment.authors.forEach(author => {
    cy.get('[data-test-id="submission-add-author"] button').click()
    cy.get('[data-test-id="author-card-email"]').type(
      `${Cypress.env('email') +
        Math.random()
          .toString(22)
          .substring(8)}@thinslices.com`,
    )
    cy.get('[data-test-id="author-card-firstname"]').type(author.firstName)
    cy.get('[data-test-id="author-card-lastname"]').type(author.lastName)
    cy.get('[data-test-id="author-card-affiliation"]').type(author.affiliation)

    cy
      .get('[data-test-id="item"]')
      .find('[role="listbox"]')
      .click()
    cy
      .get('[role="option"]')
      .contains(author.country)
      .click()

    cy.get('[data-test-id="author-card-save"] span').click()
  })

  cy.wait(4000)
  cy.get('[data-test-id="submission-next"]').click()
  cy.wait(4000)

  cy
    .get(`input[type="file"]`)
    .first()
    .then(filePicker => {
      cy.uploadFile({ fileName: 'file2.pdf', filePicker })
    })

  cy.wait(4000)

  cy
    .get('[data-test-id="cover-letter"]')
    .find('input[type="file"]')
    .first()
    .then(filePicker => {
      cy.uploadFile({ fileName: 'file2.pdf', filePicker })
    })
  cy.wait(4000)
}

Cypress.Commands.add('editManuscript', editManuscript)
