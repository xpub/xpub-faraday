const eicMakesDecision = decision => {
  switch (decision) {
    case 'Publish': {
      cy
        .get('[data-test-id="contextual-box-manuscript-eic-decision"]')
        .should('be.visible')
        .contains('Your Editorial Decision')
        .click()

      cy
        .get('[data-test-id="form-eic-decision"]')
        .find('[role="listbox"]')
        .click()

      cy
        .get('[role="option"]')
        .should('contain', 'Return to Handling Editor')
        .should('contain', 'Reject')
        .contains('Publish')
        .click()
        .wait(3000)

      cy
        .get('[data-test-id="submit-decision-eic"]')
        .should('be.visible')
        .click({ force: true })

      cy.get('[data-test-id="modal-confirm"]').click()
      break
    }
    default: {
      cy
        .get(`[data-test-id="contextual-box-manuscript-eic-decision"]`)
        .should('be.visible')
        .click()

      cy
        .get('[data-test-id="contextual-box-manuscript-eic-decision"]')
        .find('[role="listbox"]')
        .click()

      cy
        .get('[data-test-id="contextual-box-manuscript-eic-decision"]')
        .find('[role="option"]')
        .contains(decision)
        .click()

      Cypress.env(
        'eicDecisionText',
        'loreimpsum dolor sit ameloreimpsum dolores.',
      )

      cy
        .get('[data-test-id="eic-decision-message"]')
        .should('be.visible')
        .type(Cypress.env('eicDecisionText'))

      cy
        .get('[data-test-id="submit-decision-eic')
        .should('be.visible')
        .click()

      cy
        .get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click()
        .wait(2000)
    }
  }
}

Cypress.Commands.add('eicMakesDecision', eicMakesDecision)
