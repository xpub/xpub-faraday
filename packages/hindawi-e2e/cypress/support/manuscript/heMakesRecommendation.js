const heMakesRecommendation = recommendation => {
  switch (recommendation) {
    case 'Publish': {
      cy
        .get(`[data-test-id="reviewer-tab-reports"]`)
        .contains('Reviewer Reports')
        .should('be.visible')
        .click()

      cy
        .get(`span`)
        .contains('Publish')
        .should('be.visible')

      cy.wait(3000)

      cy
        .get(`h3`)
        .contains('Your Editorial Recommendation')
        .should('be.visible')
        .click()

      cy
        .get(`[data-test-id="editorial-recommendation-choose-options"]`)
        .should('be.visible')
        .click()

      cy
        .get('[role="option"]')
        .contains('Publish')
        .should('be.visible')
        .click()

      cy
        .get(`[data-test-id="button-editorial-recommendation-submit"]`)
        .should('be.visible')
        .click()

      cy.get(`[data-test-id="modal-confirm"]`).click()
      break
    }

    case 'Reject': {
      cy
        .get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="listbox"]')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="option"]')
        .contains('Reject')
        .click()
      Cypress.env('heRecommendationText', 'loreimpsum!')
      Cypress.env('heOptionalText', 'optional')

      cy
        .get('[data-test-id="editorial-recommendation-message-for-author"]')
        .should('be.visible')
        .type(Cypress.env('heRecommendationText'))
      cy
        .get('[data-test-id="editorial-recommendation-message-for-eic"]')
        .should('be.visible')
        .type(Cypress.env('heOptionalText'))
      cy
        .get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()
      cy
        .get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click()
      break
    }

    default: {
      cy
        .get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="listbox"]')
        .click()
      cy
        .get('[data-test-id="contextual-box-he-recommendation"]')
        .find('[role="option"]')
        .contains(recommendation)
        .click()

      Cypress.env('heRecommendationText', 'loreimpsum!')

      cy
        .get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy
        .get('[data-test-id="contextual-box-he-recommendation-response"]')
        .should('be.visible')
        .should('contain', 'Required')

      cy
        .get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .contains('Your Editorial Recommendation')
        .click()

      cy
        .get(`[data-test-id="contextual-box-he-recommendation"]`)
        .should('be.visible')
        .click()

      cy
        .get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()

      cy
        .get('[data-test-id="contextual-box-he-recommendation-response"]')
        .should('be.visible')
        .should('contain', 'Required')

      cy
        .get('[data-test-id="editorial-recommendation-message-for-author"]')
        .should('be.visible')
        .type(Cypress.env('heRecommendationText'))
      cy
        .get('[data-test-id="button-editorial-recommendation-submit')
        .should('be.visible')
        .click()
      cy
        .get('[data-test-id="modal-confirm"]')
        .should('be.visible')
        .click()
    }
  }
}
Cypress.Commands.add('heMakesRecommendation', heMakesRecommendation)
