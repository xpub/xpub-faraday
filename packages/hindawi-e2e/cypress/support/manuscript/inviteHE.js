const inviteHE = ({ eic, he }) => {
  cy.loginApi(eic.email, eic.password)
  cy.visit(`/`)
  cy.wait(3000)

  cy
    .get(`[data-test-id="fragment-${Cypress.env('fragmentIdV1')}"]`)
    .should('be.visible')
    .click()
    .wait(2000)
  cy
    .get('[data-test-id="manuscript-invite-he-button"]')
    .should('be.visible')
    .click()
  cy.get('[data-test-id="manuscript-assign-he-filter"]')
  cy.contains('INVITE').click()
  cy.get('[data-test-id="modal-root"]').should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get('[data-test-id="modal-root"]').should('not.be.visible')
  cy
    .get('[data-test-id="manuscript-assign-he-filter"]')
    .type(Cypress.env('email') + he.email, { force: true })
  cy.contains('INVITE').click()
  cy.get('[data-test-id="modal-confirm"]').click()
  cy.wait(3000)
}
Cypress.Commands.add('inviteHE', inviteHE)
