const inviteReviewer = ({ reviewer }) => {
  reviewer.forEach(reviewer => {
    cy
      .wait(2000)
      .get('[data-test-id="invite-reviewer-email"]')
      .should('be.visible')
      .type(Cypress.env('email') + reviewer.email)
    cy
      .get('[data-test-id="invite-reviewer-first-name"]')
      .type(reviewer.firstName)
    cy.get('[data-test-id="invite-reviewer-last-name"]').type(reviewer.lastName)
    cy
      .get('[data-test-id="invite-reviewer-affiliation"]')
      .type(reviewer.affiliation)
    cy.get('[placeholder="Please select"]').click()
    cy.wait(2000)
    cy.contains(reviewer.country).click()
    cy.get('[data-type-id="button-invite-reviewer-invite"]').click()
    cy.get(`[data-test-id="modal-root"]`).should('be.visible')
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')
    cy.get('[data-type-id="button-invite-reviewer-invite"]').click()
    cy.get('[data-test-id="modal-confirm"]').click()
    cy.wait(4000)
  })
}
Cypress.Commands.add('inviteReviewer', inviteReviewer)
