const rejectManuscriptByEQA = () => {
  cy
    .get('[data-test-id="button-qa-manuscript-technical-checks"]')
    .should('be.visible')
    .click()
  cy.contains('Take a decision for manuscript .')
  cy.get('[data-test-id="eqs-no-button"]').click()
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.wait(2000)
  cy.contains('Take a decision for manuscript .')
  cy.get('[data-test-id="eqs-no-button"]').click()
  cy
    .get('[data-test-id="manuscript-return-reason"]')
    .find('textarea')
    .type('It sucks')

  cy
    .get('[data-test-id="modal-confirm"]')
    .contains('OK')
    .click()

  cy.wait(2000)

  cy.contains('Manuscript decision submitted. Thank you!')
}

Cypress.Commands.add('rejectManuscriptByEQA', rejectManuscriptByEQA)
