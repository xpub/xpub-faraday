const rejectManuscriptByEQS = ({ admin }) => {
  cy
    .get('[data-test-id="button-qa-manuscript-technical-checks"]')
    .should('be.visible')
    .click()
  cy.contains('Did manuscript titled Fragment 2 pass EQS checks?')
  cy.get('[data-test-id="eqs-no-button"]').click()
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get('[data-test-id="eqs-no-button"]').click()
  cy.get('[data-test-id="modal-confirm"]').click()

  cy.wait(2000)

  cy.contains('Manuscript declined. Thank you for your technical check!')
}

Cypress.Commands.add('rejectManuscriptByEQS', rejectManuscriptByEQS)
