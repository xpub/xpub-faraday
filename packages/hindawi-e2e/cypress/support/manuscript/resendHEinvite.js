const resendHEInvite = () => {
  cy
    .get('[data-test-id="resend-invitation-to-HE"]')
    .find('span')
    .should('be.visible')
    .click()
  cy.get(`[data-test-id="modal-root"]`).should('be.visible')
  cy.get('[data-test-id="modal-cancel"]').click()
  cy.get(`[data-test-id="modal-root"]`).should('not.be.visible')

  cy
    .get('[data-test-id="resend-invitation-to-HE"]')
    .find('span')
    .should('be.visible')
    .click()
  cy.get('[data-test-id="modal-confirm"]').click()
}
Cypress.Commands.add('resendHEInvite', resendHEInvite)
