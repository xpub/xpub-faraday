import { css } from 'styled-components'
import { lighten, th } from '@pubsweet/ui-toolkit'
import { marginHelper } from 'pubsweet-component-faraday-ui'

const primary = css`
  background-color: ${th('button.primary')};
  border: ${th('borderStyle')} ${th('borderWidth')}
    ${th('button.borderDefault')};
  color: ${th('button.primaryText')};

  &:hover {
    background-color: ${lighten('button.primary', 0.1)};
  }

  &:focus,
  &:active {
    background-color: ${th('button.primary')};
    border-color: ${th('button.borderActive')};
    outline: none;
  }

  &[disabled] {
    border: none;
    &,
    &:hover {
      background-color: ${th('button.disabled')};
    }
  }
`

const secondary = css`
  background: none;
  border: ${th('borderStyle')} ${th('button.secondaryBorderWidth')}
    ${th('colorSecondary')};
  color: ${th('colorSecondary')};

  &:hover,
  &:focus,
  &:active {
    background: none;
    border-color: ${th('button.secondaryBorderColor')};
    outline: none;
  }

  &[disabled] {
    border-color: ${th('button.secondaryBorderColor')};
    color: ${th('colorTextPlaceholder')};
    opacity: 0.1;

    &:hover {
      background: none;
    }
  }
`

const buttonSize = props => {
  switch (props.size) {
    case 'medium':
      return css`
        font-size: ${th('button.mediumSize')};
        line-height: ${th('button.mediumLineHeight')};
        height: ${th('button.mediumHeight')};
        min-width: ${th('button.mediumMinWidth')};
      `
    case 'small':
      return css`
        font-size: ${th('button.smallSize')};
        line-height: ${th('button.smallLineHeight')};
        height: ${th('button.smallHeight')};
        min-width: ${th('button.smallMinWidth')};
      `
    default:
      return css`
        font-size: ${th('fontSizeBase')};
        line-height: ${th('lineHeightBase')};
        height: ${th('button.defaultHeight')};
        min-width: ${th('button.minWidth')};
      `
  }
}

export default css`
  align-items: center;
  display: flex;
  justify-content: center;
  padding: 0 calc(${th('gridUnit')});
  text-transform: uppercase;

  ${props => (props.primary ? primary : secondary)};
  ${buttonSize};
  ${marginHelper};
`
