/* eslint-disable no-nested-ternary */

import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const checkIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
  <path fill="#fff" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/>
</svg>`

export default {
  Root: css`
    margin: calc(${th('gridUnit')} * 5) 0;
    width: calc(${th('gridUnit')} * 96);
  `,
  Step: css`
    height: 20px;
    width: 20px;
    box-sizing: border-box;
    border: ${props =>
      props.isPast
        ? 'none'
        : props.isCurrent ? '6px solid #dbdbdb' : '2px solid #dbdbdb'};
    background-color: ${props =>
      !props.isCurrent && !props.isPast
        ? th('colorBackground')
        : 'transparent'};
    z-index: 10;
  `,
  Separator: css`
    background-color: ${props =>
      props.isPast ? th('colorPrimary') : th('colorBorder')};
    margin: 0 -2px;
    z-index: 1;
  `,
  Bullet: css`
    background-color: ${th('colorPrimary')};
    width: 8px;
    height: 8px;
  `,
  Success: css`
    background: url('data:image/svg+xml;utf8,${encodeURIComponent(
      checkIcon,
    )}') center no-repeat, ${th('colorPrimary')};
  `,
  StepTitle: css`
    color: ${props =>
      props.isCurrent ? th('steps.currentStepColor') : th('colorText')};
    font-family: ${props =>
      props.isCurrent ? th('fontHeading') : th('fontReading')};
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
    width: calc(${th('gridUnit')} * 25);
  `,
}
