import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const textColor = ({ validationStatus }) =>
  validationStatus === 'error' ? th('colorError') : th('colorText')

const validatedBorder = ({ validationStatus }) =>
  validationStatus === 'error' ? th('colorError') : th('colorFurniture')

const activeBorder = ({ validationStatus }) =>
  validationStatus === 'error' ? th('colorError') : th('textField.activeBorder')

export default {
  Input: css`
    border: ${th('borderWidth')} ${th('borderStyle')} ${validatedBorder};
    color: ${textColor};

    font-family: ${th('fontReading')};
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
    height: calc(${th('gridUnit')} * 4);

    &:active,
    &:focus {
      border-color: ${activeBorder};
      outline: none;
    }

    &::placeholder {
      font-family: ${th('fontWriting')};
      font-size: ${th('fontSizeBase')};
      line-height: ${th('lineHeightBase')};
      height: calc(${th('gridUnit')} * 4);
      vertical-align: middle;
    }

    &[disabled] {
      opacity: 0.4;

      &:active,
      &:focus {
        border-color: ${th('colorBorder')};
      }
    }
  `,
  Label: css`
    font-family: ${th('fontReading')};
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
  `,
}
