/* eslint react/prefer-stateless-function: 0 */
import React, { Component } from 'react'
import { reducer } from 'redux-form'
import { Provider } from 'react-redux'
import hindawiTheme from 'hindawi-theme'
import { createLogger } from 'redux-logger'
import { th } from '@pubsweet/ui-toolkit'
import { client } from 'pubsweet-component-modal'
import styled, { ThemeProvider } from 'styled-components'
import { BrowserRouter as Router } from 'react-router-dom'
import { createStore, combineReducers, applyMiddleware } from 'redux'

import withDragDropContext from './withDragDropContext'

const logger = createLogger()
const store = createStore(
  combineReducers({
    form: reducer,
    modal: client.reducers.modal(),
  }),
  applyMiddleware(logger),
)

const StyleRoot = styled.div`
  background-color: ${th('colorBackground')};
  font-family: ${th('fontInterface')}, sans-serif;
  font-size: ${th('fontSizeBase')};
  color: ${th('colorText')};
  line-height: ${th('lineHeightBase')};

  * {
    box-sizing: border-box;
  }
`

class Wrapper extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <ThemeProvider theme={hindawiTheme}>
            <StyleRoot>{this.props.children}</StyleRoot>
          </ThemeProvider>
        </Router>
      </Provider>
    )
  }
}

export default withDragDropContext(Wrapper)
