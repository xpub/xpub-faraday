process.env.BABEL_ENV = 'development'
process.env.NODE_ENV = 'development'

const webpack = require('webpack')
const path = require('path')

module.exports = dir => {
  const include = [
    path.join(dir, 'src'),
    /@pubsweet\/[^/]+\/src/,
    /component-faraday-ui\/src/,
    /component-modal\/src/,
  ]
  return {
    devtool: 'cheap-module-source-map',
    devServer: {
      compress: true,
      disableHostCheck: true,
      public: 'styleguide.review.hindawi.com',
    },
    entry: './src/index.js',
    module: {
      rules: [
        {
          oneOf: [
            // ES6 modules
            {
              test: /\.jsx?$/,
              include,
              loader: 'babel-loader',
              options: {
                presets: [
                  [require('babel-preset-env'), { modules: false }],
                  require('babel-preset-react'),
                  require('babel-preset-stage-2'),
                ],
                cacheDirectory: true,
              },
            },
            // global CSS
            {
              test: /\.css$/,
              use: ['style-loader', 'css-loader'],
            },
            // Files
            {
              exclude: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/],
              loader: 'file-loader',
              options: {
                name: 'static/media/[name].[hash:8].[ext]',
              },
            },
          ],
        },
      ],
    },
    output: {
      filename: 'index.js',
      path: path.join(dir, 'dist'),
    },
    plugins: [
      // mock constants
      new webpack.DefinePlugin({
        PUBSWEET_COMPONENTS: '[]',
      }),
    ],
    resolve: {
      alias: {
        'styled-components': path.resolve(
          '../../',
          'node_modules',
          'styled-components',
        ),
      },
    },
    watch: true,
  }
}
