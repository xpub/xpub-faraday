module.exports = {
  draft: {
    importance: 1,
    author: {
      label: 'Complete Submission',
      needsAttention: true,
    },
    admin: {
      label: 'Complete Submission',
      needsAttention: false,
    },
  },
  technicalChecks: {
    importance: 2,
    author: {
      label: 'Submitted',
      needsAttention: false,
    },
    editorInChief: {
      label: 'QA',
      needsAttention: false,
    },
    admin: {
      label: 'Approve QA',
      needsAttention: true,
    },
  },
  submitted: {
    importance: 3,
    author: {
      label: 'Submitted',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Assign HE',
      needsAttention: true,
    },
    admin: {
      label: 'Assign HE',
      needsAttention: true,
    },
  },
  heInvited: {
    importance: 4,
    author: {
      label: 'HE Invited',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    editorInChief: {
      label: 'HE Invited',
      needsAttention: false,
    },
    admin: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
  },
  heAssigned: {
    importance: 5,
    author: {
      label: 'HE Assigned',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
    editorInChief: {
      label: 'HE Assigned',
      needsAttention: false,
    },
    admin: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
  },
  reviewersInvited: {
    importance: 6,
    author: {
      label: 'Reviewers Invited',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Check Review Process',
      needsAttention: true,
    },
    editorInChief: {
      label: 'Reviewers Invited',
      needsAttention: false,
    },
    reviewer: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    admin: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
  },
  underReview: {
    importance: 7,
    author: {
      label: 'Under Review',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Check Review Process',
      needsAttention: true,
    },
    editorInChief: {
      label: 'Under Review',
      needsAttention: false,
    },
    reviewer: {
      label: 'Complete Review',
      needsAttention: true,
    },
    admin: {
      label: 'Complete Review',
      needsAttention: true,
    },
  },
  reviewCompleted: {
    importance: 8,
    author: {
      label: 'Review Completed',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
    editorInChief: {
      label: 'Review Completed',
      needsAttention: false,
    },
    reviewer: {
      label: 'Review Completed',
      needsAttention: false,
    },
    admin: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
  },
  revisionRequested: {
    importance: 9,
    author: {
      label: 'Submit Revision',
      needsAttention: true,
    },
    handlingEditor: {
      label: 'Revision Requested',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Revision Requested',
      needsAttention: false,
    },
    reviewer: {
      label: 'Revision Requested',
      needsAttention: false,
    },
    admin: {
      label: 'Submit Revision',
      needsAttention: true,
    },
  },
  pendingApproval: {
    importance: 10,
    author: {
      label: 'Pending Approval',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Pending Approval',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Make Decision',
      needsAttention: true,
    },
    reviewer: {
      label: 'Pending Approval',
      needsAttention: false,
    },
    admin: {
      label: 'Make Decision',
      needsAttention: true,
    },
  },
  rejected: {
    importance: 11,
    author: {
      label: 'Rejected',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Rejected',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Rejected',
      needsAttention: false,
    },
    reviewer: {
      label: 'Rejected',
      needsAttention: false,
    },
    admin: {
      label: 'Rejected',
      needsAttention: false,
    },
  },
  inQA: {
    importance: 12,
    author: {
      label: 'Pending approval',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'QA',
      needsAttention: false,
    },
    editorInChief: {
      label: 'QA',
      needsAttention: false,
    },
    reviewer: {
      label: 'QA',
      needsAttention: false,
    },
    admin: {
      label: 'Approve QA',
      needsAttention: true,
    },
  },
  accepted: {
    importance: 13,
    author: {
      label: 'Accepted',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Accepted',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Accepted',
      needsAttention: false,
    },
    reviewer: {
      label: 'Accepted',
      needsAttention: false,
    },
    admin: {
      label: 'Accepted',
      needsAttention: false,
    },
  },
  withdrawalRequested: {
    importance: 14,
    author: {
      label: 'Withdrawal Requested',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Withdrawal Requested',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
    reviewer: {
      label: 'Withdrawal Requested',
      needsAttention: false,
    },
    admin: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
  },
  withdrawn: {
    importance: 15,
    author: {
      label: 'Withdrawn',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Withdrawn',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Withdrawn',
      needsAttention: false,
    },
    reviewer: {
      label: 'Withdrawn',
      needsAttention: false,
    },
    admin: {
      label: 'Withdrawn',
      needsAttention: false,
    },
  },
  deleted: {
    importance: 16,
    author: {
      label: 'Deleted',
      needsAttention: false,
    },
    handlingEditor: {
      label: 'Deleted',
      needsAttention: false,
    },
    editorInChief: {
      label: 'Deleted',
      needsAttention: false,
    },
    reviewer: {
      label: 'Deleted',
      needsAttention: false,
    },
    admin: {
      label: 'Deleted',
      needsAttention: false,
    },
  },
}
