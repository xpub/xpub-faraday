import React from 'react'
import { AuthenticatedComponent } from 'pubsweet-client'
import { Route, Switch, Redirect } from 'react-router-dom'
import { createGlobalStyle } from 'styled-components'

import {
  SubmissionWizard,
  SubmissionConfirmation,
} from 'pubsweet-component-wizard/src/components'
import {
  UserProfilePage,
  ChangePasswordPage,
} from 'pubsweet-components-faraday/src/components'

import {
  AdminRoute,
  AdminUsers,
  AdminDashboard,
} from 'pubsweet-component-user/app'
import { ManuscriptPage } from 'pubsweet-component-manuscript/src/components'
import DashboardPage from 'pubsweet-components-faraday/src/components/Dashboard'
import LoginPage from 'pubsweet-components-faraday/src/components/Login/LoginPage'

import {
  NotFound,
  InfoPage,
  ErrorPage,
  EQSDecisionPage,
  EQADecisionPage,
} from 'pubsweet-components-faraday/src/components/UIComponents/'
import {
  ConfirmAccount,
  ReviewerSignUp,
  SignUpInvitationPage,
} from 'pubsweet-components-faraday/src/components/SignUp'

import { Unsubscribe } from 'pubsweet-components-faraday/src/components/UserProfile'

import FaradayApp from './FaradayApp'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <AuthenticatedComponent>
        <Component {...props} />
      </AuthenticatedComponent>
    )}
  />
)

const GlobalStyles = createGlobalStyle`
  body {
    height: 100vh;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    #root, #root > div, #root > div > div {
      height: 100%;
    }
  }
`

const Routes = () => (
  <FaradayApp>
    <GlobalStyles />
    <Switch>
      <Route component={LoginPage} exact path="/login" />
      <Route component={SignUpInvitationPage} exact path="/invite" />
      <Route
        component={routeParams => (
          <SignUpInvitationPage
            subtitle={null}
            title="Sign up"
            type="signup"
            {...routeParams}
          />
        )}
        exact
        path="/signup"
      />
      <Route
        component={routeParams => (
          <SignUpInvitationPage
            subtitle={null}
            title="Reset password"
            type="forgotPassword"
            {...routeParams}
          />
        )}
        exact
        path="/password-reset"
      />
      <Route
        component={routeParams => (
          <SignUpInvitationPage
            subtitle={null}
            title="Set new password"
            type="setPassword"
            {...routeParams}
          />
        )}
        exact
        path="/forgot-password"
      />
      <Route component={ConfirmAccount} exact path="/confirm-signup" />
      <Route component={Unsubscribe} exact path="/unsubscribe" />
      <Redirect exact from="/" to="/dashboard" />
      <PrivateRoute component={DashboardPage} exact path="/dashboard" />
      <PrivateRoute component={UserProfilePage} exact path="/profile" />
      <PrivateRoute
        component={ChangePasswordPage}
        exact
        path="/profile/change-password"
      />
      <PrivateRoute
        component={SubmissionConfirmation}
        exact
        path="/confirmation-page"
      />
      <AdminRoute component={AdminDashboard} exact path="/admin" />
      <AdminRoute component={AdminUsers} exact path="/admin/users" />
      <PrivateRoute
        component={SubmissionWizard}
        exact
        path="/projects/:project/versions/:version/submit"
      />
      <Route component={ReviewerSignUp} exact path="/invite-reviewer" />
      <PrivateRoute
        component={ManuscriptPage}
        exact
        path="/projects/:project/versions/:version/details"
      />
      <Route component={EQSDecisionPage} exact path="/eqs-decision" />
      <Route component={EQADecisionPage} exact path="/eqa-decision" />
      <Route component={ErrorPage} exact path="/error-page" />
      <Route component={InfoPage} exact path="/info-page" />
      <Route component={NotFound} />
    </Switch>
  </FaradayApp>
)

export default Routes
