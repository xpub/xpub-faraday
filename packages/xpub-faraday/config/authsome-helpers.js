const config = require('config')
const logger = require('@pubsweet/logger')
const { omit, get, last, chain } = require('lodash')

const statuses = config.get('statuses')

const authorAllowedStatuses = ['revisionRequested', 'rejected', 'accepted']

const getTeamsByPermissions = async (
  teamIds = [],
  permissions = [],
  TeamModel,
) =>
  (await Promise.all(
    teamIds.map(async teamId => {
      const team = await TeamModel.find(teamId)
      if (!permissions.includes(team.teamType.permissions)) {
        return null
      }
      return team
    }),
  )).filter(Boolean)

const getUserPermissions = async ({
  user,
  Team,
  mapFn = t => ({
    objectId: t.object.id,
    objectType: t.object.type,
    role: t.teamType.permissions,
  }),
}) =>
  (await Promise.all(user.teams.map(teamId => Team.find(teamId)))).map(mapFn)

const isOwner = ({ user: { id }, object }) => {
  if (object.owners.includes(id)) return true
  return !!object.owners.find(own => own.id === id)
}

const isLastFragment = (collection, fragment) =>
  get(fragment, 'id', '') === last(get(collection, 'fragments', []))

const hasPermissionForObject = async ({ user, object, Team, roles = [] }) => {
  const userPermissions = await getUserPermissions({
    user,
    Team,
  })

  return !!userPermissions.find(p => {
    const hasObject =
      p.objectId === get(object, 'fragment.id') ||
      p.objectId === get(object, 'fragment.collectionId')
    if (roles.length > 0) {
      return hasObject && roles.includes(p.role)
    }
    return hasObject
  })
}

const isHandlingEditor = ({ user, object }) =>
  get(object, 'collection.handlingEditor.id') === user.id

const isInDraft = fragment => !get(fragment, 'submitted')

const filterAuthorRecommendations = (recommendations, status, isLast) => {
  const canViewRecommendations = authorAllowedStatuses.includes(status)
  if (canViewRecommendations || !isLast) {
    return recommendations.map(r => ({
      ...r,
      comments: r.comments ? r.comments.filter(c => c.public) : [],
    }))
  }
  return []
}

const filterRecommendationsFromLastVersion = (recommendations, user) =>
  recommendations
    .filter(
      r =>
        r.userId === user.id || r.recommendationType === 'editorRecommendation',
    )
    .map(r => ({
      ...r,
      comments: r.comments.filter(c => c.public === true),
    }))

const filterRecommendationsFromOlderVersions = (recommendations, user) => {
  const ownRecommendation = recommendations.find(r => r.userId === user.id)
  if (ownRecommendation) {
    return recommendations
      .filter(
        r => r.submittedOn || r.recommendationType === 'editorRecommendation',
      )
      .map(
        r =>
          r.userId !== ownRecommendation.userId
            ? { ...r, comments: r.comments.filter(c => c.public === true) }
            : { ...r },
      )
  }
  return []
}

const stripeCollectionByRole = ({ collection = {}, role = '' }) => {
  if (role === 'author') {
    if (collection.status === 'heInvited') {
      return {
        ...collection,
        handlingEditor: {
          name: 'Assigned',
        },
      }
    }
    if (collection.status === 'submitted') {
      return {
        ...collection,
        handlingEditor: {
          name: 'Unassigned',
        },
      }
    }
  }

  return collection
}

const stripeFragmentByRole = ({
  fragment = {},
  role = '',
  status = 'draft',
  user = {},
  isLast = false,
}) => {
  const { files, authors, recommendations = [] } = fragment
  let recommendationsFromFragment = []
  switch (role) {
    case 'author':
      return {
        ...fragment,
        recommendations: recommendations
          ? filterAuthorRecommendations(recommendations, status, isLast)
          : [],
      }
    case 'reviewer':
      if (isLast) {
        recommendationsFromFragment = filterRecommendationsFromLastVersion(
          recommendations,
          user,
        )
      } else {
        recommendationsFromFragment = filterRecommendationsFromOlderVersions(
          recommendations,
          user,
        )
      }
      return {
        ...fragment,
        files: omit(files, ['coverLetter']),
        authors: authors.map(a => omit(a, ['email'])),
        recommendations: recommendationsFromFragment,
      }
    case 'handlingEditor':
      return {
        ...fragment,
        recommendations: recommendations
          ? recommendations.filter(
              r =>
                r.submittedOn ||
                r.recommendationType === 'editorRecommendation',
            )
          : [],
      }
    default:
      return fragment
  }
}

const sensitiveUserProperties = ['passwordHash', 'accessTokens']

const getUsersList = async ({ UserModel, user }) => {
  const users = await UserModel.all()
  const omitUserProperties = [...sensitiveUserProperties]
  if (!user.editorInChief && !user.admin && !user.handlingEditor) {
    omitUserProperties.push('email', 'username')
  }

  const parsedUsers = users.map(foundUser =>
    omit(foundUser, omitUserProperties),
  )

  return parsedUsers
}

const parseUser = ({ user }) => omit(user, sensitiveUserProperties)

const getCollections = async ({ user, models }) => {
  const userPermisssions = await getUserPermissions({ user, Team: models.Team })

  const collections = (await Promise.all(
    userPermisssions.map(async userPermission => {
      let fragment = {}
      let collection = {}
      try {
        if (userPermission.objectType === 'collection') {
          collection = await models.Collection.find(userPermission.objectId)
          const latestFragmentId =
            collection.fragments[collection.fragments.length - 1]
          collection.currentVersion = await models.Fragment.find(
            latestFragmentId,
          )
        } else {
          fragment = await models.Fragment.find(userPermission.objectId)
          collection = await models.Collection.find(fragment.collectionId)
          const latestFragmentId =
            collection.fragments[collection.fragments.length - 1]
          collection.currentVersion = stripeFragmentByRole({
            fragment,
            role: userPermission.role,
            user,
            isLast: latestFragmentId === fragment.id,
          })
        }
      } catch (e) {
        logger.error(`DB ERROR: ${e.message}`)
        return null
      }
      const status = get(collection, 'status', 'draft')
      collection.visibleStatus = get(
        statuses,
        `${status}.${userPermission.role}.label`,
      )

      const role = get(userPermission, 'role', 'author')
      const parsedStatuses = await setCollectionStatus({
        role,
        user,
        collection,
        FragmentModel: models.Fragment,
      })
      const stripedColl = stripeCollectionByRole({
        collection,
        role: userPermission.role,
      })

      return {
        ...stripedColl,
        ...parsedStatuses,
      }
    }),
  )).filter(Boolean)
  const filterArchivedCollection = collections.filter(
    collection => collection.status !== 'deleted',
  )
  return chain(filterArchivedCollection)
    .sortBy(c => c.currentVersion.version)
    .reverse()
    .uniqBy('id')
    .value()
}

const reviewerActionStatuses = [
  'underReview',
  'reviewCompleted',
  'reviewersInvited',
]
const setCollectionStatus = async ({
  role,
  user,
  collection,
  FragmentModel,
}) => {
  const status = get(collection, 'status', 'draft')

  if (role !== 'reviewer') {
    return {
      status,
      visibleStatus: get(statuses, `${status}.${role}.label`),
    }
  }

  if (reviewerActionStatuses.includes(status)) {
    const fragmentId = last(collection.fragments)
    const fragment = await FragmentModel.find(fragmentId)

    const hasPendingInvitation = !chain(fragment)
      .get('invitations', [])
      .find(i => i.userId === user.id && !i.hasAnswer)
      .isUndefined()
      .value()

    const hasSubmittedReport = !chain(fragment)
      .get('recommendations', [])
      .find(
        r =>
          r.userId === user.id &&
          r.recommendationType === 'review' &&
          r.submittedOn,
      )
      .isUndefined()
      .value()

    const hasAcceptedInvitation = !chain(fragment)
      .get('invitations', [])
      .find(i => i.userId === user.id && i.hasAnswer && i.isAccepted)
      .isUndefined()
      .value()

    if (hasPendingInvitation && hasSubmittedReport) {
      return {
        status: 'reviewCompleted',
        visibleStatus: get(statuses, 'reviewCompleted.reviewer.label'),
      }
    }
    if (hasSubmittedReport) {
      return {
        status: 'reviewCompleted',
        visibleStatus: get(statuses, 'reviewCompleted.reviewer.label'),
      }
    }
    if (hasAcceptedInvitation) {
      return {
        status: 'underReview',
        visibleStatus: get(statuses, 'underReview.reviewer.label'),
      }
    }
    if (hasPendingInvitation) {
      return {
        status: 'reviewersInvited',
        visibleStatus: get(statuses, 'reviewersInvited.reviewer.label'),
      }
    }
  }

  return {
    status,
    visibleStatus: get(statuses, `${status}.reviewer.label`),
  }
}

module.exports = {
  isOwner,
  isInDraft,
  parseUser,
  getUsersList,
  getCollections,
  isLastFragment,
  isHandlingEditor,
  getUserPermissions,
  setCollectionStatus,
  stripeFragmentByRole,
  getTeamsByPermissions,
  stripeCollectionByRole,
  hasPermissionForObject,
}
