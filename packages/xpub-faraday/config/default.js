require('dotenv').config()
const path = require('path')
const { get } = require('lodash')
const logger = require('./loggerCustom')
const components = require('./components.json')
const journalConfig = require('../app/config/journal')

const getDbConfig = () => {
  if (process.env.DATABASE) {
    return {
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DATABASE,
      host: process.env.DB_HOST,
      port: 5432,
      ssl: true,
    }
  }
  return {}
}

module.exports = {
  authsome: {
    mode: path.resolve(__dirname, 'authsome-mode.js'),
    teams: {
      handlingEditor: {
        name: 'Handling Editors',
      },
      reviewer: {
        name: 'Reviewer',
      },
    },
  },
  validations: path.resolve(__dirname, 'validations.js'),
  pubsweet: {
    components,
  },
  dbManager: {
    migrationsPath: path.join(process.cwd(), 'migrations'),
  },
  'pubsweet-server': {
    db: getDbConfig(),
    pool: { min: 0, max: 10 },
    ignoreTerminatedConnectionError: true,
    port: 3000,
    logger,
    uploads: 'uploads',
    secret: 'SECRET',
    enableExperimentalGraphql: true,
    graphiql: true,
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    baseUrl: process.env.CLIENT_BASE_URL || 'http://localhost:3000',
    'login-redirect': '/',
    'redux-log': process.env.NODE_ENV !== 'production',
    theme: process.env.PUBSWEET_THEME,
  },
  orcid: {
    clientID: process.env.ORCID_CLIENT_ID,
    clientSecret: process.env.ORCID_CLIENT_SECRET,
    callbackPath: '/api/users/orcid/callback',
    successPath: '/profile',
  },
  'mail-transport': {
    sendmail: true,
  },
  'password-reset': {
    url:
      process.env.PUBSWEET_PASSWORD_RESET_URL ||
      'http://localhost:3000/password-reset',
    sender: process.env.PUBSWEET_PASSWORD_RESET_SENDER || 'dev@example.com',
  },
  'pubsweet-component-aws-s3': {
    secretAccessKey: process.env.AWS_S3_SECRET_KEY,
    accessKeyId: process.env.AWS_S3_ACCESS_KEY,
    region: process.env.AWS_S3_REGION,
    bucket: process.env.AWS_S3_BUCKET,
    validations: path.resolve(__dirname, 'upload-validations.js'),
  },
  'mts-service': {
    ftp: {
      user: process.env.FTP_USERNAME,
      password: process.env.FTP_PASSWORD,
      host: process.env.FTP_HOST,
      port: 21,
      localRoot: `./`,
      remoteRoot: '/BCA/',
      exclude: ['*.js'],
    },
    journal: get(journalConfig, 'metadata.mts'),
  },
  'invite-reset-password': {
    url: process.env.PUBSWEET_INVITE_PASSWORD_RESET_URL || '/invite',
  },
  'forgot-password': {
    url: process.env.PUBSWEET_FORGOT_PASSWORD_URL || '/forgot-password',
  },
  'invite-reviewer': {
    url: process.env.PUBSWEET_INVITE_REVIEWER_URL || '/invite-reviewer',
  },
  'confirm-signup': {
    url: process.env.PUBSWEET_CONFIRM_SIGNUP_URL || '/confirm-signup',
  },
  'eqs-decision': {
    url: process.env.PUBSWEET_EQS_DECISION || '/eqs-decision',
  },
  'eqa-decision': {
    url: process.env.PUBSWEET_EQA_DECISION || '/eqa-decision',
  },
  unsubscribe: {
    url: process.env.PUBSWEET_UNSUBSCRIBE_URL || '/unsubscribe',
  },
  roles: get(journalConfig, 'rolesAccess'),
  mailer: {
    from: 'hindawi@thinslices.com',
    path: `${__dirname}/mailer`,
  },
  SES: {
    accessKey: process.env.AWS_SES_ACCESS_KEY,
    secretKey: process.env.AWS_SES_SECRET_KEY,
    region: process.env.AWS_SES_REGION,
  },
  publicKeys: ['pubsweet-client', 'authsome', 'validations'],
  statuses: get(journalConfig, 'statuses'),
  publons: {
    key: process.env.PUBLONS_KEY || '',
    reviewersUrl: 'https://api.clarivate.com/reviewer-connect/api/',
  },
  journalConfig,
  journal: {
    name: get(journalConfig, 'metadata.nameText'),
    staffEmail: get(journalConfig, 'metadata.email'),
    ...get(journalConfig, 'metadata.emailData'),
  },
  features: {
    mts: JSON.parse(get(process, 'env.FEATURE_MTS', true)),
  },
  recommendations: {
    minor: 'minor',
    major: 'major',
    reject: 'reject',
    publish: 'publish',
    type: {
      review: 'review',
      editor: 'editorRecommendation',
    },
  },
  passwordStrengthRegex: new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&,.?;\'*><)([}{}":`~+=_-\\|/])(?=.{6,128})',
  ),
  reminders: {
    reviewer: {
      days: {
        first: process.env.REMINDER_REVIEWER_FIRST || 4,
        second: process.env.REMINDER_REVIEWER_SECOND || 7,
        third: process.env.REMINDER_REVIEWER_THIRD || 14,
      },
      remove: process.env.REMINDER_REMOVE_REVIEWER || 21,
      timeUnit: process.env.REMINDER_REVIEWER_TIME_UNIT || 'days',
    },
  },
  hostname: process.env.HOSTNAME || 'http://localhost:3000',
  daysExpectedForReview: 14,
}
