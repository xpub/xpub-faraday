const { cloneDeep, get } = require('lodash')
const fixturesService = require('pubsweet-component-fixture-service')
const ah = require('../../config/authsome-helpers')

describe('Authsome Helpers', () => {
  let testFixtures = {}
  let models
  beforeEach(() => {
    testFixtures = cloneDeep(fixturesService.fixtures)
    models = fixturesService.Model.build(testFixtures)
  })

  describe('stripeCollectionByRole', () => {
    it('should return a collection', () => {
      const { collection } = testFixtures.collections
      const newCollection = ah.stripeCollectionByRole({
        collection,
      })
      expect(newCollection).toBeTruthy()
    })

    it('Author should see Assigned instead of HE name on dashboard before HE accepts invitation', () => {
      const { collection } = testFixtures.collections
      collection.handlingEditor = {
        ...collection.handlingEditor,
        isAccepted: false,
      }
      collection.status = 'heInvited'
      const role = 'author'
      const newCollection = ah.stripeCollectionByRole({
        collection,
        role,
      })
      const { handlingEditor = {} } = newCollection

      expect(handlingEditor.email).toBeFalsy()
      expect(handlingEditor.name).toEqual('Assigned')
    })

    it('EIC and Admin should see HE name before HE accepts invitation', () => {
      const { collection } = testFixtures.collections
      collection.handlingEditor = {
        ...collection.handlingEditor,
        isAccepted: false,
      }
      collection.status = 'heInvited'
      const role = 'admin'
      const newCollection = ah.stripeCollectionByRole({
        collection,
        role,
      })
      const { handlingEditor = {} } = newCollection
      expect(handlingEditor.email).toBeTruthy()
      expect(handlingEditor.name).not.toEqual('Assigned')
    })

    it('EIC and Admin should see HE name after HE accepts invitation', () => {
      const { collection } = testFixtures.collections
      collection.handlingEditor = {
        ...collection.handlingEditor,
        isAccepted: true,
      }
      collection.status = 'heAssigned'
      const role = 'admin'
      const newCollection = ah.stripeCollectionByRole({
        collection,
        role,
      })
      const { handlingEditor = {} } = newCollection
      expect(handlingEditor.email).toBeTruthy()
      expect(handlingEditor.name).not.toEqual('Assigned')
    })

    it('Atuhor should see HE name on dashboard after HE accepts invitation', () => {
      const { collection } = testFixtures.collections
      collection.handlingEditor = {
        ...collection.handlingEditor,
        isAccepted: true,
      }
      collection.status = 'heAssigned'
      const role = 'author'
      const newCollection = ah.stripeCollectionByRole({
        collection,
        role,
      })
      const { handlingEditor = {} } = newCollection
      expect(handlingEditor.name).not.toEqual('Assigned')
    })

    it('Author should see Unassigned insted of HE name before HE is invited', () => {
      const { collection } = testFixtures.collections
      collection.status = 'submitted'
      const role = 'author'
      const newCollection = ah.stripeCollectionByRole({
        collection,
        role,
      })
      const { handlingEditor = {} } = newCollection
      expect(handlingEditor.name).toEqual('Unassigned')
    })

    it('stripeCollection - returns if collection does not have HE', () => {
      const { collection } = testFixtures.collections
      delete collection.handlingEditor
      const role = 'admin'
      const newCollection = ah.stripeCollectionByRole({ collection, role })
      expect(newCollection.handlingEditor).toBeFalsy()
    })
  })

  describe('stripeFragmentByRole', () => {
    it('reviewer should not see authors email', () => {
      const { fragment } = testFixtures.fragments
      const result = ah.stripeFragmentByRole({ fragment, role: 'reviewer' })
      const { authors = [] } = result
      expect(authors[0].email).toBeFalsy()
    })
    it('other roles than reviewer should see authors emails', () => {
      const { fragment } = testFixtures.fragments
      const result = ah.stripeFragmentByRole({ fragment, role: 'author' })
      const { authors = [] } = result

      expect(authors[0].email).toBeTruthy()
    })

    it('reviewer should not see cover letter', () => {
      const { fragment } = testFixtures.fragments
      const result = ah.stripeFragmentByRole({ fragment, role: 'reviewer' })
      const { files = {} } = result
      expect(files.coverLetter).toBeFalsy()
    })
    it('reviewer should not see private comments on the last version of the manuscript', () => {
      const { fragment } = testFixtures.fragments
      fragment.recommendations = [
        {
          comments: [
            {
              content: 'private',
              public: false,
            },
            {
              content: 'public',
              public: true,
            },
          ],
        },
      ]
      const result = ah.stripeFragmentByRole({
        fragment,
        role: 'reviewer',
        isLast: true,
      })
      const { recommendations } = result
      expect(recommendations).toHaveLength(1)
      expect(recommendations[0].comments).toHaveLength(1)
      expect(recommendations[0].comments[0].public).toEqual(true)
    })

    it('reviewer should see other reviewers recommendations on previous version if he submitted a review on that fragment', () => {
      const { fragment } = testFixtures.fragments
      const { answerReviewer } = testFixtures.users

      const result = ah.stripeFragmentByRole({
        fragment,
        role: 'reviewer',
        isLast: false,
        user: answerReviewer,
      })
      const { recommendations } = result
      expect(recommendations).toHaveLength(8)
    })

    it('reviewer should not see other reviewers recommendations on latest fragment', () => {
      const { fragment } = testFixtures.fragments
      const { answerReviewer } = testFixtures.users

      const result = ah.stripeFragmentByRole({
        fragment,
        role: 'reviewer',
        isLast: true,
        user: answerReviewer,
      })
      const { recommendations } = result
      expect(recommendations).toHaveLength(7)
    })

    it('reviewer should not see any reviewer recommendation on previous version if he did not submit a review on that fragment', () => {
      const { fragment } = testFixtures.fragments
      const { inactiveReviewer } = testFixtures.users

      const result = ah.stripeFragmentByRole({
        fragment,
        role: 'reviewer',
        isLast: false,
        user: inactiveReviewer,
      })
      const { recommendations } = result
      expect(recommendations).toHaveLength(0)
    })

    it('author should not see recommendations if a decision has not been made', () => {
      const { fragment } = testFixtures.fragments
      fragment.recommendations = [
        {
          comments: [
            {
              content: 'private',
              public: false,
            },
            {
              content: 'public',
              public: true,
            },
          ],
        },
      ]
      const { recommendations } = ah.stripeFragmentByRole({
        fragment,
        role: 'author',
        status: 'underReview',
        isLast: true,
      })
      expect(recommendations).toHaveLength(0)
    })
    it('author should see reviews only if recommendation has been made and only public ones', () => {
      const { fragment } = testFixtures.fragments
      fragment.recommendations = [
        {
          comments: [
            {
              content: 'private',
              public: false,
            },
            {
              content: 'public',
              public: true,
            },
          ],
        },
      ]
      const result = ah.stripeFragmentByRole({
        fragment,
        role: 'author',
        status: 'revisionRequested',
      })
      const publicComments = get(result, 'recommendations[0].comments')
      expect(publicComments).toHaveLength(1)
    })

    it('HE should not see unsubmitted recommendations', () => {
      const { fragment } = testFixtures.fragments
      fragment.recommendations = [
        {
          comments: [
            {
              content: 'private',
              public: false,
            },
            {
              content: 'public',
              public: true,
            },
          ],
        },
      ]
      const { recommendations } = ah.stripeFragmentByRole({
        fragment,
        role: 'handlingEditor',
      })
      expect(recommendations).toHaveLength(0)
    })
    it('HE should see submitted recommendations', () => {
      const { fragment } = testFixtures.fragments
      fragment.recommendations = [
        {
          comments: [
            {
              content: 'private',
              public: false,
            },
            {
              content: 'public',
              public: true,
            },
          ],
          submittedOn: 1122333,
        },
      ]
      const { recommendations } = ah.stripeFragmentByRole({
        fragment,
        role: 'handlingEditor',
      })
      expect(recommendations).toHaveLength(1)
    })
  })

  it('getUsersList - should return parsed users when the user is admin', async () => {
    const { admin } = testFixtures.users
    const parsedUsers = await ah.getUsersList({
      UserModel: models.User,
      user: admin,
    })

    parsedUsers.forEach(puser => {
      expect(puser).not.toHaveProperty('passwordResetToken')
      expect(puser).not.toHaveProperty('passwordHash')
      expect(puser).not.toHaveProperty('invitationToken')
      expect(puser).not.toHaveProperty('confirmationToken')
      expect(puser).toHaveProperty('email')
      expect(puser).toHaveProperty('username')
    })
  })
  it('getUsersList - should return parsed users with no email/username when the user is a reviewer/author', async () => {
    const { user } = testFixtures.users
    const parsedUsers = await ah.getUsersList({
      UserModel: models.User,
      user,
    })

    parsedUsers.forEach(puser => {
      expect(puser).not.toHaveProperty('passwordResetToken')
      expect(puser).not.toHaveProperty('passwordHash')
      expect(puser).not.toHaveProperty('invitationToken')
      expect(puser).not.toHaveProperty('confirmationToken')
      expect(puser).not.toHaveProperty('email')
      expect(puser).not.toHaveProperty('username')
    })
  })
  it('parseUser - should return a parsed user with no sensitive data', async () => {
    const { admin } = testFixtures.users
    const parsedUser = await ah.parseUser({
      user: admin,
    })

    expect(parsedUser).not.toHaveProperty('passwordResetToken')
    expect(parsedUser).not.toHaveProperty('passwordHash')
    expect(parsedUser).not.toHaveProperty('invitationToken')
    expect(parsedUser).not.toHaveProperty('confirmationToken')
    expect(parsedUser).toHaveProperty('email')
    expect(parsedUser).toHaveProperty('username')
  })
})
